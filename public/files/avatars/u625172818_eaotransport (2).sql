-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 18, 2020 at 07:37 AM
-- Server version: 10.3.24-MariaDB-cll-lve
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u625172818_eaotransport`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `bank_code` int(10) NOT NULL,
  `bank_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_status` int(11) NOT NULL,
  `deletion_indicator` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`bank_code`, `bank_name`, `bank_status`, `deletion_indicator`) VALUES
(1, 'Commercial Bank of Africa', 0, 0),
(2, 'Kenya Commercial Bank', 0, 0),
(3, 'Equity Bank Kenya', 0, 0),
(4, 'Standard Chartered Bank Kenya', 0, 0),
(5, 'Tanzania Bank', 1, 0),
(6, 'Absa Bank Tanzania Limited.', 0, 0),
(7, 'Stanbic Bank', 0, 0),
(8, 'DIAMOND TRUST BANK', 1, 0),
(11, 'Stanbic Bank Uganda', 0, 0),
(12, 'Crane Bank Limited', 1, 0),
(13, 'National Bank of Commerce NBC', 0, 0),
(14, 'NCBA Bank ', 0, 0),
(15, 'NCBA', 1, 0),
(16, 'Crane Bank Ltd', 0, 0),
(17, 'Diamond Trust Bank', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bank_branch`
--

CREATE TABLE `bank_branch` (
  `branch_code` int(10) NOT NULL,
  `bank_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_status` int(11) NOT NULL,
  `deletion_indicator` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank_branch`
--

INSERT INTO `bank_branch` (`branch_code`, `bank_code`, `branch_name`, `branch_status`, `deletion_indicator`) VALUES
(1, '1', 'Mamlaka', 0, 0),
(2, '1', 'Wabera', 0, 0),
(3, '1', 'Mombasa', 0, 0),
(4, '2', 'Moi Avenue Nairobi', 0, 0),
(5, '2', 'Treasury Sq Mombasa', 0, 0),
(6, '2', 'Eldoret', 0, 0),
(7, '3', 'Bomet', 0, 0),
(8, '3', 'Moi Avenue  Mombasa', 0, 0),
(9, '3', 'Githurai', 0, 0),
(10, '4', 'Kericho', 0, 0),
(11, '4', 'Kitale', 0, 0),
(12, '4', 'Westlands', 0, 0),
(13, '3', 'Karen', 0, 0),
(14, '5', 'Dar Street', 1, 0),
(15, '6', 'Absa Ohio Branch – Dar es Salaam.', 0, 0),
(16, '7', 'International Life House ', 0, 0),
(17, '8', 'KAREN', 1, 0),
(18, '2', 'Industrial Area', 0, 0),
(19, '11', 'Lugogo', 0, 0),
(20, '13', 'Victoria Branch', 0, 0),
(21, '14', 'Junction Branch ', 0, 0),
(22, '16', 'Mbarara Road ', 0, 0),
(23, '17', 'Karen', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `booking_code` varchar(500) DEFAULT NULL,
  `users_id` int(11) NOT NULL,
  `booking_status` int(11) NOT NULL COMMENT '1=> New,  2=> 1Review,  3=> pending,   4=>Confirmed,   5=>Assigned,   6=> Received      7=>Picked Up,      8=>In-Transit,    9=>Delivered,  10=>Incident',
  `users_company_code` int(11) NOT NULL,
  `cust_invoice_status` int(11) NOT NULL COMMENT '0 => New,    1=>Pending,    2=>Completed',
  `trans_invoice_status` int(11) NOT NULL COMMENT '0 => New,    1=>Pending,    2=>Completed',
  `cargo_type_id` int(11) NOT NULL,
  `cargo_packaging_id` int(11) NOT NULL,
  `origin_country_code` int(11) NOT NULL,
  `origin_city_code` int(11) NOT NULL,
  `destination_country_code` int(11) NOT NULL,
  `destination_city_code` int(11) NOT NULL,
  `tonnage_code` int(11) NOT NULL,
  `vehicle_feet` float DEFAULT NULL,
  `customer_price` int(11) NOT NULL,
  `agent_price` int(11) NOT NULL,
  `fixed_price` int(11) DEFAULT NULL,
  `currency` varchar(50) NOT NULL,
  `pick_up_date` date NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `delivery_document` varchar(500) DEFAULT NULL,
  `return_container_origin` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`booking_id`, `booking_code`, `users_id`, `booking_status`, `users_company_code`, `cust_invoice_status`, `trans_invoice_status`, `cargo_type_id`, `cargo_packaging_id`, `origin_country_code`, `origin_city_code`, `destination_country_code`, `destination_city_code`, `tonnage_code`, `vehicle_feet`, `customer_price`, `agent_price`, `fixed_price`, `currency`, `pick_up_date`, `delivery_date`, `delivery_document`, `return_container_origin`) VALUES
(1, 'ARW1', 4, 9, 3, 1, 0, 15, 10, 1, 2, 5, 57, 19, 40, 3350, 3183, 3350, '$', '2020-09-21', '2020-09-25', 'africa_continental_free_trade.png', NULL),
(2, 'ARW2', 50, 2, 39, 0, 0, 15, 10, 1, 1, 9, 50, 17, 40, 10500, 9975, NULL, '$', '2020-09-18', NULL, NULL, NULL),
(3, 'ARW3', 50, 2, 39, 0, 0, 19, 6, 1, 2, 3, 34, 12, 40, 2000, 1900, NULL, '$', '2020-09-18', NULL, NULL, NULL),
(4, 'ARW4', 50, 9, 39, 1, 1, 20, 10, 1, 1, 1, 2, 16, 40, 110000, 104500, 110000, 'KSh', '2020-09-21', '2020-10-10', 'images_-_2020-09-20T104742_192.jpeg', NULL),
(5, 'ARW5', 50, 2, 39, 0, 0, 15, 4, 1, 2, 12, 51, 26, 40, 6500, 6175, NULL, '$', '2020-10-01', NULL, NULL, 'Nairobi'),
(6, 'ARW6', 4, 9, 3, 0, 0, 15, 4, 1, 2, 12, 51, 15, 40, 6600, 6270, 6600, '$', '2020-10-01', '2020-10-10', 'truck3.jpeg', 'Suswa ICD'),
(7, 'ARW7', 4, 9, 3, 0, 0, 15, 4, 2, 31, 1, 2, 27, 40, 2800, 2660, 2800, '$', '2020-10-02', '2020-10-06', 'clock9.png', 'Dar es Salaam'),
(8, 'ARW8', 32, 9, 24, 0, 1, 15, 4, 1, 2, 2, 31, 15, 40, 3200, 3040, 3200, '$', '2020-10-02', '2020-10-07', 'clock6.png', 'Nairobi'),
(9, 'ARW9', 4, 9, 3, 0, 0, 15, 4, 1, 2, 3, 34, 15, 40, 1950, 1853, 1950, '$', '2020-10-02', '2020-10-08', 'clock10.png', 'Kampala'),
(10, 'ARW10', 61, 9, 48, 1, 1, 15, 3, 1, 1, 1, 2, 19, 40, 87000, 82650, 87000, 'KSh', '2020-10-02', '2020-10-03', 'clock9.png', 'Mombasa'),
(11, 'ARW11', 61, 9, 48, 1, 0, 15, 4, 1, 1, 3, 34, 21, 40, 2300, 2185, 2365, '$', '2020-10-09', '2020-10-14', 'Screenshot_20201008-142136_Google_Play_Store.jpg', 'Mombasa'),
(12, 'ARW12', 63, 9, 51, 0, 1, 15, 4, 1, 1, 3, 34, 27, 40, 2300, 2185, 2300, '$', '2020-10-15', '2020-10-20', 'EAOTA_-_DRIVER_CERTIFICATE.png', 'Mombasa'),
(13, 'ARW13', 32, 9, 24, 0, 0, 15, 4, 2, 31, 1, 2, 22, 40, 2800, 2660, 2800, '$', '2020-11-11', '2020-11-13', 'images_-_2020-09-20T104742_192.jpeg', 'Dar es Salaam '),
(14, 'ARW14', 63, 9, 51, 1, 1, 15, 8, 2, 31, 3, 34, 21, 40, 3500, 3325, 3500, '$', '2020-11-02', '2020-11-06', 'africa_continental_free_trade1.png', NULL),
(15, 'ARW15', 4, 9, 3, 0, 1, 15, 4, 1, 1, 3, 34, 16, 40, 2300, 2185, 2300, '$', '2020-10-16', '2020-10-20', 'africa_continental_free_trade2.png', 'Mombasa'),
(16, 'ARW16', 63, 9, 51, 2, 2, 15, 8, 1, 2, 3, 34, 3, 40, 1900, 1805, 1995, '$', '1970-01-01', '2020-10-29', 'Picture1.jpg', NULL),
(17, 'ARW17', 65, 9, 53, 2, 2, 15, 4, 3, 37, 8, 38, 27, 40, 2000, 1900, 2000, '$', '2020-11-02', '2020-11-05', 'images_-_2020-10-16T091052_047.jpeg', 'Jinja'),
(18, 'ARW18', 61, 9, 48, 2, 2, 15, 4, 1, 5, 8, 38, 21, 40, 4800, 4560, 4800, '$', '2020-11-13', '2020-11-18', 'clock11.png', NULL),
(19, 'ARW19', 61, 1, 48, 0, 0, 15, 10, 1, 2, 3, 36, 3, 40, 1750, 1663, NULL, '$', '2020-11-12', NULL, NULL, 'Nairobi'),
(20, 'ARW20', 4, 8, 3, 0, 0, 15, 4, 1, 2, 1, 1, 27, 40, 90000, 85500, 90000, 'KSh', '2020-11-14', '2020-11-17', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `booking_accident`
--

CREATE TABLE `booking_accident` (
  `accident_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `incident_status` int(11) NOT NULL COMMENT '0 => ''new'' 1 => ''resolved''',
  `description` varchar(400) NOT NULL,
  `accident_doc` varchar(500) NOT NULL,
  `prior_booking_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_accident`
--

INSERT INTO `booking_accident` (`accident_id`, `booking_id`, `incident_status`, `description`, `accident_doc`, `prior_booking_status`) VALUES
(1, 10, 1, 'Mechanical issue. Fixing ongoing. ', 'IMG-20201001-WA0036.jpg', 8),
(2, 10, 1, 'Mechanical issue. Fixing ongoing. ', 'IMG-20201001-WA0032.jpg', 8),
(3, 4, 1, 'Truck incident', 'Screenshot_20200927_135912.jpg', 6),
(4, 12, 1, 'Mechanical Breakdown. Fixing ongoing. ', 'images_-_2020-09-20T104742_192.jpeg', 8),
(5, 14, 1, 'Mechanical breakdown', 'images_-_2020-09-20T104742_1921.jpeg', 8),
(6, 16, 1, 'Mechanical issues ', 'images_-_2020-10-19T153313_967.jpeg', 8);

-- --------------------------------------------------------

--
-- Table structure for table `booking_contact`
--

CREATE TABLE `booking_contact` (
  `booking_contact_id` int(11) NOT NULL,
  `booking_proposal_id` int(11) NOT NULL,
  `onload_contact_name` varchar(200) NOT NULL,
  `onload_contact_number` varchar(20) NOT NULL,
  `offload_contact_name` varchar(200) NOT NULL,
  `offload_contact_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_contact`
--

INSERT INTO `booking_contact` (`booking_contact_id`, `booking_proposal_id`, `onload_contact_name`, `onload_contact_number`, `offload_contact_name`, `offload_contact_number`) VALUES
(1, 1, 'Sofia', '0720851896', 'wanja', '9742955126'),
(2, 4, 'Sofia', '0720851896', 'wanja', '9742955126'),
(3, 5, 'Akida', '9872346473', 'Akida', '9872346473'),
(4, 6, 'Sofia', '0720851896', 'wanja', '9742955126'),
(5, 7, 'Millicent Owino', '0792532005', 'Samson Owili', '0757129031'),
(6, 2, 'Millicent Owino', '704490491', 'Millicent Owino', '704490491'),
(7, 9, 'Samson Owili', '0757129031', 'Millicent Owino', '0792532005'),
(8, 10, 'Sofia', '0720851896', 'Sofia', '0720851896'),
(9, 11, 'Tero Luganda', '123456789', 'Tero Luganda', '123456789'),
(10, 12, 'Akida', '9872346473', 'Akida', '9872346473'),
(11, 14, 'Tero Luganda', '123456789', 'Tero Luganda', '123456789'),
(12, 15, 'Sofia', '0720851896', 'wanja', '9742955126'),
(13, 16, 'Tero Luganda', '123456789', 'Tero Luganda', '123456789'),
(14, 17, 'Noni Kiragu', '0000900351', 'Noni Kiragu', '0000900351'),
(15, 19, 'Wanja Getambu', '0722900351', 'Wanja Getambu', '0722900351'),
(16, 20, 'wanja', '9742955126', 'Sofia', '0720851896');

-- --------------------------------------------------------

--
-- Table structure for table `booking_item`
--

CREATE TABLE `booking_item` (
  `booking_item_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `item_name` text NOT NULL,
  `item_quantity` int(11) NOT NULL,
  `item_packaging_type` text NOT NULL,
  `item_weight` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_item`
--

INSERT INTO `booking_item` (`booking_item_id`, `booking_id`, `item_name`, `item_quantity`, `item_packaging_type`, `item_weight`) VALUES
(1, 1, 'Food Items', 1000, 'Cartons', '21 MT'),
(2, 2, 'syringe', 1000, 'Carton', '12'),
(3, 2, 'gloves', 500, 'Caron', '5'),
(4, 3, 'wine', 40, 'crates', '27400'),
(5, 4, 'Tobbacco', 100, 'Carton', '23'),
(6, 5, 'Machines', 10, 'Pallets', '7MT'),
(7, 6, 'Unga', 12000, 'Cartons', '25MT'),
(8, 7, 'Machinery', 5, 'Pallets', '7.5 MT'),
(9, 8, 'Machinery', 10, 'Pallets', '23MT'),
(10, 9, 'Machinery', 10, 'Pallets', '25MT'),
(11, 10, 'Billboard Paper', 1000, 'Cartons', '18 MT'),
(12, 11, 'Tiles', 10000, 'Boxes', '14MT'),
(13, 12, 'Chemicals', 500, 'Bags', '6.5 MT'),
(14, 13, 'Machinery', 10, 'Pallets', '14MT'),
(15, 14, 'Tiles', 10000, 'Boxes', '18MT'),
(16, 15, 'Tiles', 10000, 'Cartons', '22 MT'),
(17, 16, 'Machinery', 10, 'Pallets', '16 MT'),
(18, 17, 'Brickette Machine', 1, 'Pallets', '6.8MT'),
(19, 18, 'Books', 10000, 'Boxes', '14000 Kgs'),
(20, 19, 'Medical Drugs', 0, 'tbcc', '15mt'),
(21, 20, 'Books', 110000, 'Boxes', '7000 Kgs');

-- --------------------------------------------------------

--
-- Table structure for table `booking_proposal`
--

CREATE TABLE `booking_proposal` (
  `booking_proposal_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `transporter_user_id` int(11) NOT NULL,
  `trans_company_code` int(11) NOT NULL,
  `quoted_price` int(11) NOT NULL,
  `commission_rate` int(11) NOT NULL,
  `customer_quoted_price` int(11) NOT NULL,
  `offer_status` int(11) NOT NULL COMMENT '1=>new_request, 2=>pending, 3=>confirmed, 4=>rejected, 5=>accepted, 6=>cancelled, 7=>accident'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_proposal`
--

INSERT INTO `booking_proposal` (`booking_proposal_id`, `booking_id`, `transporter_user_id`, `trans_company_code`, `quoted_price`, `commission_rate`, `customer_quoted_price`, `offer_status`) VALUES
(1, 1, 2, 1, 3183, 167, 3350, 5),
(2, 4, 3, 2, 104500, 5500, 110000, 5),
(3, 5, 3, 2, 6175, 325, 6500, 1),
(4, 7, 30, 22, 2660, 140, 2800, 5),
(5, 8, 3, 2, 3040, 160, 3200, 5),
(6, 9, 3, 2, 1853, 97, 1950, 5),
(7, 10, 3, 2, 82650, 4350, 87000, 5),
(8, 4, 2, 1, 104500, 5500, 110000, 4),
(9, 11, 3, 2, 2250, 115, 2365, 5),
(10, 6, 2, 1, 6270, 330, 6600, 5),
(11, 12, 3, 2, 2185, 115, 2300, 5),
(12, 13, 30, 22, 2660, 140, 2800, 5),
(13, 3, 3, 2, 1900, 100, 2000, 1),
(14, 14, 30, 22, 3325, 175, 3500, 5),
(15, 15, 3, 2, 2185, 115, 2300, 5),
(16, 16, 3, 2, 1900, 95, 1995, 5),
(17, 17, 66, 54, 1900, 100, 2000, 5),
(18, 2, 3, 2, 9975, 525, 10500, 1),
(19, 18, 3, 2, 4560, 240, 4800, 5),
(20, 20, 3, 2, 85500, 4500, 90000, 5);

-- --------------------------------------------------------

--
-- Table structure for table `booking_transporter`
--

CREATE TABLE `booking_transporter` (
  `booking_transporter_id` int(11) NOT NULL,
  `booking_proposal_id` int(11) NOT NULL,
  `truck_id` int(11) NOT NULL,
  `trailer_reg_no` varchar(200) DEFAULT NULL,
  `driver_id` int(11) NOT NULL,
  `created_user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_transporter`
--

INSERT INTO `booking_transporter` (`booking_transporter_id`, `booking_proposal_id`, `truck_id`, `trailer_reg_no`, `driver_id`, `created_user_id`, `created_at`) VALUES
(1, 1, 1, 'KA-011-A', 2, 2, '2020-09-16 13:57:40'),
(2, 4, 15, 'K73847', 11, 30, '2020-09-29 11:09:28'),
(3, 5, 2, 'ZE 7854', 1, 3, '2020-09-29 11:20:16'),
(4, 6, 2, 'ZE 7854', 1, 3, '2020-09-29 12:25:46'),
(5, 7, 2, 'ZE 7854', 1, 3, '2020-10-01 09:16:01'),
(6, 2, 2, '', 1, 3, '2020-10-08 09:25:34'),
(7, 9, 2, 'ZE 7854', 1, 3, '2020-10-08 11:11:04'),
(8, 10, 14, '', 2, 2, '2020-10-08 12:35:39'),
(9, 11, 2, 'ZE 7854', 1, 3, '2020-10-12 10:07:44'),
(10, 12, 15, 'K73847', 11, 30, '2020-10-12 10:33:40'),
(11, 14, 15, 'K73847', 11, 30, '2020-10-14 02:23:02'),
(12, 15, 2, 'ZE 7854', 1, 3, '2020-10-15 15:55:09'),
(13, 16, 2, 'ZE 7854', 1, 3, '2020-10-21 04:17:38'),
(14, 17, 27, 'UZ 1234', 17, 66, '2020-10-21 04:44:00'),
(15, 19, 2, 'ZE 7854', 1, 3, '2020-11-10 11:56:08'),
(16, 20, 2, 'ZE 7854', 1, 3, '2020-11-12 09:12:20');

-- --------------------------------------------------------

--
-- Table structure for table `cargo_agreement`
--

CREATE TABLE `cargo_agreement` (
  `cargo_agreement_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `cargo_agreement_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cargo_agreement_updatable` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cargo_agreement`
--

INSERT INTO `cargo_agreement` (`cargo_agreement_id`, `user_type`, `cargo_agreement_text`, `cargo_agreement_updatable`) VALUES
(1, 1, '<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>DATED THIS <span style=\"text-decoration: underline;\">_________________</span> DAY OF&nbsp;</strong><span style=\"text-decoration: underline;\"><strong>_________________</strong></span><strong>&nbsp;2018</strong></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-decoration: none;\" align=\"left\">&nbsp;</p>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"font-weight: normal; text-decoration: none; text-align: center;\">BETWEEN</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY&nbsp;</strong></span></span><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>(EAOTA)</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\">AND</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>_____________________________________________</strong></span></span></p>\r\n<h2 class=\"western\" lang=\"en-US\">&nbsp;</h2>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT FOR TRANSPORT SERVICES</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-align: center;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>CARGO OWNER</strong></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>AGENCY AGREEMENT</strong></u></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>THIS AGREEMENT</strong></u></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"> is made on the&nbsp;<strong style=\"letter-spacing: -0.266667px;\"><span style=\"text-decoration: underline;\">___________</span></strong>&nbsp;day of </span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Month:</strong></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;<span style=\"text-decoration: underline;\"><strong style=\"letter-spacing: -0.266667px;\"><span style=\"text-decoration: underline;\">___________</span></strong></span>&nbsp;</span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Year:&nbsp; <span style=\"text-decoration: underline;\">___________</span></strong></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>BETWEEN:</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><u><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY </strong></u></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"> of Post Office Box Number 90237 - 80100 Mombasa in the said Republic (the &ldquo;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Agent</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns);</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>AND</strong></span></span></span></span> </span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"text-decoration: underline;\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">_________________________<span style=\"font-family: Times New Roman, serif;\">_______</span></span></span></span></span><span style=\"font-family: font-family: Times New Roman, serif;\">&nbsp;<span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">of Post Office Box Number <span style=\"text-decoration: underline;\">_______________________</span> aforesaid (the </span></span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Cargo Owner/Representative</strong></span></span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns) of the other part.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>WHEREAS: </strong></span></span></p>\r\n<ol type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner being the owner and/or beneficial owner of the Cargo the subject matter of this Agreement warranting at all times to have the authority of all persons owning or having an interest in the said Cargo and holding all valid licences and having complied with all statutory provisions and being of good repute financial standing and professional competence;</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"2\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner shall at all times and for the purposes of this contract be deemed to be the owner of the cargo or the agent of the owner and or other interested party; and</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"3\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA having the capacity to provide logistical support to the Cargo Owner to deliver the Cargo to a pre-agreed destination(s) hereby agrees to undertake the work (hereinafter referred to as &ldquo;the Services&rdquo;) on the terms and conditions set out in this Agreement.</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"4\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA and the Cargo Owner intend to give their cooperation secure footing by executing this Agreement on the date aforementioned having no intention whatsoever to; <br />(i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;grant imply or impose an exclusivity relationship or at all in any regard, or<br />(ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;create a partnership </span></span></p>\r\n</li>\r\n</ol>\r\n<p lang=\"en-US\" style=\"margin-left: 1.27cm; letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>NOW THEREFORE IT IS EXPRESSLY AGREED</strong></span></span></span><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"> as follows:</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><strong style=\"letter-spacing: -0.2pt; font-size: 10pt;\">Article 1 &ndash; DEFINITIONS &amp; INTERPRETATION</strong></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"> <br />1.1&nbsp;&nbsp;&nbsp;&nbsp;The following terms shall have the following meanings:</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.1&nbsp; &nbsp;&nbsp;</span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\">Cargo&rdquo; means goods in bulk or contained in one container or any number of separate containers transported in one load from the point of delivery to the point of destination.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 13.3333px; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\">1.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Container&rdquo; means a box of transport equipment (including refrigerated containers) of a permanent character specially designed to facilitate the carriage of goods by one or more modes of transport without intermediate re-loading and fitted with devices permitting its ready handling, storage and transfer.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Damage&rdquo; includes loss, partial loss or other damage of whatsoever nature arising out of or in connection with the services rendered under this Agreement or incidental thereto.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Equipment Interchange Report&rdquo; (hereinafter &ldquo;EIR&rdquo;) means a report on the condition or description of a container.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Goods&rdquo; includes goods, wares, merchandise and articles of every kind whatsoever except live animals, radioactive material and explosives shipped in Containers.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Working Day&rdquo; means any day other than Sunday or a gazetted Bank or Public Holiday</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Normal Working Hours&rdquo; means the hours of 8.00am to 12.30pm and 2.00pm to 5.00pm of any Working Day.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Port&rdquo; means any port that may be so designated under this agreement mutually by the parties.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.9&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Stuffing/Destuffing&rdquo; means the process of loading and discharging and shall include all and any handling and any other duties which shall be undertaken by the EAOTA, its servants and/or agents.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;TEU&rdquo; means twenty foot equivalent unit.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.11&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Territory&rdquo; means Kenya, Uganda, Rwanda, Burundi or Republic of Congo.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.1.12&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&ldquo;Vehicle&rdquo; means any and/or all those trucks and/or trailers which shall be provided by the Transporter under this Agreement and which shall meet all the specifications outlined herein</span></span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.1&nbsp; &nbsp; &nbsp;</span></span></span><span style=\"font-size: 13.3333px; letter-spacing: -0.266667px;\">The Headings contained in this agreement are for reference purposes only and should not be incorporated into this agreement and shall not be deemed to be any indication of the meaning of the clauses to which they relate.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.2&nbsp; &nbsp; &nbsp;</span></span></span><span style=\"font-size: 13.3333px; letter-spacing: -0.266667px;\">The words imparting the singular only shall also include the plural and vice versa where the context requires.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.3&nbsp; &nbsp; &nbsp;</span></span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\">Cargo Owner&rdquo; includes any appointed representative/agent/authorised employee and/or servant, authorised clearing and forwarding agent , authorised persons acting on the behalf of the Cargo Owner.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.4&nbsp; &nbsp;</span></span></span><span style=\"font-size: 12pt; letter-spacing: -0.2pt;\">&ldquo;</span><span style=\"font-size: 12pt; letter-spacing: -0.2pt; font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">Cargo Owner Representative&rdquo; means person appointed by a cargo owner to act on his behalf on any transaction under this agreement including but not limited to clearing &amp; forwarding agents, employees/servants and appointed agents and all other acting on behalf of the cargo owner under this agreement.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.5&nbsp; &nbsp;&nbsp;</span></span></span><span style=\"letter-spacing: -0.2pt;\">&ldquo;<span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">GIT&rdquo; means Goods In Transit Insurance and covers Insurance taken for all Cargo on Transit.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.6&nbsp; &nbsp;</span></span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px; font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">Licensors&rdquo; means independent third party transport providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorisations or licences and who are listed on EAOTA&rsquo;s website</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.7&nbsp; &nbsp;</span></span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px; font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">EAC&rdquo; means East Africa Community.</span></span></span></p>\r\n<p>&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 2 &ndash; SERVICES</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"letter-spacing: -0.2pt;\"> <span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span lang=\"en-US\">2.1 The Services constitute an online agency platform that enables users of EAOTA&rsquo;S Services to arrange and schedule transportation and/or logistic services with independent third party providers of search services, including independent third party transport providers and independent third party logistics providers under an agency agreement with EAOTA. Unless otherwise agreed by EAOTA in a separate written agreement with you, the services are made available for Commercial use </span></span></span></span></span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><em>(Transportation of Cargo). </em></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">You acknowledge that EAOTA does not provide transportation or logistics services or function as a transportation carrier and that all such transportation and logistics services are provided by independent third party contractors who are not employed by EAOTA.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.2 The transportation services will be made available by independent third party providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorizations or licences.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3 The services may be made available or accessed in connection with third party services and content (including Advertising) that EAOTA does not control. You acknowledge that the different terms of use and privacy policies may apply to your use of such third party services and content. EAOTA does not endorse such third party services and content and in no event shall EAOTA be responsible or liable for any services of such third party providers.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.4 The services and all rights therein are and shall remain EAOTA&rsquo;s property or the property of EAOTA&rsquo;s licensors. Neither this agreement nor your use of the services convey or grant to you any rights (i) in or related to the services except for the limited license granted as per the terms of this agreement (ii) to use or reference in any manner EAOTA&rsquo;s company names, logos, trademarks or services marks or those of EAOTA&rsquo;S licensors.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><strong style=\"font-size: 10pt; letter-spacing: -0.2pt;\">Article 3 - PERIOD OF AGREEMENT</strong></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall commence and take effect from <span style=\"text-decoration: underline;\">_____________________</span> and shall remain in full force and effect for the Term as set out hereafter and may be renewed by mutual agreement in writing between the parties.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be for a term of One (1) year effective from the Commencement Date. Thereafter there shall be an option to renew the Agreement on the same terms.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall be terminable at any time without notice by either party in the event of gross irremediable or repudiatory breach of contract by the other party. In the event of an irremediable breach of any term of this contract, the aggrieved party shall give to the other party notice of the immediate termination of this contract.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The meaning of an irremediable breach includes but is not limited to acts of fraud any inability to transact business with local or central government and/or any other authority withdrawal of licences and permits freezing of bank accounts and/or the closure of operations/ offices for whatever reason.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be terminable on three (3) month&rsquo;s notice in writing given by either party.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall automatically terminate in the event that either party enters into compulsory or voluntary liquidation (save for purposes of reconstruction or amalgamation) or if a receiver is appointed in respect of the whole or any part of its assets or if either party makes an assignment for the benefit of or composition with its creditors generally or threatens to do any of these things.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 4 &ndash; CARGO OWNER/ REPREENTATIVE OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.1 The Cargo Owner shall notify EAOTA of the expected time of arrival of any cargo or the expected time of collection of the Cargo from the point of collection, which forms the subject matter of this Agreement as early as possible and in any event not later than 7 days before the Expected Time of Arrival/collection of the said Cargo. </span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Arrangements for loading, border clearance and offloading will be made by the cargo owner/representative unless stated otherwise (in writing) and accepted (also in writing). Delays of more than </span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>48 hours</strong></u></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"> will attract a daily fee as described in the Cargo Owner Job Card being USD 200 per day.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.2.1 Should any delays be expected given an abnormal occurrence, this must be communicated to East African Online Transport Agency Ltd (EAOTA) immediately.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cargo Owner shall at all times endeavour to ensure that the Cargo delivered to the Transporter is consistent with the quantity, quality, weight, state and all other particulars as described in the Delivery note.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.1 The weight of the consignment declared is the maximum weight and all cargo carried will be less than the declared weight.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.2 Should the weight exceed declared weight or the goods not conform to the particulars described in the Delivery note, the cargo owner/representative will be fully liable for all arising legal and cost issues. This includes a daily fine for each day the vehicle is impounded or kept off the road. In these instances, the cargo owner will also be required to fully settle the cost of the transport.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.3 The Cargo Owner must ensure that the Cargo is well codded and/or labelled indicating the nature of the cargo whether delicate or otherwise described and manner in which the Cargo should be handled. EAOTA will not be held liable for any negligence on the Cargo Owner&rsquo;s part in failing to label and or code the Cargo appropriately.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.4 The Cargo Owner/representative/Agent must ensure that the Cargo is in good and acceptable condition at the point of loading the Cargo on to the truck.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.4 The Cargo Owner/representative confirms that he/she/they have relevant instructions and are fully authorised to enter this agreement and/or transact under this agreement and that the terms of this agreement shall be binding on the Cargo Owner and or representative or agent.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To make payments promptly.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 5 - EAOTA&rsquo;S OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall provide the services herein as follows:</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To employ adequate and qualified staff for the purpose of availing logistical support to the Cargo Owner for purposes of transporting, handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To source and obtain reliable transporters for purposes of transporting handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To comply with legislative requirements in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">4.1.4 EAOTA is a Payment Collection Agent and will collect payment from the Cargo Owner on behalf of the Transporter.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 6 &ndash; LIABILITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.1 EAOTA is an online trasport agent and is only a facilitator of the transaction and is in no way liable for loss, theft or damage of any of the goods and/or Cargo carried by trucks secured through EAOTA. </span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.1.1 EAOTA shall provide all relevant details regarding the transporter whose truck has been secured by the Cargo Owner. The Cargo Owner is free to seek further relevant information regarding the transporter from other reliable sources. Loading cargo on the truck is confirmation that the Cargo Owner has contracted the said transporter to move the cargo.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.2 All transport services to be provided subject to conditions being suitable for safe transportation. The transporter and EAOTA will not be liable for any loss or delays due to poor weather conditions, civil unrest or war, Act of God or any other delays beyond the control of the transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.3 Cargo owner/representatives must ensure goods being transported are legal and that movement of these goods in no way breaks any laws of any of the countries it is originating from, passing through or destined for.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.3.1 The cargo owner/representative will be liable for any legal issues arising from the contents found to be in the container/load being transported.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.4 In no event shall EAOTA&rsquo;s total liability to you in connection with the services for all damages, losses and causes of action exceed One Hundred Dollars (USD100).</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.5 In the event the Cargo Owner fails to insure his goods as per the provisions of this agreement and any loss or damage is reported upon delivery of the goods the Cargo Owner shall not in any case deduct more than 30% of the transport fee payable to the Transporter.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">5.6 </span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner shall be responsible for payment of all duties payable at Boarder points in accordance with the relevant laws as may be enforced from time to time so as to facilitiate seemless transportation of the Cargo, any delays in payment of such duties that may occassion undue dentention at the boarder points shall attract a penalty of USD 100 for every 24 Hours of such undue detention.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">5.7 </span></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">By loading cargo onto the truck, the Cargo Owner/Representative agrees to hold EAOTA and its officers, directors, employees and agents HARMLESS from any and all claims, demands, losses, liabilities, and expenses which are not covered by the terms of this agency agreement but which arise out of or in connection with your use of the services or your breach or violation of any of these Terms.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 7 &ndash; PAYMENT</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall charge the Cargo Owner for services rendered under this Agreement or incidental thereto at the rates and tariffs set out in the Cargo Owner/Representative Job Card.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall raise an invoice per completed transaction.&nbsp;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment shall fall due immediately all supporting documents duly confirming that the services herein have been performed and /or rendered are submitted to the Cargo Owner/Representative together with EAOTA&rsquo;s invoice.<br />&nbsp;<br />6.4&nbsp;&nbsp;&nbsp; Payment delays of beyond 30 days from the due date of the payment will be considered defaults. EAOTA therefore reserves the right to engage the services of a debt collector to follow up the payment and if non-payment persists institute legal proceedings against the Cargo Owner/Representative for recovery of the same. </span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">6.5 Further, payment delays of over thirty (30) days after the allowable credit period shall attract an interest at the rate of 1.5% per month for every month the amount remains unpaid.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">6.6 The Cargo Owner/Representative shall be bound by the payment terms chosen on the platform and shall be liable for interest in accordance with clause 6.5 for any delays exceeding thirty (30) days upon lapse of EAOTA&rsquo;s allowable credit period.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 8 &ndash; INSURANCE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">7.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner/representative </span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\"><strong>must </strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">ensure Goods in Transit (GIT) insurance is obtained from load port to final destination.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">7.1.1 <span style=\"font-family: Times New Roman, serif;\">Cargo owner/representative is at liberty to insure the entire consignement including duty payable at various boder points. EAOTA shall not at any point be held liable for lack of payment of such duty by the Cargo owner for any reason whatsoever. The Cargo owner acknowledges that such payment shall be borne exclusively at all times by the Cargo Owner/Representative.</span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">7.1.2 EAOTA shall not be held liable for any damage on the Cargo transported under this agreement and not insured while on transit. In the event the Cargo Owner fails/neglects and or refuses to Insure his Cargo while on Transit, the Cargo Owner accepts all liability for any damage caused on the Cargo while on Transit.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 9 &ndash; CONFIDENTIALITY</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br />8.1Personal information released to EAOTA will be treated with utmost privacy and confidentiality unless otherwise requested for purposes of conducting investigations on the Cargo by a legally recognised investigation body including but not limited to Kenya Police or the Directorate of Criminal Investigation.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 10 &ndash; FORCE MAJEURE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">9.1 Both parties shall be released from their respective obligations in the event of national emergency, war, act of principalities, prohibitive governmental regulation, destruction of facilities by lighting, earthquake, storm, flood, tempest or fire, labour disturbances or of any other cause beyond the reasonable control of the parties or either of them rendering the performance of this Agreement impossible;</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">9.1.1 </span></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">PROVIDED ALWAYS that the affected party or both of them shall give written notice immediately upon becoming aware of an event of force majeure and in any event not later than seven (7) days of becoming so aware.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 11 &ndash; INVALIDITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">10.1 If any provision of this Agreement is declared by any judicial or other competent authority to be void, voidable, illegal or otherwise unenforceable or indications to that effect are received by either of the parties from any competent authorities the parties shall amend that provision in such reasonable manner as achieves the intention of the parties without illegality PROVIDED ALWAYS that the remaining provisions of this Agreement shall remain in full force and effect.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><strong style=\"font-size: 10pt; letter-spacing: -0.2pt;\">Article 12 &ndash; ARBITRATION</strong></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All disputes which shall at any time arise between the parties and which disputes shall be incapable of amicable resolution within 14 days and which shall touch on or concern this Agreement shall be referred to a single arbitrator to be agreed upon by the parties or in default of agreement to be nominated by the Chairperson for the time being of the Institute of Chartered Arbitrators in accordance with the Arbitration Act, 1995 or any statutory modification or re-enactment of it for the time being in force in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">11.2 Where any dispute, conflict, claim or controversy arises in connection with or in relating to the services or terms of this agreement outside the Jurisdiction of Kenya but within East Africa Community (EAC) such dispute </span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">will be subject to laws that relate to importation, prohibition, entry, examination, landing and exportation of goods as stipulated in the&nbsp;EAC Customs Management Act.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.3 Further, any dispute, conflict, claim, or controversy that arises outside the EAC shall be first mandatorily submitted to mediation proceedings under the International Chamber of Commerce Mediation Rules. If such dispute has not been settled within sixty (60) days after a request for mediation has been submitted as stipulated herein, such dispute can be referred to and shall be exclusively and finally resolved by Arbitration under the Rules of Arbitration of the International Chamber of Commerce. The Place of both Mediation and Arbitration shall be Nairobi, Kenya to the extent viable under the applicable Law. The Language of the Mediation and/or Arbitration shall be English with the exception of using both English and the relevant native language in the event any of the parties does not speak English. </span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 13 &ndash; JURISDICTION&shy;</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">13.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be governed by Kenyan Law to the extent allowable under this agreement. Formation and Interpretation of this Agreement shall be deemed to have been made in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">13.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-family: Times New Roman, serif;\">Any proceedings arising out of or in connection with this Agreement may be brought in any court of competent jurisdiction in Kenya to the extent allowable under this Agreement.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>IN WITNESS WHEREOF</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"> the parties hereto have hereunto executed this Agreement the day and year first herein before written</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Signed <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span> <span style=\"font-size: 10pt;\">Date</span> <span style=\"text-decoration: underline;\">&hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">Signed <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span> Date <span style=\"text-decoration: underline;\">&hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></p>', '<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>DATED THIS <span style=\"text-decoration: underline;\">______11/06/2020___________</span> DAY OF&nbsp;</strong><span style=\"text-decoration: underline;\"><strong>___15/06/2020______________</strong></span><strong>&nbsp;2018</strong></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-decoration: none;\" align=\"left\">&nbsp;</p>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"font-weight: normal; text-decoration: none; text-align: center;\">BETWEEN</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY&nbsp;</strong></span></span><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>(EAOTA)</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\">AND</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>__________Manasa___________________________________</strong></span></span></p>\r\n<h2 class=\"western\" lang=\"en-US\">&nbsp;</h2>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT FOR TRANSPORT SERVICES</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-align: center;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>CARGO OWNER</strong></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>AGENCY AGREEMENT</strong></u></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>THIS AGREEMENT</strong></u></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"> is made on the&nbsp;<strong style=\"letter-spacing: -0.266667px;\"><span style=\"text-decoration: underline;\">___11______</span></strong>&nbsp;day of </span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Month:</strong></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;<span style=\"text-decoration: underline;\"><strong style=\"letter-spacing: -0.266667px;\"><span style=\"text-decoration: underline;\">_______June____</span></strong></span>&nbsp;</span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Year:&nbsp; <span style=\"text-decoration: underline;\">______2020_____</span></strong></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>BETWEEN:</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><u><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY </strong></u></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"> of Post Office Box Number 90237 - 80100 Mombasa in the said Republic (the &ldquo;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Agent</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns);</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>AND</strong></span></span></span></span> </span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"text-decoration: underline;\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">_________________________<span style=\"font-family: Times New Roman, serif;\">_______</span></span></span></span></span><span style=\"font-family: font-family: Times New Roman, serif;\">&nbsp;<span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">of Post Office Box Number <span style=\"text-decoration: underline;\">_______________________</span> aforesaid (the </span></span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Cargo Owner/Representative</strong></span></span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns) of the other part.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>WHEREAS: </strong></span></span></p>\r\n<ol type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner being the owner and/or beneficial owner of the Cargo the subject matter of this Agreement warranting at all times to have the authority of all persons owning or having an interest in the said Cargo and holding all valid licences and having complied with all statutory provisions and being of good repute financial standing and professional competence;</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"2\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner shall at all times and for the purposes of this contract be deemed to be the owner of the cargo or the agent of the owner and or other interested party; and</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"3\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA having the capacity to provide logistical support to the Cargo Owner to deliver the Cargo to a pre-agreed destination(s) hereby agrees to undertake the work (hereinafter referred to as &ldquo;the Services&rdquo;) on the terms and conditions set out in this Agreement.</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"4\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA and the Cargo Owner intend to give their cooperation secure footing by executing this Agreement on the date aforementioned having no intention whatsoever to; <br />(i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;grant imply or impose an exclusivity relationship or at all in any regard, or<br />(ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;create a partnership </span></span></p>\r\n</li>\r\n</ol>\r\n<p lang=\"en-US\" style=\"margin-left: 1.27cm; letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>NOW THEREFORE IT IS EXPRESSLY AGREED</strong></span></span></span><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"> as follows:</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><strong style=\"letter-spacing: -0.2pt; font-size: 10pt;\">Article 1 &ndash; DEFINITIONS &amp; INTERPRETATION</strong></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"> <br />1.1&nbsp;&nbsp;&nbsp;&nbsp;The following terms shall have the following meanings:</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.1&nbsp; &nbsp;&nbsp;</span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\">Cargo&rdquo; means goods in bulk or contained in one container or any number of separate containers transported in one load from the point of delivery to the point of destination.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 13.3333px; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\">1.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Container&rdquo; means a box of transport equipment (including refrigerated containers) of a permanent character specially designed to facilitate the carriage of goods by one or more modes of transport without intermediate re-loading and fitted with devices permitting its ready handling, storage and transfer.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Damage&rdquo; includes loss, partial loss or other damage of whatsoever nature arising out of or in connection with the services rendered under this Agreement or incidental thereto.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Equipment Interchange Report&rdquo; (hereinafter &ldquo;EIR&rdquo;) means a report on the condition or description of a container.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Goods&rdquo; includes goods, wares, merchandise and articles of every kind whatsoever except live animals, radioactive material and explosives shipped in Containers.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Working Day&rdquo; means any day other than Sunday or a gazetted Bank or Public Holiday</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Normal Working Hours&rdquo; means the hours of 8.00am to 12.30pm and 2.00pm to 5.00pm of any Working Day.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Port&rdquo; means any port that may be so designated under this agreement mutually by the parties.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.9&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Stuffing/Destuffing&rdquo; means the process of loading and discharging and shall include all and any handling and any other duties which shall be undertaken by the EAOTA, its servants and/or agents.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;TEU&rdquo; means twenty foot equivalent unit.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">1.1.11&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Territory&rdquo; means Kenya, Uganda, Rwanda, Burundi or Republic of Congo.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.1.12&nbsp;&nbsp;&nbsp;&nbsp; </span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&ldquo;Vehicle&rdquo; means any and/or all those trucks and/or trailers which shall be provided by the Transporter under this Agreement and which shall meet all the specifications outlined herein</span></span></span></span><span style=\"font-size: 12pt;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.1&nbsp; &nbsp; &nbsp;</span></span></span><span style=\"font-size: 13.3333px; letter-spacing: -0.266667px;\">The Headings contained in this agreement are for reference purposes only and should not be incorporated into this agreement and shall not be deemed to be any indication of the meaning of the clauses to which they relate.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.2&nbsp; &nbsp; &nbsp;</span></span></span><span style=\"font-size: 13.3333px; letter-spacing: -0.266667px;\">The words imparting the singular only shall also include the plural and vice versa where the context requires.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.3&nbsp; &nbsp; &nbsp;</span></span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px;\"><span style=\"font-size: 10pt;\">Cargo Owner&rdquo; includes any appointed representative/agent/authorised employee and/or servant, authorised clearing and forwarding agent , authorised persons acting on the behalf of the Cargo Owner.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.4&nbsp; &nbsp;</span></span></span><span style=\"font-size: 12pt; letter-spacing: -0.2pt;\">&ldquo;</span><span style=\"font-size: 12pt; letter-spacing: -0.2pt; font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">Cargo Owner Representative&rdquo; means person appointed by a cargo owner to act on his behalf on any transaction under this agreement including but not limited to clearing &amp; forwarding agents, employees/servants and appointed agents and all other acting on behalf of the cargo owner under this agreement.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.5&nbsp; &nbsp;&nbsp;</span></span></span><span style=\"letter-spacing: -0.2pt;\">&ldquo;<span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">GIT&rdquo; means Goods In Transit Insurance and covers Insurance taken for all Cargo on Transit.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.6&nbsp; &nbsp;</span></span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px; font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">Licensors&rdquo; means independent third party transport providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorisations or licences and who are listed on EAOTA&rsquo;s website</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; letter-spacing: -0.266667px; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.7&nbsp; &nbsp;</span></span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"letter-spacing: -0.266667px; font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\">EAC&rdquo; means East Africa Community.</span></span></span></p>\r\n<p>&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 2 &ndash; SERVICES</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"letter-spacing: -0.2pt;\"> <span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span lang=\"en-US\">2.1 The Services constitute an online agency platform that enables users of EAOTA&rsquo;S Services to arrange and schedule transportation and/or logistic services with independent third party providers of search services, including independent third party transport providers and independent third party logistics providers under an agency agreement with EAOTA. Unless otherwise agreed by EAOTA in a separate written agreement with you, the services are made available for Commercial use </span></span></span></span></span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><em>(Transportation of Cargo). </em></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">You acknowledge that EAOTA does not provide transportation or logistics services or function as a transportation carrier and that all such transportation and logistics services are provided by independent third party contractors who are not employed by EAOTA.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.2 The transportation services will be made available by independent third party providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorizations or licences.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3 The services may be made available or accessed in connection with third party services and content (including Advertising) that EAOTA does not control. You acknowledge that the different terms of use and privacy policies may apply to your use of such third party services and content. EAOTA does not endorse such third party services and content and in no event shall EAOTA be responsible or liable for any services of such third party providers.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.4 The services and all rights therein are and shall remain EAOTA&rsquo;s property or the property of EAOTA&rsquo;s licensors. Neither this agreement nor your use of the services convey or grant to you any rights (i) in or related to the services except for the limited license granted as per the terms of this agreement (ii) to use or reference in any manner EAOTA&rsquo;s company names, logos, trademarks or services marks or those of EAOTA&rsquo;S licensors.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><strong style=\"font-size: 10pt; letter-spacing: -0.2pt;\">Article 3 - PERIOD OF AGREEMENT</strong></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall commence and take effect from <span style=\"text-decoration: underline;\">_____________________</span> and shall remain in full force and effect for the Term as set out hereafter and may be renewed by mutual agreement in writing between the parties.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be for a term of One (1) year effective from the Commencement Date. Thereafter there shall be an option to renew the Agreement on the same terms.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall be terminable at any time without notice by either party in the event of gross irremediable or repudiatory breach of contract by the other party. In the event of an irremediable breach of any term of this contract, the aggrieved party shall give to the other party notice of the immediate termination of this contract.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The meaning of an irremediable breach includes but is not limited to acts of fraud any inability to transact business with local or central government and/or any other authority withdrawal of licences and permits freezing of bank accounts and/or the closure of operations/ offices for whatever reason.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be terminable on three (3) month&rsquo;s notice in writing given by either party.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall automatically terminate in the event that either party enters into compulsory or voluntary liquidation (save for purposes of reconstruction or amalgamation) or if a receiver is appointed in respect of the whole or any part of its assets or if either party makes an assignment for the benefit of or composition with its creditors generally or threatens to do any of these things.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 4 &ndash; CARGO OWNER/ REPREENTATIVE OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.1 The Cargo Owner shall notify EAOTA of the expected time of arrival of any cargo or the expected time of collection of the Cargo from the point of collection, which forms the subject matter of this Agreement as early as possible and in any event not later than 7 days before the Expected Time of Arrival/collection of the said Cargo. </span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Arrangements for loading, border clearance and offloading will be made by the cargo owner/representative unless stated otherwise (in writing) and accepted (also in writing). Delays of more than </span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>48 hours</strong></u></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"> will attract a daily fee as described in the Cargo Owner Job Card being USD 200 per day.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.2.1 Should any delays be expected given an abnormal occurrence, this must be communicated to East African Online Transport Agency Ltd (EAOTA) immediately.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cargo Owner shall at all times endeavour to ensure that the Cargo delivered to the Transporter is consistent with the quantity, quality, weight, state and all other particulars as described in the Delivery note.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.1 The weight of the consignment declared is the maximum weight and all cargo carried will be less than the declared weight.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.2 Should the weight exceed declared weight or the goods not conform to the particulars described in the Delivery note, the cargo owner/representative will be fully liable for all arising legal and cost issues. This includes a daily fine for each day the vehicle is impounded or kept off the road. In these instances, the cargo owner will also be required to fully settle the cost of the transport.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.3 The Cargo Owner must ensure that the Cargo is well codded and/or labelled indicating the nature of the cargo whether delicate or otherwise described and manner in which the Cargo should be handled. EAOTA will not be held liable for any negligence on the Cargo Owner&rsquo;s part in failing to label and or code the Cargo appropriately.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">2.3.4 The Cargo Owner/representative/Agent must ensure that the Cargo is in good and acceptable condition at the point of loading the Cargo on to the truck.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.4 The Cargo Owner/representative confirms that he/she/they have relevant instructions and are fully authorised to enter this agreement and/or transact under this agreement and that the terms of this agreement shall be binding on the Cargo Owner and or representative or agent.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">3.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To make payments promptly.</span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 5 - EAOTA&rsquo;S OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall provide the services herein as follows:</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To employ adequate and qualified staff for the purpose of availing logistical support to the Cargo Owner for purposes of transporting, handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To source and obtain reliable transporters for purposes of transporting handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To comply with legislative requirements in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">4.1.4 EAOTA is a Payment Collection Agent and will collect payment from the Cargo Owner on behalf of the Transporter.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 6 &ndash; LIABILITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.1 EAOTA is an online trasport agent and is only a facilitator of the transaction and is in no way liable for loss, theft or damage of any of the goods and/or Cargo carried by trucks secured through EAOTA. </span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.1.1 EAOTA shall provide all relevant details regarding the transporter whose truck has been secured by the Cargo Owner. The Cargo Owner is free to seek further relevant information regarding the transporter from other reliable sources. Loading cargo on the truck is confirmation that the Cargo Owner has contracted the said transporter to move the cargo.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.2 All transport services to be provided subject to conditions being suitable for safe transportation. The transporter and EAOTA will not be liable for any loss or delays due to poor weather conditions, civil unrest or war, Act of God or any other delays beyond the control of the transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.3 Cargo owner/representatives must ensure goods being transported are legal and that movement of these goods in no way breaks any laws of any of the countries it is originating from, passing through or destined for.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.3.1 The cargo owner/representative will be liable for any legal issues arising from the contents found to be in the container/load being transported.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.4 In no event shall EAOTA&rsquo;s total liability to you in connection with the services for all damages, losses and causes of action exceed One Hundred Dollars (USD100).</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">5.5 In the event the Cargo Owner fails to insure his goods as per the provisions of this agreement and any loss or damage is reported upon delivery of the goods the Cargo Owner shall not in any case deduct more than 30% of the transport fee payable to the Transporter.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">5.6 </span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner shall be responsible for payment of all duties payable at Boarder points in accordance with the relevant laws as may be enforced from time to time so as to facilitiate seemless transportation of the Cargo, any delays in payment of such duties that may occassion undue dentention at the boarder points shall attract a penalty of USD 100 for every 24 Hours of such undue detention.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">5.7 </span></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">By loading cargo onto the truck, the Cargo Owner/Representative agrees to hold EAOTA and its officers, directors, employees and agents HARMLESS from any and all claims, demands, losses, liabilities, and expenses which are not covered by the terms of this agency agreement but which arise out of or in connection with your use of the services or your breach or violation of any of these Terms.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 7 &ndash; PAYMENT</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall charge the Cargo Owner for services rendered under this Agreement or incidental thereto at the rates and tariffs set out in the Cargo Owner/Representative Job Card.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall raise an invoice per completed transaction.&nbsp;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment shall fall due immediately all supporting documents duly confirming that the services herein have been performed and /or rendered are submitted to the Cargo Owner/Representative together with EAOTA&rsquo;s invoice.<br />&nbsp;<br />6.4&nbsp;&nbsp;&nbsp; Payment delays of beyond 30 days from the due date of the payment will be considered defaults. EAOTA therefore reserves the right to engage the services of a debt collector to follow up the payment and if non-payment persists institute legal proceedings against the Cargo Owner/Representative for recovery of the same. </span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">6.5 Further, payment delays of over thirty (30) days after the allowable credit period shall attract an interest at the rate of 1.5% per month for every month the amount remains unpaid.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt; font-family: Times New Roman, serif;\">6.6 The Cargo Owner/Representative shall be bound by the payment terms chosen on the platform and shall be liable for interest in accordance with clause 6.5 for any delays exceeding thirty (30) days upon lapse of EAOTA&rsquo;s allowable credit period.</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 8 &ndash; INSURANCE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">7.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner/representative </span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\"><strong>must </strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">ensure Goods in Transit (GIT) insurance is obtained from load port to final destination.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">7.1.1 <span style=\"font-family: Times New Roman, serif;\">Cargo owner/representative is at liberty to insure the entire consignement including duty payable at various boder points. EAOTA shall not at any point be held liable for lack of payment of such duty by the Cargo owner for any reason whatsoever. The Cargo owner acknowledges that such payment shall be borne exclusively at all times by the Cargo Owner/Representative.</span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">7.1.2 EAOTA shall not be held liable for any damage on the Cargo transported under this agreement and not insured while on transit. In the event the Cargo Owner fails/neglects and or refuses to Insure his Cargo while on Transit, the Cargo Owner accepts all liability for any damage caused on the Cargo while on Transit.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 9 &ndash; CONFIDENTIALITY</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br />8.1Personal information released to EAOTA will be treated with utmost privacy and confidentiality unless otherwise requested for purposes of conducting investigations on the Cargo by a legally recognised investigation body including but not limited to Kenya Police or the Directorate of Criminal Investigation.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 10 &ndash; FORCE MAJEURE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">9.1 Both parties shall be released from their respective obligations in the event of national emergency, war, act of principalities, prohibitive governmental regulation, destruction of facilities by lighting, earthquake, storm, flood, tempest or fire, labour disturbances or of any other cause beyond the reasonable control of the parties or either of them rendering the performance of this Agreement impossible;</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">9.1.1 </span></span></span></span><span style=\"font-size: 12pt; font-family: Times New Roman, serif;\"><span style=\"font-size: 10pt;\"><span style=\"letter-spacing: -0.2pt;\">PROVIDED ALWAYS that the affected party or both of them shall give written notice immediately upon becoming aware of an event of force majeure and in any event not later than seven (7) days of becoming so aware.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 11 &ndash; INVALIDITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">10.1 If any provision of this Agreement is declared by any judicial or other competent authority to be void, voidable, illegal or otherwise unenforceable or indications to that effect are received by either of the parties from any competent authorities the parties shall amend that provision in such reasonable manner as achieves the intention of the parties without illegality PROVIDED ALWAYS that the remaining provisions of this Agreement shall remain in full force and effect.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><strong style=\"font-size: 10pt; letter-spacing: -0.2pt;\">Article 12 &ndash; ARBITRATION</strong></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All disputes which shall at any time arise between the parties and which disputes shall be incapable of amicable resolution within 14 days and which shall touch on or concern this Agreement shall be referred to a single arbitrator to be agreed upon by the parties or in default of agreement to be nominated by the Chairperson for the time being of the Institute of Chartered Arbitrators in accordance with the Arbitration Act, 1995 or any statutory modification or re-enactment of it for the time being in force in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">11.2 Where any dispute, conflict, claim or controversy arises in connection with or in relating to the services or terms of this agreement outside the Jurisdiction of Kenya but within East Africa Community (EAC) such dispute </span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">will be subject to laws that relate to importation, prohibition, entry, examination, landing and exportation of goods as stipulated in the&nbsp;EAC Customs Management Act.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.3 Further, any dispute, conflict, claim, or controversy that arises outside the EAC shall be first mandatorily submitted to mediation proceedings under the International Chamber of Commerce Mediation Rules. If such dispute has not been settled within sixty (60) days after a request for mediation has been submitted as stipulated herein, such dispute can be referred to and shall be exclusively and finally resolved by Arbitration under the Rules of Arbitration of the International Chamber of Commerce. The Place of both Mediation and Arbitration shall be Nairobi, Kenya to the extent viable under the applicable Law. The Language of the Mediation and/or Arbitration shall be English with the exception of using both English and the relevant native language in the event any of the parties does not speak English. </span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 13 &ndash; JURISDICTION&shy;</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">13.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be governed by Kenyan Law to the extent allowable under this agreement. Formation and Interpretation of this Agreement shall be deemed to have been made in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">13.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"font-family: Times New Roman, serif;\">Any proceedings arising out of or in connection with this Agreement may be brought in any court of competent jurisdiction in Kenya to the extent allowable under this Agreement.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>IN WITNESS WHEREOF</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"> the parties hereto have hereunto executed this Agreement the day and year first herein before written</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Signed <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span> <span style=\"font-size: 10pt;\">Date</span> <span style=\"text-decoration: underline;\">&hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-size: 10pt; letter-spacing: -0.2pt;\">Signed <span style=\"text-decoration: underline;\">&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span> Date <span style=\"text-decoration: underline;\">&hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></p>');

-- --------------------------------------------------------

--
-- Table structure for table `cargo_packaging`
--

CREATE TABLE `cargo_packaging` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `package_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cargo_packaging`
--

INSERT INTO `cargo_packaging` (`id`, `delete_status`, `package_type`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, 'loose', '2020-04-18 18:25:02', 1, NULL, 0, NULL, 0),
(2, 0, 'liquid', '2020-04-18 18:25:43', 1, NULL, 0, NULL, 0),
(3, 0, '20 ft container', '2020-04-18 18:26:17', 1, NULL, 0, NULL, 0),
(4, 0, '40 ft container', '2020-04-18 18:26:42', 1, NULL, 0, NULL, 0),
(5, 0, '45 ft container', '2020-04-18 18:27:38', 1, NULL, 0, NULL, 0),
(6, 0, 'crates', '2020-04-18 18:28:53', 1, '2020-09-16 19:12:59', 1, NULL, 0),
(7, 1, 'beer pallets', '2020-04-18 18:29:13', 1, NULL, 0, '2020-09-16 19:12:45', 1),
(8, 0, 'general pallets', '2020-04-18 18:29:30', 1, NULL, 0, NULL, 0),
(9, 0, 'bags/bales', '2020-04-18 18:29:53', 1, NULL, 0, NULL, 0),
(10, 0, 'carton', '2020-04-18 18:30:22', 1, '2020-06-11 17:06:44', 1, NULL, 0),
(11, 0, 'drums', '2020-09-16 19:13:22', 1, NULL, 0, NULL, 0),
(12, 0, 'jericans', '2020-09-16 19:13:39', 1, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cargo_type`
--

CREATE TABLE `cargo_type` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cargo_type`
--

INSERT INTO `cargo_type` (`id`, `delete_status`, `type`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 1, 'bulk cement trailer', '2020-04-18 17:48:35', 1, NULL, 0, '2020-06-28 17:46:59', 1),
(2, 1, 'car carrier trailer', '2020-04-18 17:48:58', 1, NULL, 0, '2020-06-28 17:46:51', 1),
(3, 1, 'double semi trailer', '2020-04-18 17:49:40', 1, NULL, 0, '2020-06-28 17:46:40', 1),
(4, 1, 'low bed trailer', '2020-04-18 17:50:27', 1, NULL, 0, '2020-06-28 17:46:31', 1),
(5, 1, 'hc container', '2020-04-18 17:51:05', 1, NULL, 0, '2020-06-28 17:46:21', 1),
(6, 1, 'flatbed-40ft', '2020-04-18 17:57:00', 1, NULL, 0, '2020-06-28 17:45:51', 1),
(7, 1, 'flat bed - 20ft', '2020-04-18 17:57:20', 1, NULL, 0, '2020-06-28 17:45:41', 1),
(8, 1, '40 ft skeleton', '2020-04-18 17:58:32', 1, NULL, 0, '2020-06-28 17:45:31', 1),
(9, 1, '40 ft flat bed', '2020-04-18 17:58:49', 1, NULL, 0, '2020-06-28 17:45:22', 1),
(10, 1, '10 wheeler flat bed', '2020-04-18 17:59:33', 1, NULL, 0, '2020-06-28 17:45:13', 1),
(11, 1, '10 mt truck', '2020-04-18 18:03:48', 1, NULL, 0, '2020-06-28 17:45:03', 1),
(12, 1, 'high sided', '2020-04-18 18:04:35', 1, NULL, 0, '2020-06-28 17:44:54', 1),
(13, 1, 'tippers', '2020-04-18 18:05:09', 1, NULL, 0, '2020-06-28 17:44:45', 1),
(14, 1, '40 ft high sided', '2020-04-18 20:02:44', 1, '2020-06-11 17:06:10', 1, '2020-06-28 17:41:10', 1),
(15, 0, 'general cargo', '2020-06-28 17:41:32', 1, NULL, 0, NULL, 0),
(16, 0, 'hazardous cargo', '2020-06-28 17:41:50', 1, NULL, 0, NULL, 0),
(17, 0, 'petroleum related cargo', '2020-06-28 17:42:15', 1, NULL, 0, NULL, 0),
(18, 1, 'petroleum related cargo', '2020-06-28 17:43:04', 1, NULL, 0, '2020-06-28 17:43:14', 1),
(19, 0, 'alcohol related cargo', '2020-06-28 17:43:37', 1, NULL, 0, NULL, 0),
(20, 0, 'tobacco related cargo', '2020-06-28 17:44:32', 1, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_code` int(11) NOT NULL,
  `country_code` int(11) NOT NULL,
  `city_name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_status` int(11) NOT NULL,
  `deletion_indicator` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_code`, `country_code`, `city_name`, `city_status`, `deletion_indicator`) VALUES
(1, 1, 'mombasa', 0, 0),
(2, 1, 'nairobi', 0, 0),
(3, 1, 'nairobi central business district cbd', 0, 0),
(4, 1, 'Ongata Rongai', 0, 0),
(5, 1, 'ruiru', 0, 0),
(6, 1, 'thika', 0, 0),
(7, 1, 'kitengela', 0, 0),
(8, 1, 'nakuru', 0, 0),
(9, 1, 'mombasa - changamwe', 0, 0),
(10, 1, 'kiambu', 0, 0),
(11, 1, 'kitale', 0, 0),
(12, 1, 'eldoret', 0, 0),
(13, 1, 'kisumu', 0, 0),
(14, 1, 'mlolongo', 0, 0),
(15, 1, 'mtwapa', 0, 0),
(16, 1, 'nairobi - westlands', 0, 0),
(17, 1, 'nairobi - industrial area', 0, 0),
(18, 1, 'ngong', 0, 0),
(19, 1, 'machakos', 0, 0),
(20, 1, 'kikuyu', 0, 0),
(21, 1, 'kerugoya', 0, 0),
(22, 1, 'sultan hamud', 0, 0),
(23, 1, 'nairobi kpa inland container depot - icd', 0, 0),
(24, 1, 'Kilifi', 0, 0),
(26, 1, 'athi river', 0, 0),
(27, 1, 'bomet', 0, 0),
(28, 1, 'muranga', 0, 0),
(29, 1, 'karatina', 0, 0),
(30, 1, 'mazeras', 0, 0),
(31, 2, 'dar es salaam', 0, 0),
(32, 1, 'thika', 0, 0),
(33, 5, 'kagitumba', 0, 0),
(34, 3, 'kampala', 0, 0),
(35, 3, 'mbarara', 0, 0),
(36, 3, 'entebbe', 0, 0),
(37, 3, 'jinja', 0, 0),
(38, 8, 'bujumbura', 0, 0),
(39, 6, 'lusaka', 0, 0),
(40, 6, 'ndola', 0, 0),
(41, 7, 'khartoum', 0, 0),
(42, 1, 'naivasha inland container depot (suswa)', 0, 0),
(43, 1, 'nairobi - embakasi', 0, 0),
(44, 10, 'juba', 0, 0),
(45, 10, 'nimule', 0, 0),
(46, 10, 'wau', 0, 0),
(47, 9, 'goma', 0, 0),
(48, 9, 'kisangani', 0, 0),
(49, 9, 'bunia', 0, 0),
(50, 9, 'lubumbashi', 0, 0),
(51, 12, 'addis ababa', 0, 0),
(52, 1, 'moyale', 0, 0),
(53, 1, 'nairobi - baba dogo', 0, 0),
(54, 1, 'nairobi - kasarani/ruaraka', 0, 0),
(55, 1, 'limuru', 0, 0),
(56, 5, 'kigali', 0, 1),
(57, 5, 'kigali', 0, 0),
(58, 1, 'mombasa - port of mombasa', 0, 0),
(59, 1, 'lamu - port of lamu', 0, 0),
(60, 1, 'mombasa - port reiz', 0, 0),
(61, 3, 'mbarara', 0, 0),
(62, 3, 'fort portal', 0, 0),
(63, 3, 'lira', 0, 0),
(64, 2, 'arusha', 0, 0),
(65, 2, 'mwanza', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `commission_rate`
--

CREATE TABLE `commission_rate` (
  `id` int(11) NOT NULL,
  `start_rate` int(11) NOT NULL,
  `end_rate` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `currency_type` varchar(100) NOT NULL,
  `delete_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commission_rate`
--

INSERT INTO `commission_rate` (`id`, `start_rate`, `end_rate`, `commission`, `currency_type`, `delete_status`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 100, 1000, 10, 'Ksh', 0, '2020-05-02 13:52:20', 1, '2020-05-02 13:56:02', 1, NULL, 0),
(2, 1000, 10000, 20, 'Ksh', 1, '2020-05-02 13:56:21', 1, '2020-05-02 13:57:09', 1, '2020-07-23 16:28:31', 1),
(3, 1000, 5000, 15, 'Ksh', 0, '2020-05-02 14:15:40', 1, '2020-07-23 16:28:13', 1, NULL, 0),
(4, 5000, 15000, 20, 'Ksh', 0, '2020-06-25 14:06:15', 1, '2020-07-23 16:27:11', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_code` int(11) NOT NULL,
  `country_name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_status` int(11) NOT NULL,
  `deletion_indicator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Country Codes ';

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_code`, `country_name`, `country_status`, `deletion_indicator`) VALUES
(1, 'kenya', 0, 0),
(2, 'tanzania', 0, 0),
(3, 'uganda', 0, 0),
(4, 'united arab emirates', 0, 1),
(5, 'rwanda', 0, 0),
(6, 'zambia', 0, 0),
(7, 'sudan', 0, 0),
(8, 'burundi', 0, 0),
(9, 'democratic republic of congo', 0, 0),
(10, 'south sudan', 0, 0),
(11, 'mozambique', 0, 0),
(12, 'ethiopia', 0, 0),
(13, 'zimbabwe', 0, 0),
(14, 'malawi', 0, 0),
(15, 'botswana', 0, 0),
(16, 'lesotho', 0, 0),
(17, 'south africa', 0, 0),
(18, 'central africa republic', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `credit_term`
--

CREATE TABLE `credit_term` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `credit_days` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `credit_term`
--

INSERT INTO `credit_term` (`id`, `delete_status`, `credit_days`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, '15 days', '2020-09-28 15:31:08', 1, NULL, 0, NULL, 0),
(2, 0, '21 days', '2020-09-28 15:31:20', 1, NULL, 0, NULL, 0),
(3, 0, '30 days', '2020-09-28 15:31:33', 1, NULL, 0, NULL, 0),
(4, 0, '0 days', '2020-09-28 20:48:17', 1, NULL, 0, NULL, 0),
(5, 0, '7 days', '2020-09-28 20:48:26', 1, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_details`
--

CREATE TABLE `driver_details` (
  `driver_code` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `company_code` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_dob` date NOT NULL,
  `country_code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_issued` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `identification_copy` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_contact_prefix` int(11) NOT NULL,
  `driver_contact_no` bigint(10) NOT NULL,
  `alternate_contact_prefix` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alternate_contact_no` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletion_indicator` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approval_status` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_details`
--

INSERT INTO `driver_details` (`driver_code`, `users_id`, `company_code`, `driver_name`, `gender`, `driver_dob`, `country_code`, `identification_type`, `identification_no`, `passport_no`, `country_issued`, `expiry_date`, `identification_copy`, `driver_contact_prefix`, `driver_contact_no`, `alternate_contact_prefix`, `alternate_contact_no`, `deletion_indicator`, `approval_status`) VALUES
(1, 5, '2', 'Absko', 'Male', '1980-02-04', '1', 'National ID', 'KDSJ3434', '', 'Kenya', NULL, 'truck2.jpeg', 32, 798434774, '434', '2387238723', '0', 'active'),
(2, 6, '1', 'Barasa', 'Male', '1980-04-06', '1', 'National ID', 'KDJSD8237', '', 'Kenya', NULL, '', 254, 784834684, '', '', '0', 'active'),
(11, 34, '22', 'Grace', 'Male', '2020-07-08', '2', 'National ID', 'KIRO92392', NULL, 'Tanzania', NULL, 'truck3.jpeg', 255, 949384452, '', '', '0', 'active'),
(12, 35, '23', 'Imani', 'Male', '2020-07-08', '2', 'National ID', 'TRJ8238', NULL, 'Tanzania', NULL, '', 255, 984183814, '', '', '0', 'active'),
(13, 37, '26', 'AKash', 'Male', '2020-08-20', '1', 'National ID', 'KJD97234', NULL, 'Kenya', NULL, '', 254, 8073429158, '', '', '0', 'active'),
(14, 43, '32', 'Jane', 'Female', '1985-09-01', '1', 'National ID', '1823623479586', NULL, 'Kenya', NULL, '', 254, 729713222, '256', '9684348977', '0', 'active'),
(15, 57, '40', 'Maxwell Omondi', 'Male', '2000-01-01', '1', 'National ID', '33331287', NULL, 'Kenya', NULL, '', 254, 792532005, '', '', '0', 'active'),
(16, 67, '54', 'Esther Muthoni', 'Female', '1980-10-01', '8', 'Passport No', NULL, '1234567', 'Burundi', '1970-01-01', '', 256, 726816559, '254', '0726816559', '0', 'active'),
(17, 68, '54', 'Kiragu Kiragu', 'Male', '2000-10-22', '3', 'Passport No', NULL, '334455', 'Uganda', '2021-02-11', '', 256, 799000000, '254', '0123000000', '0', 'active'),
(18, 69, '54', 'Tutan', 'Male', '1999-09-29', '1', 'National ID', '123455', NULL, 'Kenya', NULL, '', 254, 223344, '', '', '0', 'rejected'),
(19, 70, '36', 'CYRUS MUNYIRI', 'Male', '1985-01-05', '1', 'National ID', '23717929', NULL, 'Kenya', NULL, 'CYRUS_ID.jpg', 254, 712692076, '254', '0735789102', '0', 'active'),
(20, 71, '36', 'SIMON MUGOH', 'Male', '1979-01-21', '1', 'National ID', '21037294', NULL, 'Kenya', NULL, 'SIMON_ID.docx', 254, 727119187, '', '', '0', 'active'),
(21, 72, '36', 'JOSEPH KINYANJUI', 'Male', '1965-08-30', '1', 'National ID', '6847455', NULL, 'Kenya', NULL, 'JOSEPH_ID.jpg', 254, 726806823, '', '', '0', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `driver_documents`
--

CREATE TABLE `driver_documents` (
  `driver_doc_code` int(11) NOT NULL,
  `driver_code` int(11) NOT NULL,
  `license_number` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dl_doc` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dl_expdate` date NOT NULL,
  `driver_photo` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `port_pass` int(11) NOT NULL,
  `port_pass_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port_pass_expdate` date DEFAULT NULL,
  `port_pass_doc` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_doc_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_document` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deletion_indicator` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `driver_documents`
--

INSERT INTO `driver_documents` (`driver_doc_code`, `driver_code`, `license_number`, `dl_doc`, `dl_expdate`, `driver_photo`, `port_pass`, `port_pass_no`, `port_pass_expdate`, `port_pass_doc`, `other_doc_name`, `other_document`, `deletion_indicator`) VALUES
(1, 1, 'WLK8376', 'license.jpg', '2022-02-04', 'driver_pic.jpg', 1, 'K7478388', '2022-02-08', 'truck_img15.jpg', 'pic29.jpg', 'pic124.jpg', 0),
(2, 2, 'WLH8854', 'license2.jpg', '2025-02-04', 'driver_pic1.jpg', 1, 'K873487', '2024-02-05', 'fitness3.jpg', 'License', 'fitness4.jpg', 0),
(9, 11, 'DL983283', 'license11.jpg', '2020-08-07', 'pic.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(10, 12, 'DL82389', 'license12.jpg', '2020-08-05', 'pic5.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(11, 13, 'KSJ98239', 'pic111.jpg', '2020-09-25', 'license13.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(12, 14, 'TN209785', 'clock.png', '2020-12-01', 'clock1.png', 0, NULL, NULL, NULL, 'Certificate for Dangerous Goods', 'clock2.png', 0),
(13, 15, '123456789', 'Scan0010.pdf', '2021-01-01', 'Scan0012.pdf', 0, NULL, NULL, NULL, NULL, NULL, 0),
(14, 16, 'kzp 117', 'QUESTIONS.jpg', '2020-11-07', 'njogu.jpg', 1, '112233', '2020-12-31', 'kpa.jpg', 'Curriculum Vitae,ID Copy', 'BETHWEL_KIPLAGAT_KENGEN_CV.doc,clock4.png', 0),
(15, 17, '12345', 'clock3.png', '2021-01-21', 'doreen.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(16, 18, '1234556', 'clock5.png', '2021-01-14', 'doreen1.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(17, 19, 'OCZ115K', 'CYRUS_DL.docx', '2022-04-16', 'DRIVER_CYRUS.jpeg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(18, 20, 'IPF63', 'SIMON_DL.jpg', '2021-02-06', 'DRIVER_SIMON.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0),
(19, 21, 'GTR5', 'JOSEPH_DL1.jpg', '2022-01-22', 'DRIVER_JOSEPH.jpg', 0, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `incomplete_booking`
--

CREATE TABLE `incomplete_booking` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `users_company_code` int(11) NOT NULL,
  `cargo_type_id` int(11) DEFAULT NULL,
  `cargo_packaging_id` int(11) DEFAULT NULL,
  `origin_country_code` int(11) DEFAULT NULL,
  `origin_city_code` int(11) DEFAULT NULL,
  `destination_country_code` int(11) DEFAULT NULL,
  `destination_city_code` int(11) DEFAULT NULL,
  `tonnage_code` int(11) DEFAULT NULL,
  `booking_status` int(11) NOT NULL,
  `pick_up_date` date DEFAULT NULL,
  `vehicle_feet` float DEFAULT NULL,
  `start_km` int(11) DEFAULT NULL,
  `end_km` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `return_container_origin` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `incomplete_booking_item`
--

CREATE TABLE `incomplete_booking_item` (
  `id` int(11) NOT NULL,
  `incomplete_booking_id` int(11) NOT NULL,
  `item_name` text DEFAULT NULL,
  `item_quantity` int(11) DEFAULT NULL,
  `item_packaging_type` text DEFAULT NULL,
  `item_weight` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `interest_rate`
--

CREATE TABLE `interest_rate` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `rate_of_interest` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `invoice_code` varchar(500) DEFAULT NULL,
  `booking_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `invoice_type` int(11) NOT NULL COMMENT '0=>Customer,    1=>Transporter',
  `invoice_status` int(11) NOT NULL COMMENT '0=>Pending,    1=>Completed',
  `commission_rate` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `tax_percentage` int(11) DEFAULT NULL,
  `tax_amount` int(11) DEFAULT NULL,
  `grand_total` int(11) NOT NULL,
  `paid_amount` int(11) NOT NULL,
  `pending_amount` int(11) NOT NULL,
  `company_invoice` text DEFAULT NULL,
  `tax_receipt` text DEFAULT NULL,
  `payment_due_date` date DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `payment_term_id` int(11) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `credit_term_id` int(11) DEFAULT NULL,
  `bank_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `invoice_code`, `booking_id`, `users_id`, `invoice_type`, `invoice_status`, `commission_rate`, `amount`, `tax_percentage`, `tax_amount`, `grand_total`, `paid_amount`, `pending_amount`, `company_invoice`, `tax_receipt`, `payment_due_date`, `payment_date`, `payment_term_id`, `tax_rate_id`, `credit_term_id`, `bank_id`) VALUES
(1, 'INV0001', 1, 4, 0, 0, 167, 3350, 0, 0, 3350, 0, 3350, NULL, 'clock28.png', '2020-09-25', NULL, 10, 4, 1, 32),
(2, 'INV0002', 8, 3, 1, 0, 160, 3040, 0, 0, 3040, 0, 3040, 'clock30.png', 'clock31.png', '2020-10-30', NULL, 1, 4, 3, 3),
(3, 'INV0003', 10, 3, 1, 0, 4350, 82650, 16, 13224, 95874, 0, 95874, 'clock32.png', 'clock33.png', '2020-10-30', NULL, 1, 3, NULL, 3),
(4, 'INV0004', 10, 61, 0, 0, 4350, 87000, 16, 13920, 100920, 0, 100920, NULL, 'clock34.png', '2020-10-05', NULL, 10, 3, 4, 32),
(5, 'INV0005', 11, 61, 0, 0, 115, 2365, 0, 0, 2365, 0, 2365, NULL, 'clock35.png', '2020-11-06', NULL, 10, 4, 2, 32),
(6, 'INV0006', 4, 3, 1, 0, 5500, 104500, 0, 0, 104500, 100200, 4300, NULL, 'truck3.jpeg', '2020-10-26', NULL, 1, 0, NULL, 3),
(7, 'INV0007', 4, 50, 0, 0, 5500, 110000, 16, 17600, 127600, 0, 127600, NULL, 'logo2.jpg', '2020-11-06', NULL, 10, 3, 2, 32),
(8, 'INV0008', 12, 3, 1, 0, 115, 2185, 0, 0, 2185, 0, 2185, 'clock36.png', 'clock37.png', '2020-11-03', NULL, 1, 4, NULL, 38),
(9, 'INV0009', 14, 30, 1, 0, 175, 3325, 0, 0, 3325, 0, 3325, 'clock38.png', 'clock39.png', '2020-11-27', NULL, 1, 4, NULL, 40),
(10, 'INV0010', 14, 63, 0, 0, 175, 3500, 0, 0, 3500, 0, 3500, NULL, 'africa.jpg', '2020-11-13', NULL, 10, 4, 5, 39),
(11, 'INV0011', 15, 3, 1, 0, 115, 2185, 0, 0, 2185, 0, 2185, 'clock40.png', 'clock41.png', '2020-11-06', NULL, 1, 4, NULL, 38),
(12, 'INV0012', 16, 3, 1, 1, 95, 1900, 0, 0, 1900, 1900, 0, 'CAPRICORN_LOGISTICS_-_NOV_23_4.pdf', 'clock42.png', '2020-11-26', '2020-10-21', 1, 4, NULL, 38),
(13, 'INV0013', 16, 63, 0, 1, 95, 1995, 0, 0, 1995, 1995, 0, NULL, 'clock43.png', '2020-11-12', '2020-10-21', 10, 4, 1, 33),
(14, 'INV0014', 17, 66, 1, 1, 100, 1900, 0, 0, 1900, 1900, 0, 'INV_JAN_11_1.pdf', 'clock44.png', '2020-11-27', '2020-10-21', 1, 4, NULL, 44),
(15, 'INV0015', 17, 65, 0, 1, 100, 2000, 0, 0, 2000, 2000, 0, NULL, 'clock45.png', '2020-11-09', '2020-10-21', 10, 4, 5, 29),
(16, 'INV0016', 18, 61, 0, 1, 240, 4800, 0, 0, 4800, 4800, 0, NULL, 'clock46.png', '2020-11-26', '2020-11-10', 10, 4, 5, 33),
(17, 'INV0017', 18, 3, 1, 1, 240, 4560, 0, 0, 4560, 4560, 0, 'clock47.png', 'clock48.png', '2020-12-10', '2020-11-10', 1, 4, NULL, 38);

-- --------------------------------------------------------

--
-- Table structure for table `payment_credit_days`
--

CREATE TABLE `payment_credit_days` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `payment_credit` int(49) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_credit_days`
--

INSERT INTO `payment_credit_days` (`id`, `delete_status`, `payment_credit`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, 5, '2020-04-22 07:01:20', 1, NULL, 0, NULL, 0),
(2, 0, 10, '2020-04-22 07:01:28', 1, NULL, 0, NULL, 0),
(3, 0, 15, '2020-04-22 07:01:38', 1, NULL, 0, NULL, 0),
(4, 0, 20, '2020-04-22 07:01:45', 1, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_mode`
--

CREATE TABLE `payment_mode` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `mode_of_payment` varchar(99) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_term`
--

CREATE TABLE `payment_term` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `payment_term` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_term`
--

INSERT INTO `payment_term` (`id`, `delete_status`, `payment_term`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(7, 0, '50% Cash on loading, Balance on delivery', '2020-04-22 06:20:21', 1, '2020-07-23 19:32:24', 1, NULL, 0),
(8, 1, '10% Cash on loading, Balance on delivery', '2020-04-22 06:35:59', 1, '2020-04-22 06:39:28', 1, '2020-07-23 19:32:06', 1),
(9, 0, '100% Cash on loading', '2020-04-22 06:36:11', 1, NULL, 0, NULL, 0),
(10, 0, '100% Cash on delivery', '2020-04-22 06:36:29', 1, NULL, 0, NULL, 0),
(11, 0, '70% Cash on loading, Balance on delivery', '2020-04-22 06:39:55', 1, '2020-07-23 19:31:53', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `post_payment_term`
--

CREATE TABLE `post_payment_term` (
  `id` int(11) NOT NULL,
  `delete_status` int(11) NOT NULL,
  `payment_term` varchar(500) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_payment_term`
--

INSERT INTO `post_payment_term` (`id`, `delete_status`, `payment_term`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, '21 days after delivery', '2020-07-30 10:54:01', 1, '2020-07-30 10:57:48', 1, '2020-07-30 10:59:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rate_card`
--

CREATE TABLE `rate_card` (
  `id` int(11) NOT NULL,
  `delete_status` int(11) NOT NULL,
  `origin_country_code` int(11) NOT NULL,
  `origin_city_code` int(11) NOT NULL,
  `destination_country_code` int(11) NOT NULL,
  `destination_city_code` int(11) NOT NULL,
  `currency` varchar(200) NOT NULL,
  `rate` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate_card`
--

INSERT INTO `rate_card` (`id`, `delete_status`, `origin_country_code`, `origin_city_code`, `destination_country_code`, `destination_city_code`, `currency`, `rate`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, 1, 1, 1, 2, 'KSh', 90000, '2020-07-09 13:14:04', 1, '2020-09-16 19:02:20', 1, NULL, 0),
(2, 0, 1, 1, 1, 4, 'KSh', 90000, '2020-07-09 14:25:22', 1, '2020-09-16 19:02:03', 1, '2020-07-09 17:58:42', 1),
(3, 0, 2, 31, 2, 64, '$', 1000, '2020-09-16 18:59:12', 1, NULL, 0, NULL, 0),
(4, 0, 5, 57, 1, 2, '$', 2000, '2020-09-16 19:03:15', 1, NULL, 0, NULL, 0),
(5, 0, 1, 2, 5, 57, '$', 3500, '2020-09-16 19:03:42', 1, NULL, 0, NULL, 0),
(6, 0, 1, 1, 3, 34, '$', 2400, '2020-09-16 19:04:16', 1, NULL, 0, NULL, 0),
(7, 0, 1, 2, 9, 47, '$', 6200, '2020-09-16 19:06:23', 1, '2020-09-16 19:10:38', 1, NULL, 0),
(8, 0, 1, 42, 3, 34, '$', 2000, '2020-09-16 19:07:16', 1, NULL, 0, NULL, 0),
(9, 0, 1, 2, 3, 34, '$', 2000, '2020-09-16 19:07:42', 1, NULL, 0, NULL, 0),
(10, 0, 1, 2, 12, 51, '$', 6800, '2020-09-16 19:08:07', 1, NULL, 0, NULL, 0),
(11, 0, 1, 2, 8, 38, '$', 4800, '2020-09-16 19:08:35', 1, NULL, 0, NULL, 0),
(12, 0, 3, 35, 1, 2, '$', 1100, '2020-09-16 19:09:17', 1, NULL, 0, NULL, 0),
(13, 0, 3, 34, 1, 2, '$', 1000, '2020-09-16 19:09:45', 1, NULL, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sub_commission`
--

CREATE TABLE `sub_commission` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `sub_user_commission` int(11) NOT NULL,
  `user_commission_rate` int(11) NOT NULL,
  `total_commission` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_commission`
--

INSERT INTO `sub_commission` (`id`, `users_id`, `customer_id`, `booking_id`, `sub_user_commission`, `user_commission_rate`, `total_commission`, `status`) VALUES
(1, 28, 4, 1, 134, 33, 167, 0),
(2, 28, 61, 10, 3480, 870, 4350, 0),
(3, 28, 61, 11, 92, 23, 115, 0),
(4, 28, 50, 4, 4400, 1100, 5500, 0),
(5, 33, 63, 14, 158, 18, 175, 0),
(6, 28, 63, 16, 76, 19, 95, 0),
(7, 38, 65, 17, 60, 40, 100, 0),
(8, 28, 61, 18, 192, 48, 240, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate`
--

CREATE TABLE `tax_rate` (
  `id` int(11) NOT NULL,
  `tax_name` varchar(500) NOT NULL,
  `tax_percent` int(200) NOT NULL,
  `delete_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_rate`
--

INSERT INTO `tax_rate` (`id`, `tax_name`, `tax_percent`, `delete_status`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 'service tax', 15, 1, '2020-05-08 09:50:36', 1, '2020-05-08 09:58:18', 1, '2020-06-28 21:22:55', 1),
(2, 'custom tax', 20, 1, '2020-05-08 10:01:38', 1, '2020-06-11 17:25:14', 1, '2020-06-28 21:22:47', 1),
(3, 'vat  kenya drc zambia ss', 16, 0, '2020-06-28 21:22:15', 1, '2020-09-16 18:28:09', 1, NULL, 0),
(4, 'vat transit', 0, 0, '2020-06-28 21:22:34', 1, NULL, 0, NULL, 0),
(5, 'vat ethiopia south africa', 15, 0, '2020-09-16 18:27:57', 1, NULL, 0, NULL, 0),
(6, 'vat rwanda tanzania uganda burundi', 18, 0, '2020-09-16 18:30:30', 1, '2020-09-16 18:31:12', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tonnage`
--

CREATE TABLE `tonnage` (
  `tonnage_code` int(11) NOT NULL,
  `tonnage_weight` float NOT NULL,
  `deletion_indicator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tonnage`
--

INSERT INTO `tonnage` (`tonnage_code`, `tonnage_weight`, `deletion_indicator`) VALUES
(1, 0, 0),
(2, 15, 0),
(3, 20, 0),
(4, 32, 1),
(5, 0, 1),
(6, 4, 1),
(7, 5, 0),
(8, 25, 0),
(9, 9, 0),
(10, 14, 0),
(11, 26, 0),
(12, 28, 0),
(13, 35, 1),
(14, 6, 0),
(15, 27, 0),
(16, 24, 0),
(17, 23, 0),
(18, 22, 0),
(19, 21, 0),
(20, 19, 0),
(21, 18, 0),
(22, 17, 0),
(23, 16, 0),
(24, 13, 0),
(25, 12, 0),
(26, 11, 0),
(27, 10, 0),
(28, 8, 0),
(29, 7, 0),
(30, 29, 0),
(31, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trailer_details`
--

CREATE TABLE `trailer_details` (
  `trailer_id` int(11) NOT NULL,
  `truck_id` int(11) NOT NULL,
  `trailer_reg_no` varchar(200) NOT NULL,
  `trailer_type` varchar(200) NOT NULL,
  `trailer_log_book` text NOT NULL,
  `trailer_insurance_type` varchar(200) NOT NULL,
  `trailer_insurer` varchar(200) NOT NULL,
  `trailer_ins_expiry_date` date NOT NULL,
  `trailer_insurance_doc` text NOT NULL,
  `trailer_front_photo` varchar(200) NOT NULL,
  `trailer_back_photo` varchar(200) NOT NULL,
  `other_trailer_doc_name` text DEFAULT NULL,
  `other_trailer_document` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trailer_details`
--

INSERT INTO `trailer_details` (`trailer_id`, `truck_id`, `trailer_reg_no`, `trailer_type`, `trailer_log_book`, `trailer_insurance_type`, `trailer_insurer`, `trailer_ins_expiry_date`, `trailer_insurance_doc`, `trailer_front_photo`, `trailer_back_photo`, `other_trailer_doc_name`, `other_trailer_document`) VALUES
(1, 9, 'KDJ2378', '4', 'rights8.jpeg', 'Goods in Transit', 'Gemini', '2020-07-24', 'change_over17.jpg', 'download7.png', 'truck7.png', 'sdfsdf^&dfgdfg', 'change_over24.jpg^&fitness13.jpg'),
(3, 1, 'UGY-011-A', '5', 'download2.png', 'Comprehensive Insurance', 'Life Insurer', '2020-07-30', 'fitness4.jpg', 'download3.png', 'fitness_level1.jpg', NULL, NULL),
(4, 2, 'ZE 7854', '5', 'africa.jpg', 'Comprehensive Insurance', 'CFC', '2020-09-17', 'africa_2.jpg', 'clock.png', 'clock1.png', NULL, NULL),
(5, 14, 'UG 5677', '1', 'clock6.png', 'Comprehensive Insurance', 'CFC', '2020-08-05', 'clock7.png', 'clock8.png', 'clock9.png', NULL, NULL),
(6, 15, 'K73847', '7', 'pic122.jpg', 'Third Party Insurance', 'Gemini', '2020-07-31', 'pic123.jpg', 'truck1.jpg', 'truck_img1.jpg', NULL, NULL),
(7, 16, 'T 999 637', '8', 'pic116.jpg', 'Comprehensive Insurance', 'Gemini', '2020-08-07', 'pic125.jpg', 'truck_img3.jpg', 'truck3.jpg', NULL, NULL),
(8, 19, 'ZD 2653', '7', 'clock15.png', 'Comprehensive Insurance', 'UAP', '2020-12-03', 'clock16.png', 'clock17.png', 'clock18.png', NULL, NULL),
(9, 20, 'ZF2343', '5', 'NTSA2.jpg', 'Comprehensive Insurance', 'UAP', '2021-02-02', 'Scan0002.pdf', 'NTSA3.jpg', 'NTSA4.jpg', NULL, NULL),
(10, 21, 'ZF 5439', '6', 'Scan00121.pdf', 'Comprehensive Insurance', 'UAP', '2022-08-04', 'Scan0009.pdf', 'NTSA7.jpg', 'NTSA8.jpg', NULL, NULL),
(11, 22, 'ZG 1465', '7', '', 'Comprehensive Insurance', 'UAP', '0000-00-00', 'Scan00152.pdf', 'Scan00011.jpg', 'Scan0001_(2).jpg', NULL, NULL),
(12, 23, '', '7', '', '', '', '0000-00-00', '', '', '', NULL, NULL),
(13, 24, 'ZA1980', '1', 'Scan0001_(2)1.jpg', 'Comprehensive Insurance', 'UAP', '0000-00-00', 'Scan0001.pdf', 'Scan00014.jpg', 'Scan00015.jpg', NULL, NULL),
(14, 26, 'ZP 5353', '7', 'africa_continental_free_trade.png', 'Comprehensive Insurance', 'UAP', '2021-01-22', 'images.jpeg', 'IMG-20191114-WA0003.jpg', 'IMG-20191114-WA00031.jpg', NULL, NULL),
(15, 27, 'UZ 1234', '5', 'africa_continental_free_trade1.png', 'Comprehensive Insurance', 'Resolution', '2021-02-13', 'clock22.png', 'TRUCK1.jpg', 'IMG-20191118-WA00361.jpg', NULL, NULL),
(16, 28, 'UZ 1234', '5', 'clock25.png', 'Comprehensive Insurance', 'UAP', '2021-04-22', 'clock26.png', 'IMG-20191118-WA00364.jpg', 'IMG-20191118-WA00365.jpg', NULL, NULL),
(17, 25, 'ZF3388', '1', 'ZF_3388_LOGBOOK.jpg', 'Comprehensive Insurance', 'CIC GENERAL INSURANCE LTD', '2021-01-27', 'KCG_INSURANCE1.pdf', 'KCG_896A_(2)1.jpeg', 'KCG_896A_(2)2.jpeg', NULL, NULL),
(18, 29, 'ZG 0952', '1', 'ZG_0952.jpg', 'Comprehensive Insurance', 'CIC GENERAL INSURANCE', '2021-06-30', 'B11167459.pdf', 'ZG09521.jpeg', 'ZG09522.jpeg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transporter_agreement`
--

CREATE TABLE `transporter_agreement` (
  `transporter_agreement_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `transporter_agreement_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `transporter_agreement_updatable` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transporter_agreement`
--

INSERT INTO `transporter_agreement` (`transporter_agreement_id`, `user_type`, `transporter_agreement_text`, `transporter_agreement_updatable`) VALUES
(2, 1, '<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>DATED THIS <span style=\"text-decoration: underline;\">_________________</span> DAY OF&nbsp;</strong><span style=\"text-decoration: underline;\"><strong>_________________</strong></span><strong>&nbsp;2018</strong></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-decoration: none;\" align=\"left\">&nbsp;</p>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"font-weight: normal; text-decoration: none; text-align: center;\">BETWEEN</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY&nbsp;</strong></span></span><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>(EAOTA)</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\">AND</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>_____________________________________________</strong></span></span></p>\r\n<h2 class=\"western\" lang=\"en-US\">&nbsp;</h2>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT FOR TRANSPORT SERVICES</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-align: center;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>CARGO OWNER</strong></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>AGENCY AGREEMENT</strong></u></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>THIS AGREEMENT</strong></u></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;is made on the ______________________day of&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Month:</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;______________________________</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Year: _____________</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>BETWEEN:</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><u><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY&nbsp;</strong></u></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">of Post Office Box Number 90237 - 80100 Mombasa in the said Republic (the &ldquo;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Agent</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns);</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>AND</strong></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><u></u><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">_______________________________________</span></span></span>&nbsp;<span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">of Post Office Box Number _______________________ aforesaid (the &ldquo;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Cargo Owner/Representative</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns) of the other part.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>WHEREAS:</strong></span></span></p>\r\n<ol type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner being the owner and/or beneficial owner of the Cargo the subject matter of this Agreement warranting at all times to have the authority of all persons owning or having an interest in the said Cargo and holding all valid licences and having complied with all statutory provisions and being of good repute financial standing and professional competence;</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"2\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner shall at all times and for the purposes of this contract be deemed to be the owner of the cargo or the agent of the owner and or other interested party; and</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"3\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA having the capacity to provide logistical support to the Cargo Owner to deliver the Cargo to a pre-agreed destination(s) hereby agrees to undertake the work (hereinafter referred to as &ldquo;the Services&rdquo;) on the terms and conditions set out in this Agreement.</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"4\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA and the Cargo Owner intend to give their cooperation secure footing by executing this Agreement on the date aforementioned having no intention whatsoever to;&nbsp;<br />(i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;grant imply or impose an exclusivity relationship or at all in any regard, or<br />(ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;create a partnership</span></span></p>\r\n</li>\r\n</ol>\r\n<p lang=\"en-US\" style=\"margin-left: 1.27cm; letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>NOW THEREFORE IT IS EXPRESSLY AGREED</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&nbsp;as follows:</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 1 &ndash; DEFINITIONS &amp; INTERPRETATION</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /><br />1.1&nbsp;&nbsp;&nbsp;&nbsp;The following terms shall have the following meanings:</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.1&nbsp; &nbsp; &nbsp;</span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Cargo&rdquo; means goods in bulk or contained in one container or any number of separate containers transported in one load from the point of delivery to the point of destination.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.266667px;\">1.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Container&rdquo; means a box of transport equipment (including refrigerated containers) of a permanent character specially designed to facilitate the carriage of goods by one or more modes of transport without intermediate re-loading and fitted with devices permitting its ready handling, storage and transfer.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Damage&rdquo; includes loss, partial loss or other damage of whatsoever nature arising out of or in connection with the services rendered under this Agreement or incidental thereto.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Equipment Interchange Report&rdquo; (hereinafter &ldquo;EIR&rdquo;) means a report on the condition or description of a container.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Goods&rdquo; includes goods, wares, merchandise and articles of every kind whatsoever except live animals, radioactive material and explosives shipped in Containers.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Working Day&rdquo; means any day other than Sunday or a gazetted Bank or Public Holiday.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Normal Working Hours&rdquo; means the hours of 8.00am to 12.30pm and 2.00pm to 5.00pm of any Working Day.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Port&rdquo; means any port that may be so designated under this agreement mutually by the parties.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.9&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Stuffing/Destuffing&rdquo; means the process of loading and discharging and shall include all and any handling and any other duties which shall be undertaken by the EAOTA, its servants and/or agents.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;TEU&rdquo; means twenty foot equivalent unit.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.11&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Territory&rdquo; means Kenya, Uganda, Rwanda, Burundi or Republic of Congo.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">1.1.12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&ldquo;Vehicle&rdquo; means any and/or all those trucks and/or trailers which shall be provided by the Transporter under this Agreement and which shall meet all the specifications outlined herein</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.1&nbsp; &nbsp; &nbsp;</span><span style=\"letter-spacing: -0.266667px;\">The Headings contained in this agreement are for reference purposes only and should not be incorporated into this agreement and shall not be deemed to be any indication of the meaning of the clauses to which they relate.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.2&nbsp; &nbsp; &nbsp;</span><span style=\"letter-spacing: -0.266667px;\">The words imparting the singular only shall also include the plural and vice versa where the context requires.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.3&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">Cargo Owner&rdquo; includes any appointed representative/agent/authorised employee and/or servant, authorised clearing and forwarding agent , authorised persons acting on the behalf of the Cargo Owner.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.4&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">Cargo Owner Representative&rdquo; means person appointed by a cargo owner to act on his behalf on any transaction under this agreement including but not limited to clearing &amp; forwarding agents, employees/servants and appointed agents and all other acting on behalf of the cargo owner under this agreement.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.5&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">GIT&rdquo; means Goods In Transit Insurance and covers Insurance taken for all Cargo on Transit.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.6&nbsp; &nbsp; &nbsp;</span></span></span></span></span><span style=\"letter-spacing: -0.2pt;\">&ldquo;<span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Licensors&rdquo; means independent third party transport providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorisations or licences and who are listed on EAOTA&rsquo;s website</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.7&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont,  Roboto, Oxygen, Ubuntu, Cantarell,sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">EAC&rdquo; means East Africa Community.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 2 &ndash; SERVICES</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span lang=\"en-US\">2.1 The Services constitute an online agency platform that enables users of EAOTA&rsquo;S Services to arrange and schedule transportation and/or logistic services with independent third party providers of search services, including independent third party transport providers and independent third party logistics providers under an agency agreement with EAOTA. Unless otherwise agreed by EAOTA in a separate written agreement with you, the services are made available for Commercial use&nbsp;</span></span></span></span></span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><em>(Transportation of Cargo).&nbsp;</em></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">You acknowledge that EAOTA does not provide transportation or logistics services or function as a transportation carrier and that all such transportation and logistics services are provided by independent third party contractors who are not employed by EAOTA.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.2 The transportation services will be made available by independent third party providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorizations or licences.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3 The services may be made available or accessed in connection with third party services and content (including Advertising) that EAOTA does not control. You acknowledge that the different terms of use and privacy policies may apply to your use of such third party services and content. EAOTA does not endorse such third party services and content and in no event shall EAOTA be responsible or liable for any services of such third party providers.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.4 The services and all rights therein are and shall remain EAOTA&rsquo;s property or the property of EAOTA&rsquo;s licensors. Neither this agreement nor your use of the services convey or grant to you any rights (i) in or related to the services except for the limited license granted as per the terms of this agreement (ii) to use or reference in any manner EAOTA&rsquo;s company names, logos, trademarks or services marks or those of EAOTA&rsquo;S licensors.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 3 - PERIOD OF AGREEMENT</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall commence and take effect from _____________________________ and shall remain in full force and effect for the Term as set out hereafter and may be renewed by mutual agreement in writing between the parties.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be for a term of One (1) year effective from the Commencement Date. Thereafter there shall be an option to renew the Agreement on the same terms.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall be terminable at any time without notice by either party in the event of gross irremediable or repudiatory breach of contract by the other party. In the event of an irremediable breach of any term of this contract, the aggrieved party shall give to the other party notice of the immediate termination of this contract.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The meaning of an irremediable breach includes but is not limited to acts of fraud any inability to transact business with local or central government and/or any other authority withdrawal of licences and permits freezing of bank accounts and/or the closure of operations/ offices for whatever reason.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be terminable on three (3) month&rsquo;s notice in writing given by either party.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall automatically terminate in the event that either party enters into compulsory or voluntary liquidation (save for purposes of reconstruction or amalgamation) or if a receiver is appointed in respect of the whole or any part of its assets or if either party makes an assignment for the benefit of or composition with its creditors generally or threatens to do any of these things.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 4 &ndash; CARGO OWNER/ REPREENTATIVE OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.1 The Cargo Owner shall notify EAOTA of the expected time of arrival of any cargo or the expected time of collection of the Cargo from the point of collection, which forms the subject matter of this Agreement as early as possible and in any event not later than 7 days before the Expected Time of Arrival/collection of the said Cargo.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Arrangements for loading, border clearance and offloading will be made by the cargo owner/representative unless stated otherwise (in writing) and accepted (also in writing). Delays of more than&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>48 hours</strong></u></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;will attract a daily fee as described in the Cargo Owner Job Card being USD 200 per day.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.2.1 Should any delays be expected given an abnormal occurrence, this must be communicated to East African Online Transport Agency Ltd (EAOTA) immediately.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cargo Owner shall at all times endeavour to ensure that the Cargo delivered to the Transporter is consistent with the quantity, quality, weight, state and all other particulars as described in the Delivery note.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.1 The weight of the consignment declared is the maximum weight and all cargo carried will be less than the declared weight.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.2 Should the weight exceed declared weight or the goods not conform to the particulars described in the Delivery note, the cargo owner/representative will be fully liable for all arising legal and cost issues. This includes a daily fine for each day the vehicle is impounded or kept off the road. In these instances, the cargo owner will also be required to fully settle the cost of the transport.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.3 The Cargo Owner must ensure that the Cargo is well codded and/or labelled indicating the nature of the cargo whether delicate or otherwise described and manner in which the Cargo should be handled. EAOTA will not be held liable for any negligence on the Cargo Owner&rsquo;s part in failing to label and or code the Cargo appropriately.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.4 The Cargo Owner/representative/Agent must ensure that the Cargo is in good and acceptable condition at the point of loading the Cargo on to the truck.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.4 The Cargo Owner/representative confirms that he/she/they have relevant instructions and are fully authorised to enter this agreement and/or transact under this agreement and that the terms of this agreement shall be binding on the Cargo Owner and or representative or agent.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To make payments promptly.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 5 - EAOTA&rsquo;S OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall provide the services herein as follows:</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To employ adequate and qualified staff for the purpose of availing logistical support to the Cargo Owner for purposes of transporting, handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To source and obtain reliable transporters for purposes of transporting handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To comply with legislative requirements in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.4 EAOTA is a Payment Collection Agent and will collect payment from the Cargo Owner on behalf of the Transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 6 &ndash; LIABILITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.1 EAOTA is an online trasport agent and is only a facilitator of the transaction and is in no way liable for loss, theft or damage of any of the goods and/or Cargo carried by trucks secured through EAOTA.</span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.1.1 EAOTA shall provide all relevant details regarding the transporter whose truck has been secured by the Cargo Owner. The Cargo Owner is free to seek further relevant information regarding the transporter from other reliable sources. Loading cargo on the truck is confirmation that the Cargo Owner has contracted the said transporter to move the cargo.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.2 All transport services to be provided subject to conditions being suitable for safe transportation. The transporter and EAOTA will not be liable for any loss or delays due to poor weather conditions, civil unrest or war, Act of God or any other delays beyond the control of the transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.3 Cargo owner/representatives must ensure goods being transported are legal and that movement of these goods in no way breaks any laws of any of the countries it is originating from, passing through or destined for.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.3.1 The cargo owner/representative will be liable for any legal issues arising from the contents found to be in the container/load being transported.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.4 In no event shall EAOTA&rsquo;s total liability to you in connection with the services for all damages, losses and causes of action exceed One Hundred Dollars (USD100).</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.5 In the event the Cargo Owner fails to insure his goods as per the provisions of this agreement and any loss or damage is reported upon delivery of the goods the Cargo Owner shall not in any case deduct more than 30% of the transport fee payable to the Transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">5.6&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner shall be responsible for payment of all duties payable at Boarder points in accordance with the relevant laws as may be enforced from time to time so as to facilitiate seemless transportation of the Cargo, any delays in payment of such duties that may occassion undue dentention at the boarder points shall attract a penalty of USD 100 for every 24 Hours of such undue detention.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">5.7&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">By loading cargo onto the truck, the Cargo Owner/Representative agrees to hold EAOTA and its officers, directors, employees and agents HARMLESS from any and all claims, demands, losses, liabilities, and expenses which are not covered by the terms of this agency agreement but which arise out of or in connection with your use of the services or your breach or violation of any of these Terms.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 7 &ndash; PAYMENT</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall charge the Cargo Owner for services rendered under this Agreement or incidental thereto at the rates and tariffs set out in the Cargo Owner/Representative Job Card.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall raise an invoice per completed transaction.&nbsp;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment shall fall due immediately all supporting documents duly confirming that the services herein have been performed and /or rendered are submitted to the Cargo Owner/Representative together with EAOTA&rsquo;s invoice.<br />&nbsp;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.4&nbsp;&nbsp;&nbsp; Payment delays of beyond 30 days from the due date of the payment will be considered defaults. EAOTA therefore reserves the right to engage the services of a debt collector to follow up the payment and if non-payment persists institute legal proceedings against the Cargo Owner/Representative for recovery of the same.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.5 Further, payment delays of over thirty (30) days after the allowable credit period shall attract an interest at the rate of 1.5% per month for every month the amount remains unpaid.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.6 The Cargo Owner/Representative shall be bound by the payment terms chosen on the platform and shall be liable for interest in accordance with clause 6.5 for any delays exceeding thirty (30) days upon lapse of EAOTA&rsquo;s allowable credit period.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 8 &ndash; INSURANCE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">7.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner/representative&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\"><strong>must&nbsp;</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">ensure Goods in Transit (GIT) insurance is obtained from load port to final destination.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">7.1.1Cargo owner/representative is at liberty to insure the entire consignement including duty payable at various boder points. EAOTA shall not at any point be held liable for lack of payment of such duty by the Cargo owner for any reason whatsoever. The Cargo owner acknowledges that such payment shall be borne exclusively at all times by the Cargo Owner/Representative.</span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">7.1.2 EAOTA shall not be held liable for any damage on the Cargo transported under this agreement and not insured while on transit. In the event the Cargo Owner fails/neglects and or refuses to Insure his Cargo while on Transit, the Cargo Owner accepts all liability for any damage caused on the Cargo while on Transit.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 9 &ndash; CONFIDENTIALITY</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br />8.1Personal information released to EAOTA will be treated with utmost privacy and confidentiality unless otherwise requested for purposes of conducting investigations on the Cargo by a legally recognised investigation body including but not limited to Kenya Police or the Directorate of Criminal Investigation.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 10 &ndash; FORCE MAJEURE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">9.1 Both parties shall be released from their respective obligations in the event of national emergency, war, act of principalities, prohibitive governmental regulation, destruction of facilities by lighting, earthquake, storm, flood, tempest or fire, labour disturbances or of any other cause beyond the reasonable control of the parties or either of them rendering the performance of this Agreement impossible;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">9.1.1&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">PROVIDED ALWAYS that the affected party or both of them shall give written notice immediately upon becoming aware of an event of force majeure and in any event not later than seven (7) days of becoming so aware.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 11 &ndash; INVALIDITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">10.1 If any provision of this Agreement is declared by any judicial or other competent authority to be void, voidable, illegal or otherwise unenforceable or indications to that effect are received by either of the parties from any competent authorities the parties shall amend that provision in such reasonable manner as achieves the intention of the parties without illegality PROVIDED ALWAYS that the remaining provisions of this Agreement shall remain in full force and effect.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 12 &ndash; ARBITRATION</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All disputes which shall at any time arise between the parties and which disputes shall be incapable of amicable resolution within 14 days and which shall touch on or concern this Agreement shall be referred to a single arbitrator to be agreed upon by the parties or in default of agreement to be nominated by the Chairperson for the time being of the Institute of Chartered Arbitrators in accordance with the Arbitration Act, 1995 or any statutory modification or re-enactment of it for the time being in force in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">11.2 Where any dispute, conflict, claim or controversy arises in connection with or in relating to the services or terms of this agreement outside the Jurisdiction of Kenya but within East Africa Community (EAC) such dispute&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">will be subject to laws that relate to importation, prohibition, entry, examination, landing and exportation of goods as stipulated in the&nbsp;EAC Customs Management Act.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.3 Further, any dispute, conflict, claim, or controversy that arises outside the EAC shall be first mandatorily submitted to mediation proceedings under the International Chamber of Commerce Mediation Rules. If such dispute has not been settled within sixty (60) days after a request for mediation has been submitted as stipulated herein, such dispute can be referred to and shall be exclusively and finally resolved by Arbitration under the Rules of Arbitration of the International Chamber of Commerce. The Place of both Mediation and Arbitration shall be Nairobi, Kenya to the extent viable under the applicable Law. The Language of the Mediation and/or Arbitration shall be English with the exception of using both English and the relevant native language in the event any of the parties does not speak English.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 13 &ndash; JURISDICTION&shy;</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">13.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be governed by Kenyan Law to the extent allowable under this agreement. Formation and Interpretation of this Agreement shall be deemed to have been made in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">13.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Any proceedings arising out of or in connection with this Agreement may be brought in any court of competent jurisdiction in Kenya to the extent allowable under this Agreement.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>IN WITNESS WHEREOF</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&nbsp;the parties hereto have hereunto executed this Agreement the day and year first herein before written</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Signed &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.Date &hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Signed &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.Date &hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>', '<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>DATED THIS <span style=\"text-decoration: underline;\">_____08-06-2020____________</span> DAY OF&nbsp;</strong><span style=\"text-decoration: underline;\"><strong>_______AUGUST__________</strong></span><strong>&nbsp;2018</strong></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-decoration: none;\" align=\"left\">&nbsp;</p>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"font-weight: normal; text-decoration: none; text-align: center;\">BETWEEN</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY&nbsp;</strong></span></span><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>(EAOTA)</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\">AND</span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 100%; text-align: center;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span lang=\"en-US\"><strong>_____________________________________________</strong></span></span></p>\r\n<h2 class=\"western\" lang=\"en-US\">&nbsp;</h2>\r\n<h2 class=\"western\" lang=\"en-US\" style=\"text-align: center;\">AGENCY AGREEMENT FOR TRANSPORT SERVICES</h2>\r\n<p class=\"western\" lang=\"en-US\" style=\"line-height: 100%; text-align: center;\" align=\"center\"><span style=\"font-family: Times New Roman, serif;\"><strong>CARGO OWNER</strong></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>AGENCY AGREEMENT</strong></u></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>THIS AGREEMENT</strong></u></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;is made on the ______________________day of&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Month:</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;______________________________</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Year: _____________</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>BETWEEN:</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><u><strong>EAST AFRICAN ONLINE TRANSPORT AGENCY&nbsp;</strong></u></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">of Post Office Box Number 90237 - 80100 Mombasa in the said Republic (the &ldquo;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Agent</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns);</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>AND</strong></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><u></u><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">_______________________________________</span></span></span>&nbsp;<span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">of Post Office Box Number _______________________ aforesaid (the &ldquo;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>Cargo Owner/Representative</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&rdquo; which expression shall include its successors and assigns) of the other part.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>WHEREAS:</strong></span></span></p>\r\n<ol type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner being the owner and/or beneficial owner of the Cargo the subject matter of this Agreement warranting at all times to have the authority of all persons owning or having an interest in the said Cargo and holding all valid licences and having complied with all statutory provisions and being of good repute financial standing and professional competence;</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"2\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">The Cargo Owner shall at all times and for the purposes of this contract be deemed to be the owner of the cargo or the agent of the owner and or other interested party; and</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"3\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA having the capacity to provide logistical support to the Cargo Owner to deliver the Cargo to a pre-agreed destination(s) hereby agrees to undertake the work (hereinafter referred to as &ldquo;the Services&rdquo;) on the terms and conditions set out in this Agreement.</span></span></p>\r\n</li>\r\n</ol>\r\n<ol start=\"4\" type=\"A\">\r\n<li>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">EAOTA and the Cargo Owner intend to give their cooperation secure footing by executing this Agreement on the date aforementioned having no intention whatsoever to;&nbsp;<br />(i)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;grant imply or impose an exclusivity relationship or at all in any regard, or<br />(ii)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;create a partnership</span></span></p>\r\n</li>\r\n</ol>\r\n<p lang=\"en-US\" style=\"margin-left: 1.27cm; letter-spacing: -0.2pt; line-height: 100%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>NOW THEREFORE IT IS EXPRESSLY AGREED</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&nbsp;as follows:</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 1 &ndash; DEFINITIONS &amp; INTERPRETATION</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /><br />1.1&nbsp;&nbsp;&nbsp;&nbsp;The following terms shall have the following meanings:</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.1&nbsp; &nbsp; &nbsp;</span></span><span style=\"letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Cargo&rdquo; means goods in bulk or contained in one container or any number of separate containers transported in one load from the point of delivery to the point of destination.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.266667px;\">1.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Container&rdquo; means a box of transport equipment (including refrigerated containers) of a permanent character specially designed to facilitate the carriage of goods by one or more modes of transport without intermediate re-loading and fitted with devices permitting its ready handling, storage and transfer.</span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Damage&rdquo; includes loss, partial loss or other damage of whatsoever nature arising out of or in connection with the services rendered under this Agreement or incidental thereto.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Equipment Interchange Report&rdquo; (hereinafter &ldquo;EIR&rdquo;) means a report on the condition or description of a container.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Goods&rdquo; includes goods, wares, merchandise and articles of every kind whatsoever except live animals, radioactive material and explosives shipped in Containers.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.6&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Working Day&rdquo; means any day other than Sunday or a gazetted Bank or Public Holiday.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.7&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Normal Working Hours&rdquo; means the hours of 8.00am to 12.30pm and 2.00pm to 5.00pm of any Working Day.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.8&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Port&rdquo; means any port that may be so designated under this agreement mutually by the parties.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.9&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Stuffing/Destuffing&rdquo; means the process of loading and discharging and shall include all and any handling and any other duties which shall be undertaken by the EAOTA, its servants and/or agents.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.10&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;TEU&rdquo; means twenty foot equivalent unit.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">1.1.11&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&ldquo;Territory&rdquo; means Kenya, Uganda, Rwanda, Burundi or Republic of Congo.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">1.1.12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&ldquo;Vehicle&rdquo; means any and/or all those trucks and/or trailers which shall be provided by the Transporter under this Agreement and which shall meet all the specifications outlined herein</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.1&nbsp; &nbsp; &nbsp;</span><span style=\"letter-spacing: -0.266667px;\">The Headings contained in this agreement are for reference purposes only and should not be incorporated into this agreement and shall not be deemed to be any indication of the meaning of the clauses to which they relate.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.2&nbsp; &nbsp; &nbsp;</span><span style=\"letter-spacing: -0.266667px;\">The words imparting the singular only shall also include the plural and vice versa where the context requires.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.3&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">Cargo Owner&rdquo; includes any appointed representative/agent/authorised employee and/or servant, authorised clearing and forwarding agent , authorised persons acting on the behalf of the Cargo Owner.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.4&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">Cargo Owner Representative&rdquo; means person appointed by a cargo owner to act on his behalf on any transaction under this agreement including but not limited to clearing &amp; forwarding agents, employees/servants and appointed agents and all other acting on behalf of the cargo owner under this agreement.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.5&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont, Roboto, Oxygen, Ubuntu, Cantarell, sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">GIT&rdquo; means Goods In Transit Insurance and covers Insurance taken for all Cargo on Transit.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.6&nbsp; &nbsp; &nbsp;</span></span></span></span></span><span style=\"letter-spacing: -0.2pt;\">&ldquo;<span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Licensors&rdquo; means independent third party transport providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorisations or licences and who are listed on EAOTA&rsquo;s website</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"letter-spacing: -0.2pt;\">1.2.7&nbsp; &nbsp; &nbsp;</span><span style=\"font-family: -apple-system, BlinkMacSystemFont,  Roboto, Oxygen, Ubuntu, Cantarell,sans-serif; letter-spacing: -0.266667px;\">&ldquo;</span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\">EAC&rdquo; means East Africa Community.</span></span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 2 &ndash; SERVICES</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"letter-spacing: -0.2pt;\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span lang=\"en-US\">2.1 The Services constitute an online agency platform that enables users of EAOTA&rsquo;S Services to arrange and schedule transportation and/or logistic services with independent third party providers of search services, including independent third party transport providers and independent third party logistics providers under an agency agreement with EAOTA. Unless otherwise agreed by EAOTA in a separate written agreement with you, the services are made available for Commercial use&nbsp;</span></span></span></span></span><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><em>(Transportation of Cargo).&nbsp;</em></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">You acknowledge that EAOTA does not provide transportation or logistics services or function as a transportation carrier and that all such transportation and logistics services are provided by independent third party contractors who are not employed by EAOTA.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.2 The transportation services will be made available by independent third party providers, including transportation network company drivers, transportation charter permit holders or holders of similar transportation permits, authorizations or licences.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3 The services may be made available or accessed in connection with third party services and content (including Advertising) that EAOTA does not control. You acknowledge that the different terms of use and privacy policies may apply to your use of such third party services and content. EAOTA does not endorse such third party services and content and in no event shall EAOTA be responsible or liable for any services of such third party providers.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.4 The services and all rights therein are and shall remain EAOTA&rsquo;s property or the property of EAOTA&rsquo;s licensors. Neither this agreement nor your use of the services convey or grant to you any rights (i) in or related to the services except for the limited license granted as per the terms of this agreement (ii) to use or reference in any manner EAOTA&rsquo;s company names, logos, trademarks or services marks or those of EAOTA&rsquo;S licensors.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 3 - PERIOD OF AGREEMENT</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall commence and take effect from _____________________________ and shall remain in full force and effect for the Term as set out hereafter and may be renewed by mutual agreement in writing between the parties.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be for a term of One (1) year effective from the Commencement Date. Thereafter there shall be an option to renew the Agreement on the same terms.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall be terminable at any time without notice by either party in the event of gross irremediable or repudiatory breach of contract by the other party. In the event of an irremediable breach of any term of this contract, the aggrieved party shall give to the other party notice of the immediate termination of this contract.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The meaning of an irremediable breach includes but is not limited to acts of fraud any inability to transact business with local or central government and/or any other authority withdrawal of licences and permits freezing of bank accounts and/or the closure of operations/ offices for whatever reason.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be terminable on three (3) month&rsquo;s notice in writing given by either party.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This agreement shall automatically terminate in the event that either party enters into compulsory or voluntary liquidation (save for purposes of reconstruction or amalgamation) or if a receiver is appointed in respect of the whole or any part of its assets or if either party makes an assignment for the benefit of or composition with its creditors generally or threatens to do any of these things.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 4 &ndash; CARGO OWNER/ REPREENTATIVE OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.1 The Cargo Owner shall notify EAOTA of the expected time of arrival of any cargo or the expected time of collection of the Cargo from the point of collection, which forms the subject matter of this Agreement as early as possible and in any event not later than 7 days before the Expected Time of Arrival/collection of the said Cargo.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">3.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Arrangements for loading, border clearance and offloading will be made by the cargo owner/representative unless stated otherwise (in writing) and accepted (also in writing). Delays of more than&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><u><strong>48 hours</strong></u></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">&nbsp;will attract a daily fee as described in the Cargo Owner Job Card being USD 200 per day.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.2.1 Should any delays be expected given an abnormal occurrence, this must be communicated to East African Online Transport Agency Ltd (EAOTA) immediately.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The Cargo Owner shall at all times endeavour to ensure that the Cargo delivered to the Transporter is consistent with the quantity, quality, weight, state and all other particulars as described in the Delivery note.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.1 The weight of the consignment declared is the maximum weight and all cargo carried will be less than the declared weight.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.2 Should the weight exceed declared weight or the goods not conform to the particulars described in the Delivery note, the cargo owner/representative will be fully liable for all arising legal and cost issues. This includes a daily fine for each day the vehicle is impounded or kept off the road. In these instances, the cargo owner will also be required to fully settle the cost of the transport.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.3 The Cargo Owner must ensure that the Cargo is well codded and/or labelled indicating the nature of the cargo whether delicate or otherwise described and manner in which the Cargo should be handled. EAOTA will not be held liable for any negligence on the Cargo Owner&rsquo;s part in failing to label and or code the Cargo appropriately.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">2.3.4 The Cargo Owner/representative/Agent must ensure that the Cargo is in good and acceptable condition at the point of loading the Cargo on to the truck.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.4 The Cargo Owner/representative confirms that he/she/they have relevant instructions and are fully authorised to enter this agreement and/or transact under this agreement and that the terms of this agreement shall be binding on the Cargo Owner and or representative or agent.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">3.5&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To make payments promptly.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 5 - EAOTA&rsquo;S OBLIGATIONS</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall provide the services herein as follows:</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To employ adequate and qualified staff for the purpose of availing logistical support to the Cargo Owner for purposes of transporting, handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To source and obtain reliable transporters for purposes of transporting handling and/or dealing with the Cargo from the point of Departure to Destination;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To comply with legislative requirements in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">4.1.4 EAOTA is a Payment Collection Agent and will collect payment from the Cargo Owner on behalf of the Transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 6 &ndash; LIABILITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.1 EAOTA is an online trasport agent and is only a facilitator of the transaction and is in no way liable for loss, theft or damage of any of the goods and/or Cargo carried by trucks secured through EAOTA.</span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.1.1 EAOTA shall provide all relevant details regarding the transporter whose truck has been secured by the Cargo Owner. The Cargo Owner is free to seek further relevant information regarding the transporter from other reliable sources. Loading cargo on the truck is confirmation that the Cargo Owner has contracted the said transporter to move the cargo.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.2 All transport services to be provided subject to conditions being suitable for safe transportation. The transporter and EAOTA will not be liable for any loss or delays due to poor weather conditions, civil unrest or war, Act of God or any other delays beyond the control of the transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.3 Cargo owner/representatives must ensure goods being transported are legal and that movement of these goods in no way breaks any laws of any of the countries it is originating from, passing through or destined for.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.3.1 The cargo owner/representative will be liable for any legal issues arising from the contents found to be in the container/load being transported.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.4 In no event shall EAOTA&rsquo;s total liability to you in connection with the services for all damages, losses and causes of action exceed One Hundred Dollars (USD100).</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">5.5 In the event the Cargo Owner fails to insure his goods as per the provisions of this agreement and any loss or damage is reported upon delivery of the goods the Cargo Owner shall not in any case deduct more than 30% of the transport fee payable to the Transporter.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">5.6&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner shall be responsible for payment of all duties payable at Boarder points in accordance with the relevant laws as may be enforced from time to time so as to facilitiate seemless transportation of the Cargo, any delays in payment of such duties that may occassion undue dentention at the boarder points shall attract a penalty of USD 100 for every 24 Hours of such undue detention.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">5.7&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">By loading cargo onto the truck, the Cargo Owner/Representative agrees to hold EAOTA and its officers, directors, employees and agents HARMLESS from any and all claims, demands, losses, liabilities, and expenses which are not covered by the terms of this agency agreement but which arise out of or in connection with your use of the services or your breach or violation of any of these Terms.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 7 &ndash; PAYMENT</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall charge the Cargo Owner for services rendered under this Agreement or incidental thereto at the rates and tariffs set out in the Cargo Owner/Representative Job Card.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EAOTA shall raise an invoice per completed transaction.&nbsp;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment shall fall due immediately all supporting documents duly confirming that the services herein have been performed and /or rendered are submitted to the Cargo Owner/Representative together with EAOTA&rsquo;s invoice.<br />&nbsp;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.4&nbsp;&nbsp;&nbsp; Payment delays of beyond 30 days from the due date of the payment will be considered defaults. EAOTA therefore reserves the right to engage the services of a debt collector to follow up the payment and if non-payment persists institute legal proceedings against the Cargo Owner/Representative for recovery of the same.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.5 Further, payment delays of over thirty (30) days after the allowable credit period shall attract an interest at the rate of 1.5% per month for every month the amount remains unpaid.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">6.6 The Cargo Owner/Representative shall be bound by the payment terms chosen on the platform and shall be liable for interest in accordance with clause 6.5 for any delays exceeding thirty (30) days upon lapse of EAOTA&rsquo;s allowable credit period.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 8 &ndash; INSURANCE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">7.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">Cargo owner/representative&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\"><strong>must&nbsp;</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"de-DE\">ensure Goods in Transit (GIT) insurance is obtained from load port to final destination.</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">7.1.1Cargo owner/representative is at liberty to insure the entire consignement including duty payable at various boder points. EAOTA shall not at any point be held liable for lack of payment of such duty by the Cargo owner for any reason whatsoever. The Cargo owner acknowledges that such payment shall be borne exclusively at all times by the Cargo Owner/Representative.</span></span></p>\r\n<p class=\"western\" lang=\"de-DE\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">7.1.2 EAOTA shall not be held liable for any damage on the Cargo transported under this agreement and not insured while on transit. In the event the Cargo Owner fails/neglects and or refuses to Insure his Cargo while on Transit, the Cargo Owner accepts all liability for any damage caused on the Cargo while on Transit.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"left\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 9 &ndash; CONFIDENTIALITY</strong></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br />8.1Personal information released to EAOTA will be treated with utmost privacy and confidentiality unless otherwise requested for purposes of conducting investigations on the Cargo by a legally recognised investigation body including but not limited to Kenya Police or the Directorate of Criminal Investigation.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><br /></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 10 &ndash; FORCE MAJEURE</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">9.1 Both parties shall be released from their respective obligations in the event of national emergency, war, act of principalities, prohibitive governmental regulation, destruction of facilities by lighting, earthquake, storm, flood, tempest or fire, labour disturbances or of any other cause beyond the reasonable control of the parties or either of them rendering the performance of this Agreement impossible;</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">9.1.1&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">PROVIDED ALWAYS that the affected party or both of them shall give written notice immediately upon becoming aware of an event of force majeure and in any event not later than seven (7) days of becoming so aware.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 11 &ndash; INVALIDITY</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">10.1 If any provision of this Agreement is declared by any judicial or other competent authority to be void, voidable, illegal or otherwise unenforceable or indications to that effect are received by either of the parties from any competent authorities the parties shall amend that provision in such reasonable manner as achieves the intention of the parties without illegality PROVIDED ALWAYS that the remaining provisions of this Agreement shall remain in full force and effect.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><strong>Article 12 &ndash; ARBITRATION</strong></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;All disputes which shall at any time arise between the parties and which disputes shall be incapable of amicable resolution within 14 days and which shall touch on or concern this Agreement shall be referred to a single arbitrator to be agreed upon by the parties or in default of agreement to be nominated by the Chairperson for the time being of the Institute of Chartered Arbitrators in accordance with the Arbitration Act, 1995 or any statutory modification or re-enactment of it for the time being in force in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">11.2 Where any dispute, conflict, claim or controversy arises in connection with or in relating to the services or terms of this agreement outside the Jurisdiction of Kenya but within East Africa Community (EAC) such dispute&nbsp;</span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\">will be subject to laws that relate to importation, prohibition, entry, examination, landing and exportation of goods as stipulated in the&nbsp;EAC Customs Management Act.</span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">11.3 Further, any dispute, conflict, claim, or controversy that arises outside the EAC shall be first mandatorily submitted to mediation proceedings under the International Chamber of Commerce Mediation Rules. If such dispute has not been settled within sixty (60) days after a request for mediation has been submitted as stipulated herein, such dispute can be referred to and shall be exclusively and finally resolved by Arbitration under the Rules of Arbitration of the International Chamber of Commerce. The Place of both Mediation and Arbitration shall be Nairobi, Kenya to the extent viable under the applicable Law. The Language of the Mediation and/or Arbitration shall be English with the exception of using both English and the relevant native language in the event any of the parties does not speak English.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><strong>Article 13 &ndash; JURISDICTION&shy;</strong></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">13.1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This Agreement shall be governed by Kenyan Law to the extent allowable under this agreement. Formation and Interpretation of this Agreement shall be deemed to have been made in Kenya.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">13.2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Any proceedings arising out of or in connection with this Agreement may be brought in any court of competent jurisdiction in Kenya to the extent allowable under this Agreement.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"line-height: 150%;\" align=\"justify\"><span style=\"font-family: Garamond, serif;\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\"><strong>IN WITNESS WHEREOF</strong></span></span></span></span><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\"><span style=\"letter-spacing: -0.2pt;\"><span lang=\"en-US\">&nbsp;the parties hereto have hereunto executed this Agreement the day and year first herein before written</span></span></span></span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Signed &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.Date &hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Name &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">(Name of Authorised Officer)</span></span></p>\r\n<p class=\"western\" lang=\"en-GB\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\"><span style=\"font-family: Times New Roman, serif;\"><span style=\"font-size: small;\">Signed &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.Date &hellip;...&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..</span></span></p>\r\n<p class=\"western\" lang=\"en-US\" style=\"letter-spacing: -0.2pt; line-height: 150%;\" align=\"justify\">&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `truck_details`
--

CREATE TABLE `truck_details` (
  `truck_id` int(11) NOT NULL,
  `truck_status` int(11) NOT NULL,
  `company_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_reg_no` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` int(11) NOT NULL,
  `truck_make_id` int(200) NOT NULL,
  `model` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colour` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chassis_no` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `differential` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_log_book` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tonnage_code` int(11) NOT NULL,
  `vehicle_feet_id` int(11) NOT NULL,
  `trailer_pass` int(11) NOT NULL,
  `trailer_reg_no` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `approval_status` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delete_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `truck_details`
--

INSERT INTO `truck_details` (`truck_id`, `truck_status`, `company_code`, `vehicle_reg_no`, `country_code`, `truck_make_id`, `model`, `year`, `colour`, `chassis_no`, `differential`, `truck_log_book`, `tonnage_code`, `vehicle_feet_id`, `trailer_pass`, `trailer_reg_no`, `vehicle_type_id`, `approval_status`, `delete_status`) VALUES
(1, 1, '1', 'UGX-001-A', 3, 2, 'TATA ', '08/06/2020', 'Yellow', '1HGBH41JXMN109186', '6*4', 'change_over18.jpg', 16, 10, 1, 'UGY-011-A', 5, 'active', 0),
(2, 1, '2', 'KA0793', 1, 2, 'TATA ', '08/06/2020', 'Blue', 'QWKER234892384', '6*4', 'change_over18.jpg', 12, 10, 1, 'ZE 7854', 6, 'active', 0),
(14, 1, '1', 'UGX 456X', 3, 1, 'Actros', '01/09/2010', 'White', 'ASDDJSF234567', '6*4', 'clock2.png', 12, 10, 1, 'UG 5677', 0, 'active', 0),
(15, 1, '22', 'T 999 238', 2, 2, '2012', '08/05/2020', 'Yellow', 'H2398478134', '6*2', 'pic121.jpg', 12, 4, 1, 'K73847', 0, 'active', 0),
(16, 1, '23', 'T 999 243', 2, 1, '2012', '07/08/2020', 'Blue', 'H923723878774', '6*2', 'pic124.jpg', 12, 4, 1, 'T 999 637', 0, 'active', 0),
(17, 1, '2', 'KA0794', 1, 1, 'Mercedes', '08/26/2020', 'Voilet', 'QWKER234894268', '6*4', 'license131.jpg', 12, 14, 0, '', 0, 'active', 0),
(18, 1, '26', 'KA9-738', 1, 2, '2000', '08/20/2020', 'White', 'JH923874867364', '6*4', 'pic5.jpg', 12, 10, 0, '', 0, 'active', 0),
(19, 1, '32', 'KJE 123n', 1, 1, 'Actros', '01/01/2009', 'White', 'WDB9332454265475', '6*4', 'clock10.png', 12, 10, 1, 'ZD 2653', 0, 'active', 0),
(20, 1, '40', 'KCS458L', 1, 27, 'MERCEDES BENZ', '06/01/2019', 'WHITE', 'W12368070879', '6*4', 'Scan0015.pdf', 15, 10, 1, 'ZF2343', 0, 'active', 0),
(21, 1, '40', 'KCC943Q', 1, 28, 'MT000001-EL', '10/28/2019', 'WHITE', 'G33338888', '6*4', 'Scan.pdf', 12, 10, 1, 'ZF 5439', 0, 'active', 0),
(22, 0, '40', 'KCV234L', 1, 27, 'MERCEDES BENZ', '12/12/2019', 'MAROON', 'W34678776L', '10*4', 'Scan00151.pdf', 16, 10, 1, 'ZG 1465', 0, 'pending', 0),
(23, 0, '36', 'KCB 933W', 1, 6, 'EXZ51K', '01/08/2015', 'WHITE', 'JALEXZ51KD7000029', '6*4', 'KCB_933_W_LOGBOOK.jpg', 12, 2, 1, '', 0, 'pending', 0),
(24, 0, '40', 'KBU275Y', 1, 25, 'SCANIA', '10/28/2014', 'WHITE', 'W346780580J67', '10*4', 'Scan00153.pdf', 17, 10, 1, 'ZA1980', 0, 'pending', 0),
(25, 1, '36', 'KCG 896A', 1, 6, 'EXZ51K', '12/28/2015', 'WHITE', 'JALEX51KD7000073', '6*4', 'KCG_896A.pdf', 12, 2, 1, 'ZF3388', 0, 'active', 0),
(26, 1, '54', 'KAT 130 B', 1, 18, 'ND425', '10/01/2015', 'White', '123456788', '6*4', 'logo.jpg', 12, 10, 1, 'ZP 5353', 0, 'active', 0),
(27, 1, '54', 'UGX 112T', 3, 1, 'Axor', '10/05/2014', 'White', '1223334455', '6*2', 'clock20.png', 16, 10, 1, 'UZ 1234', 0, 'active', 0),
(28, 1, '2', 'UXP 123T', 3, 3, 'P430', '04/01/2012', 'Red', '1234578466', '6*4', 'clock23.png', 31, 10, 1, 'UZ 1234', 0, 'active', 0),
(29, 1, '36', 'KCV 961H', 1, 5, 'TGS33.400', '06/28/2019', 'ORANGE', 'WMA30WZZ8HM737293', '6*4', 'KCV_961H.jpg', 12, 4, 1, 'ZG 0952', 0, 'active', 0);

-- --------------------------------------------------------

--
-- Table structure for table `truck_documents`
--

CREATE TABLE `truck_documents` (
  `truck_doc_id` int(11) NOT NULL,
  `truck_id` int(11) NOT NULL,
  `truck_front_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_back_photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_insurance_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_insurer` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_ins_expiry_date` date DEFAULT NULL,
  `truck_insurance_doc` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_port_pass` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_portpass_num` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `truck_portpass_expdate` date DEFAULT NULL,
  `truck_portpass_doc` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_truck_doc_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_truck_document` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deletion_indicator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `truck_documents`
--

INSERT INTO `truck_documents` (`truck_doc_id`, `truck_id`, `truck_front_photo`, `truck_back_photo`, `truck_insurance_type`, `truck_insurer`, `truck_ins_expiry_date`, `truck_insurance_doc`, `truck_port_pass`, `truck_portpass_num`, `truck_portpass_expdate`, `truck_portpass_doc`, `other_truck_doc_name`, `other_truck_document`, `deletion_indicator`) VALUES
(1, 1, 'pic114.jpg', 'pic312.jpg', 'GIT', 'New World', '2024-02-02', 'pic1276.jpg', '0', NULL, NULL, NULL, NULL, NULL, 0),
(2, 2, 'pic115.jpg', 'pic414.jpg', 'Third Party Insurance', 'Max', '2024-02-04', 'truck.jpg', '1', 'Port Pass', '2024-02-07', 'pic116.jpg', NULL, NULL, 0),
(11, 14, 'clock3.png', 'clock4.png', 'Carrier’s Liability', 'CFC', '2020-10-07', 'clock5.png', '0', NULL, NULL, NULL, NULL, NULL, 0),
(12, 15, 'truck.jpg', 'truck_img.jpg', 'Comprehensive Insurance', 'Gemini', '2020-08-07', 'pic415.jpg', '0', NULL, NULL, NULL, NULL, NULL, 0),
(13, 16, 'truck2.jpg', 'truck_img2.jpg', 'Fidelity Insurance', 'Gemini', '2020-08-06', 'pic.jpg', '0', NULL, NULL, NULL, NULL, NULL, 0),
(14, 17, 'pic1277.jpg', 'pic1121.jpg', 'Third Party Insurance', 'Assured Insurance', '2020-09-04', 'truck54.jpg', '0', NULL, NULL, NULL, NULL, NULL, 0),
(15, 18, 'pic117.jpg', 'pic126.jpg', 'Fidelity Insurance', 'World Insurance', '2020-09-30', 'truck4.jpg', '0', NULL, NULL, NULL, NULL, NULL, 0),
(16, 19, 'clock11.png', 'clock12.png', 'Comprehensive Insurance', 'UAP', '2020-12-10', 'clock13.png', '0', NULL, NULL, NULL, NULL, NULL, 0),
(17, 20, 'NTSA.jpg', 'Scan0001.jpg', 'Comprehensive Insurance', 'UAP', '2021-04-30', 'NTSA1.jpg', '0', NULL, NULL, NULL, NULL, NULL, 0),
(18, 21, 'NTSA5.jpg', 'NTSA6.jpg', 'Carrier’s Liability', 'ICEALION', '2022-08-04', 'Scan0012.pdf', '0', NULL, NULL, NULL, NULL, NULL, 0),
(19, 22, NULL, NULL, 'Comprehensive Insurance', 'UAP', '2021-03-13', 'Scan0014.pdf', '0', NULL, NULL, NULL, NULL, NULL, 0),
(20, 23, NULL, NULL, '', '', NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, 0),
(21, 24, 'Scan00012.jpg', 'Scan00013.jpg', 'Comprehensive Insurance', 'UAP', '2021-10-02', 'Scan00154.pdf', '0', NULL, NULL, NULL, NULL, NULL, 0),
(22, 25, 'KCG_896A_(1).jpeg', 'KCG_896A_(2).jpeg', 'Comprehensive Insurance', 'CIC GENERAL INSURANCE', '2021-01-27', 'KCG_INSURANCE.pdf', '0', NULL, NULL, NULL, NULL, NULL, 0),
(23, 26, 'IMG-20191107-WA0089.jpg', 'IMG-20191118-WA0036.jpg', 'Comprehensive Insurance', 'UAP', '2021-01-22', 'QUESTIONS.jpg', '1', '11223344', '1970-01-01', 'TRUCK.jpg', NULL, NULL, 0),
(24, 27, 'IMG-20191113-WA0008.jpg', 'truck5.jpg', 'Comprehensive Insurance', 'Resolution', '2021-03-25', 'clock21.png', '0', NULL, NULL, NULL, NULL, NULL, 0),
(25, 28, 'IMG-20191118-WA00362.jpg', 'IMG-20191118-WA00363.jpg', 'Comprehensive Insurance', 'UAP', '2021-06-24', 'clock24.png', '0', NULL, NULL, NULL, NULL, NULL, 0),
(26, 29, 'KCV961H_(1).jpeg', 'ZG0952.jpeg', 'Comprehensive Insurance', 'CIC GENERAL INSURANCE', '2021-06-30', 'B11167458_(1).pdf', '0', NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `truck_insurance`
--

CREATE TABLE `truck_insurance` (
  `insurance_id` int(11) NOT NULL,
  `truck_id` int(11) NOT NULL,
  `insurance_type` varchar(500) DEFAULT NULL,
  `insurer` varchar(500) DEFAULT NULL,
  `expiry_date` date DEFAULT NULL,
  `insurance_doc` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `truck_insurance`
--

INSERT INTO `truck_insurance` (`insurance_id`, `truck_id`, `insurance_type`, `insurer`, `expiry_date`, `insurance_doc`) VALUES
(1, 14, 'Third Party Insurance', 'Gemini', '2020-08-07', 'truck53.jpg'),
(2, 19, 'GIT', 'UAP', '2020-11-04', 'clock14.png'),
(3, 26, 'Carrier’s Liability', 'UAP', '2021-03-12', 'clock19.png');

-- --------------------------------------------------------

--
-- Table structure for table `truck_locations`
--

CREATE TABLE `truck_locations` (
  `location_id` int(11) NOT NULL,
  `truck_id` int(11) NOT NULL,
  `country_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `truck_locations`
--

INSERT INTO `truck_locations` (`location_id`, `truck_id`, `country_code`) VALUES
(1, 17, 1),
(2, 2, 1),
(3, 1, 1),
(4, 15, 2),
(5, 16, 2),
(6, 14, 1),
(7, 18, 1),
(8, 19, 9),
(9, 19, 1),
(10, 19, 2),
(11, 19, 3),
(12, 19, 6),
(13, 20, 9),
(14, 20, 5),
(15, 20, 10),
(16, 20, 2),
(17, 20, 3),
(18, 20, 6),
(19, 21, 18),
(20, 21, 12),
(21, 21, 16),
(22, 21, 14),
(23, 21, 17),
(24, 21, 13),
(25, 22, 9),
(26, 22, 12),
(27, 22, 1),
(28, 22, 5),
(29, 22, 10),
(30, 22, 2),
(31, 22, 3),
(32, 22, 6),
(33, 23, 8),
(34, 23, 9),
(35, 23, 14),
(36, 23, 5),
(37, 23, 2),
(38, 23, 3),
(39, 23, 6),
(40, 24, 9),
(41, 24, 12),
(42, 24, 1),
(43, 24, 2),
(44, 24, 3),
(45, 24, 6),
(46, 25, 8),
(47, 25, 9),
(48, 25, 14),
(49, 25, 5),
(50, 25, 2),
(51, 25, 3),
(52, 25, 6),
(53, 1, 9),
(54, 1, 5),
(55, 1, 3),
(56, 14, 5),
(57, 14, 3),
(58, 26, 8),
(59, 26, 1),
(60, 26, 5),
(61, 26, 10),
(62, 26, 3),
(63, 27, 8),
(64, 27, 1),
(65, 27, 5),
(66, 27, 10),
(67, 27, 3),
(68, 28, 8),
(69, 28, 9),
(70, 28, 1),
(71, 28, 5),
(72, 28, 10),
(73, 28, 2),
(74, 28, 3),
(75, 29, 8),
(76, 29, 9),
(77, 29, 1),
(78, 29, 14),
(79, 29, 5),
(80, 29, 3),
(81, 29, 6);

-- --------------------------------------------------------

--
-- Table structure for table `truck_make`
--

CREATE TABLE `truck_make` (
  `id` int(11) NOT NULL,
  `delete_status` int(11) NOT NULL,
  `truck_make` varchar(500) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `truck_make`
--

INSERT INTO `truck_make` (`id`, `delete_status`, `truck_make`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, 'Mercedes', '2020-07-28 14:51:11', 1, '2020-07-28 14:52:26', 1, '2020-07-28 14:52:55', 1),
(2, 0, 'Tata', '2020-07-29 12:43:35', 1, NULL, NULL, NULL, 0),
(3, 0, 'scania', '2020-09-16 17:19:24', 1, NULL, NULL, NULL, 0),
(4, 0, 'renault', '2020-09-16 17:23:20', 1, NULL, NULL, NULL, 0),
(5, 0, 'man', '2020-09-16 17:29:19', 1, NULL, NULL, NULL, 0),
(6, 0, 'isuzu', '2020-09-16 17:29:30', 1, NULL, NULL, NULL, 0),
(7, 0, 'volvo', '2020-09-16 18:31:48', 1, NULL, NULL, NULL, 0),
(8, 0, 'ud', '2020-09-16 18:32:05', 1, NULL, NULL, NULL, 0),
(9, 0, 'howo', '2020-09-16 18:36:22', 1, NULL, NULL, NULL, 0),
(10, 0, 'sinotruk', '2020-09-16 18:40:22', 1, NULL, NULL, NULL, 0),
(11, 0, 'daf', '2020-09-16 18:41:00', 1, NULL, NULL, NULL, 0),
(12, 0, 'bedford', '2020-09-16 18:41:09', 1, NULL, NULL, NULL, 0),
(13, 0, 'hino', '2020-09-16 18:47:43', 1, NULL, NULL, NULL, 0),
(14, 0, 'tata', '2020-09-16 18:52:06', 1, NULL, NULL, NULL, 0),
(15, 0, 'volkswagen', '2020-09-16 18:52:42', 1, NULL, NULL, NULL, 0),
(16, 0, 'iveco', '2020-09-16 18:53:19', 1, NULL, NULL, NULL, 0),
(17, 0, 'leyland', '2020-09-16 18:53:43', 1, NULL, NULL, NULL, 0),
(18, 0, 'beiben', '2020-09-16 18:54:09', 1, NULL, NULL, NULL, 0),
(19, 0, 'ashok leyland', '2020-09-16 18:54:42', 1, NULL, NULL, NULL, 0),
(20, 0, 'eicher', '2020-09-16 18:55:13', 1, NULL, NULL, NULL, 0),
(21, 0, 'faw', '2020-09-16 18:55:27', 1, NULL, NULL, NULL, 0),
(22, 0, 'mitsubishi', '2020-09-16 18:55:52', 1, NULL, NULL, NULL, 0),
(23, 0, 'komatsu', '2020-09-16 18:56:07', 1, NULL, NULL, NULL, 0),
(24, 0, 'sitrak', '2020-09-16 18:56:21', 1, NULL, NULL, NULL, 0),
(25, 0, 'shacman', '2020-09-16 18:56:35', 1, NULL, NULL, NULL, 0),
(26, 0, 'xcmg', '2020-09-16 18:56:59', 1, NULL, NULL, NULL, 0),
(27, 0, 'tata', '2020-09-16 18:57:16', 1, NULL, NULL, NULL, 0),
(28, 0, 'ford', '2020-09-16 18:57:38', 1, NULL, NULL, NULL, 0),
(29, 0, 'freightliner', '2020-09-16 18:57:52', 1, NULL, NULL, NULL, 0),
(30, 0, 'mack', '2020-09-16 18:58:09', 1, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL COMMENT '1=>super admin,    2=>transporter,  3=>cargo_user, 4=>cfs, 5=>cf_agent, 6=>cf_transporter, 7=>consolidators, 8=>independent_agent, 9=>ports, 10=>shipping_lines, 11=>driver,  12=> sub admin,  13=> office management',
  `approval_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `delete_status` int(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `user_type`, `approval_status`, `created_at`, `updated_at`, `delete_status`) VALUES
(1, 'admin@admin.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 'active', '2020-04-20 05:01:33', '2020-06-10 03:53:55', 0),
(2, 'femcokenya', '5f4dcc3b5aa765d61d8327deb882cf99', 2, 'active', '2020-04-20 07:21:25', '2020-04-20 09:07:44', 0),
(3, 'shabibyltd', '5f4dcc3b5aa765d61d8327deb882cf99', 2, 'active', '2020-04-20 07:37:43', '2020-04-20 09:19:22', 0),
(4, 'sofiagetambu', '5f4dcc3b5aa765d61d8327deb882cf99', 3, 'active', '2020-04-20 08:16:54', '2020-04-20 08:54:03', 0),
(5, '0798434774', '8870b9910d28bb3140e32325be7bc4ee', 11, 'active', '2020-04-20 09:34:57', '2020-05-12 12:43:38', 0),
(6, '0784834684', '467dee70ce2a7e2c46091d11f28f8e72', 11, 'active', '2020-04-20 10:45:13', '2020-05-12 12:43:42', 0),
(28, 'subadmin@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 12, 'active', '2020-07-13 12:50:13', '2020-07-24 14:33:35', 0),
(29, 'office@mail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 13, 'active', '2020-07-13 13:22:23', '2020-07-13 07:52:23', 0),
(30, 'atlantic', '5f4dcc3b5aa765d61d8327deb882cf99', 2, 'active', '2020-07-24 14:13:16', '2020-10-22 11:59:44', 0),
(31, 'hyperzone', '5f4dcc3b5aa765d61d8327deb882cf99', 2, 'active', '2020-07-24 14:15:35', '2020-07-24 15:00:39', 0),
(32, 'evergreen', '5f4dcc3b5aa765d61d8327deb882cf99', 3, 'active', '2020-07-24 14:18:42', '2020-07-24 14:59:45', 0),
(33, 'tanzania', '5f4dcc3b5aa765d61d8327deb882cf99', 12, 'active', '2020-07-24 14:37:54', '2020-07-24 14:40:52', 0),
(34, '0949384452', '99c3db6c52562f4263f1df54de940d73', 11, 'active', '2020-07-24 15:17:08', '2020-07-24 15:40:54', 0),
(35, '0984183814', '17fec373bcc13240ad41a636dc764528', 11, 'active', '2020-07-24 15:24:51', '2020-07-24 15:40:59', 0),
(36, 'hyper@mail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 2, 'active', '2020-08-28 10:26:35', '2020-08-28 10:36:28', 0),
(37, '8073429158', '699780327fbbdec1c2a221061394d10a', 11, 'active', '2020-08-28 10:39:11', '2020-08-28 10:41:08', 0),
(38, 'ugandalimited', '5f4dcc3b5aa765d61d8327deb882cf99', 12, 'active', '2020-08-28 11:06:51', '2020-08-28 11:06:51', 0),
(41, 'offfice2@admin.com', '5f4dcc3b5aa765d61d8327deb882cf99', 13, 'active', '2020-09-04 23:50:47', '2020-09-04 18:20:47', 0),
(42, 'samson', '2242a97ea96f6a6d4c7d67c4ff194fd0', 2, 'active', '2020-09-09 07:32:16', '2020-09-09 07:44:15', 0),
(43, '0729713222', 'f8998eeec837d5432c31704a150c7934', 11, 'active', '2020-09-09 07:50:02', '2020-09-09 08:04:46', 0),
(44, 'milli', '7fc3da7e128f1aad65c2a51b69f7be90', 5, 'pending', '2020-09-09 08:05:55', '2020-09-09 08:05:55', 0),
(45, 'sofialtd', '80aa0b79c05f03239d602c4138b5831a', 2, 'pending', '2020-09-11 07:54:24', '2020-09-11 07:54:24', 0),
(46, 'headoffice', 'b9531b4f130c39bbff268010e61d5f23', 13, 'active', '2020-09-11 08:02:00', '2020-09-11 08:02:00', 0),
(47, 'Roadmasters', '9a22e697c6d3f61523a091044683db59', 2, 'active', '2020-09-16 11:30:40', '2020-09-16 11:57:06', 0),
(48, 'TZH', '8f7c1f4fc7b3b7e443b69da6dde38c60', 2, 'rejected', '2020-09-16 12:15:49', '2020-10-21 03:18:10', 0),
(49, 'jomologistics', '68baf7138db6f9b30e43aaf29871879b', 6, 'active', '2020-09-17 14:42:59', '2020-09-18 07:54:19', 0),
(50, 'millicent', 'a340ba673ff5bb6c119c7d022348d270', 5, 'active', '2020-09-18 07:20:22', '2020-09-18 07:50:28', 0),
(52, 'maxwell', '856f0142e3416862b8585c9cdf2381cf', 2, 'active', '2020-09-18 07:26:21', '2020-09-18 08:45:55', 0),
(53, 'Murage', 'a805bb5f014923669ae083bddf03e9b7', 2, 'active', '2020-09-18 08:33:46', '2020-09-21 07:25:35', 0),
(54, 'wanja', '0cef6b042f77b31a820d535730dc25f4', 8, 'pending', '2020-09-18 08:56:58', '2020-10-26 11:45:44', 0),
(55, 'linkfreight', 'f12aeeebbf2191cce858f5897f2a261e', 2, 'active', '2020-09-18 09:31:11', '2020-10-20 06:29:07', 0),
(57, '792532005', '75887b4bc55a49c7951e47fe8f8ad627', 11, 'active', '2020-09-18 11:22:22', '2020-09-18 11:43:01', 0),
(58, 'boxlot', '796093028b524c31d19f58adb6259d56', 2, 'pending', '2020-09-21 07:30:21', '2020-09-21 07:30:21', 0),
(59, 'jojo235', 'dead1cf0e7e01dd47edd712d5ec07b9e', 2, 'pending', '2020-09-23 03:22:46', '2020-09-23 03:22:46', 0),
(60, 'testing123', '7f2ababa423061c509f4923dd04b6cf1', 2, 'pending', '2020-09-28 14:09:10', '2020-09-28 14:09:10', 0),
(61, 'EAOTA', '8f7c1f4fc7b3b7e443b69da6dde38c60', 5, 'active', '2020-09-28 14:29:09', '2020-10-08 13:11:52', 0),
(62, 'tussockridge', '0a2709b347df4bdd5b67ac8870fe40c6', 2, 'pending', '2020-10-06 08:33:22', '2020-10-06 08:33:22', 0),
(63, 'Uganda Impex', '5f4dcc3b5aa765d61d8327deb882cf99', 5, 'active', '2020-10-12 09:40:03', '2020-10-12 09:47:36', 0),
(64, 'Edam', '15f1bb576011e9b41a9423dbb83042b0', 2, 'pending', '2020-10-14 07:15:05', '2020-10-14 07:15:05', 0),
(65, 'boxlotkenya', 'cacf0bc788ea180714ddbe764bac42e5', 5, 'active', '2020-10-21 02:34:17', '2020-10-21 02:57:55', 0),
(66, 'WanjaTransport', '1a78066dfea6a49073527a02da8d6a37', 2, 'active', '2020-10-21 03:01:48', '2020-10-21 03:17:20', 0),
(67, '0726816559', '57e59f7af30a5e1f8f0e9635f1136bb5', 11, 'active', '2020-10-21 03:33:11', '2020-10-21 03:54:39', 0),
(68, '0799000000', '74248849d41aa215a07b9807c5fe4d8d', 11, 'active', '2020-10-21 03:45:26', '2020-10-21 03:50:27', 0),
(69, '00223344', '2d0b4e5040e10f48a903cb91b65b9e97', 11, 'pending', '2020-10-21 05:06:35', '2020-10-21 05:06:35', 0),
(70, '0712692076', 'd9dfb5631e7139a33a33ae7f7402afe5', 11, 'pending', '2020-10-27 12:43:04', '2020-10-27 12:43:04', 0),
(71, '0727119187', '2a3172de3c6376422328433e9cc4c6a2', 11, 'pending', '2020-10-27 13:01:58', '2020-10-27 13:01:58', 0),
(72, '0726806823', '12fff111c448eea94ea56a68a478a3bb', 11, 'pending', '2020-10-27 13:13:45', '2020-10-27 13:13:45', 0),
(73, 'sam', '856f0142e3416862b8585c9cdf2381cf', 2, 'pending', '2020-11-05 10:43:41', '2020-11-05 10:43:41', 0),
(74, 'MH TRANSPORT', '27f59b3bba54c0eba4c6622d7757c94b', 2, 'pending', '2020-11-06 12:29:47', '2020-11-06 12:29:47', 0),
(75, 'Wanja7', '0cdce23a02779c2655d62649de1ae2de', 2, 'active', '2020-11-10 09:14:50', '2020-11-10 09:58:27', 0),
(76, 'muthengera', 'da8abc988248d3aa6fb9027178399127', 2, 'pending', '2020-11-10 11:51:41', '2020-11-10 11:51:41', 0),
(77, 'Eline', 'd17a07490f865addad1602c4e5675711', 2, 'pending', '2020-11-17 09:13:26', '2020-11-17 09:13:26', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_bank_details`
--

CREATE TABLE `users_bank_details` (
  `bank_id` int(20) NOT NULL,
  `users_id` int(20) NOT NULL,
  `country_code` int(20) NOT NULL,
  `bank_code` int(30) NOT NULL,
  `branch_code` int(30) NOT NULL,
  `ac_holder_name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ac_number` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_letter` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_currency` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_bank_details`
--

INSERT INTO `users_bank_details` (`bank_id`, `users_id`, `country_code`, `bank_code`, `branch_code`, `ac_holder_name`, `ac_number`, `bank_letter`, `bank_currency`) VALUES
(1, 4, 1, 1, 2, 'Sofia', '87387443784', 'pic21.jpg', 'Ksh'),
(2, 2, 3, 11, 19, 'Femco Uganda', '2348728347', 'System_Description-Issues3.docx', 'USHS'),
(3, 3, 1, 3, 8, 'SADDAM SHABIBY', '8734823747632', 'pic24.jpg', 'Ksh'),
(11, 32, 2, 1, 1, 'Darweshi', 'SMI823478', 'pic111.jpg', 'Ksh'),
(12, 30, 2, 13, 20, 'Atlantic Transporters Ltd', '8923974658', 'pic122.jpg', 'Tshs'),
(13, 31, 2, 1, 2, 'Baraka', 'NSDJ97283', 'pic46.jpg', 'Ksh'),
(14, 36, 1, 2, 4, 'Jacob', '8767826237', 'truck1.jpg', 'Ksh'),
(15, 42, 1, 2, 4, 'Samson Logistics Ltd', '123072495692', 'clock10.png', 'Ksh'),
(16, 47, 1, 3, 13, 'ROADMASTERS LTD', '1250267639667', 'Bank_details_for_Roadmasters_Limited1.docx', 'Ksh'),
(17, 48, 2, 5, 14, 'Wanjatz', '1234567', 'clock14.png', 'Ksh'),
(18, 49, 1, 7, 16, 'Jomo Logistics International Limited', '0100003428677', 'Stanbic_Letter_from_the_bank_.pdf', 'Ksh'),
(19, 50, 1, 2, 4, 'Millicent Owino', '1182831728', 'Scan0001_(2)5.jpg', 'Ksh'),
(20, 52, 1, 3, 9, 'samson', '1274861381', 'Scan00151.pdf', 'Ksh'),
(21, 55, 1, 3, 13, 'LINK FREIGHT LOGISTICS LIMITED', '0032258001', 'DTB.pdf', 'Ksh'),
(22, 53, 1, 2, 4, 'Diamu Enterprises', '1112355081', 'IMG_20200918_195058.jpg', 'Ksh'),
(25, 62, 1, 3, 13, 'TUSSOCK RIDGE FARM', '1310267067611', 'EAOTA_Bank_Authorisation_Letter.pdf', 'KSH'),
(26, 28, 1, 1, 1, 'Akash', '9982374348', 'pic21.jpg', 'Ksh'),
(27, 29, 1, 1, 1, 'Vinay', '92837834', 'pic32.jpg', 'Ksh'),
(28, 33, 2, 13, 20, 'Tanzania Logistics Ltd', '9238749834', 'pic32.jpg', 'Tshs'),
(29, 38, 3, 11, 19, 'Uganda Limited', '12345678', 'pic32.jpg', 'USD'),
(30, 41, 1, 1, 1, 'Maria', '923874983', 'pic32.jpg', 'Ksh'),
(31, 46, 1, 1, 1, 'Mani', '923879833', 'pic32.jpg', 'Ksh'),
(32, 1, 1, 2, 18, 'East African Online Transport Agency Ltd', '1154705498', 'EAOTA_BANK_DETAILS.pdf', 'Ksh'),
(33, 1, 1, 2, 18, 'East African Online Transport Agency Ltd', '1154705633', 'EAOTA_BANK_DETAILS1.pdf', 'USD'),
(34, 38, 3, 11, 19, 'Uganda Limited', '5678910', 'clock.png', 'USHS'),
(35, 63, 3, 12, 19, 'Uganda Import Company Limited', '1234567', 'EAOTA_BANK_DETAILS2.pdf', 'USD'),
(36, 63, 3, 12, 19, 'Uganda Import Company Limited', '5556666777', 'EAOTA_BANK_DETAILS3.pdf', 'USHS'),
(37, 2, 3, 11, 19, 'Femco Uganda', '1234509', 'EAOTA_BANK_DETAILS4.pdf', 'USD'),
(38, 3, 1, 3, 8, 'SHABIBY TRANSPORT LIMITED', '339900229345', 'clock15.png', 'USD'),
(39, 33, 2, 13, 20, 'Tanzania Logistics Ltd', '12349876', 'EAOTA_BANK_DETAILS2.pdf', 'USD'),
(40, 30, 2, 13, 20, 'Atlantic Transporters Ltd', '1298380138', 'clock16.png', 'USD'),
(41, 65, 1, 14, 21, 'Box Lot Kenya Limited', '1000366486', 'EAOTA_BANK_DETAILS5.pdf', 'Kshs'),
(42, 65, 1, 15, 21, 'Box Lot Kenya Limited', '1002125998', 'EAOTA_BANK_DETAILS6.pdf', 'USD'),
(43, 66, 3, 16, 22, 'Wanja Transporters Ltd', '111222333', 'EAOTA_BANK_DETAILS7.pdf', 'Ushs'),
(44, 66, 3, 11, 19, 'Wanja Transporters Ltd', '555666777', 'EAOTA_BANK_DETAILS8.pdf', 'USD'),
(45, 66, 1, 2, 5, 'Wanja Transporters Ltd', '000999444', 'EAOTA_BANK_DETAILS9.pdf', 'Kshs'),
(46, 61, 1, 2, 18, 'Wanja7 Ltd', '1234567', 'EAOTA_BANK_DETAILS10.pdf', 'USD'),
(47, 75, 1, 2, 18, 'Wanja 7 Ltd', '12345678', 'EAOTA_BANK_DETAILS11.pdf', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `users_commission`
--

CREATE TABLE `users_commission` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_commission`
--

INSERT INTO `users_commission` (`id`, `users_id`, `commission`) VALUES
(1, 27, 10),
(2, 28, 20),
(3, 33, 10),
(4, 38, 40);

-- --------------------------------------------------------

--
-- Table structure for table `users_company_details`
--

CREATE TABLE `users_company_details` (
  `company_code` int(11) NOT NULL,
  `delete_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `users_id` int(11) NOT NULL,
  `company_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_address` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plot_no` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `town` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` int(11) NOT NULL,
  `city_code` int(11) NOT NULL,
  `pincode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_prefix` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` bigint(12) NOT NULL,
  `landline_prefix` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landline_number` bigint(12) NOT NULL,
  `email_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_company_details`
--

INSERT INTO `users_company_details` (`company_code`, `delete_status`, `users_id`, `company_type`, `company_name`, `postal_address`, `plot_no`, `street`, `town`, `country_code`, `city_code`, `pincode`, `mobile_prefix`, `mobile_number`, `landline_prefix`, `landline_number`, `email_id`) VALUES
(1, '0', 2, 'Corporate', 'FEMCO INVESTMENTS', 'P.O.BOX 12112- 00200', 'Uganda Fourth', 'Lugogo', 'Kampala', 3, 34, '12112-00200', '256', 722800504, '256', 722800504, 'femco@uganda.ug'),
(2, '0', 3, 'Individual', 'SHABIBY TRANSPORT SERVICES', 'MWINYI MPATE ROAD', 'P.O.BOX 2658 - 80100', '3rd Main Road', 'Mombasa', 1, 1, '80100', '254', 720889564, '254', 202038740, 'wgetambu@gmail.com'),
(3, '0', 4, 'Individual', 'Sofia Business Solutions', 'P.O. Box 34116 - 80100, Mombasa', 'Ali Manson Building 3rd Floor, Moi Avenue, Mombasa', '11th Street', 'mombasa', 1, 1, '067398042', '254', 97208518961, '254', 412316559, 'sofiagetambu@yahoo.com'),
(20, '0', 28, 'individual', 'EAOTA Branch', 'Gayatrinagar', 'Avenue Road', '3rd street', 'mombasa', 1, 1, '067398042', '254', 983498346, '254', 734738474, 'sub@gmail.com'),
(21, '0', 29, 'individual', 'EAOTA Branch', 'Jalahalli , Mathikere', '34 avenue road', '4th Street', 'bangalore', 1, 1, '560021', '56', 98872983783, '234', 92378882, 'office@mail.com'),
(22, '0', 30, 'Individual', 'Atlantic', 'Po Box 33790', 'Plot 2384/75', 'Nyerere Road', 'arusha', 2, 64, '84743', '254', 938487443, '254', 837434664, 'vikramtomail@gmail.com'),
(23, '0', 31, 'Individual', 'Hyper Zone', 'Po Box 33790', 'Plot 582/9', 'Mkwepu Junction', 'mwanza', 2, 65, '76464', '254', 983475456, '254', 934834774, 'hyperzone@mail.com'),
(24, '0', 32, 'Individual', 'Evergreen', 'P.O.BOX 12425- 054384', 'Plot 582/9 Ap. 201', 'Mkwepu Junction', 'Dar Es Salaam', 2, 31, '74387', '255', 8478347827, '254', 222128424, 'evergreen@mail.com'),
(25, '0', 33, 'individual', 'EAOTA Branch', 'P.O. Box 24785 - 80100', 'Plot 582/9 Ap. 201', 'Rwanda Street', 'Changombe', 2, 1, '067398042', '255', 9824787342, '255', 756894082, 'partner@mail.com'),
(26, '0', 36, 'Individual', 'Hyper Transporter', 'P.O. Box 34116 - 80100, Mombasa', '465 4th floor', '5th street', 'Mombasa', 1, 2, '87464', '254', 9742955117, '254', 7847637654, 'hyper@mail.com'),
(27, '0', 38, 'individual', 'EAOTA Branch', 'P.O 234', '345 5th cross', '4th Street', 'Kampala', 3, 34, '782387', '256', 9742955224, '256', 8237623636, 'ugandalimited@mail.com'),
(28, '0', 39, 'individual', 'EAOTA Branch', 'gayatrinagar', 'Test', '4th Street', 'bangalore', 1, 1, '560021', '254', 9843798374, '245', 9734837474, 'b.khathi@gmail.com'),
(29, '0', 41, 'individual', 'EAOTA Branch', 'gayatrinagar', 'Test', '4th Street', 'bangalore', 1, 1, '560021', '254', 9843798374, '245', 9734837474, 'sub@mail.com'),
(32, '0', 42, 'Individual', 'Samson Logistics', '90237', 'New Rehema', 'Rhapta', 'Nairobi', 1, 2, 'a293408645', '254', 757129031, '254', 757129031, 'samson@samson.tok'),
(33, '0', 44, '', 'Millicent Cargo', '', '', '', '', 0, 0, '', '254', 792532005, '254', 792532005, 'milli@milli.ken'),
(34, '0', 45, '', 'Sofia Ltd', '', '', '', '', 0, 0, '', '254', 777851896, '254', 720851895, 'sofia@sofrjaeb.hrg'),
(35, '0', 46, 'individual', 'EAOTA Branch', '90237', 'new rehema', 'rhapta', 'nairobi', 1, 2, '12345677898', '254', 722900351, '254', 722900351, 'officemgtkenya@eaota.eaota'),
(36, '0', 47, 'Corporate', 'ROADMASTERS LTD', '491', '115', 'KERARAPON DRIVE', 'NGONG', 1, 18, 'P051449260M', '254', 719645711, '254', 719645711, 'roadmastersltd@gmail.com'),
(37, '0', 48, 'Corporate', 'TZ Hauliers', '983485', '1234', 'Dar road', 'Dar', 2, 31, '1555555', '255', 12384653846, '255', 1398742484, 'tz@haulier.tz'),
(38, '0', 49, 'Corporate', 'Jomo Logistics International Limited', '50855', 'View Park Towers/209/8595', '50855', 'Nairobi', 1, 1, 'P051458765V', '254', 716856925, '254', 716856925, 'jomologistics@gmail.com'),
(39, '0', 50, 'Corporate', 'Millicent Ltd', '4059', 'lr4567', 'Kirinyaga', 'nairobi', 1, 3, 'p0516731699i', '+254', 704490491, '+254', 704490491, 'millieowino@gmail.com'),
(40, '0', 52, 'Individual', 'sam logistics', '2030', 'Lr1235', 'Thika', 'nairobi central business district cbd', 1, 3, 'p0317169981z', '+254', 757129031, '+254', 757129031, 'samsonomondi42@gmail.com'),
(41, '0', 53, 'Corporate', 'Diamu enterprises', '7839', 'Mlolongo', 'Mlolongo', 'Nairobi', 1, 2, 'P051190509M', '0726884566', 726884566, '0721916118 ', 721916118, 'muragep@diamuenterprises.com'),
(42, '0', 54, '', 'Wanja Getambu', '', '', '', '', 0, 0, '', '254', 777900351, '254', 777900351, 'wgetambu@gmail.com'),
(43, '0', 55, 'Corporate', 'LINK FREIGHT LOGISTICS LIMITED', 'P.O. BOX 50312 - 00100', '7747/1', 'LANGATA ROAD', 'nairobi', 1, 2, 'P051412546W', '+254', 722338841, '+254', 721121287, 'Info@linkfreightltd.co.ke'),
(45, '0', 58, '', 'Box Lot Ltd', '', '', '', '', 0, 0, '', '254', 777900351, '254', 777900351, 'boxlotkenya@gmail.com'),
(46, '0', 59, '', 'jojolone200', '', '', '', '', 0, 0, '', '+254', 767551083, '065', 32325, 'jojolone200@gmail.com'),
(47, '0', 60, '', 'Testing123', '', '', '', '', 0, 0, '', '256', 444555666, '256', 111222333, 'test@123wanja.com'),
(48, '0', 61, 'Individual', 'Wanja Seven Ltd', '90237', '123', 'Rhapta', 'Nairobi', 1, 2, '1234567', '254', 722900351, '254', 722900351, 'wgetambu@gmail.com'),
(49, '0', 62, 'Corporate', 'TUSSOCK RIDGE', 'P.O. Box 177 - 20103 Eldama Ravine', '66 NANDI ROAD, KAREN', 'NANDI ROAD', 'NAIROBI', 1, 3, 'A002681973A', '+254', 722729315, '+254', 765976801, 'info@tussockridge.com'),
(50, '0', 1, 'Corporate', 'East African Online Transport Agency Ltd', '90237', '5th Floor New Rehema House', 'Rhapta Road Westlands', 'Nairobi', 1, 2, 'P051235351Z', '254', 729713222, '254', 7044940491, 'info@eaotransport.com'),
(51, '0', 63, 'Corporate', 'Uganda Import Company', '12345', 'Lugogo Towers', 'Lugogo', 'Kampala', 3, 34, '12345678', '256', 12340000, '256', 12340000, 'ugimpex@uganda.ug'),
(52, '0', 64, '', 'Edam Enterprises Limited', '', '', '', '', 0, 0, '', '0722', 995396, '0722', 995396, 'edam.ltd@gmail.com'),
(53, '0', 65, 'Corporate', 'Boxlot Kenya Ltd', '90237', '234 Rhapta Place', 'Rhapta Road', 'Nairobi', 1, 2, 'A112222222', '254', 777900351, '254', 777900351, 'boxlotkenya@gmail.com'),
(54, '0', 66, 'Corporate', 'Wanja Transporters Ltd', '111', '111', 'Mbarara Road', 'Kampala', 3, 34, '1111111111', '256', 722900351, '256', 722900351, 'wgetambu@gmail.com'),
(55, '0', 73, '', 'sam transporters', '', '', '', '', 0, 0, '', '+254', 757129031, '+254', 734701017, 'samsonomondi42@gmail.com'),
(56, '0', 74, '', 'MH TRANSPORT CO LIMITED', '', '', '', '', 0, 0, '', '+254', 785899899, '00', 0, 'mhtransportcompanylimited@gmail.com'),
(57, '0', 75, 'Individual', 'Wanja Seven Ltd', '90237', '123', 'Rhapte', 'Nairobi', 1, 2, '123456', '254', 722900351, '254', 722900351, 'wgetambu@gmail.com'),
(58, '0', 76, '', 'MUTHENGERA TRANSPORTERS LTD', '', '', '', '', 0, 0, '', '+254', 721848216, '+254', 721848216, 'muthengeratransltd@gmail.com'),
(59, '0', 77, '', 'ELINE CARGO', '', '', '', '', 0, 0, '', '+254', 722378242, '+254', 722378242, 'elinecargo@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users_contact_details`
--

CREATE TABLE `users_contact_details` (
  `contact_id` int(11) NOT NULL,
  `users_id` bigint(50) NOT NULL,
  `contact_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_prefix` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_id` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='transporter_contact_details';

--
-- Dumping data for table `users_contact_details`
--

INSERT INTO `users_contact_details` (`contact_id`, `users_id`, `contact_name`, `designation`, `country_code`, `phone_prefix`, `phone_no`, `email_id`) VALUES
(1, 4, 'Sofia,wanja', 'Director,Director', '1,1', '254,254', '0720851896,9742955126', 'john@eao-transport.com,wanja@gmail.com'),
(2, 2, 'LENARD ASENA,Pampel', 'Director,Director', '3,3', '256,256', '0202195336,8875873475', 'femcokenya@yahoo.com,pampel@mail.com'),
(3, 3, 'SADDAM SHABIBY', 'Director', '1', '254', '726899986', 'wgetambu@gmail.com'),
(11, 32, 'Akida', 'Director', '2', '254', '9872346473', 'akida@mail.com'),
(12, 30, 'Adil', 'Director', '2', '255', '9823378686', 'adil@mail.com'),
(13, 31, 'Aailyah', 'Director', '2', '255', '7749040405', 'Aailyah@mail.com'),
(14, 36, 'Jay', 'Director', '1', '254', '9742955114', 'jay@mail.com'),
(15, 42, 'Millicent', 'Operations', '1', '254', '070449041', 'admin@millicent.tok'),
(16, 47, 'DANIEL MUTHANJI', 'DIRECTOR', '1', '254', '722764371', 'danmuthanji@gmail.com'),
(17, 48, 'Wanjatz', 'Director', '2', '255', '234865097', 'tz@haulier.tz'),
(18, 49, 'Lawrence Njiru', 'Director', '1', '254', '716856925', 'jomologistics@gmail.com'),
(19, 50, 'Millicent Owino', 'Director', '1', '+254', '704490491', 'millieowino@gmail.com'),
(20, 52, 'samson', 'director', '1', '+254', '757129031', 'samsonomondi42@gmail.com'),
(21, 55, 'LUKE NJAGI', 'TRANSPORT MANAGER', '1', '+254', '722918451', 'Info@linkfreightltd.co.ke'),
(22, 53, 'Peris Murage', 'Marketing Executive', '1', 'Ms', '0726884566', 'muragep@diamuenterprises.com'),
(23, 61, 'Wanja Getambu', 'Director', '1', '254', '0722900351', 'wgetambu@gmail.com'),
(24, 62, 'GERALD CHERUIYOT,MARYANNE CHERUIYOT', 'CEO / DIRECTOR,DIRECTOR', '1,1', '+254,+254', '722729315,722587093', 'gerald.cheruiyot@tussockridge.com,maryanne.cheruiyot@tussockridge.com'),
(25, 63, 'Tero Luganda', 'Operations', '3', '256', '123456789', 'tero@luganda.ug'),
(26, 65, 'Noni Kiragu', 'Operations Management ', '1', '254', '0000900351', 'youboxafrica@gmail.com'),
(27, 66, 'Andrew Kiragu,Noni Muthoni', 'Operations ,Operations', '1,3', '254,256', '0722900351,0722900351', 'andrewkiragungunya@eaomail.com,nonimuthonikiragu@eaomail.com'),
(28, 75, 'Wanja Getambu', 'Director ', '1', '254', '0722900351', 'wgetambu@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users_corporate_director`
--

CREATE TABLE `users_corporate_director` (
  `director_id` int(10) NOT NULL,
  `users_id` int(10) NOT NULL,
  `dir_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_prefix` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_no` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification_type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `identification_no` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passport_no` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_issued` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiry_date` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identification_copy` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletion_status` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_corporate_director`
--

INSERT INTO `users_corporate_director` (`director_id`, `users_id`, `dir_name`, `mobile_prefix`, `mobile_no`, `email_id`, `identification_type`, `identification_no`, `passport_no`, `country_issued`, `expiry_date`, `identification_copy`, `deletion_status`) VALUES
(1, 2, 'Lenard,subash', '256,256', '9742955117,87238776', 'lenard@mail.com,subash@mail.com', 'Passport No,National ID', 'LDKSDJ2323,JH983923', 'P24,', 'Uganda,Uganda', '09/11/2020,', 'ID_-_WANJA2.docx,ID_-_WANJA3.docx', 0),
(2, 23, 'Big', '254', '9823982389', 'lkajsd@gmail.com', 'National ID', 'UQUE727', NULL, 'Kenya', NULL, '', 0),
(3, 47, 'DANIEL MUTHANJI', '254', '722764371', 'danmuthanji@gmail.com', 'National ID', '5358591', '', 'KENYA', '', '', 0),
(4, 48, 'Wanjatz', '255', '23486584097', 'tz@haulier.tz', 'National ID', '12345678', '', 'Kenya', '', 'truck4.jpeg', 0),
(5, 49, 'Lawrence Njiru', '0716856925', '716856925', 'jomologistics@gmail.com', 'National ID', '14676666', '', 'Kenya', '', '', 0),
(6, 50, 'Millicent Owino', '+254', '704490491', 'millieowino@gmail.com', 'National ID', '29291287', '', 'Kenya', '', '', 0),
(7, 53, 'Mbugua Njiraini', 'Mr', '0721916118', 'info@diamuenterprises.com', 'National ID', '11369796', '', 'Kenya', '', '', 0),
(8, 61, 'Wanja Getambu,Sofia Getambu,Esther Getambu', '254,254,254', '0722900351,0720851896,0726816559', 'wanja.kiragu@eaotransport.com,sofia.getambu@eaotransport.com,esthergetambu@gmail.com', 'National ID,National ID,National ID', '28754314,26129005,1681083', ',,', 'Kenya,Kenya,Kenya', ',,', 'ID_-_WANJA.docx,Sofia_Getambu_-_ID.pdf,ID_-_ESTHER_GETAMBU.docx', 1),
(9, 62, 'Gerald Cheruiyot,MARYANNE CHERUIYOT', '+254,+254', '722729315,722587093', 'gerald.cheruiyot@tussockridge.com,maryanne.cheruiyot@tussockridge.com', 'National ID,National ID', '13426731,20921831', ',', 'Kenya,Kenya', ',', 'Gerald_ID_Compressed.pdf,maryanne-kabue-cheruiyot-id.pdf', 0),
(10, 55, 'RASHID GACHOKI,RAHIMA GACHOKI', '254,254', '722338841,722760387', 'rashid@linkfreightltd.co.ke,info@linkfreightltd.co.ke', 'National ID,National ID', '20137016,23061370', ',', 'KENYA,KENYA', ',', 'Rashid_ID1.pdf,Rahima_ID.pdf', 0),
(11, 63, 'Sebo Sebowanga', '256', '1234556', 'sebo@sebowanga.ug', 'National ID', '1234567', '', 'Uganda', '', 'ID_-_WANJA1.docx', 0),
(12, 65, 'Wanja Kiragu', '254 ', '777900351', 'boxlotkenya@gmail.com', 'National ID', '28754314', '', 'Kenya', '', 'ID_-_WANJA4.docx', 0),
(13, 66, 'Wanja Getambu Kiragu', '256', '722900351', 'wgetambu@gmail.com', 'National ID', '28754314', '', 'Uganda', '', 'QUESTIONS.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_corporate_document`
--

CREATE TABLE `users_corporate_document` (
  `corporate_doc_id` int(20) NOT NULL,
  `users_id` int(20) NOT NULL,
  `certificate` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_id` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_profile` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proof_director` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `association_name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `membership_certificate` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `corp_kta_receipt` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `corp_kifwa_receipt` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deletion_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_corporate_document`
--

INSERT INTO `users_corporate_document` (`corporate_doc_id`, `users_id`, `certificate`, `tax_id`, `company_profile`, `proof_director`, `association_name`, `membership_certificate`, `corp_kta_receipt`, `corp_kifwa_receipt`, `deletion_status`) VALUES
(1, 2, 'pic6.jpg', 'pic48.jpg', 'pic113.jpg', 'pic114.jpg', 'Gayatri', 'pic123.jpg', 'pic7.jpg', NULL, 0),
(2, 47, 'Roadmasters_Ltd_Incorporation_Certificate.jpg', 'ROADMASTERS_LTD_PIN.pdf', 'ROADMASTERS_LTD_PROFILE.pdf', 'ROADMASTERS_LTD_PROFILE1.pdf', 'ROADMASTERS LTD', 'ROADMASTERS_LTD_CR12.pdf', 'ROADMASTERS_LTD_CR121.pdf', NULL, 0),
(3, 48, 'clock11.png', 'logo.jpg', 'clock12.png', 'clock13.png', 'Non', 'africa_2.jpg', 'africa7.jpg', NULL, 0),
(4, 49, 'JOMO_CERT_OF_INCORP.pdf', 'JOMO_PIN.pdf', '2020_Jomo_Logistics_Official_Profile.pdf', '2020_Jomo_Logistics_Official_Profile1.pdf', 'KIFWA', 'KIFWA_LICENCE_(1).pdf', 'KIFWA_LICENCE_(1)1.pdf', NULL, 0),
(5, 50, 'Scan0001_(2).jpg', 'Scan0001_(2)1.jpg', 'Scan0001_(2)2.jpg', 'Scan0001_(2)3.jpg', 'KIFWA', 'Scan0001_(2)4.jpg', NULL, NULL, 0),
(6, 53, 'COI.pdf', 'PIN_DIAMU.pdf', 'diamu_co__profile.pdf', 'diamu_co__profile1.pdf', '', 'COI1.pdf', 'COI2.pdf', NULL, 0),
(7, 61, 'Legal_docs-EAO_certificate_of_Inc.jpg', 'EAOTA_ITAX_PIN_P051235351Z.pdf', 'EAOTA_COMPANY_PROFILE.pdf', 'CR_12___EAOTA_(2).pdf', 'KIFWA', 'KIFWA_CERTIFICATE.pdf', NULL, NULL, 1),
(8, 62, 'BUSINESS_REGISTRATION.pdf', 'Gerald_KRA_PIN.pdf', 'maryanne-kabue-cheruiyot-pin.pdf', 'BUSINESS_REGISTRATION2.pdf', 'Not member', 'Hino_Truck_Docs.pdf', 'Hino_Truck_Docs1.pdf', NULL, 0),
(9, 55, 'Certificate_of_Incorporation.pdf', 'Link_Freight_Pin_Cert.pdf', 'COMPANY_PROFILE.docx', 'Link_CR12_form.pdf', 'NOT MEMBER', 'Doc_.docx', 'Doc_1.docx', NULL, 0),
(10, 63, 'Legal_docs-EAO_certificate_of_Inc1.jpg', 'EAOTA_ITAX_PIN_P051235351Z1.pdf', 'EAOTA_COMPANY_PROFILE1.pdf', 'CR_12___EAOTA_(2)1.pdf', 'UCIFA', 'KIFWA_CERTIFICATE1.pdf', NULL, NULL, 0),
(11, 65, 'Legal_docs-EAO_certificate_of_Inc2.jpg', 'EAOTA_ITAX_PIN_P051235351Z2.pdf', 'EAOTA_COMPANY_PROFILE2.pdf', 'CR_12___EAOTA_(2)2.pdf', 'KIFWA', 'KIFWA_CERTIFICATE2.pdf', NULL, NULL, 0),
(12, 66, 'Legal_docs-EAO_certificate_of_Inc3.jpg', 'EAOTA_ITAX_PIN_P051235351Z3.pdf', 'EAOTA_COMPANY_PROFILE3.pdf', 'CR_12___EAOTA_(2)3.pdf', 'KTA', 'KIFWA_CERTIFICATE3.pdf', 'MISSION.png', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_individual_document`
--

CREATE TABLE `users_individual_document` (
  `document_id` int(20) NOT NULL,
  `users_id` int(20) NOT NULL,
  `id_copy` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pin_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pin_copy` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kta_receipt` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kifwa_receipt` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ind_identification_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ind_identification_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ind_passport_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ind_country_issued` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ind_expiry_date` date DEFAULT NULL,
  `deletion_status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_individual_document`
--

INSERT INTO `users_individual_document` (`document_id`, `users_id`, `id_copy`, `pin_number`, `pin_copy`, `kta_receipt`, `kifwa_receipt`, `ind_identification_type`, `ind_identification_no`, `ind_passport_no`, `ind_country_issued`, `ind_expiry_date`, `deletion_status`) VALUES
(1, 4, 'pic2.jpg', '84729', 'pic4.jpg', NULL, NULL, 'National ID', 'P327237', NULL, 'Kenya', NULL, 0),
(2, 2, 'pic1.jpg', '2384798', 'pic22.jpg', 'pic41.jpg', NULL, '', '', '', '', NULL, 1),
(3, 3, 'pic31.jpg', '8463', 'pic23.jpg', 'pic32.jpg', NULL, 'Passport No', NULL, 'SKF434', 'Kenya', '2020-07-30', 0),
(11, 32, 'pic12.jpg', 'P74634', 'pic43.jpg', NULL, NULL, 'National ID', 'KJF823723', NULL, 'Tanzania', NULL, 0),
(12, 30, 'pic121.jpg', 'K8472', 'pic44.jpg', 'pic112.jpg', NULL, 'National ID', 'P83737', NULL, 'Tanzania', NULL, 0),
(13, 31, 'pic123.jpg', 'HR843', 'pic45.jpg', 'pic113.jpg', NULL, 'National ID', 'HFR7328', NULL, 'Tanzania', NULL, 0),
(14, 36, 'truck.jpg', '8734', 'pic49.jpg', 'fitness3.jpg', NULL, 'National ID', 'J8782733', NULL, 'Kenya', NULL, 0),
(15, 42, 'clock7.png', 'aius082349', 'clock8.png', 'clock9.png', NULL, 'National ID', '96120395740', NULL, 'Kenya', NULL, 0),
(16, 52, 'Scan0015.pdf', 'p0317169981z', 'Scan0001.jpg', 'Scan0001.pdf', NULL, 'National ID', '33006557', NULL, 'Kenya', NULL, 0),
(17, 55, 'Rashid_ID.pdf', 'A002626432T', 'Rashid_KRA_pin_Certificate.pdf', 'Doc1.docx', NULL, 'National ID', '20137016', NULL, 'Kenya', NULL, 1),
(18, 61, 'ID_-_WANJA5.docx', '123456789', 'EAOTA_ITAX_PIN_P051235351Z4.pdf', 'KIFWA_CERTIFICATE4.pdf', NULL, 'National ID', '28754314', NULL, 'Kenya', NULL, 0),
(19, 75, 'Sofia_Getambu_-_ID1.pdf', '1234567', 'EAOTA_ITAX_PIN_P051235351Z5.pdf', 'KIFWA_CERTIFICATE5.pdf', NULL, 'National ID', '28754314', NULL, 'Kenya', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_notifications`
--

CREATE TABLE `users_notifications` (
  `id` int(11) NOT NULL,
  `type` varchar(200) NOT NULL,
  `users_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `booking_code` varchar(100) DEFAULT NULL,
  `user_profile` varchar(500) DEFAULT NULL,
  `type_message` text DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_notifications`
--

INSERT INTO `users_notifications` (`id`, `type`, `users_id`, `booking_id`, `booking_code`, `user_profile`, `type_message`, `status`, `created_at`) VALUES
(1, 'profile', 47, 0, NULL, NULL, 'Account Activated', 1, '2020-09-16 17:27:06'),
(2, 'profile', 48, 0, NULL, NULL, 'Please add proof of directorship', 1, '2020-09-16 18:03:44'),
(3, 'booking', 2, 1, 'ARW1', NULL, 'New booking', 1, '2020-09-16 19:18:16'),
(4, 'booking', 3, 1, 'ARW1', NULL, 'New booking', 1, '2020-09-16 19:18:16'),
(5, 'booking', 0, 1, 'ARW1', NULL, 'New booking', 0, '2020-09-16 19:18:16'),
(6, 'booking', 0, 1, 'ARW1', NULL, 'New booking', 0, '2020-09-16 19:18:16'),
(7, 'booking', 36, 1, 'ARW1', NULL, 'New booking', 0, '2020-09-16 19:18:16'),
(8, 'booking', 42, 1, 'ARW1', NULL, 'New booking', 0, '2020-09-16 19:18:16'),
(9, 'booking', 1, 1, 'ARW1', NULL, 'New booking', 1, '2020-09-16 19:18:16'),
(10, 'booking', 28, 1, 'ARW1', NULL, 'New booking', 0, '2020-09-16 19:18:16'),
(11, 'booking', 29, 1, 'ARW1', NULL, 'New booking', 0, '2020-09-16 19:18:16'),
(12, 'booking', 4, 1, 'ARW1', NULL, 'Requested booking', 1, '2020-09-16 19:20:05'),
(13, 'booking', 1, 1, 'ARW1', NULL, 'Requested booking', 1, '2020-09-16 19:20:05'),
(14, 'booking', 28, 1, 'ARW1', NULL, 'Requested booking', 0, '2020-09-16 19:20:05'),
(15, 'booking', 29, 1, 'ARW1', NULL, 'Requested booking', 0, '2020-09-16 19:20:05'),
(16, 'booking', 41, 1, 'ARW1', NULL, 'Requested booking', 0, '2020-09-16 19:20:05'),
(17, 'booking', 46, 1, 'ARW1', NULL, 'Requested booking', 0, '2020-09-16 19:20:05'),
(18, 'booking', 2, 1, 'ARW1', NULL, 'Confirmed booking', 1, '2020-09-16 19:26:37'),
(19, 'booking', 1, 1, 'ARW1', NULL, 'Confirmed booking', 1, '2020-09-16 19:26:37'),
(20, 'booking', 28, 1, 'ARW1', NULL, 'Confirmed booking', 0, '2020-09-16 19:26:37'),
(21, 'booking', 29, 1, 'ARW1', NULL, 'Confirmed booking', 0, '2020-09-16 19:26:37'),
(22, 'booking', 41, 1, 'ARW1', NULL, 'Confirmed booking', 0, '2020-09-16 19:26:37'),
(23, 'booking', 46, 1, 'ARW1', NULL, 'Confirmed booking', 0, '2020-09-16 19:26:37'),
(24, 'booking', 6, 1, 'ARW1', NULL, 'New Driver Booking', 1, '2020-09-16 19:27:40'),
(25, 'booking', 4, 1, 'ARW1', NULL, 'Accepted booking', 1, '2020-09-16 19:27:41'),
(26, 'booking', 1, 1, 'ARW1', NULL, 'Accepted booking', 1, '2020-09-16 19:27:41'),
(27, 'booking', 28, 1, 'ARW1', NULL, 'Accepted booking', 0, '2020-09-16 19:27:41'),
(28, 'booking', 29, 1, 'ARW1', NULL, 'Accepted booking', 0, '2020-09-16 19:27:41'),
(29, 'booking', 41, 1, 'ARW1', NULL, 'Accepted booking', 0, '2020-09-16 19:27:41'),
(30, 'booking', 46, 1, 'ARW1', NULL, 'Accepted booking', 0, '2020-09-16 19:27:41'),
(31, 'profile', 50, 0, NULL, NULL, 'Account Activated', 1, '2020-09-18 13:20:28'),
(32, 'profile', 49, 0, NULL, NULL, 'Account Activated', 0, '2020-09-18 13:24:19'),
(33, 'booking', 2, 2, 'ARW2', NULL, 'New booking', 1, '2020-09-18 14:15:42'),
(34, 'booking', 3, 2, 'ARW2', NULL, 'New booking', 1, '2020-09-18 14:15:42'),
(35, 'booking', 0, 2, 'ARW2', NULL, 'New booking', 0, '2020-09-18 14:15:42'),
(36, 'booking', 0, 2, 'ARW2', NULL, 'New booking', 0, '2020-09-18 14:15:42'),
(37, 'booking', 36, 2, 'ARW2', NULL, 'New booking', 0, '2020-09-18 14:15:42'),
(38, 'booking', 42, 2, 'ARW2', NULL, 'New booking', 0, '2020-09-18 14:15:42'),
(39, 'booking', 1, 2, 'ARW2', NULL, 'New booking', 1, '2020-09-18 14:15:42'),
(40, 'booking', 28, 2, 'ARW2', NULL, 'New booking', 0, '2020-09-18 14:15:42'),
(41, 'booking', 29, 2, 'ARW2', NULL, 'New booking', 0, '2020-09-18 14:15:42'),
(42, 'profile', 52, 0, NULL, NULL, 'Account Activated', 1, '2020-09-18 14:15:55'),
(43, 'truck', 52, 0, NULL, 'KCS458L', 'Truck Account Activated', 1, '2020-09-18 16:47:38'),
(44, 'truck', 52, 0, NULL, 'KCC943Q', 'Truck Account Activated', 1, '2020-09-18 17:12:52'),
(45, 'driver', 52, 0, NULL, 'Maxwell Omondi', 'Driver Account Activated', 1, '2020-09-18 17:13:01'),
(46, 'booking', 3, 3, 'ARW3', NULL, 'New booking', 1, '2020-09-18 17:20:03'),
(47, 'booking', 2, 3, 'ARW3', NULL, 'New booking', 1, '2020-09-18 17:20:03'),
(48, 'booking', 0, 3, 'ARW3', NULL, 'New booking', 0, '2020-09-18 17:20:03'),
(49, 'booking', 36, 3, 'ARW3', NULL, 'New booking', 0, '2020-09-18 17:20:03'),
(50, 'booking', 42, 3, 'ARW3', NULL, 'New booking', 0, '2020-09-18 17:20:03'),
(51, 'booking', 52, 3, 'ARW3', NULL, 'New booking', 1, '2020-09-18 17:20:03'),
(52, 'booking', 1, 3, 'ARW3', NULL, 'New booking', 1, '2020-09-18 17:20:03'),
(53, 'booking', 28, 3, 'ARW3', NULL, 'New booking', 0, '2020-09-18 17:20:03'),
(54, 'booking', 29, 3, 'ARW3', NULL, 'New booking', 0, '2020-09-18 17:20:03'),
(55, 'booking', 41, 3, 'ARW3', NULL, 'New booking', 0, '2020-09-18 17:20:03'),
(56, 'profile', 53, 0, NULL, NULL, 'Account Activated', 1, '2020-09-21 12:55:35'),
(57, 'profile', 55, 0, NULL, NULL, 'Hello. You have selected company type as Individual and not Corporate. Please select  company type as \'corporate\'  and fill in the company details as well as upload the company documents. ', 1, '2020-09-21 12:57:18'),
(58, 'booking', 2, 4, 'ARW4', NULL, 'New booking', 1, '2020-09-21 13:00:33'),
(59, 'booking', 3, 4, 'ARW4', NULL, 'New booking', 1, '2020-09-21 13:00:33'),
(60, 'booking', 0, 4, 'ARW4', NULL, 'New booking', 0, '2020-09-21 13:00:33'),
(61, 'booking', 0, 4, 'ARW4', NULL, 'New booking', 0, '2020-09-21 13:00:33'),
(62, 'booking', 36, 4, 'ARW4', NULL, 'New booking', 0, '2020-09-21 13:00:33'),
(63, 'booking', 42, 4, 'ARW4', NULL, 'New booking', 0, '2020-09-21 13:00:33'),
(64, 'booking', 52, 4, 'ARW4', NULL, 'New booking', 1, '2020-09-21 13:00:33'),
(65, 'booking', 0, 4, 'ARW4', NULL, 'New booking', 0, '2020-09-21 13:00:33'),
(66, 'booking', 1, 4, 'ARW4', NULL, 'New booking', 1, '2020-09-21 13:00:33'),
(67, 'booking', 28, 4, 'ARW4', NULL, 'New booking', 0, '2020-09-21 13:00:33'),
(68, 'booking', 50, 4, 'ARW4', NULL, 'Requested booking', 1, '2020-09-21 13:07:48'),
(69, 'booking', 1, 4, 'ARW4', NULL, 'Requested booking', 1, '2020-09-21 13:07:48'),
(70, 'booking', 28, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-09-21 13:07:48'),
(71, 'booking', 29, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-09-21 13:07:48'),
(72, 'booking', 41, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-09-21 13:07:48'),
(73, 'booking', 46, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-09-21 13:07:48'),
(74, 'booking', 2, 5, 'ARW5', NULL, 'New booking', 1, '2020-09-22 13:13:23'),
(75, 'booking', 3, 5, 'ARW5', NULL, 'New booking', 1, '2020-09-22 13:13:23'),
(76, 'booking', 0, 5, 'ARW5', NULL, 'New booking', 0, '2020-09-22 13:13:23'),
(77, 'booking', 0, 5, 'ARW5', NULL, 'New booking', 0, '2020-09-22 13:13:23'),
(78, 'booking', 36, 5, 'ARW5', NULL, 'New booking', 0, '2020-09-22 13:13:23'),
(79, 'booking', 42, 5, 'ARW5', NULL, 'New booking', 0, '2020-09-22 13:13:23'),
(80, 'booking', 52, 5, 'ARW5', NULL, 'New booking', 1, '2020-09-22 13:13:23'),
(81, 'booking', 0, 5, 'ARW5', NULL, 'New booking', 0, '2020-09-22 13:13:23'),
(82, 'booking', 1, 5, 'ARW5', NULL, 'New booking', 1, '2020-09-22 13:13:23'),
(83, 'booking', 28, 5, 'ARW5', NULL, 'New booking', 0, '2020-09-22 13:13:23'),
(84, 'booking', 50, 5, 'ARW5', NULL, 'Requested booking', 1, '2020-09-22 13:15:50'),
(85, 'booking', 1, 5, 'ARW5', NULL, 'Requested booking', 1, '2020-09-22 13:15:50'),
(86, 'booking', 28, 5, 'ARW5', NULL, 'Requested booking', 0, '2020-09-22 13:15:50'),
(87, 'booking', 29, 5, 'ARW5', NULL, 'Requested booking', 0, '2020-09-22 13:15:50'),
(88, 'booking', 41, 5, 'ARW5', NULL, 'Requested booking', 0, '2020-09-22 13:15:50'),
(89, 'booking', 46, 5, 'ARW5', NULL, 'Requested booking', 0, '2020-09-22 13:15:50'),
(90, 'booking', 3, 6, 'ARW6', NULL, 'New booking', 1, '2020-09-25 12:09:04'),
(91, 'booking', 2, 6, 'ARW6', NULL, 'New booking', 1, '2020-09-25 12:09:04'),
(92, 'booking', 0, 6, 'ARW6', NULL, 'New booking', 0, '2020-09-25 12:09:04'),
(93, 'booking', 36, 6, 'ARW6', NULL, 'New booking', 0, '2020-09-25 12:09:04'),
(94, 'booking', 42, 6, 'ARW6', NULL, 'New booking', 0, '2020-09-25 12:09:04'),
(95, 'booking', 52, 6, 'ARW6', NULL, 'New booking', 1, '2020-09-25 12:09:04'),
(96, 'booking', 0, 6, 'ARW6', NULL, 'New booking', 0, '2020-09-25 12:09:04'),
(97, 'booking', 1, 6, 'ARW6', NULL, 'New booking', 1, '2020-09-25 12:09:04'),
(98, 'booking', 28, 6, 'ARW6', NULL, 'New booking', 0, '2020-09-25 12:09:04'),
(99, 'booking', 29, 6, 'ARW6', NULL, 'New booking', 0, '2020-09-25 12:09:04'),
(100, 'booking', 30, 7, 'ARW7', NULL, 'New booking', 1, '2020-09-29 16:29:02'),
(101, 'booking', 31, 7, 'ARW7', NULL, 'New booking', 0, '2020-09-29 16:29:02'),
(102, 'booking', 1, 7, 'ARW7', NULL, 'New booking', 1, '2020-09-29 16:29:02'),
(103, 'booking', 33, 7, 'ARW7', NULL, 'New booking', 1, '2020-09-29 16:29:02'),
(104, 'booking', 4, 7, 'ARW7', NULL, 'Requested booking', 1, '2020-09-29 16:30:37'),
(105, 'booking', 1, 7, 'ARW7', NULL, 'Requested booking', 1, '2020-09-29 16:30:37'),
(106, 'booking', 33, 7, 'ARW7', NULL, 'Requested booking', 1, '2020-09-29 16:30:37'),
(107, 'booking', 30, 7, 'ARW7', NULL, 'Confirmed booking', 1, '2020-09-29 16:38:26'),
(108, 'booking', 1, 7, 'ARW7', NULL, 'Confirmed booking', 1, '2020-09-29 16:38:26'),
(109, 'booking', 33, 7, 'ARW7', NULL, 'Confirmed booking', 1, '2020-09-29 16:38:26'),
(110, 'booking', 34, 7, 'ARW7', NULL, 'New Driver Booking', 1, '2020-09-29 16:39:28'),
(111, 'booking', 4, 7, 'ARW7', NULL, 'Accepted booking', 1, '2020-09-29 16:39:28'),
(112, 'booking', 1, 7, 'ARW7', NULL, 'Accepted booking', 1, '2020-09-29 16:39:28'),
(113, 'booking', 33, 7, 'ARW7', NULL, 'Accepted booking', 1, '2020-09-29 16:39:28'),
(114, 'booking', 3, 8, 'ARW8', NULL, 'New booking', 1, '2020-09-29 16:47:26'),
(115, 'booking', 2, 8, 'ARW8', NULL, 'New booking', 1, '2020-09-29 16:47:26'),
(116, 'booking', 0, 8, 'ARW8', NULL, 'New booking', 0, '2020-09-29 16:47:26'),
(117, 'booking', 36, 8, 'ARW8', NULL, 'New booking', 0, '2020-09-29 16:47:26'),
(118, 'booking', 42, 8, 'ARW8', NULL, 'New booking', 0, '2020-09-29 16:47:26'),
(119, 'booking', 52, 8, 'ARW8', NULL, 'New booking', 1, '2020-09-29 16:47:26'),
(120, 'booking', 0, 8, 'ARW8', NULL, 'New booking', 0, '2020-09-29 16:47:26'),
(121, 'booking', 1, 8, 'ARW8', NULL, 'New booking', 1, '2020-09-29 16:47:26'),
(122, 'booking', 28, 8, 'ARW8', NULL, 'New booking', 0, '2020-09-29 16:47:26'),
(123, 'booking', 29, 8, 'ARW8', NULL, 'New booking', 0, '2020-09-29 16:47:26'),
(124, 'booking', 32, 8, 'ARW8', NULL, 'Requested booking', 1, '2020-09-29 16:48:27'),
(125, 'booking', 1, 8, 'ARW8', NULL, 'Requested booking', 1, '2020-09-29 16:48:27'),
(126, 'booking', 28, 8, 'ARW8', NULL, 'Requested booking', 0, '2020-09-29 16:48:27'),
(127, 'booking', 29, 8, 'ARW8', NULL, 'Requested booking', 0, '2020-09-29 16:48:27'),
(128, 'booking', 41, 8, 'ARW8', NULL, 'Requested booking', 0, '2020-09-29 16:48:27'),
(129, 'booking', 46, 8, 'ARW8', NULL, 'Requested booking', 0, '2020-09-29 16:48:27'),
(130, 'booking', 3, 8, 'ARW8', NULL, 'Confirmed booking', 1, '2020-09-29 16:48:59'),
(131, 'booking', 1, 8, 'ARW8', NULL, 'Confirmed booking', 1, '2020-09-29 16:48:59'),
(132, 'booking', 28, 8, 'ARW8', NULL, 'Confirmed booking', 0, '2020-09-29 16:48:59'),
(133, 'booking', 29, 8, 'ARW8', NULL, 'Confirmed booking', 0, '2020-09-29 16:48:59'),
(134, 'booking', 41, 8, 'ARW8', NULL, 'Confirmed booking', 0, '2020-09-29 16:48:59'),
(135, 'booking', 46, 8, 'ARW8', NULL, 'Confirmed booking', 0, '2020-09-29 16:48:59'),
(136, 'booking', 5, 8, 'ARW8', NULL, 'New Driver Booking', 1, '2020-09-29 16:50:16'),
(137, 'booking', 32, 8, 'ARW8', NULL, 'Accepted booking', 1, '2020-09-29 16:50:16'),
(138, 'booking', 1, 8, 'ARW8', NULL, 'Accepted booking', 1, '2020-09-29 16:50:16'),
(139, 'booking', 28, 8, 'ARW8', NULL, 'Accepted booking', 0, '2020-09-29 16:50:16'),
(140, 'booking', 29, 8, 'ARW8', NULL, 'Accepted booking', 0, '2020-09-29 16:50:16'),
(141, 'booking', 41, 8, 'ARW8', NULL, 'Accepted booking', 0, '2020-09-29 16:50:16'),
(142, 'booking', 46, 8, 'ARW8', NULL, 'Accepted booking', 0, '2020-09-29 16:50:16'),
(143, 'booking', 3, 9, 'ARW9', NULL, 'New booking', 1, '2020-09-29 17:45:33'),
(144, 'booking', 2, 9, 'ARW9', NULL, 'New booking', 1, '2020-09-29 17:45:33'),
(145, 'booking', 0, 9, 'ARW9', NULL, 'New booking', 0, '2020-09-29 17:45:33'),
(146, 'booking', 36, 9, 'ARW9', NULL, 'New booking', 0, '2020-09-29 17:45:33'),
(147, 'booking', 42, 9, 'ARW9', NULL, 'New booking', 0, '2020-09-29 17:45:33'),
(148, 'booking', 52, 9, 'ARW9', NULL, 'New booking', 1, '2020-09-29 17:45:33'),
(149, 'booking', 0, 9, 'ARW9', NULL, 'New booking', 0, '2020-09-29 17:45:33'),
(150, 'booking', 1, 9, 'ARW9', NULL, 'New booking', 1, '2020-09-29 17:45:33'),
(151, 'booking', 28, 9, 'ARW9', NULL, 'New booking', 0, '2020-09-29 17:45:33'),
(152, 'booking', 29, 9, 'ARW9', NULL, 'New booking', 0, '2020-09-29 17:45:33'),
(153, 'booking', 4, 9, 'ARW9', NULL, 'Requested booking', 1, '2020-09-29 17:47:53'),
(154, 'booking', 1, 9, 'ARW9', NULL, 'Requested booking', 1, '2020-09-29 17:47:53'),
(155, 'booking', 28, 9, 'ARW9', NULL, 'Requested booking', 0, '2020-09-29 17:47:53'),
(156, 'booking', 29, 9, 'ARW9', NULL, 'Requested booking', 0, '2020-09-29 17:47:53'),
(157, 'booking', 41, 9, 'ARW9', NULL, 'Requested booking', 0, '2020-09-29 17:47:53'),
(158, 'booking', 46, 9, 'ARW9', NULL, 'Requested booking', 0, '2020-09-29 17:47:53'),
(159, 'booking', 3, 9, 'ARW9', NULL, 'Confirmed booking', 1, '2020-09-29 17:51:27'),
(160, 'booking', 1, 9, 'ARW9', NULL, 'Confirmed booking', 1, '2020-09-29 17:51:27'),
(161, 'booking', 28, 9, 'ARW9', NULL, 'Confirmed booking', 0, '2020-09-29 17:51:27'),
(162, 'booking', 29, 9, 'ARW9', NULL, 'Confirmed booking', 0, '2020-09-29 17:51:27'),
(163, 'booking', 41, 9, 'ARW9', NULL, 'Confirmed booking', 0, '2020-09-29 17:51:27'),
(164, 'booking', 46, 9, 'ARW9', NULL, 'Confirmed booking', 0, '2020-09-29 17:51:27'),
(165, 'booking', 5, 9, 'ARW9', NULL, 'New Driver Booking', 1, '2020-09-29 17:55:46'),
(166, 'booking', 4, 9, 'ARW9', NULL, 'Accepted booking', 1, '2020-09-29 17:55:46'),
(167, 'booking', 1, 9, 'ARW9', NULL, 'Accepted booking', 1, '2020-09-29 17:55:46'),
(168, 'booking', 28, 9, 'ARW9', NULL, 'Accepted booking', 0, '2020-09-29 17:55:46'),
(169, 'booking', 29, 9, 'ARW9', NULL, 'Accepted booking', 0, '2020-09-29 17:55:46'),
(170, 'booking', 41, 9, 'ARW9', NULL, 'Accepted booking', 0, '2020-09-29 17:55:46'),
(171, 'booking', 46, 9, 'ARW9', NULL, 'Accepted booking', 0, '2020-09-29 17:55:46'),
(172, 'profile', 61, 0, NULL, NULL, 'Account Activated', 1, '2020-10-01 14:38:08'),
(173, 'booking', 2, 10, 'ARW10', NULL, 'New booking', 1, '2020-10-01 14:41:16'),
(174, 'booking', 3, 10, 'ARW10', NULL, 'New booking', 1, '2020-10-01 14:41:16'),
(175, 'booking', 0, 10, 'ARW10', NULL, 'New booking', 0, '2020-10-01 14:41:16'),
(176, 'booking', 0, 10, 'ARW10', NULL, 'New booking', 0, '2020-10-01 14:41:16'),
(177, 'booking', 36, 10, 'ARW10', NULL, 'New booking', 0, '2020-10-01 14:41:16'),
(178, 'booking', 42, 10, 'ARW10', NULL, 'New booking', 0, '2020-10-01 14:41:16'),
(179, 'booking', 52, 10, 'ARW10', NULL, 'New booking', 1, '2020-10-01 14:41:16'),
(180, 'booking', 0, 10, 'ARW10', NULL, 'New booking', 0, '2020-10-01 14:41:16'),
(181, 'booking', 1, 10, 'ARW10', NULL, 'New booking', 1, '2020-10-01 14:41:16'),
(182, 'booking', 28, 10, 'ARW10', NULL, 'New booking', 0, '2020-10-01 14:41:16'),
(183, 'booking', 61, 10, 'ARW10', NULL, 'Requested booking', 1, '2020-10-01 14:43:45'),
(184, 'booking', 1, 10, 'ARW10', NULL, 'Requested booking', 1, '2020-10-01 14:43:45'),
(185, 'booking', 28, 10, 'ARW10', NULL, 'Requested booking', 0, '2020-10-01 14:43:45'),
(186, 'booking', 29, 10, 'ARW10', NULL, 'Requested booking', 0, '2020-10-01 14:43:45'),
(187, 'booking', 41, 10, 'ARW10', NULL, 'Requested booking', 0, '2020-10-01 14:43:45'),
(188, 'booking', 46, 10, 'ARW10', NULL, 'Requested booking', 0, '2020-10-01 14:43:45'),
(189, 'booking', 3, 10, 'ARW10', NULL, 'Confirmed booking', 1, '2020-10-01 14:44:58'),
(190, 'booking', 1, 10, 'ARW10', NULL, 'Confirmed booking', 1, '2020-10-01 14:44:58'),
(191, 'booking', 28, 10, 'ARW10', NULL, 'Confirmed booking', 0, '2020-10-01 14:44:58'),
(192, 'booking', 29, 10, 'ARW10', NULL, 'Confirmed booking', 0, '2020-10-01 14:44:58'),
(193, 'booking', 41, 10, 'ARW10', NULL, 'Confirmed booking', 0, '2020-10-01 14:44:58'),
(194, 'booking', 46, 10, 'ARW10', NULL, 'Confirmed booking', 0, '2020-10-01 14:44:58'),
(195, 'booking', 5, 10, 'ARW10', NULL, 'New Driver Booking', 1, '2020-10-01 14:46:01'),
(196, 'booking', 61, 10, 'ARW10', NULL, 'Accepted booking', 1, '2020-10-01 14:46:01'),
(197, 'booking', 1, 10, 'ARW10', NULL, 'Accepted booking', 1, '2020-10-01 14:46:01'),
(198, 'booking', 28, 10, 'ARW10', NULL, 'Accepted booking', 0, '2020-10-01 14:46:01'),
(199, 'booking', 29, 10, 'ARW10', NULL, 'Accepted booking', 0, '2020-10-01 14:46:01'),
(200, 'booking', 41, 10, 'ARW10', NULL, 'Accepted booking', 0, '2020-10-01 14:46:01'),
(201, 'booking', 46, 10, 'ARW10', NULL, 'Accepted booking', 0, '2020-10-01 14:46:01'),
(202, 'booking', 61, 10, 'ARW10', NULL, 'Incident booking', 1, '2020-10-01 14:51:53'),
(203, 'booking', 1, 10, 'ARW10', NULL, 'Incident booking', 1, '2020-10-01 14:51:53'),
(204, 'booking', 28, 10, 'ARW10', NULL, 'Incident booking', 0, '2020-10-01 14:51:53'),
(205, 'booking', 29, 10, 'ARW10', NULL, 'Incident booking', 0, '2020-10-01 14:51:53'),
(206, 'booking', 41, 10, 'ARW10', NULL, 'Incident booking', 0, '2020-10-01 14:51:53'),
(207, 'booking', 46, 10, 'ARW10', NULL, 'Incident booking', 0, '2020-10-01 14:51:53'),
(208, 'booking', 3, 10, 'ARW10', NULL, 'Incident booking', 1, '2020-10-01 14:51:53'),
(209, 'booking', 61, 10, 'ARW10', NULL, 'Incident resolved', 1, '2020-10-01 14:53:13'),
(210, 'booking', 1, 10, 'ARW10', NULL, 'Incident resolved', 1, '2020-10-01 14:53:13'),
(211, 'booking', 28, 10, 'ARW10', NULL, 'Incident resolved', 0, '2020-10-01 14:53:13'),
(212, 'booking', 29, 10, 'ARW10', NULL, 'Incident resolved', 0, '2020-10-01 14:53:13'),
(213, 'booking', 41, 10, 'ARW10', NULL, 'Incident resolved', 0, '2020-10-01 14:53:13'),
(214, 'booking', 46, 10, 'ARW10', NULL, 'Incident resolved', 0, '2020-10-01 14:53:13'),
(215, 'booking', 3, 10, 'ARW10', NULL, 'Incident resolved', 1, '2020-10-01 14:53:13'),
(216, 'booking', 50, 4, 'ARW4', NULL, 'Requested booking', 1, '2020-10-08 14:45:18'),
(217, 'booking', 1, 4, 'ARW4', NULL, 'Requested booking', 1, '2020-10-08 14:45:18'),
(218, 'booking', 28, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-10-08 14:45:18'),
(219, 'booking', 29, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-10-08 14:45:18'),
(220, 'booking', 41, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-10-08 14:45:18'),
(221, 'booking', 46, 4, 'ARW4', NULL, 'Requested booking', 0, '2020-10-08 14:45:18'),
(222, 'booking', 3, 4, 'ARW4', NULL, 'Confirmed booking', 1, '2020-10-08 14:50:27'),
(223, 'booking', 1, 4, 'ARW4', NULL, 'Confirmed booking', 1, '2020-10-08 14:50:27'),
(224, 'booking', 28, 4, 'ARW4', NULL, 'Confirmed booking', 0, '2020-10-08 14:50:27'),
(225, 'booking', 29, 4, 'ARW4', NULL, 'Confirmed booking', 0, '2020-10-08 14:50:27'),
(226, 'booking', 41, 4, 'ARW4', NULL, 'Confirmed booking', 0, '2020-10-08 14:50:27'),
(227, 'booking', 46, 4, 'ARW4', NULL, 'Confirmed booking', 0, '2020-10-08 14:50:27'),
(228, 'booking', 5, 4, 'ARW4', NULL, 'New Driver Booking', 1, '2020-10-08 14:55:34'),
(229, 'booking', 50, 4, 'ARW4', NULL, 'Accepted booking', 1, '2020-10-08 14:55:34'),
(230, 'booking', 1, 4, 'ARW4', NULL, 'Accepted booking', 1, '2020-10-08 14:55:34'),
(231, 'booking', 28, 4, 'ARW4', NULL, 'Accepted booking', 0, '2020-10-08 14:55:34'),
(232, 'booking', 29, 4, 'ARW4', NULL, 'Accepted booking', 0, '2020-10-08 14:55:34'),
(233, 'booking', 41, 4, 'ARW4', NULL, 'Accepted booking', 0, '2020-10-08 14:55:34'),
(234, 'booking', 46, 4, 'ARW4', NULL, 'Accepted booking', 0, '2020-10-08 14:55:34'),
(235, 'booking', 50, 4, 'ARW4', NULL, 'Incident booking', 1, '2020-10-08 15:02:19'),
(236, 'booking', 1, 4, 'ARW4', NULL, 'Incident booking', 1, '2020-10-08 15:02:19'),
(237, 'booking', 28, 4, 'ARW4', NULL, 'Incident booking', 0, '2020-10-08 15:02:19'),
(238, 'booking', 29, 4, 'ARW4', NULL, 'Incident booking', 0, '2020-10-08 15:02:19'),
(239, 'booking', 41, 4, 'ARW4', NULL, 'Incident booking', 0, '2020-10-08 15:02:19'),
(240, 'booking', 46, 4, 'ARW4', NULL, 'Incident booking', 0, '2020-10-08 15:02:19'),
(241, 'booking', 3, 4, 'ARW4', NULL, 'Incident booking', 1, '2020-10-08 15:02:19'),
(242, 'booking', 50, 4, 'ARW4', NULL, 'Incident resolved', 1, '2020-10-08 16:11:53'),
(243, 'booking', 1, 4, 'ARW4', NULL, 'Incident resolved', 1, '2020-10-08 16:11:53'),
(244, 'booking', 28, 4, 'ARW4', NULL, 'Incident resolved', 0, '2020-10-08 16:11:53'),
(245, 'booking', 29, 4, 'ARW4', NULL, 'Incident resolved', 0, '2020-10-08 16:11:53'),
(246, 'booking', 41, 4, 'ARW4', NULL, 'Incident resolved', 0, '2020-10-08 16:11:53'),
(247, 'booking', 46, 4, 'ARW4', NULL, 'Incident resolved', 0, '2020-10-08 16:11:53'),
(248, 'booking', 3, 4, 'ARW4', NULL, 'Incident resolved', 1, '2020-10-08 16:11:53'),
(249, 'booking', 2, 11, 'ARW11', NULL, 'New booking', 1, '2020-10-08 16:30:06'),
(250, 'booking', 3, 11, 'ARW11', NULL, 'New booking', 1, '2020-10-08 16:30:06'),
(251, 'booking', 36, 11, 'ARW11', NULL, 'New booking', 0, '2020-10-08 16:30:06'),
(252, 'booking', 42, 11, 'ARW11', NULL, 'New booking', 0, '2020-10-08 16:30:06'),
(253, 'booking', 52, 11, 'ARW11', NULL, 'New booking', 1, '2020-10-08 16:30:06'),
(254, 'booking', 1, 11, 'ARW11', NULL, 'New booking', 1, '2020-10-08 16:30:06'),
(255, 'booking', 28, 11, 'ARW11', NULL, 'New booking', 0, '2020-10-08 16:30:06'),
(256, 'booking', 29, 11, 'ARW11', NULL, 'New booking', 0, '2020-10-08 16:30:06'),
(257, 'booking', 41, 11, 'ARW11', NULL, 'New booking', 0, '2020-10-08 16:30:06'),
(258, 'booking', 46, 11, 'ARW11', NULL, 'New booking', 0, '2020-10-08 16:30:06'),
(259, 'booking', 61, 11, 'ARW11', NULL, 'Requested booking', 1, '2020-10-08 16:31:18'),
(260, 'booking', 1, 11, 'ARW11', NULL, 'Requested booking', 1, '2020-10-08 16:31:18'),
(261, 'booking', 28, 11, 'ARW11', NULL, 'Requested booking', 0, '2020-10-08 16:31:18'),
(262, 'booking', 29, 11, 'ARW11', NULL, 'Requested booking', 0, '2020-10-08 16:31:18'),
(263, 'booking', 41, 11, 'ARW11', NULL, 'Requested booking', 0, '2020-10-08 16:31:18'),
(264, 'booking', 46, 11, 'ARW11', NULL, 'Requested booking', 0, '2020-10-08 16:31:18'),
(265, 'booking', 3, 11, 'ARW11', NULL, 'Confirmed booking', 1, '2020-10-08 16:32:22'),
(266, 'booking', 1, 11, 'ARW11', NULL, 'Confirmed booking', 1, '2020-10-08 16:32:22'),
(267, 'booking', 28, 11, 'ARW11', NULL, 'Confirmed booking', 0, '2020-10-08 16:32:22'),
(268, 'booking', 29, 11, 'ARW11', NULL, 'Confirmed booking', 0, '2020-10-08 16:32:22'),
(269, 'booking', 41, 11, 'ARW11', NULL, 'Confirmed booking', 0, '2020-10-08 16:32:22'),
(270, 'booking', 46, 11, 'ARW11', NULL, 'Confirmed booking', 0, '2020-10-08 16:32:22'),
(271, 'booking', 5, 11, 'ARW11', NULL, 'New Driver Booking', 1, '2020-10-08 16:41:04'),
(272, 'booking', 61, 11, 'ARW11', NULL, 'Accepted booking', 1, '2020-10-08 16:41:04'),
(273, 'booking', 1, 11, 'ARW11', NULL, 'Accepted booking', 1, '2020-10-08 16:41:04'),
(274, 'booking', 28, 11, 'ARW11', NULL, 'Accepted booking', 0, '2020-10-08 16:41:04'),
(275, 'booking', 29, 11, 'ARW11', NULL, 'Accepted booking', 0, '2020-10-08 16:41:04'),
(276, 'booking', 41, 11, 'ARW11', NULL, 'Accepted booking', 0, '2020-10-08 16:41:04'),
(277, 'booking', 46, 11, 'ARW11', NULL, 'Accepted booking', 0, '2020-10-08 16:41:04'),
(278, 'booking', 4, 6, 'ARW6', NULL, 'Requested booking', 1, '2020-10-08 18:04:38'),
(279, 'booking', 1, 6, 'ARW6', NULL, 'Requested booking', 1, '2020-10-08 18:04:38'),
(280, 'booking', 28, 6, 'ARW6', NULL, 'Requested booking', 0, '2020-10-08 18:04:38'),
(281, 'booking', 29, 6, 'ARW6', NULL, 'Requested booking', 0, '2020-10-08 18:04:38'),
(282, 'booking', 41, 6, 'ARW6', NULL, 'Requested booking', 0, '2020-10-08 18:04:38'),
(283, 'booking', 46, 6, 'ARW6', NULL, 'Requested booking', 0, '2020-10-08 18:04:38'),
(284, 'booking', 2, 6, 'ARW6', NULL, 'Confirmed booking', 1, '2020-10-08 18:05:07'),
(285, 'booking', 1, 6, 'ARW6', NULL, 'Confirmed booking', 1, '2020-10-08 18:05:07'),
(286, 'booking', 28, 6, 'ARW6', NULL, 'Confirmed booking', 0, '2020-10-08 18:05:07'),
(287, 'booking', 29, 6, 'ARW6', NULL, 'Confirmed booking', 0, '2020-10-08 18:05:07'),
(288, 'booking', 41, 6, 'ARW6', NULL, 'Confirmed booking', 0, '2020-10-08 18:05:07'),
(289, 'booking', 46, 6, 'ARW6', NULL, 'Confirmed booking', 0, '2020-10-08 18:05:07'),
(290, 'booking', 6, 6, 'ARW6', NULL, 'New Driver Booking', 1, '2020-10-08 18:05:39'),
(291, 'booking', 4, 6, 'ARW6', NULL, 'Accepted booking', 1, '2020-10-08 18:05:39'),
(292, 'booking', 1, 6, 'ARW6', NULL, 'Accepted booking', 1, '2020-10-08 18:05:39'),
(293, 'booking', 28, 6, 'ARW6', NULL, 'Accepted booking', 0, '2020-10-08 18:05:39'),
(294, 'booking', 29, 6, 'ARW6', NULL, 'Accepted booking', 0, '2020-10-08 18:05:39'),
(295, 'booking', 41, 6, 'ARW6', NULL, 'Accepted booking', 0, '2020-10-08 18:05:39'),
(296, 'booking', 46, 6, 'ARW6', NULL, 'Accepted booking', 0, '2020-10-08 18:05:39'),
(297, 'profile', 63, 0, NULL, NULL, 'Account Activated', 1, '2020-10-12 15:17:36'),
(298, 'booking', 3, 12, 'ARW12', NULL, 'New booking', 1, '2020-10-12 15:31:51'),
(299, 'booking', 36, 12, 'ARW12', NULL, 'New booking', 0, '2020-10-12 15:31:51'),
(300, 'booking', 42, 12, 'ARW12', NULL, 'New booking', 0, '2020-10-12 15:31:51'),
(301, 'booking', 52, 12, 'ARW12', NULL, 'New booking', 1, '2020-10-12 15:31:51'),
(302, 'booking', 1, 12, 'ARW12', NULL, 'New booking', 1, '2020-10-12 15:31:51'),
(303, 'booking', 28, 12, 'ARW12', NULL, 'New booking', 0, '2020-10-12 15:31:51'),
(304, 'booking', 29, 12, 'ARW12', NULL, 'New booking', 0, '2020-10-12 15:31:51'),
(305, 'booking', 41, 12, 'ARW12', NULL, 'New booking', 0, '2020-10-12 15:31:51'),
(306, 'booking', 46, 12, 'ARW12', NULL, 'New booking', 0, '2020-10-12 15:31:51'),
(307, 'booking', 63, 12, 'ARW12', NULL, 'Requested booking', 1, '2020-10-12 15:34:18'),
(308, 'booking', 1, 12, 'ARW12', NULL, 'Requested booking', 1, '2020-10-12 15:34:18'),
(309, 'booking', 28, 12, 'ARW12', NULL, 'Requested booking', 0, '2020-10-12 15:34:18'),
(310, 'booking', 29, 12, 'ARW12', NULL, 'Requested booking', 0, '2020-10-12 15:34:18'),
(311, 'booking', 41, 12, 'ARW12', NULL, 'Requested booking', 0, '2020-10-12 15:34:18'),
(312, 'booking', 46, 12, 'ARW12', NULL, 'Requested booking', 0, '2020-10-12 15:34:18'),
(313, 'booking', 3, 12, 'ARW12', NULL, 'Confirmed booking', 1, '2020-10-12 15:35:37'),
(314, 'booking', 1, 12, 'ARW12', NULL, 'Confirmed booking', 1, '2020-10-12 15:35:37'),
(315, 'booking', 28, 12, 'ARW12', NULL, 'Confirmed booking', 0, '2020-10-12 15:35:37'),
(316, 'booking', 29, 12, 'ARW12', NULL, 'Confirmed booking', 0, '2020-10-12 15:35:37'),
(317, 'booking', 41, 12, 'ARW12', NULL, 'Confirmed booking', 0, '2020-10-12 15:35:37'),
(318, 'booking', 46, 12, 'ARW12', NULL, 'Confirmed booking', 0, '2020-10-12 15:35:37'),
(319, 'booking', 5, 12, 'ARW12', NULL, 'New Driver Booking', 1, '2020-10-12 15:37:44'),
(320, 'booking', 63, 12, 'ARW12', NULL, 'Accepted booking', 1, '2020-10-12 15:37:44'),
(321, 'booking', 1, 12, 'ARW12', NULL, 'Accepted booking', 1, '2020-10-12 15:37:44'),
(322, 'booking', 28, 12, 'ARW12', NULL, 'Accepted booking', 0, '2020-10-12 15:37:44'),
(323, 'booking', 29, 12, 'ARW12', NULL, 'Accepted booking', 0, '2020-10-12 15:37:44'),
(324, 'booking', 41, 12, 'ARW12', NULL, 'Accepted booking', 0, '2020-10-12 15:37:44'),
(325, 'booking', 46, 12, 'ARW12', NULL, 'Accepted booking', 0, '2020-10-12 15:37:44'),
(326, 'booking', 63, 12, 'ARW12', NULL, 'Incident booking', 1, '2020-10-12 15:41:37'),
(327, 'booking', 1, 12, 'ARW12', NULL, 'Incident booking', 1, '2020-10-12 15:41:37'),
(328, 'booking', 28, 12, 'ARW12', NULL, 'Incident booking', 0, '2020-10-12 15:41:37'),
(329, 'booking', 29, 12, 'ARW12', NULL, 'Incident booking', 0, '2020-10-12 15:41:37'),
(330, 'booking', 41, 12, 'ARW12', NULL, 'Incident booking', 0, '2020-10-12 15:41:37'),
(331, 'booking', 46, 12, 'ARW12', NULL, 'Incident booking', 0, '2020-10-12 15:41:37'),
(332, 'booking', 3, 12, 'ARW12', NULL, 'Incident booking', 1, '2020-10-12 15:41:37'),
(333, 'booking', 63, 12, 'ARW12', NULL, 'Incident resolved', 1, '2020-10-12 15:42:57'),
(334, 'booking', 1, 12, 'ARW12', NULL, 'Incident resolved', 1, '2020-10-12 15:42:57'),
(335, 'booking', 28, 12, 'ARW12', NULL, 'Incident resolved', 0, '2020-10-12 15:42:57'),
(336, 'booking', 29, 12, 'ARW12', NULL, 'Incident resolved', 0, '2020-10-12 15:42:57'),
(337, 'booking', 41, 12, 'ARW12', NULL, 'Incident resolved', 0, '2020-10-12 15:42:57'),
(338, 'booking', 46, 12, 'ARW12', NULL, 'Incident resolved', 0, '2020-10-12 15:42:57'),
(339, 'booking', 30, 13, 'ARW13', NULL, 'New booking', 1, '2020-10-12 15:59:09'),
(340, 'booking', 31, 13, 'ARW13', NULL, 'New booking', 0, '2020-10-12 15:59:09'),
(341, 'booking', 1, 13, 'ARW13', NULL, 'New booking', 1, '2020-10-12 15:59:09'),
(342, 'booking', 33, 13, 'ARW13', NULL, 'New booking', 1, '2020-10-12 15:59:09'),
(343, 'booking', 32, 13, 'ARW13', NULL, 'Requested booking', 1, '2020-10-12 16:01:52'),
(344, 'booking', 1, 13, 'ARW13', NULL, 'Requested booking', 1, '2020-10-12 16:01:52'),
(345, 'booking', 33, 13, 'ARW13', NULL, 'Requested booking', 1, '2020-10-12 16:01:52'),
(346, 'booking', 30, 13, 'ARW13', NULL, 'Confirmed booking', 1, '2020-10-12 16:02:40'),
(347, 'booking', 1, 13, 'ARW13', NULL, 'Confirmed booking', 1, '2020-10-12 16:02:40'),
(348, 'booking', 33, 13, 'ARW13', NULL, 'Confirmed booking', 1, '2020-10-12 16:02:40'),
(349, 'booking', 34, 13, 'ARW13', NULL, 'New Driver Booking', 1, '2020-10-12 16:03:40'),
(350, 'booking', 32, 13, 'ARW13', NULL, 'Accepted booking', 1, '2020-10-12 16:03:41'),
(351, 'booking', 1, 13, 'ARW13', NULL, 'Accepted booking', 1, '2020-10-12 16:03:41'),
(352, 'booking', 33, 13, 'ARW13', NULL, 'Accepted booking', 1, '2020-10-12 16:03:41'),
(353, 'booking', 50, 3, 'ARW3', NULL, 'Requested booking', 1, '2020-10-14 07:33:33'),
(354, 'booking', 1, 3, 'ARW3', NULL, 'Requested booking', 1, '2020-10-14 07:33:33'),
(355, 'booking', 28, 3, 'ARW3', NULL, 'Requested booking', 0, '2020-10-14 07:33:33'),
(356, 'booking', 29, 3, 'ARW3', NULL, 'Requested booking', 0, '2020-10-14 07:33:33'),
(357, 'booking', 41, 3, 'ARW3', NULL, 'Requested booking', 0, '2020-10-14 07:33:33'),
(358, 'booking', 46, 3, 'ARW3', NULL, 'Requested booking', 0, '2020-10-14 07:33:33'),
(359, 'booking', 30, 14, 'ARW14', NULL, 'New booking', 1, '2020-10-14 07:49:06'),
(360, 'booking', 31, 14, 'ARW14', NULL, 'New booking', 0, '2020-10-14 07:49:06'),
(361, 'booking', 1, 14, 'ARW14', NULL, 'New booking', 1, '2020-10-14 07:49:06'),
(362, 'booking', 33, 14, 'ARW14', NULL, 'New booking', 1, '2020-10-14 07:49:06'),
(363, 'booking', 63, 14, 'ARW14', NULL, 'Requested booking', 1, '2020-10-14 07:49:41'),
(364, 'booking', 1, 14, 'ARW14', NULL, 'Requested booking', 1, '2020-10-14 07:49:41'),
(365, 'booking', 33, 14, 'ARW14', NULL, 'Requested booking', 1, '2020-10-14 07:49:41'),
(366, 'booking', 30, 14, 'ARW14', NULL, 'Confirmed booking', 1, '2020-10-14 07:51:10'),
(367, 'booking', 1, 14, 'ARW14', NULL, 'Confirmed booking', 1, '2020-10-14 07:51:10'),
(368, 'booking', 33, 14, 'ARW14', NULL, 'Confirmed booking', 1, '2020-10-14 07:51:10'),
(369, 'booking', 34, 14, 'ARW14', NULL, 'New Driver Booking', 1, '2020-10-14 07:53:02'),
(370, 'booking', 63, 14, 'ARW14', NULL, 'Accepted booking', 1, '2020-10-14 07:53:02'),
(371, 'booking', 1, 14, 'ARW14', NULL, 'Accepted booking', 1, '2020-10-14 07:53:02'),
(372, 'booking', 33, 14, 'ARW14', NULL, 'Accepted booking', 1, '2020-10-14 07:53:02'),
(373, 'booking', 63, 14, 'ARW14', NULL, 'Incident booking', 1, '2020-10-14 07:55:56'),
(374, 'booking', 1, 14, 'ARW14', NULL, 'Incident booking', 1, '2020-10-14 07:55:56'),
(375, 'booking', 33, 14, 'ARW14', NULL, 'Incident booking', 1, '2020-10-14 07:55:56'),
(376, 'booking', 30, 14, 'ARW14', NULL, 'Incident booking', 1, '2020-10-14 07:55:56'),
(377, 'booking', 63, 14, 'ARW14', NULL, 'Incident resolved', 1, '2020-10-14 07:57:39'),
(378, 'booking', 1, 14, 'ARW14', NULL, 'Incident resolved', 1, '2020-10-14 07:57:39'),
(379, 'booking', 33, 14, 'ARW14', NULL, 'Incident resolved', 1, '2020-10-14 07:57:39'),
(380, 'booking', 3, 15, 'ARW15', NULL, 'New booking', 1, '2020-10-15 21:15:16'),
(381, 'booking', 36, 15, 'ARW15', NULL, 'New booking', 0, '2020-10-15 21:15:16'),
(382, 'booking', 42, 15, 'ARW15', NULL, 'New booking', 0, '2020-10-15 21:15:16'),
(383, 'booking', 52, 15, 'ARW15', NULL, 'New booking', 1, '2020-10-15 21:15:16'),
(384, 'booking', 1, 15, 'ARW15', NULL, 'New booking', 1, '2020-10-15 21:15:16'),
(385, 'booking', 28, 15, 'ARW15', NULL, 'New booking', 0, '2020-10-15 21:15:16'),
(386, 'booking', 29, 15, 'ARW15', NULL, 'New booking', 0, '2020-10-15 21:15:16'),
(387, 'booking', 41, 15, 'ARW15', NULL, 'New booking', 0, '2020-10-15 21:15:16'),
(388, 'booking', 46, 15, 'ARW15', NULL, 'New booking', 0, '2020-10-15 21:15:16'),
(389, 'booking', 4, 15, 'ARW15', NULL, 'Requested booking', 1, '2020-10-15 21:18:45'),
(390, 'booking', 1, 15, 'ARW15', NULL, 'Requested booking', 1, '2020-10-15 21:18:45'),
(391, 'booking', 28, 15, 'ARW15', NULL, 'Requested booking', 0, '2020-10-15 21:18:45'),
(392, 'booking', 29, 15, 'ARW15', NULL, 'Requested booking', 0, '2020-10-15 21:18:45'),
(393, 'booking', 41, 15, 'ARW15', NULL, 'Requested booking', 0, '2020-10-15 21:18:45'),
(394, 'booking', 46, 15, 'ARW15', NULL, 'Requested booking', 0, '2020-10-15 21:18:45'),
(395, 'booking', 3, 15, 'ARW15', NULL, 'Confirmed booking', 1, '2020-10-15 21:22:38'),
(396, 'booking', 1, 15, 'ARW15', NULL, 'Confirmed booking', 1, '2020-10-15 21:22:38'),
(397, 'booking', 28, 15, 'ARW15', NULL, 'Confirmed booking', 0, '2020-10-15 21:22:38'),
(398, 'booking', 29, 15, 'ARW15', NULL, 'Confirmed booking', 0, '2020-10-15 21:22:38'),
(399, 'booking', 41, 15, 'ARW15', NULL, 'Confirmed booking', 0, '2020-10-15 21:22:38'),
(400, 'booking', 46, 15, 'ARW15', NULL, 'Confirmed booking', 0, '2020-10-15 21:22:38'),
(401, 'booking', 5, 15, 'ARW15', NULL, 'New Driver Booking', 1, '2020-10-15 21:25:09'),
(402, 'booking', 4, 15, 'ARW15', NULL, 'Accepted booking', 1, '2020-10-15 21:25:10'),
(403, 'booking', 1, 15, 'ARW15', NULL, 'Accepted booking', 1, '2020-10-15 21:25:10'),
(404, 'booking', 28, 15, 'ARW15', NULL, 'Accepted booking', 0, '2020-10-15 21:25:10'),
(405, 'booking', 29, 15, 'ARW15', NULL, 'Accepted booking', 0, '2020-10-15 21:25:10'),
(406, 'booking', 41, 15, 'ARW15', NULL, 'Accepted booking', 0, '2020-10-15 21:25:10'),
(407, 'booking', 46, 15, 'ARW15', NULL, 'Accepted booking', 0, '2020-10-15 21:25:10'),
(408, 'profile', 55, 0, NULL, NULL, 'Account Activated', 1, '2020-10-20 11:59:07'),
(409, 'profile', 65, 0, NULL, NULL, 'Account Activated', 1, '2020-10-21 08:27:55'),
(410, 'profile', 66, 0, NULL, NULL, 'Account Activated', 1, '2020-10-21 08:47:20'),
(411, 'profile', 48, 0, NULL, NULL, 'Upload clear copy of director ID', 0, '2020-10-21 08:48:10'),
(412, 'truck', 66, 0, NULL, 'KAT 130 B', 'Truck Account Activated', 1, '2020-10-21 09:17:45'),
(413, 'truck', 66, 0, NULL, 'UGX 112T', 'Truck Account Activated', 1, '2020-10-21 09:17:51'),
(414, 'driver', 66, 0, NULL, 'Esther Muthoni', 'Upload clear copy of ID', 0, '2020-10-21 09:19:55'),
(415, 'driver', 66, 0, NULL, 'Kiragu Kiragu', 'Driver Account Activated', 1, '2020-10-21 09:20:27'),
(416, 'driver', 66, 0, NULL, 'Esther Muthoni', 'Driver Account Activated', 0, '2020-10-21 09:24:39'),
(417, 'booking', 3, 16, 'ARW16', NULL, 'New booking', 1, '2020-10-21 09:34:09'),
(418, 'booking', 36, 16, 'ARW16', NULL, 'New booking', 0, '2020-10-21 09:34:09'),
(419, 'booking', 42, 16, 'ARW16', NULL, 'New booking', 0, '2020-10-21 09:34:09'),
(420, 'booking', 52, 16, 'ARW16', NULL, 'New booking', 1, '2020-10-21 09:34:09'),
(421, 'booking', 66, 16, 'ARW16', NULL, 'New booking', 1, '2020-10-21 09:34:09'),
(422, 'booking', 1, 16, 'ARW16', NULL, 'New booking', 1, '2020-10-21 09:34:09'),
(423, 'booking', 28, 16, 'ARW16', NULL, 'New booking', 0, '2020-10-21 09:34:09'),
(424, 'booking', 29, 16, 'ARW16', NULL, 'New booking', 0, '2020-10-21 09:34:09'),
(425, 'booking', 41, 16, 'ARW16', NULL, 'New booking', 0, '2020-10-21 09:34:09'),
(426, 'booking', 46, 16, 'ARW16', NULL, 'New booking', 0, '2020-10-21 09:34:09'),
(427, 'booking', 63, 16, 'ARW16', NULL, 'Requested booking', 1, '2020-10-21 09:39:34'),
(428, 'booking', 1, 16, 'ARW16', NULL, 'Requested booking', 1, '2020-10-21 09:39:34'),
(429, 'booking', 28, 16, 'ARW16', NULL, 'Requested booking', 0, '2020-10-21 09:39:34'),
(430, 'booking', 29, 16, 'ARW16', NULL, 'Requested booking', 0, '2020-10-21 09:39:34'),
(431, 'booking', 41, 16, 'ARW16', NULL, 'Requested booking', 0, '2020-10-21 09:39:34'),
(432, 'booking', 46, 16, 'ARW16', NULL, 'Requested booking', 0, '2020-10-21 09:39:34'),
(433, 'booking', 3, 16, 'ARW16', NULL, 'Confirmed booking', 1, '2020-10-21 09:40:44'),
(434, 'booking', 1, 16, 'ARW16', NULL, 'Confirmed booking', 1, '2020-10-21 09:40:44'),
(435, 'booking', 28, 16, 'ARW16', NULL, 'Confirmed booking', 0, '2020-10-21 09:40:44'),
(436, 'booking', 29, 16, 'ARW16', NULL, 'Confirmed booking', 0, '2020-10-21 09:40:44'),
(437, 'booking', 41, 16, 'ARW16', NULL, 'Confirmed booking', 0, '2020-10-21 09:40:44'),
(438, 'booking', 46, 16, 'ARW16', NULL, 'Confirmed booking', 0, '2020-10-21 09:40:44'),
(439, 'truck', 3, 0, NULL, 'UXP 123T', 'Truck Account Activated', 1, '2020-10-21 09:42:26'),
(440, 'booking', 5, 16, 'ARW16', NULL, 'New Driver Booking', 1, '2020-10-21 09:47:38'),
(441, 'booking', 63, 16, 'ARW16', NULL, 'Accepted booking', 1, '2020-10-21 09:47:38'),
(442, 'booking', 1, 16, 'ARW16', NULL, 'Accepted booking', 1, '2020-10-21 09:47:38'),
(443, 'booking', 28, 16, 'ARW16', NULL, 'Accepted booking', 0, '2020-10-21 09:47:38'),
(444, 'booking', 29, 16, 'ARW16', NULL, 'Accepted booking', 0, '2020-10-21 09:47:38'),
(445, 'booking', 41, 16, 'ARW16', NULL, 'Accepted booking', 0, '2020-10-21 09:47:38'),
(446, 'booking', 46, 16, 'ARW16', NULL, 'Accepted booking', 0, '2020-10-21 09:47:38'),
(447, 'booking', 63, 16, 'ARW16', NULL, 'Incident booking', 1, '2020-10-21 09:52:14'),
(448, 'booking', 1, 16, 'ARW16', NULL, 'Incident booking', 1, '2020-10-21 09:52:14'),
(449, 'booking', 28, 16, 'ARW16', NULL, 'Incident booking', 0, '2020-10-21 09:52:14'),
(450, 'booking', 29, 16, 'ARW16', NULL, 'Incident booking', 0, '2020-10-21 09:52:14'),
(451, 'booking', 41, 16, 'ARW16', NULL, 'Incident booking', 0, '2020-10-21 09:52:14'),
(452, 'booking', 46, 16, 'ARW16', NULL, 'Incident booking', 0, '2020-10-21 09:52:14'),
(453, 'booking', 3, 16, 'ARW16', NULL, 'Incident booking', 1, '2020-10-21 09:52:14'),
(454, 'booking', 63, 16, 'ARW16', NULL, 'Incident resolved', 1, '2020-10-21 09:54:48'),
(455, 'booking', 1, 16, 'ARW16', NULL, 'Incident resolved', 1, '2020-10-21 09:54:48'),
(456, 'booking', 28, 16, 'ARW16', NULL, 'Incident resolved', 0, '2020-10-21 09:54:48'),
(457, 'booking', 29, 16, 'ARW16', NULL, 'Incident resolved', 0, '2020-10-21 09:54:48'),
(458, 'booking', 41, 16, 'ARW16', NULL, 'Incident resolved', 0, '2020-10-21 09:54:48'),
(459, 'booking', 46, 16, 'ARW16', NULL, 'Incident resolved', 0, '2020-10-21 09:54:48'),
(460, 'booking', 2, 17, 'ARW17', NULL, 'New booking', 0, '2020-10-21 10:08:22'),
(461, 'booking', 66, 17, 'ARW17', NULL, 'New booking', 1, '2020-10-21 10:08:22'),
(462, 'booking', 3, 17, 'ARW17', NULL, 'New booking', 1, '2020-10-21 10:08:22'),
(463, 'booking', 1, 17, 'ARW17', NULL, 'New booking', 1, '2020-10-21 10:08:22'),
(464, 'booking', 38, 17, 'ARW17', NULL, 'New booking', 1, '2020-10-21 10:08:22'),
(465, 'booking', 65, 17, 'ARW17', NULL, 'Requested booking', 1, '2020-10-21 10:12:06'),
(466, 'booking', 1, 17, 'ARW17', NULL, 'Requested booking', 1, '2020-10-21 10:12:06'),
(467, 'booking', 38, 17, 'ARW17', NULL, 'Requested booking', 1, '2020-10-21 10:12:06'),
(468, 'booking', 66, 17, 'ARW17', NULL, 'Confirmed booking', 1, '2020-10-21 10:13:06'),
(469, 'booking', 1, 17, 'ARW17', NULL, 'Confirmed booking', 1, '2020-10-21 10:13:06'),
(470, 'booking', 38, 17, 'ARW17', NULL, 'Confirmed booking', 1, '2020-10-21 10:13:06'),
(471, 'booking', 68, 17, 'ARW17', NULL, 'New Driver Booking', 1, '2020-10-21 10:14:00'),
(472, 'booking', 65, 17, 'ARW17', NULL, 'Accepted booking', 1, '2020-10-21 10:14:00'),
(473, 'booking', 1, 17, 'ARW17', NULL, 'Accepted booking', 1, '2020-10-21 10:14:00'),
(474, 'booking', 38, 17, 'ARW17', NULL, 'Accepted booking', 1, '2020-10-21 10:14:00'),
(475, 'driver', 66, 0, NULL, 'Tutan', 'DL Copy is not clear', 0, '2020-10-21 10:37:25'),
(476, 'truck', 47, 0, NULL, 'KCG 896A', 'Truck Account Activated', 0, '2020-11-02 14:42:04'),
(477, 'truck', 47, 0, NULL, 'KCV 961H', 'Truck Account Activated', 0, '2020-11-02 14:44:06'),
(478, 'driver', 47, 0, NULL, 'CYRUS MUNYIRI', 'Driver Account Activated', 0, '2020-11-02 14:45:18'),
(479, 'driver', 47, 0, NULL, 'SIMON MUGOH', 'Driver Account Activated', 0, '2020-11-02 14:46:05'),
(480, 'driver', 47, 0, NULL, 'JOSEPH KINYANJUI', 'Driver Account Activated', 0, '2020-11-02 14:46:53'),
(481, 'booking', 50, 2, 'ARW2', NULL, 'Requested booking', 1, '2020-11-05 14:05:34'),
(482, 'booking', 1, 2, 'ARW2', NULL, 'Requested booking', 1, '2020-11-05 14:05:34'),
(483, 'booking', 28, 2, 'ARW2', NULL, 'Requested booking', 0, '2020-11-05 14:05:34'),
(484, 'booking', 29, 2, 'ARW2', NULL, 'Requested booking', 0, '2020-11-05 14:05:34'),
(485, 'booking', 41, 2, 'ARW2', NULL, 'Requested booking', 0, '2020-11-05 14:05:34'),
(486, 'booking', 46, 2, 'ARW2', NULL, 'Requested booking', 0, '2020-11-05 14:05:34'),
(487, 'profile', 75, 0, NULL, NULL, 'Upload clear picture of your ID', 1, '2020-11-10 15:14:15'),
(488, 'profile', 75, 0, NULL, NULL, 'Account Activated', 1, '2020-11-10 15:28:27'),
(489, 'booking', 3, 18, 'ARW18', NULL, 'New booking', 1, '2020-11-10 16:43:28'),
(490, 'booking', 36, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(491, 'booking', 42, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(492, 'booking', 52, 18, 'ARW18', NULL, 'New booking', 1, '2020-11-10 16:43:28'),
(493, 'booking', 47, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(494, 'booking', 66, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(495, 'booking', 1, 18, 'ARW18', NULL, 'New booking', 1, '2020-11-10 16:43:28'),
(496, 'booking', 28, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(497, 'booking', 29, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(498, 'booking', 41, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(499, 'booking', 46, 18, 'ARW18', NULL, 'New booking', 0, '2020-11-10 16:43:28'),
(500, 'booking', 61, 18, 'ARW18', NULL, 'Requested booking', 1, '2020-11-10 16:52:37'),
(501, 'booking', 1, 18, 'ARW18', NULL, 'Requested booking', 1, '2020-11-10 16:52:37'),
(502, 'booking', 28, 18, 'ARW18', NULL, 'Requested booking', 0, '2020-11-10 16:52:37'),
(503, 'booking', 29, 18, 'ARW18', NULL, 'Requested booking', 0, '2020-11-10 16:52:37'),
(504, 'booking', 41, 18, 'ARW18', NULL, 'Requested booking', 0, '2020-11-10 16:52:37'),
(505, 'booking', 46, 18, 'ARW18', NULL, 'Requested booking', 0, '2020-11-10 16:52:37'),
(506, 'booking', 3, 18, 'ARW18', NULL, 'Confirmed booking', 1, '2020-11-10 17:06:07'),
(507, 'booking', 1, 18, 'ARW18', NULL, 'Confirmed booking', 1, '2020-11-10 17:06:07'),
(508, 'booking', 28, 18, 'ARW18', NULL, 'Confirmed booking', 0, '2020-11-10 17:06:07'),
(509, 'booking', 29, 18, 'ARW18', NULL, 'Confirmed booking', 0, '2020-11-10 17:06:07'),
(510, 'booking', 41, 18, 'ARW18', NULL, 'Confirmed booking', 0, '2020-11-10 17:06:07'),
(511, 'booking', 46, 18, 'ARW18', NULL, 'Confirmed booking', 0, '2020-11-10 17:06:07'),
(512, 'booking', 5, 18, 'ARW18', NULL, 'New Driver Booking', 1, '2020-11-10 17:26:08'),
(513, 'booking', 61, 18, 'ARW18', NULL, 'Accepted booking', 1, '2020-11-10 17:26:08'),
(514, 'booking', 1, 18, 'ARW18', NULL, 'Accepted booking', 1, '2020-11-10 17:26:08'),
(515, 'booking', 28, 18, 'ARW18', NULL, 'Accepted booking', 0, '2020-11-10 17:26:08'),
(516, 'booking', 29, 18, 'ARW18', NULL, 'Accepted booking', 0, '2020-11-10 17:26:08'),
(517, 'booking', 41, 18, 'ARW18', NULL, 'Accepted booking', 0, '2020-11-10 17:26:08'),
(518, 'booking', 46, 18, 'ARW18', NULL, 'Accepted booking', 0, '2020-11-10 17:26:08'),
(519, 'booking', 3, 19, 'ARW19', NULL, 'New booking', 1, '2020-11-10 18:08:57'),
(520, 'booking', 36, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(521, 'booking', 42, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(522, 'booking', 52, 19, 'ARW19', NULL, 'New booking', 1, '2020-11-10 18:08:57'),
(523, 'booking', 47, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(524, 'booking', 66, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(525, 'booking', 1, 19, 'ARW19', NULL, 'New booking', 1, '2020-11-10 18:08:57'),
(526, 'booking', 28, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(527, 'booking', 29, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(528, 'booking', 41, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(529, 'booking', 46, 19, 'ARW19', NULL, 'New booking', 0, '2020-11-10 18:08:57'),
(530, 'booking', 3, 20, 'ARW20', NULL, 'New booking', 1, '2020-11-12 14:34:43'),
(531, 'booking', 36, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(532, 'booking', 42, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(533, 'booking', 52, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(534, 'booking', 47, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(535, 'booking', 66, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(536, 'booking', 1, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(537, 'booking', 28, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(538, 'booking', 29, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(539, 'booking', 41, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(540, 'booking', 46, 20, 'ARW20', NULL, 'New booking', 0, '2020-11-12 14:34:43'),
(541, 'booking', 4, 20, 'ARW20', NULL, 'Requested booking', 1, '2020-11-12 14:37:24'),
(542, 'booking', 1, 20, 'ARW20', NULL, 'Requested booking', 0, '2020-11-12 14:37:24'),
(543, 'booking', 28, 20, 'ARW20', NULL, 'Requested booking', 0, '2020-11-12 14:37:24'),
(544, 'booking', 29, 20, 'ARW20', NULL, 'Requested booking', 0, '2020-11-12 14:37:24'),
(545, 'booking', 41, 20, 'ARW20', NULL, 'Requested booking', 0, '2020-11-12 14:37:24'),
(546, 'booking', 46, 20, 'ARW20', NULL, 'Requested booking', 0, '2020-11-12 14:37:24'),
(547, 'booking', 3, 20, 'ARW20', NULL, 'Confirmed booking', 1, '2020-11-12 14:39:13'),
(548, 'booking', 1, 20, 'ARW20', NULL, 'Confirmed booking', 0, '2020-11-12 14:39:13'),
(549, 'booking', 28, 20, 'ARW20', NULL, 'Confirmed booking', 0, '2020-11-12 14:39:13'),
(550, 'booking', 29, 20, 'ARW20', NULL, 'Confirmed booking', 0, '2020-11-12 14:39:13'),
(551, 'booking', 41, 20, 'ARW20', NULL, 'Confirmed booking', 0, '2020-11-12 14:39:13'),
(552, 'booking', 46, 20, 'ARW20', NULL, 'Confirmed booking', 0, '2020-11-12 14:39:13'),
(553, 'booking', 5, 20, 'ARW20', NULL, 'New Driver Booking', 1, '2020-11-12 14:42:20'),
(554, 'booking', 4, 20, 'ARW20', NULL, 'Accepted booking', 0, '2020-11-12 14:42:20'),
(555, 'booking', 1, 20, 'ARW20', NULL, 'Accepted booking', 0, '2020-11-12 14:42:20'),
(556, 'booking', 28, 20, 'ARW20', NULL, 'Accepted booking', 0, '2020-11-12 14:42:20'),
(557, 'booking', 29, 20, 'ARW20', NULL, 'Accepted booking', 0, '2020-11-12 14:42:20'),
(558, 'booking', 41, 20, 'ARW20', NULL, 'Accepted booking', 0, '2020-11-12 14:42:20'),
(559, 'booking', 46, 20, 'ARW20', NULL, 'Accepted booking', 0, '2020-11-12 14:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `users_office_partner`
--

CREATE TABLE `users_office_partner` (
  `up_id` int(11) NOT NULL,
  `partner_user_id` int(11) NOT NULL,
  `office_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_office_partner`
--

INSERT INTO `users_office_partner` (`up_id`, `partner_user_id`, `office_user_id`) VALUES
(1, 28, 41),
(2, 1, 29),
(3, 1, 46);

-- --------------------------------------------------------

--
-- Table structure for table `users_pwd_reset`
--

CREATE TABLE `users_pwd_reset` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `active_code` int(11) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_pwd_reset`
--

INSERT INTO `users_pwd_reset` (`id`, `users_id`, `active_code`, `date_time`, `status`) VALUES
(1, 54, 596150, '2020-10-26 11:45:44', 1),
(2, 30, 770312, '2020-10-22 11:59:44', 1),
(3, 54, 478473, '2020-10-26 11:45:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_body_type`
--

CREATE TABLE `vehicle_body_type` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `body_type` varchar(49) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vehicle_body_type`
--

INSERT INTO `vehicle_body_type` (`id`, `delete_status`, `body_type`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, 'flat beds', '2020-04-18 19:43:18', 1, NULL, 0, NULL, 0),
(2, 0, 'tippers', '2020-04-18 19:44:01', 1, NULL, 0, NULL, 0),
(3, 0, 'curtain-siders', '2020-04-18 19:44:23', 1, NULL, 0, NULL, 0),
(4, 0, 'box bodies', '2020-04-18 19:44:58', 1, NULL, 0, NULL, 0),
(5, 0, 'dump body', '2020-04-18 19:48:44', 1, '2020-06-11 17:54:16', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_feet`
--

CREATE TABLE `vehicle_feet` (
  `id` int(11) NOT NULL,
  `delete_status` int(11) NOT NULL,
  `vehicle_feet` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicle_feet`
--

INSERT INTO `vehicle_feet` (`id`, `delete_status`, `vehicle_feet`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 1, 0, '2020-04-21 09:53:17', 1, NULL, 0, '2020-06-28 17:53:56', 1),
(2, 0, 14, '2020-04-21 09:53:35', 1, '2020-06-28 17:54:12', 1, NULL, 0),
(3, 0, 17, '2020-04-21 09:53:49', 1, '2020-06-28 17:53:44', 1, NULL, 0),
(4, 0, 20, '2020-04-21 09:54:04', 1, '2020-06-28 17:52:53', 1, NULL, 0),
(6, 0, 25, '2020-04-21 09:55:30', 1, '2020-06-28 17:52:41', 1, NULL, 0),
(7, 1, 22, '2020-04-21 09:55:51', 1, NULL, 0, '2020-06-28 17:52:12', 1),
(8, 1, 28, '2020-04-21 11:22:06', 1, NULL, 0, '2020-06-28 17:51:58', 1),
(9, 1, 32, '2020-04-22 12:39:25', 1, NULL, 0, '2020-06-28 17:51:44', 1),
(10, 0, 40, '2020-04-22 12:40:00', 1, NULL, 0, NULL, 0),
(11, 1, 9.5, '2020-04-22 12:40:35', 1, NULL, 0, '2020-06-28 17:50:58', 1),
(12, 1, 17.5, '2020-04-22 12:51:16', 1, NULL, 0, '2020-06-28 17:50:49', 1),
(13, 1, 50, '2020-04-23 14:16:33', 1, NULL, 0, '2020-06-28 17:50:36', 1),
(14, 0, 60, '2020-05-01 10:38:14', 1, '2020-06-11 17:23:56', 1, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `id` int(11) NOT NULL,
  `delete_status` int(1) NOT NULL,
  `vehicle_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `deleted_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Transport Vehicle Type';

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`id`, `delete_status`, `vehicle_type`, `created_at`, `created_user_id`, `updated_at`, `updated_user_id`, `deleted_at`, `deleted_user_id`) VALUES
(1, 0, 'Drop Side / High Sided', '2020-04-18 19:16:28', 1, '2020-07-23 17:56:01', 1, NULL, 0),
(2, 1, 'Straight Truck', '2020-04-18 19:18:16', 1, NULL, 0, '2020-07-23 17:55:42', 1),
(3, 0, 'Low Loader / Low Boy', '2020-04-18 19:19:03', 1, '2020-07-23 17:56:50', 1, NULL, 0),
(4, 1, 'Tail Lift Truck', '2020-04-18 19:20:03', 1, NULL, 0, '2020-07-23 17:55:36', 1),
(5, 0, 'Flatbed', '2020-04-18 19:20:27', 1, '2020-07-23 17:56:14', 1, NULL, 0),
(6, 0, 'Skeleton', '2020-04-18 19:21:18', 1, '2020-07-23 17:57:01', 1, NULL, 0),
(7, 0, 'Covered High Cube', '2020-07-23 17:57:24', 1, NULL, 0, NULL, 0),
(8, 0, 'Covered Regular', '2020-07-23 17:57:38', 1, '2020-09-28 20:41:21', 1, NULL, 0),
(9, 0, 'Fiberglass Insulated', '2020-07-23 17:58:21', 1, NULL, 0, NULL, 0),
(10, 0, 'Refrigerated', '2020-07-23 17:58:42', 1, NULL, 0, NULL, 0),
(11, 0, 'Super Link', '2020-07-23 19:04:28', 1, NULL, 0, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`bank_code`);

--
-- Indexes for table `bank_branch`
--
ALTER TABLE `bank_branch`
  ADD PRIMARY KEY (`branch_code`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `booking_accident`
--
ALTER TABLE `booking_accident`
  ADD PRIMARY KEY (`accident_id`);

--
-- Indexes for table `booking_contact`
--
ALTER TABLE `booking_contact`
  ADD PRIMARY KEY (`booking_contact_id`);

--
-- Indexes for table `booking_item`
--
ALTER TABLE `booking_item`
  ADD PRIMARY KEY (`booking_item_id`);

--
-- Indexes for table `booking_proposal`
--
ALTER TABLE `booking_proposal`
  ADD PRIMARY KEY (`booking_proposal_id`);

--
-- Indexes for table `booking_transporter`
--
ALTER TABLE `booking_transporter`
  ADD PRIMARY KEY (`booking_transporter_id`);

--
-- Indexes for table `cargo_agreement`
--
ALTER TABLE `cargo_agreement`
  ADD PRIMARY KEY (`cargo_agreement_id`);

--
-- Indexes for table `cargo_packaging`
--
ALTER TABLE `cargo_packaging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargo_type`
--
ALTER TABLE `cargo_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_code`);

--
-- Indexes for table `commission_rate`
--
ALTER TABLE `commission_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_code`);

--
-- Indexes for table `credit_term`
--
ALTER TABLE `credit_term`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `driver_details`
--
ALTER TABLE `driver_details`
  ADD PRIMARY KEY (`driver_code`);

--
-- Indexes for table `driver_documents`
--
ALTER TABLE `driver_documents`
  ADD PRIMARY KEY (`driver_doc_code`);

--
-- Indexes for table `incomplete_booking`
--
ALTER TABLE `incomplete_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomplete_booking_item`
--
ALTER TABLE `incomplete_booking_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interest_rate`
--
ALTER TABLE `interest_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_credit_days`
--
ALTER TABLE `payment_credit_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_mode`
--
ALTER TABLE `payment_mode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_term`
--
ALTER TABLE `payment_term`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_payment_term`
--
ALTER TABLE `post_payment_term`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rate_card`
--
ALTER TABLE `rate_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_commission`
--
ALTER TABLE `sub_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax_rate`
--
ALTER TABLE `tax_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tonnage`
--
ALTER TABLE `tonnage`
  ADD PRIMARY KEY (`tonnage_code`);

--
-- Indexes for table `trailer_details`
--
ALTER TABLE `trailer_details`
  ADD PRIMARY KEY (`trailer_id`);

--
-- Indexes for table `transporter_agreement`
--
ALTER TABLE `transporter_agreement`
  ADD PRIMARY KEY (`transporter_agreement_id`);

--
-- Indexes for table `truck_details`
--
ALTER TABLE `truck_details`
  ADD PRIMARY KEY (`truck_id`);

--
-- Indexes for table `truck_documents`
--
ALTER TABLE `truck_documents`
  ADD PRIMARY KEY (`truck_doc_id`);

--
-- Indexes for table `truck_insurance`
--
ALTER TABLE `truck_insurance`
  ADD PRIMARY KEY (`insurance_id`);

--
-- Indexes for table `truck_locations`
--
ALTER TABLE `truck_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `truck_make`
--
ALTER TABLE `truck_make`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_bank_details`
--
ALTER TABLE `users_bank_details`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `users_commission`
--
ALTER TABLE `users_commission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_company_details`
--
ALTER TABLE `users_company_details`
  ADD PRIMARY KEY (`company_code`);

--
-- Indexes for table `users_contact_details`
--
ALTER TABLE `users_contact_details`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `users_corporate_director`
--
ALTER TABLE `users_corporate_director`
  ADD PRIMARY KEY (`director_id`);

--
-- Indexes for table `users_corporate_document`
--
ALTER TABLE `users_corporate_document`
  ADD PRIMARY KEY (`corporate_doc_id`);

--
-- Indexes for table `users_individual_document`
--
ALTER TABLE `users_individual_document`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `users_notifications`
--
ALTER TABLE `users_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_office_partner`
--
ALTER TABLE `users_office_partner`
  ADD PRIMARY KEY (`up_id`);

--
-- Indexes for table `users_pwd_reset`
--
ALTER TABLE `users_pwd_reset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_body_type`
--
ALTER TABLE `vehicle_body_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_feet`
--
ALTER TABLE `vehicle_feet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `bank_code` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `bank_branch`
--
ALTER TABLE `bank_branch`
  MODIFY `branch_code` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `booking_accident`
--
ALTER TABLE `booking_accident`
  MODIFY `accident_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `booking_contact`
--
ALTER TABLE `booking_contact`
  MODIFY `booking_contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `booking_item`
--
ALTER TABLE `booking_item`
  MODIFY `booking_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `booking_proposal`
--
ALTER TABLE `booking_proposal`
  MODIFY `booking_proposal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `booking_transporter`
--
ALTER TABLE `booking_transporter`
  MODIFY `booking_transporter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cargo_agreement`
--
ALTER TABLE `cargo_agreement`
  MODIFY `cargo_agreement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cargo_packaging`
--
ALTER TABLE `cargo_packaging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `cargo_type`
--
ALTER TABLE `cargo_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `commission_rate`
--
ALTER TABLE `commission_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `credit_term`
--
ALTER TABLE `credit_term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `driver_details`
--
ALTER TABLE `driver_details`
  MODIFY `driver_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `driver_documents`
--
ALTER TABLE `driver_documents`
  MODIFY `driver_doc_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `incomplete_booking`
--
ALTER TABLE `incomplete_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `incomplete_booking_item`
--
ALTER TABLE `incomplete_booking_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `interest_rate`
--
ALTER TABLE `interest_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `payment_credit_days`
--
ALTER TABLE `payment_credit_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_mode`
--
ALTER TABLE `payment_mode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_term`
--
ALTER TABLE `payment_term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `post_payment_term`
--
ALTER TABLE `post_payment_term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rate_card`
--
ALTER TABLE `rate_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sub_commission`
--
ALTER TABLE `sub_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tax_rate`
--
ALTER TABLE `tax_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tonnage`
--
ALTER TABLE `tonnage`
  MODIFY `tonnage_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `trailer_details`
--
ALTER TABLE `trailer_details`
  MODIFY `trailer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `transporter_agreement`
--
ALTER TABLE `transporter_agreement`
  MODIFY `transporter_agreement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `truck_details`
--
ALTER TABLE `truck_details`
  MODIFY `truck_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `truck_documents`
--
ALTER TABLE `truck_documents`
  MODIFY `truck_doc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `truck_insurance`
--
ALTER TABLE `truck_insurance`
  MODIFY `insurance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `truck_locations`
--
ALTER TABLE `truck_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `truck_make`
--
ALTER TABLE `truck_make`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `users_bank_details`
--
ALTER TABLE `users_bank_details`
  MODIFY `bank_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `users_commission`
--
ALTER TABLE `users_commission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users_company_details`
--
ALTER TABLE `users_company_details`
  MODIFY `company_code` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `users_contact_details`
--
ALTER TABLE `users_contact_details`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `users_corporate_director`
--
ALTER TABLE `users_corporate_director`
  MODIFY `director_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users_corporate_document`
--
ALTER TABLE `users_corporate_document`
  MODIFY `corporate_doc_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users_individual_document`
--
ALTER TABLE `users_individual_document`
  MODIFY `document_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users_notifications`
--
ALTER TABLE `users_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=560;

--
-- AUTO_INCREMENT for table `users_office_partner`
--
ALTER TABLE `users_office_partner`
  MODIFY `up_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users_pwd_reset`
--
ALTER TABLE `users_pwd_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `vehicle_body_type`
--
ALTER TABLE `vehicle_body_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vehicle_feet`
--
ALTER TABLE `vehicle_feet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
