<?php



session_start();
require_once 'init.php';
use CoinGate\CoinGate;
// use DB;


$orderid = uniqid();

if (isset($_GET['crowdfund']) && $_GET['crowdfund'] == 1) {
    $_SESSION['crowdfund'] = 'true';
    $_SESSION['crowdfund_mission_id'] = $_GET['mission'];
    $_SESSION['crowdfund_amount'] = $_GET['crowdfund_amount'];
    $post_params = array(
        'order_id' => $orderid,
        'price_amount' => $_GET['crowdfund_amount'],
        'price_currency' => 'USD',
        'receive_currency' => 'EUR',
        'callback_url' => 'https://platform.gotem.io/coingate/call_back.php',
        'cancel_url' => 'https://platform.gotem.io/pcoingate/cancel',
        'success_url' => 'https://platform.gotem.io/pcoingate/success',
        'title' => 'Participate in Crowdfund',
        'description' => 'Participate in Crowdfund',
    );

    $_SESSION['post_params'] = $post_params;
    function getGoodAuthentication() {
        return array(
            'auth_token' => 'zNBAnNco2ZMpjs4ap_-dsa7XvMGZssDvJuqcRRJQ',
            'environment' => 'sandbox',
        );
    }

    $order = CoinGate::request('/orders', 'POST', $post_params, getGoodAuthentication());
// echo '<pre>';
    // echo $order["payment_url"];
    // print_r($order);
    // $u = DB::table('users')->first();
    // die('here');

    header('location:' . $order["payment_url"]);

} else {
    
    $_SESSION['crowdfund'] = 'false';
    $_SESSION['c_order_id'] = $orderid;
    $post_params = array(
        'order_id' => $orderid,
        'price_amount' => $_SESSION['mission_amount'],
        'price_currency' => 'USD',
        'receive_currency' => 'EUR',
        'callback_url' => 'https://platform.gotem.io/coingate/call_back.php',
        'cancel_url' => 'https://platform.gotem.io/pcoingate/cancel',
        'success_url' => 'https://platform.gotem.io/pcoingate/success',
        'title' => $_SESSION['mission_title'],
        'description' => $_SESSION['mission_description'],
    );
    $_SESSION['post_params'] = $post_params;
    function getGoodAuthentication() {
        return array(
            'auth_token' => 'zNBAnNco2ZMpjs4ap_-dsa7XvMGZssDvJuqcRRJQ',
            'environment' => 'sandbox',
        );
    }

    $order = CoinGate::request('/orders', 'POST', $post_params, getGoodAuthentication());
// echo '<pre>';
    // echo $order["payment_url"];
    // print_r($order);
    // $u = DB::table('users')->first();
    // die('here');

    header('location:' . $order["payment_url"]);

}
