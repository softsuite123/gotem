<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model {

	protected $table = 'profiles';

	protected $fillable = ['user_id', 'name','country_id','state_id','city_id','avatar', 'created_at', 'updated_at'];

    public function country() {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }
     public function State() {
        return $this->hasOne(States::class, 'id', 'state_id');
    }
     public function City() {
        return $this->hasOne(Cities::class, 'id', 'city_id');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

}
