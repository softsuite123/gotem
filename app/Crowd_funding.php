<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crowd_funding extends Model
{
     protected $table = 'crowd_funding';
     
     public function Mission_Title() {
        return $this->hasOne(Mission::class, 'id', 'mission_id' );
    }
}
