<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\project as Project;

class Mission extends Model
{
    public function company()
    {
        return $this->belongsTo(Company::class,'mission_company_id');
    }

    public function offers() {
        return $this->hasMany(Project::class , 'mission_id' , 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
