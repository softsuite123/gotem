<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank_account extends Model
{
    protected $table="bank_accounts";
    
    
    public function User_Name()
    {
        return $this->hasOne(User::class, 'id','user_id');
    }
}
