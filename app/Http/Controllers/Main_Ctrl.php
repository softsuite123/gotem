<?php

namespace App\Http\Controllers;

use App\Applicant;
use App\Company;
use App\Milestone;
use App\Mission;
use App\project;
use App\User;
use DB;
use App\Country;
use Auth;
use Illuminate\Http\Request;

class Main_Ctrl extends Controller {

    public function search() {
        // dd(request()->all());
         $current_date = date('Y-m-d');
        $country         = request('country');
        $company         = request('company');
        $mission_privacy = request('mission_privacy');

        if ($mission_privacy!="" && $country=="" && $company=="") {
           $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->Where('mission_privacy',$mission_privacy)->get()->paginate(12);
       }
       elseif ($mission_privacy!="" && $country=="" && $company!="") {
         $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->Where('mission_privacy',$mission_privacy)->Where('mission_company_id',$company)->get()->paginate(12);
     }
          elseif ($mission_privacy!="" && $country!="" && $company=="") {
         $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->Where('mission_privacy',$mission_privacy)->where('address','like','%'.$country.'%')->get()->paginate(12);
     }
     elseif ($mission_privacy!="" && $country!="" && $company!="") {
         $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->where('address','like','%'.$country.'%')->Where('mission_privacy','Private')->Where('mission_company_id',$company)->get()->paginate(12);
     }
     elseif ($mission_privacy=="" && $country!="" && $company=="") {
         $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->where('address','like','%'.$country.'%')->get()->paginate(12);
     }
      elseif ($mission_privacy=="" && $country!="" && $company!="") {
         $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->where('address','like','%'.$country.'%')->Where('mission_company_id',$company)->get()->paginate(12);
     }
          elseif ($mission_privacy=="" && $country=="" && $company!="") {
         $missions = Mission::where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->Where('mission_company_id',$company)->get()->paginate(12);
     }
  else{
         $missions = Mission::all()->where('files_share_with_public','=','1')->where('deadline','>=',$current_date)->paginate(12);
     }


        // // echo $country."**".$company."**".$missiontype;exit();
        // $missions = DB::select("SELECT * FROM `missions` WHERE address LIKE '%$country%' OR mission_company_id='$company' OR mission_privacy LIKE '%$missiontype%'");
        // $missions = Mission::where('address','like','%'.$country.'%')->orWhere('mission_privacy','Private')->orWhere('mission_company_id',$company)->get()->paginate(12);



     $companies = Company::all();
     $countries = Country::all();
     return view('freelancer.browse-mission', compact('missions', 'companies','countries'));

 }
    public function showMission($slug) {
        // dd('here');
        $data = explode('-', $slug);
        $mission_id = end($data);
        $data['mission'] = Mission::findOrFail($mission_id);
        $data['current_date'] = date('Y-m-d');
        // dd($mission);
        return view('general.single-mission-case', $data);
    }

    public function mymissions_offers($mission_id)
    {
        $user_id = Auth::User()->id;
        $offers = project::where('mission_id', $mission_id)->where('view_offer',1)->get();
//        dd($data['offers']);
        return view('employer.view_offer', ['offers' => $offers]);
    }

    public function applyMission(Mission $mission,Request $r) {
        // dd(request()->all());
        //dd($r->offer_amount);
        request()->validate([
            'message' => 'required',
        ]);
        // if (request('make_offer')) {
        //     request()->validate([
        //         'offer_amount' => 'required|integer',
        //     ]);
        // }
        // if (request('milestone')) {
        //     request()->validate([
        //         'milestone' => 'required',
        //         'amount' => 'required',
        //     ]);
        // }
        // dd(request('offer_amount'));
        $is_applied = Applicant::where('mission_id', $mission->id)->where('applicant_id', \Auth::id())->first();


        if ($is_applied!="") {
            return back()->with('error', 'You have already applied');
        }
        $is_verified = auth()->user()->is_verified;
        // dd($is_verified);
        // dd();

        if ($is_verified!=1) {
            // dd("sdsds");
            return back()->with('error', 'Please verify your identity first');
        }
        $is_freelancer = auth()->user()->user_type == \App\User::USER_FREELANCER ? true : false;

        // if (!$is_freelancer) {
        //     return back()->with('error', 'You are not authorized to apply');
        // }

        DB::beginTransaction();
        // if(!$is_applied && $is_verified && $is_freelancer){
        $project = new project();
        $project->employer_id = $mission->user_id;
        $project->mission_id = $mission->id;
        $project->applicant_id = \Auth::id();
        $project->message = request('message');
        if (request('make_offer')) {
            $project->is_offer = true;
            $project->offer_amount = request('offer_amount');
        }

        if (request('milestones')) {
            $project->is_milestone = true;
            $project->save();
            // dd($project->id);

            $milestones = request('milestone');
            // dd($milestones);
            $amount = request('amount');
// dd($milestones);
            foreach ($milestones as $key => $value) {
                $milestoneModel = new Milestone();
                $milestoneModel->project_id = $project->id;
                $milestoneModel->milestone_title = $value;
                $milestoneModel->milestone_amount = $amount[$key];
                $milestoneModel->description = $r->description[$key];
                $milestoneModel->timer = $r->timer[$key];
                $milestoneModel->save();
                // dd($milestoneModel);
            }
        }
        if (!request('is_milestone')) {
            $project->save();
        }

        $applicant = new Applicant();
        $applicant->mission_id = $mission->id;
        $applicant->employer_id = $mission->user_id;
        $applicant->applicant_id = \Auth::id();
        $applicant->save();
        // }
        $user = $r->user_id;
        $master = $r->mission_id_notify;
        $title=  "Mission Applied:".$r->title;

    $sql="INSERT INTO notifications(title,status,type,source,master,user) VALUES('$title','0','1','Apply Mission','$master','$user')";
    DB::insert($sql);



        DB::commit();
        return redirect()->route('freelancer.dashboard')->with('success', 'Application submitted successfully');
    }

    public function settings() {
        return view('general.settings');
    }

    public function postsettings() {
        $user = auth()->user();
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'unique:users,email,' . $user->id,
            'account_type' => 'required',
        ]);

        $user->name = request('first_name') . ' ' . request('last_name');
        $user->email = request('email');
        $user->user_type = request('account_type');
        $user->save();

        return back()->with('success', 'Settings saved successfully');
    }

    public function withdraw() {

        request()->validate([
            'amount' => 'required|numeric',
        ]);

        if (request('amount') < 10) {
            return back()->with('error', 'Min amount is 10 to withdraw');
        }

        if (auth()->user()->user_type == User::USER_FREELANCER) {
            $balance = 0;
            $withdraw = 0;

            $orders = DB::table('orders')->where('applicant_id', \Auth::id())->where('status', 'accepted')->get();
            foreach ($orders as $order) {
                $balance += $order->amount;
                //Balance Amount
            }

            $withdraws = DB::table('withdraw_history')->where('user_id', \Auth::id())->where('status', '!=', 'rejected')->get();
            // dd($withdraws);
            if($withdraws){
                 foreach ($withdraws as $withdrawl) {
                $withdraw += $withdrawl->amount;
             }
            }

            $userBalance = $balance - $withdraw - ($balance * .2);

            if ($userBalance < request('amount')) {
                return back()->with('error', 'Insufficient funds');
            }

            DB::table('withdraw_history')->insert(['user_id' => \Auth::id(), 'amount' => request('amount'), 'hash' => request('hash')]);

            return back()->with('success', 'Withdraw Request sent successfully');
        }
    }
    
    public function employer_withdraw(Request $req) {
        
        request()->validate([
            'amount' => 'required|numeric',
            
        ]);
        
        if (request('amount') < 10) {
            return back()->with('error', 'Min amount is 10 to withdraw');
            
        }

        // if (auth()->user()->user_type == User::USER_FREELANCER) {
            $balance = 0;
            $withdraw = 0;
            $data['account']=DB::table('bank_accounts')->where('user_id', \Auth::id())->first();
            $orders = DB::table('orders')->where('applicant_id', \Auth::id())->where('status', 'accepted')->get();
            foreach ($orders as $order) {
                $balance += $order->amount;
                //Balance Amount
            }
            $data['balance']=$balance;
            $withdraws = DB::table('withdraw_history')->where('user_id', \Auth::id())->where('status', '!=', 'rejected')->get();
            // dd($withdraws);
            if($withdraws){
                 foreach ($withdraws as $withdrawl) {
                $withdraw += $withdrawl->amount;
             }
            }

            $data['userBalance']=$userBalance = $balance - $withdraw - ($balance * .2);

            if ($userBalance < request('amount')) {
                return redirect()->route('balance',$data)->with('error', 'Insufficient funds');
            }

            DB::table('withdraw_history')->insert(['user_id' => \Auth::id(), 'amount' => request('amount'), 'hash' => request('hash')]);

            return view('employer.balance',$data)->with('success', 'Withdraw Request sent successfully');
        // }
    }




    public function get_notifications(Request $r)
    {
        $user_id = Auth::user()->id;
        $user_type=Auth::user()->user_type;
        $limit=0;
          $li="";$sql="";
          if ($r->a==1) {
              $limit="LIMIT 5";
          }
          else{
            $limit="";
          }
        if ($user_type==1) {
           $sql="SELECT * FROM notifications WHERE type='1' && user='$user_id' ORDER BY created_at DESC $limit";
            $res=DB::select($sql);
              foreach ($res   as  $value) {
            $li .= " <li class='notifications-not-read'>";
            $li .="<a href='../employer/view_offer'>";
            $li .=" <span class='notification-icon'>";
            $li .="<i class='icon-material-outline-group'></i></span>";
            $li .=" <span class='notification-text'>";
           $li .="<strong>".$value->source.":</strong>".$value->title."<span class='color'></span>";
            $li .="</span></a></li>";
        }
            DB::update("UPDATE notifications SET status='1' WHERE user='$user_id'");
            echo $li;
        }
        else{

   $sql = "SELECT * FROM notifications  WHERE type='2'  ORDER BY created_at DESC  $limit";
                $res=DB::select($sql);
        foreach ($res   as  $value) {
            $li .= " <li class='notifications-not-read'>";
            $li .="<a href='../mission/".str_slug($value->title).'-'."$value->master"."'>";
            $li .=" <span class='notification-icon'>";
            $li .="<i class='icon-material-outline-group'></i></span>";
            $li .=" <span class='notification-text'>";
            $li .="<strong>".$value->source.":</strong>".$value->title."<span class='color'></span>";
            $li .="</span></a></li>";
        }
        DB::update("UPDATE notifications SET status='1' WHERE type='2'");
            echo $li;

        }


    }






        public function get_notifications_count()
    {
        $user_id = Auth::user()->id;
          $user_type=Auth::user()->user_type;
          $sql="";
          if ($user_type==1) {
             $sql="SELECT COUNT(id) as total FROM notifications WHERE type='1' && user='$user_id' && status='0'";
            $res=DB::select($sql);
            echo $res[0]->total;
          }

          else{
             $sql = "SELECT COUNT(id) as total FROM notifications WHERE  status='0' && type='2' ";
                $res=DB::select($sql);
            echo $res[0]->total;
          }

    }

    public function del_employment_history($id)
    {
            DB::table('employment_histories')->where('id',$id)->delete();
             return back();
    }

}
