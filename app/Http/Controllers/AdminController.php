<?php

namespace App\Http\Controllers;
use App\Mission;
use App\Profile;
use App\User;
use App\Country;
use App\States;
use App\Cities;
use App\Company;
use Image;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;

class AdminController extends Controller {


    public function add_user()
    {
         $this->authorizeUser();
         $data['countries'] = Country::all();
         $data['states'] = states::all();
         $data['cities'] = cities::all();
         // $countries = Country::all();
        return view('admin.add_user',$data);
    }
    public function __construct() {
        $this->middleware('web')->except(['adminlogin']);
    }

    public function adminlogin() {
        return view('admin.login');
    }

    public function admindashboard() {
        $this->authorizeUser();
        return view('admin.dashboard');
    }

    public function adminall_users() {
        $this->authorizeUser();
        $users = User::all();

        return view('admin.all_users', ['users' => $users]);
    }

    public function adminall_cases() {
        $this->authorizeUser();
        $missions = Mission::orderByDesc('created_at')->get();

        return view('admin.all_cases', ['missions' => $missions]);
    }

    public function adminverify_request() {
        $this->authorizeUser();

        $requests = Profile::where('verification_submitted', true)->where('verification_status', 'pending')->get();

        return view('admin.verify_request', ['requests' => $requests]);
    }

    public function adminverify_profile(Profile $profile) {

        $this->authorizeUser();
        return view('admin.verify_profile', ['profile' => $profile]);
    }

    public function admin_approve_verified_account(Profile $profile) {

        $this->authorizeUser();
        $profile->verification_status = 'verified';
        $profile->save();
        $user = User::find($profile->user_id);

        $user->is_verified = true;
        $user->save();

        return redirect()->route('identity.requests')->with('success', 'Verified Successfully');
    }

    public function adminverified_account() {
        $this->authorizeUser();
        $verified_users = User::where('is_verified', true)->get();

        return view('admin.verified_account', ['users' => $verified_users]);
    }

    public function withdrawRequests() {
        $requests = DB::table('withdraw_history')->get();
        return view('admin.withdraw_requests', compact('requests'));
    }

    public function authorizeUser() {
        // dd(auth()->user()->user_type != 0);
        if (\Request::segment(1) == 'admin' && \Request::segment(2) == 'login') {
            return true;
        } elseif (auth()->user()->user_type != 0) {
            // dd('you are not authorized to see this page');
        }
    }

    public function acceptWithdrawRequest($id) {

        DB::table('withdraw_history')->where('id', $id)->update(['status' => 'accepted']);
        return back()->with('success', 'Withdraw request accepted successfully');
    }

    public function rejectWithdrawRequest($id) {

        DB::table('withdraw_history')->where('id', $id)->update(['status' => 'rejected']);
        return back()->with('error', 'Withdraw request rejected successfully');
    }
 public function load_states(Request $r)

    {
        // $this->authorizeUser();
        $id = $r->id;
        $states=states::all()->where('country_id',$id);
 
        $opt="<option value=''>Choose State</option>";
        foreach ($states as  $value) {
            $opt .="<option value='".$value->id."'>".$value->name."</option>";

        }
        echo $opt;
    }


    public function load_city(Request $r)

    {
        // $this->authorizeUser();
        $id = $r->id;
        $cities=cities::all()->where('state_id',$id);
 
        $opt="<option value=''>Choose City</option>";
        foreach ($cities as  $value) {

            $opt .="<option value='".$value->id."'>".$value->name."</option>";

        }
        echo $opt;
    }

    public function save_user(Request $r)
    {
        // echo $r->name." ".$r->email." ".$r->pass." ".$r->usertype." ".$r->country." ".$r->state." ".$r->city." ".$r->rate;
        $u = new User();
        $u->name = $r->name;
        $u->email = $r->email;
        $u->password = Hash::make($r->pass);
        $u->user_type = $r->usertype;
        $u->is_verified =1;
        $u->save();
        $master = $u->id;

        if ($r->hasFile('avatar')) 
         {
            request()->validate([
                'avatar' => 'required|image|mimes:jpg,jpeg,png',
            ]);
                    if($r->file('avatar')->isValid()) {
                    try {
                    $file = $r->file('avatar');
                    $name =  $file->getClientOriginalName();
                    $r->file('avatar')->move("public/files/avatars/", $name);
                    } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    }
                        }
        $avatarPath = 'public/files/avatars/'.$name;
        }


        $profile = new Profile();
        $profile->user_id = $master;
        $profile->name = $r->name;
        $profile->country_id = $r->country;
        $profile->state_id = $r->state;
        $profile->city_id = $r->city;
        $profile->hourly_rate = $r->rate;
        $profile->verification_submitted = 1;
        $profile->verification_status='verified';
    if($r->avatar)
    {
        $profile->avatar = $avatarPath;
}
        $profile->save();
        return back();

// avatar,
    }
    public function del_user($id)
    {
          $this->authorizeUser();
        DB::table('users')->where('id',$id)->delete();
        DB::table('profiles')->where('user_id',$id)->delete();
        return back();
    }

    public function add_mission()
    {
         $this->authorizeUser();
            $data['mission_com'] = DB::select("SELECT * FROM mission_companies");
           $data['users'] = User::all()->where('user_type',1);
         return view('admin.add_mission',$data);

    }

    public function add_new_mission(Request $request)
    {

         $request->validate([
            'title' => 'required',
            'mission_type' => 'required',
            'mission_company' => 'required',
            'address' => 'required',
            'estimated_budget' => 'required',
            'deadline' => 'required',
            'mission_privacy' => 'required',
            'mission_description' => 'required',
            'mission_objective' => 'required',
      
        ]);
               
        $mission = new Mission();

        DB::beginTransaction();
       
      
        $mission->title = $request->title;
      if ($request->user=='') {
              $mission->user_id = \Auth::id(); 
        }
        else{
              $mission->user_id = $request->user;  
        }
        $mission->mission_type = $request->mission_type;
        $mission->mission_company_id = $request->mission_company;
        if ($request->is_remote) {
            $mission->is_remote = true;
        }
        $mission->address = $request->address;
        $mission->estimated_budget = $request->estimated_budget;
        if ($request->is_urgent) {
            $mission->is_urgent = true;
        }
        $mission->deadline = $request->deadline;
        $mission->mission_privacy = $request->mission_privacy;
        $mission->mission_description = $request->mission_description;
        $mission->mission_objective = $request->mission_objective;
        $mission->latitude = $request->latitude;
        $mission->longitude = $request->longitude;
        if ($request->share_with_public) {
            $mission->files_share_with_public = true;
        }
        if ($request->enable_crowdfunding) {
            $mission->enable_crowdfunding = true;
        }
        if($request->min_raise_amount && $request->max_raise_amount) {
            $mission->min_raise_amount = $request->min_raise_amount;
            $mission->max_raise_amount = $request->max_raise_amount;
        }
      
        if ($request->allow_multiple_source_participate) {
            $mission->allow_multiple_source_participate = true;
        }

       $files = array();
    

 if ($request->hasFile('avatar')) 
         {
             $this->validate($request, [
                'avatar.*' => 'mimes:jpg,jpeg,png|max:2000',
            ], [
                'avatar.*.mimes' => 'Only jpeg, png, jpg and bmp images are allowed',
                'avatar.*.max' => 'Sorry! Maximum allowed size for an image is 2MB',
            ]);
                  
                            foreach (request('avatar') as $key => $value) {
                                $file = $request->file('avatar')[$key];
                                $name =  $file->getClientOriginalName();
                                $request->file('avatar')[$key]->move("public/files/avatars/", $name);
                                $files[] = 'public/files/avatars/'.$name;
                            }
                      
        }
        


       $files = json_encode($files);

       $mission->mission_files = $files;

        $mission->save();
        DB::commit();
        return back()->with('success', 'Mission Added Successfully');

    }


    public function edit_user($id)
    {
        $sql="SELECT users.id as id,users.name,users.email,profiles.country_id as cntryId,users.user_type,profiles.hourly_rate,profiles.avatar,profiles.state_id as state,profiles.city_id as city_id FROM users
LEFT JOIN profiles ON users.id=profiles.user_id
-- LEFT JOIN countries ON profiles.country_id=countries.id
-- LEFT JOIN states ON  profiles.state_id=states.id
-- LEFT JOIN cities ON profiles.city_id=cities.id
WHERE users.id=$id";
          $data['country'] = Country::all();
        $data['detail'] = DB::select($sql);
        return view('admin.edit_user',$data);
    }

        public function update_user(Request $r)
        {  

   


        if ($r->rem_img==1) {
         $a="";   
        }
        else{
                           if ($r->hasFile('avatar')) 
         {
    request()->validate([
        'avatar' => 'required|image|mimes:jpg,jpeg,png',
            ]);
            if($r->file('avatar')->isValid()) {
            try {
            $file = $r->file('avatar');
            $name =  $file->getClientOriginalName();
            $r->file('avatar')->move("public/files/avatars/", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
                }
        }
        
        $a = ",avatar="."'public/files/avatars/".$name."'";  
         unlink($r->pre_path);

        }
          // echo $a;

            DB::update("UPDATE users SET name='$r->name',email='$r->email',user_type='$r->usertype' WHERE id='$r->id'");
            DB::update("UPDATE profiles SET name='$r->name',country_id='$r->country',state_id='$r->state',city_id='$r->city',hourly_rate='$r->rate'$a WHERE user_id='$r->id'");
       return redirect()->route('allUsers')->with('success', 'Updated Successfully');
           
        }

         public function all_missions()
        {
$data['mission'] =DB::select("SELECT  missions.id as mid,users.name as username,missions.title,missions.mission_type,missions.estimated_budget as budget,mission_companies.name as com_name,missions.enable_crowdfunding  FROM `missions`
LEFT JOIN users ON users.id=missions.user_id
LEFT JOIN mission_companies ON mission_companies.id=missions.mission_company_id");
            return view('admin.all_missions',$data);
        }


        public function del_mission($id)
        {
             DB::table('missions')->where('id',$id)->delete();
             return back();
        }

        public function edit_mission($id)
        {
         $a=DB::select("SELECT users.name username,mission_companies.name as company,missions.*
FROM `missions` 
LEFT JOIN users ON users.id=missions.user_id
LEFT JOIN mission_companies ON missions.mission_company_id=mission_companies.id
WHERE missions.id='$id'");

          $data['mission_detail']=$a;
          $data['mission_com'] = DB::select("SELECT * FROM mission_companies");
          $data['users'] = User::all()->where('user_type',1);
          $data['images']=json_decode($a[0]->mission_files);

            return view('admin.edit_mission',$data);
        }

        public function update_mission(Request $r)
        {
           
        $arr = array();
        // if(isset($r('remv'))) {
        //     foreach ($r('remv') as $key => $value) {
        //         $p = $r->pre_path[$key];
        //         $im = $r->remv[$key];  
    
        //         if ($im==0) {
        //          unlink($r->pre_path[$key]);
        //         }
        //         else{
        //             $arr[] = $p;
        //         }
        //     }
        // }

             $files = array();

 if ($r->hasFile('avatar')) 
         {
         $this->validate($r, [
            'avatar.*' => 'mimes:jpg,jpeg,png|max:2000',
        ], [
            'avatar.*.mimes' => 'Only jpeg, png, jpg and bmp images are allowed',
            'avatar.*.max' => 'Sorry! Maximum allowed size for an image is 2MB',
        ]);
        
    
        foreach ($r('avatar') as $key => $value) {
            $file = $r->file('avatar')[$key];
            $name =  $file->getClientOriginalName();
            $r->file('avatar')[$key]->move("public/files/avatars/", $name);
            $arr[] = 'public/files/avatars/'.$name;
            }
                           
        }

       $files = json_encode($arr);
  
            if ($r->is_remote=='on') {
                $remote=1;
            }
            else{
                $remote=0;
            }
            if ($r->is_urgent=='on') {
                $urgent=1;
            }
            else{
                $urgent=0;
            }
            if ($r->share_with_public=='on') {
                $share=1;
            }
            else{
                $share=0;
            }
             if ($r->enable_crowdfunding=='on') {
                $crowd=1;
            }
            else{
                $crowd=0;
            }
                if ($r->allow_multiple_source_participate=='on') {
                $participate=1;
            }
            else{
                $participate=0;
            }
            if(isset($r->longitude) && isset($r->latitude)) {
DB::update("UPDATE missions SET user_id='$r->user',title='$r->title',latitude='$r->latitude',longitude='$r->longitude'");                
            }
            
DB::update("UPDATE missions SET user_id='$r->user',title='$r->title',mission_type='$r->mission_type',mission_company_id='$r->mission_company',is_remote='$remote',address='$r->address',estimated_budget='$r->estimated_budget',is_urgent='$urgent',deadline='$r->deadline',mission_privacy='$r->mission_privacy',mission_description='$r->mission_description',mission_objective='$r->mission_objective',files_share_with_public='$share',enable_crowdfunding='$crowd',min_raise_amount='$r->min_raise_amount',max_raise_amount='$r->max_raise_amount',allow_multiple_source_participate='$participate',mission_files='$files' WHERE id='$r->id'");
return redirect()->route('allMissions')->with('success', 'Updated Successfully');
        }




}
