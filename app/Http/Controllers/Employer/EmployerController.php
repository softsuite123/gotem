<?php

namespace App\Http\Controllers\Employer;

use App\Bookmark;
use App\Company;
use App\Http\Controllers\Controller;
use App\Mission;
use App\order;
use App\project;
use App\Milestone;
use App\SourceBookmark;
use App\Rating;
use App\User;
use App\Crowd_funding;
use App\Country;
use App\Bank_account;
use DB;
use App\Profile;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Image;
use Auth;
class EmployerController extends Controller {
    public function __construct() {
        $this->middleware('auth', ['except' => ['showRegister']]);
    }

    public function showRegister() {


        return view('auth.register');
    }

    public function dashboard() {
        if (!$this->checkAuthorization()) {
            return redirect()->route('freelancer.dashboard');
        }
         $current_date = date('Y-m-d');
         // where('files_share_with_public','=','1')->
        $data['missions'] = Mission::where('deadline','>=',$current_date)->orderBy('id','desc')->inRandomOrder()->paginate(12);
        // dd($data['missions']);

        // dd($data['current_date']);
        return view('employer.dashboard', $data);
    }

    public function uploadMission() {
        if (!$this->checkAuthorization()) {
            return redirect()->route('freelancer.dashboard');
        }
        $companies = Company::all();
        return view('employer.upload-mission', compact('companies'));
    }

    public function view_offer() {
        if (!$this->checkAuthorization()) {
            return redirect()->route('freelancer.dashboard');
        }
        $offers = project::where('employer_id', \Auth::id())->where('view_offer',1)->get();
//         dd($offers);
        return view('employer.view_offer', ['offers' => $offers]);
    }

      public function submitRating(Request $request) {

        $save_rating = new Rating();
        $save_rating->user_id = Auth::User()->id;
        $save_rating->mission_id = $request->mission_id;
        $save_rating->source_id = $request->source_id;
        $save_rating->percentage = $request->rating;
        $save_rating->description = $request->description;
        $save_rating->save();

        return redirect()->back();

   }
   public function wallet_balance(Request $request)
    {
        $user_id = Auth::user()->id;


    }
    public function postMission(Request $request) {
            // dd($request->all());
        $request->validate([
            'title' => 'required',
            'mission_company' => 'required',
           // 'address' => 'required',
            'estimated_budget' => 'required',
            'deadline' => 'required',
            'mission_privacy' => 'required',
            'mission_description' => 'required',
           // 'mission_objective' => 'required',
        //    'latitude' => 'required',
          //  'longitude' => 'required',
            // 'min_raise_amount' => 'required',
            // 'max_raise_amount' => 'required',
            // 'mission_files' => 'required',
        ]);

    $veri = Auth::User()->is_verified;
    $auth_id = Auth::User()->id;


        $mission = new Mission();
        DB::beginTransaction();
        $mission->title = $request->title;
        $mission->user_id = \Auth::id();
        $mission->mission_type = $request->mission_type;
        $mission->mission_company_id = $request->mission_company;
        $mission->is_remote = $request->is_remote;
        $mission->address = $request->address;
        $mission->estimated_budget = $request->estimated_budget;
        $mission->is_urgent = $request->is_urgent;
        $mission->deadline = $request->deadline;
        $mission->mission_privacy = $request->mission_privacy;
        $mission->mission_description = $request->mission_description;
        $mission->mission_objective = $request->mission_objective;
        $mission->files_share_with_public = $request->share_with_public;
        $mission->enable_crowdfunding = $request->enable_crowdfunding;
        if($request->min_raise_amount && $request->max_raise_amount) {
            $mission->min_raise_amount = $request->min_raise_amount;
            $mission->max_raise_amount = $request->max_raise_amount;
        }
            $mission->allow_multiple_source_participate = $request->allow_multiple_source_participate;

        $mission->latitude = $request->latitude;
        $mission->longitude = $request->longitude;
        $mission->mission_files = $request->mission_files;
        $mission->mission_all_files = $request->mission_all_files;
        $mission->save();
        $master = $mission->id;

    $sql="INSERT INTO notifications(title,status,type,source,master) VALUES('$request->title','0','2','New Mission','$master')";
    DB::insert($sql);
        DB::commit();
        return redirect()->route('upload-mission')->with('success','Mission Uploaded Successfully!');

    }

   public function source(Request $r) {
        $where="";
        $data['countries'] = Country::all();
        if ($r->country!='' && $r->state=='' && $r->city=='') {

            $data['sources']= DB::table('users')->select('users.*','profiles.hourly_rate','profiles.avatar','profiles.skills','countries.name as cname','states.name as sname','cities.name as city_name')->leftJoin('profiles','users.id','profiles.user_id')->leftJoin('countries','profiles.country_id','countries.id')->leftJoin('states','profiles.state_id','states.id')->leftJoin('cities','profiles.city_id','cities.id')->where('users.user_type',User::USER_FREELANCER)->where('profiles.country_id',$r->country)->paginate(20);


        }
        else if($r->country!='' && $r->state!='' && $r->city=='') {
           $data['sources']= DB::table('users')->select('users.*','profiles.hourly_rate','profiles.avatar','profiles.skills','countries.name as cname','states.name as sname','cities.name as city_name')->leftJoin('profiles','users.id','profiles.user_id')->leftJoin('countries','profiles.country_id','countries.id')->leftJoin('states','profiles.state_id','states.id')->leftJoin('cities','profiles.city_id','cities.id')->where('users.user_type',User::USER_FREELANCER)->where('profiles.country_id',$r->country)->where('profiles.state_id',$r->state)->paginate(20);
       }
       elseif ($r->country!='' && $r->state!='' && $r->city!='') {

          $data['sources']= DB::table('users')->select('users.*','profiles.hourly_rate','profiles.avatar','profiles.skills','countries.name as cname','states.name as sname','cities.name as city_name')->leftJoin('profiles','users.id','profiles.user_id')->leftJoin('countries','profiles.country_id','countries.id')->leftJoin('states','profiles.state_id','states.id')->leftJoin('cities','profiles.city_id','cities.id')->where('users.user_type',User::USER_FREELANCER)->where('profiles.country_id',$r->country)->where('profiles.state_id',$r->state)->where('profiles.city_id',$r->city)->paginate(20);
      }

      else{

          $data['sources']= DB::table('users')->select('users.*','profiles.hourly_rate','profiles.avatar','profiles.skills','countries.name as cname','states.name as sname','cities.name as city_name')->leftJoin('profiles','users.id','profiles.user_id')->leftJoin('countries','profiles.country_id','countries.id')->leftJoin('states','profiles.state_id','states.id')->leftJoin('cities','profiles.city_id','cities.id')->where('users.user_type',User::USER_FREELANCER)->paginate(20);
      }

      return view('employer.source',$data);
  }

    public function messages() {
        return view('employer.messages');
    }

    public function saved() {
        // dd("f");
        $saved = Bookmark::where('user_id', \Auth::id())->get();
        $sourcesaved = SourceBookmark::where('user_id', \Auth::id())->get();
        return view('employer.saved', compact('saved', 'sourcesaved'));
    }

    public function crowdfund() {
        return view('employer.crowdfund');
    }

    public function personal() {
        // dd(auth()->user()->user_type);
        if (auth()->user()->user_type == User::USER_FREELANCER) {
            $route = 'freelancer.dashboard';
        } elseif (auth()->user()->user_type == User::USER_EMPLOYER) {
            $route = 'employer.dashboard';
        }
        $user = \Auth::user();
        // dd($user);
        if ($user->is_verified == 0) {

            return redirect()->route($route)->with('error', 'Verify your idenity first');
        }
        return view('employer.personal', ['user' => $user]);
    }

    public function settings() {
        return view('employer.settings');
    }

    public function participated() {
        // $items = DB::table('crowd_funding')->where('user_id', \Auth::id())->get();
        $items = Crowd_funding::where('user_id', \Auth::id())->get();
        return view('employer.participated', compact('items'));
    }

    public function group_chat() {
        return view('employer.group_chat');
    }

    public function checkAuthorization() {
        if ((int) auth()->user()->user_type !== \App\User::USER_EMPLOYER) {
            // dd('there');
            return false;
        } else {
            return true;
        }
    }

    public function balance() {
            $balance = 0;
            $withdraw = 0;
            $userBalance=0;
            $orders = DB::table('orders')->where('applicant_id', \Auth::id())->where('status', 'accepted')->get();
            $data['account']=DB::table('bank_accounts')->where('user_id', \Auth::id())->first();
//            dd($orders);
            foreach ($orders as $order) {
                $balance += $order->amount;
                //Balance Amount
            }

            $withdraws = DB::table('withdraw_history')->where('user_id', \Auth::id())->where('status', '!=', 'rejected')->get();
            // dd($withdraws);
            if($withdraws){
                foreach ($withdraws as $withdrawl) {
                    $withdraw += $withdrawl->amount;
                }


            $userBalance = $balance - $withdraw - ($balance * .2);

        }
//            dd($balance * .2);
        $data['balance']=$balance;
        $data['userBalance']=$userBalance;
        // dd($data);
        return view('employer.balance',$data);
    }
    public function save_account(Request $request){
        $account = DB::table('bank_accounts')->where('user_id', \Auth::id())->first();
        if($account){
            DB::table('bank_accounts')->where('user_id', \Auth::id())->update(
                ['user_id'=>\Auth::id(),
            'country_of_bank'=>$request->country,
            'iban'=>$request->account_number,
            'swift'=>$request->swift,
            'account_name'=>$request->account_name,
            'bank_name'=>$request->bank_name,
            'bank_address'=>$request->bank_address,
            'bank_post_code'=>$request->bank_post_code,
            'bank_city'=>$request->bank_city,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'residential_address'=>$request->address,
            'permanent_address'=>$request->permanent_address,
            'postal_code'=>$request->post_code,
            'city'=>$request->city]);
            
            
        }
        else{
            $account=new Bank_account;
            $account->user_id=\Auth::id();
            $account->country_of_bank=$request->country;
            $account->iban=$request->account_number;
            $account->swift=$request->swift;
            $account->account_name=$request->account_name;
            $account->bank_name=$request->bank_name;
            $account->bank_address=$request->bank_address;
            $account->bank_post_code=$request->bank_post_code;
            $account->bank_city=$request->bank_city;
            $account->first_name=$request->first_name;
            $account->last_name=$request->last_name;
            $account->residential_address=$request->address;
            $account->permanent_address=$request->permanent_address;
            $account->postal_code=$request->post_code;
            $account->city=$request->city;
            $account->save();
        }
        
        return view('employer.employer-withdraw');
            
    }
    public function milestone($id){
        if (!$this->checkAuthorization()) {
            return redirect()->route('freelancer.dashboard');
        }
        $data['offer'] = $offer =project::where('id', $id)->first();

        if ($offer->is_milestone) {
            $data['milestones'] = Milestone::where('project_id', $id)->get();
            $data['mission']=Mission::where('id',$offer->mission_id)->first();
        }
        else{
            $data['mission']=Mission::where('id',$offer->mission_id)->first();
        }
//        dd($data);
        $data['counter'] =1;
        return view('employer.front.milestone',$data);
    }
    public function myOrders() {
        $myOrders = [];
        $orders = DB::table('orders')->get();

        foreach ($orders as $order) {
            $project = project::where('id', $order->project_id)->first();
            if($project){
            if ($project->employer_id == \Auth::id()) {
                $myOrders[] = $order;
            }
            }

           // dd($myOrders);exit();
        }

        // dd($myOrders);
        return view('employer.my-orders', compact('myOrders'));
    }

    public function mymissions()
    {
        $user_id = \Auth::User()->id;
        //dd($user_id);
         $data['mission'] = Mission::with(['offers' =>  function($q) {
             $q->where('view_offer',1);
         }])->where('user_id',$user_id)->get();
        // dd($data['mission']);
        // $data['offers']=project::where('employer_id', $user_id)->where('view_offer',1)->get();
        return view('employer.mymissions',$data);
    }

    public function acceptWork(Request $request) {
        $milestone = $request->offer;
        $offer=project::where('id',$request->offer);
        $milestones = Milestone::where('id',$milestone);
        $offer->status = 'accepted';
        $offer->save();
        foreach ($milestones as $milestone){
            if ($milestone!=$request->mile_id){
                $update = Milestone::where('id',$milestone)->first();
                $update->view_offer = 2;
            }
        }
        return back()->with('success', 'Accepted Successfully');
    }

    public function verify_upload_mission(Request $request)
    {

         $request->validate([
            'title' => 'required',
            'mission_company' => 'required',
          //  'address' => 'required',
            'estimated_budget' => 'required',
            'deadline' => 'required',
            'mission_privacy' => 'required',
            'mission_description' => 'required',
         //  'mission_files' => 'required',
          // 'mission_objective' => 'required',
            //'latitude' => 'required',
           // 'longitude' => 'required',

        ]);
             $mission_com =  implode(",",$request->mission_company);

        $mission = new Mission();
        DB::beginTransaction();
        $mission->title = $request->title;
        $mission->user_id = \Auth::id();
        $mission->mission_type = $request->mission_type;
        $mission->mission_company_id = $mission_com;
        if ($request->is_remote) {
            $mission->is_remote = true;
        }
        $mission->address = $request->address;
        $mission->estimated_budget = $request->estimated_budget;
        if ($request->is_urgent) {
            $mission->is_urgent = true;
        }
        $mission->deadline = $request->deadline;
        $mission->mission_privacy = $request->mission_privacy;
        $mission->mission_description = $request->mission_description;
        $mission->mission_objective = $request->mission_objective;
        if ($request->share_with_public) {
            $mission->files_share_with_public = true;
        }
        if ($request->enable_crowdfunding) {
            $mission->enable_crowdfunding = true;
        }
        if($request->min_raise_amount && $request->max_raise_amount) {
            $mission->min_raise_amount = $request->min_raise_amount;
            $mission->max_raise_amount = $request->max_raise_amount;
        }

        if ($request->allow_multiple_source_participate) {
            $mission->allow_multiple_source_participate = true;
        }
         $mission->latitude = $request->latitude;
         $mission->longitude = $request->longitude;

        $files = array();
        // // $files[] = $f_image_name;

 if ($request->hasFile('mission_files'))
         {
             $this->validate($request, [
                'mission_files.*' => 'mimes:jpg,jpeg,png|max:20000',
            ], [
                'mission_files.*.mimes' => 'Only jpeg, png, jpg and bmp images are allowed',
                'mission_files.*.max' => 'Sorry! Maximum allowed size for an image is 20MB',
            ]);

                            foreach (request('mission_files') as $key => $value) {
                                $file = $request->file('mission_files')[$key];
                                $name =  $file->getClientOriginalName();
                                $request->file('mission_files')[$key]->move("public/files/avatars/", $name);
                                $files[] = 'public/files/avatars/'.$name;
                            }

        }

        $files = json_encode($files);

        $mission->mission_files = $files;

        $files_all = array();

 if ($request->hasFile('mission_all_files'))
         {
             $this->validate($request, [

                'mission_all_files.*' => 'mimes:pdf,docx,xml,ppt,doc,jpg,jpeg,png|max:200000',
            ], [
                'mission_all_files.*.mimes' => 'Only pdf, docx, xml, ppt, jpg, jpeg, png and doc files are allowed',
                'mission_all_files.*.max' => 'Sorry! Maximum allowed size for an image is 200MB',
            ]);

                            foreach (request('mission_all_files') as $key => $value) {
                                $file_all = $request->file('mission_all_files')[$key];
                                $name_all =  $file_all->getClientOriginalName();
                                $request->file('mission_all_files')[$key]->move("public/files/avatars/", $name_all);
                                $files_all[] = 'public/files/avatars/'.$name_all;
                            }

        }

        $files_alls = json_encode($files_all);

        $mission->mission_all_files = $files_alls;

        $data['mission']=$mission;
        return view('employer.view_pre_mission',$data);
    }

public function edit_profile(Request $req)
{

    $user_id      = Auth::User()->id;


//  for the resume
    if ($req->rem_resume==1) {
     $a = Auth::User()->Profile->resume;
 }
 else{
     if ($req->hasFile('resume'))
     {
         $validator = request()->validate([
            'resume' => 'required|mimes:pdf,doc,docx,newt_textbox_set_text(textbox, text)',
        ]);

         if($req->file('resume')->isValid()) {
            try {
                $file = $req->file('resume');
                $name =  $file->getClientOriginalName();
                $req->file('resume')->move("public/files/resume/", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }



    if($req->resume == NULL)
    {
        $a = Auth::User()->Profile->resume;
    }
    else{
        $a = "public/files/resume/".$name."";
        if ($req->old_resume!="" && file_exists($req->old_resume)) {
          unlink($req->old_resume);
        }
    }
}


//for the avatar
if ($req->rem_pic==1) {
 $p= Auth::User()->Profile->avatar;
}

else{
    if ($req->hasFile('avatar'))
    {
        request()->validate([
            'avatar' => 'required|image|mimes:jpg,jpeg,png',
        ]);
        if($req->file('avatar')->isValid()) {
            try {
                $file = $req->file('avatar');
                $name =  $file->getClientOriginalName();
                $req->file('avatar')->move("public/files/avatars/", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }

    if($req->avatar == NULL)
    {
        $p = Auth::User()->Profile->avatar;
    }
    else{
        $p = "public/files/avatars/".$name."";
        if ($req->old_pic!="" && file_exists($req->old_pic)) {
           unlink($req->old_pic);
       }

   }
}

        // for the cover letter
if ($req->rem_letter==1) {
    $c = Auth::User()->Profile->cover_letter;
}
else{
    if ($req->hasFile('cover_letter'))
    {
    // request()->validate([
    //     'cover_letter' => 'required|image|mimes:jpg,jpeg,png',
    //         ]);
        if($req->file('cover_letter')->isValid()) {
            try {
                $file = $req->file('cover_letter');
                $name =  $file->getClientOriginalName();
                $req->file('cover_letter')->move("public/files/cover/", $name);
            } catch (Illuminate\Filesystem\FileNotFoundException $e) {
            }
        }
    }


    if($req->cover_letter == NULL)
    {
        $c = Auth::User()->Profile->cover_letter;
    }
    else{
        $c = "public/files/cover/".$name."";
        if ($req->old_cover_letter!="" && file_exists($req->old_cover_letter)) {
           unlink($req->old_cover_letter);
       }
   }


}
//gettting the data

$update = Profile::where('user_id', $user_id)->first();
$update->name         = $req->name;
$update->hourly_rate  = $req->hourly_rate;
$update->skills       = $req->skills;
$update->about        = $req->about;
$update->country_id   = $req->country;
$update->state_id     = $req->state;
$update->city_id      = $req->city;
$update->resume       = $a;
$update->avatar       = $p;
$update->cover_letter = $c;
$update->save();
return redirect()->route('freelancer.dashboard');
}
    public function emp_change_pass(Request $request)
    {
         $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

       return back();
    }
    public function delete_my_mission($ID)
    {
        Mission::where('id',$ID)->delete();
        return back();
    }

    public function change_offer_status(Request $request)
    {
        //  session_start();
      $request->session()->put("milestone_id",$request->mile_id);
    //   $a= $request->session()->get("milestone_id");
//       dd($request->all());

        if($request->mile_id>0)
        {
                // $update = project::where('id',$request->offer)->first();

                // if($request->chck==0){
                //  $update->view_offer = 0;
                // }
                // else if($request->chck==1)
                // {
                //  $update->view_offer = 2;
                // }

                // $update->save();


                $upd_milestone = Milestone::where('id',$request->mile_id)->first();
                if($request->chck==0){
                 $upd_milestone->status = 0;
                }
                else if($request->chck==1)
                {
                 $upd_milestone->status = 2;
                }
                $upd_milestone->save();
        }




        else{
             $update = project::where('id',$request->offer)->first();

                if($request->chck==0){
                 $update->view_offer = 0;
                }
                else if($request->chck==1)
                {
                 $update->view_offer = 2;
                }

                $update->save();
        }

        echo"1";
    }


        public function cancel_offer(Request $request)
        {
        // dd($request->order_id);
            if($request->type=="full")
            {
                $updt_order = order::where('id',$request->order_id)->first();
                $updt_order->status = "cancelled";
                $updt_order->save();
            }

            else if($request->type=="milestoneton")
            {
                $mile = Milestone::where('id',$request->order_id)->first();
                // dd($mile);
                $mile->type = "cancelled";
                $mile->save();
            }
        }


}
