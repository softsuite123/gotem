<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Stripe;
use App\Crowd_funding;
use App\Milestone;
use Session;

class StripeController extends Controller
{
    /**
     * payment view
     */
    public function handleGet(Request $request)
    {
        $data['user_id']    = $request->user_id;
        $data['mission_id'] = $request->mission_id;
        $data['amount']     = $request->amount;
        return view('StripePayment',$data);
    }

    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {

        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 100 * 150,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Making test payment."
        ]);

  $funding             =  new Crowd_funding();
  $funding->user_id    = $request->user_id;
  $funding->mission_id = $request->mission_id;
  $funding->amount     = $request->amount;
  $funding->save();
        Session::flash('success', 'Payment has been successfully processed.');

        return redirect()->route('employer.dashboard');
    }


    public function makePayment(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $request->amount*100,
                "currency" => "usd",
                "source" => $request->stripeToken,
                "description" => "Make payment and chill."
        ]);
        if($request->mile_id){
            $upd_milestone=Milestone::where('id',$request->mile_id)->first();
            $upd_milestone->status = 2;
            $upd_milestone->save();
        }
        else{
            $update = project::where('id',$request->mission_id)->first();
            $update->view_offer = 2;
            $update->save();
       }
       Session::flash('success', 'Payment successfully made.');

       return back();
    }


}
