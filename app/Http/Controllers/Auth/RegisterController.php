<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    { 

      

 
      





        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
          // 'avatar' => ['required','image','mimes:jpg,jpeg,png'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'account-type-radio' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {

     
        //dd($avatarPath);




        if($data['account-type-radio'] == 'employer'){
            $user_type = User::USER_EMPLOYER;
        }else{
            $user_type = User::USER_FREELANCER;
        }
            $user_data  = User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'user_type' => $user_type,
            'password'  => Hash::make($data['password']),
        ]);

// if (request()->hasFile('imageUpload')) {
          
//             $filenameWithExt = request('imageUpload')->getClientOriginalName();
//             $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
//             $extension = request('imageUpload')->getClientOriginalExtension();
//             $fileNameToStore = $filename . '-' . time() . '.' . $extension;
//             $avatarPath = request('imageUpload')->storeAs('public/files/avatars', $fileNameToStore);

//         }
if (request()->hasFile('avatar')) 
         { 
         
         request()->validate([
               'avatar' => 'required|image|mimes:jpg,jpeg,png',
             ]);
                    if(request()->file('avatar')->isValid()) {
                    
                    $file = request()->file('avatar'); 
                    $name =  $file->getClientOriginalName();
                    request()->file('avatar')->move("public/files/avatars/", $name);
                     
                       }
        $avatarPath = 'public/files/avatars/'.$name;
        }
        else{
             $avatarPath="";
        }

  



            Profile::create([
            'user_id'    => $user_data->id,
            'name'       => $data['name'],
            'country_id' => request()->country,
            'state_id'   => request()->state,
            'city_id'    => request()->city,
            'avatar'     => $avatarPath,
            
        ]);
          return $user_data;
           
    }
}
