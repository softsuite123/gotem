<?php

namespace App\Http\Controllers;
use App\Mission;
use App\Profile;
use App\User;
use App\Country;
use App\states;
use App\cities;
use App\Company;
use Image;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;

class AdminController extends Controller {


    public function add_user()
    {
         $this->authorizeUser();
         $data['country'] = Country::all();
         $data['states'] = states::all();
         $data['cities'] = cities::all();
        return view('admin.add_user',$data);
    }
    public function __construct() {
        $this->middleware('web')->except(['adminlogin']);
    }

    public function adminlogin() {
        return view('admin.login');
    }

    public function admindashboard() {
        $this->authorizeUser();
        return view('admin.dashboard');
    }

    public function adminall_users() {
        $this->authorizeUser();
        $users = User::all();

        return view('admin.all_users', ['users' => $users]);
    }

    public function adminall_cases() {
        $this->authorizeUser();
        $missions = Mission::orderByDesc('created_at')->get();

        return view('admin.all_cases', ['missions' => $missions]);
    }

    public function adminverify_request() {
        $this->authorizeUser();

        $requests = Profile::where('verification_submitted', true)->where('verification_status', 'pending')->get();

        return view('admin.verify_request', ['requests' => $requests]);
    }

    public function adminverify_profile(Profile $profile) {

        $this->authorizeUser();
        return view('admin.verify_profile', ['profile' => $profile]);
    }

    public function admin_approve_verified_account(Profile $profile) {

        $this->authorizeUser();
        $profile->verification_status = 'verified';
        $profile->save();
        $user = User::find($profile->user_id);

        $user->is_verified = true;
        $user->save();

        return redirect()->route('identity.requests')->with('success', 'Verified Successfully');
    }

    public function adminverified_account() {
        $this->authorizeUser();
        $verified_users = User::where('is_verified', true)->get();

        return view('admin.verified_account', ['users' => $verified_users]);
    }

    public function withdrawRequests() {
        $requests = DB::table('withdraw_history')->get();
        return view('admin.withdraw_requests', compact('requests'));
    }

    public function authorizeUser() {
        // dd(auth()->user()->user_type != 0);
        if (\Request::segment(1) == 'admin' && \Request::segment(2) == 'login') {
            return true;
        } elseif (auth()->user()->user_type != 0) {
            // dd('you are not authorized to see this page');
        }
    }

    public function acceptWithdrawRequest($id) {

        DB::table('withdraw_history')->where('id', $id)->update(['status' => 'accepted']);
        return back()->with('success', 'Withdraw request accepted successfully');
    }

    public function rejectWithdrawRequest($id) {

        DB::table('withdraw_history')->where('id', $id)->update(['status' => 'rejected']);
        return back()->with('error', 'Withdraw request rejected successfully');
    }
 public function load_states(Request $r)

    {
        // $this->authorizeUser();
        $id = $r->id;
        $states=states::all()->where('country_id',$id);
 
        $opt="<option value=''>Choose State</option>";
        foreach ($states as  $value) {
            $opt .="<option value='".$value->id."'>".$value->name."</option>";

        }
        echo $opt;
    }


    public function load_city(Request $r)

    {
        // $this->authorizeUser();
        $id = $r->id;
        $cities=cities::all()->where('state_id',$id);
 
        $opt="<option value=''>Choose City</option>";
        foreach ($cities as  $value) {

            $opt .="<option value='".$value->id."'>".$value->name."</option>";

        }
        echo $opt;
    }

    public function save_user(Request $r)
    {
        // echo $r->name." ".$r->email." ".$r->pass." ".$r->usertype." ".$r->country." ".$r->state." ".$r->city." ".$r->rate;
        $u = new User();
        $u->name = $r->name;
        $u->email = $r->email;
        $u->password = Hash::make($r->pass);
        $u->user_type = $r->usertype;
        $u->is_verified =1;
        $u->save();
        $master = $u->id;

        if ($r->hasFile('avatar')) 
         {
            request()->validate([
                'avatar' => 'required|image|mimes:jpg,jpeg,png',
            ]);
                    if($r->file('avatar')->isValid()) {
                    try {
                    $file = $r->file('avatar');
                    $name =  $file->getClientOriginalName();
                    $r->file('avatar')->move("public/files/avatars/", $name);
                    } catch (Illuminate\Filesystem\FileNotFoundException $e) {
                    }
                        }
        }
        $avatarPath = 'public/files/avatars/'.$name;

        $profile = new Profile();
        $profile->user_id = $master;
        $profile->name = $r->name;
        $profile->country_id = $r->country;
        $profile->state_id = $r->state;
        $profile->city_id = $r->city;
        $profile->hourly_rate = $r->rate;
        $profile->verification_submitted = 1;
        $profile->verification_status='verified';
        $profile->avatar = $avatarPath;
        $profile->save();
        return back();

// avatar,
    }
    public function del_user($id)
    {
          $this->authorizeUser();
        DB::table('users')->where('id',$id)->delete();
        DB::table('profiles')->where('user_id',$id)->delete();
        return back();
    }

    public function add_mission()
    {
         $this->authorizeUser();
           $data['companies'] = Company::all();
           $data['users'] = User::all()->where('user_type',1);
         return view('admin.add_mission',$data);

    }

    public function add_new_mission(Request $request)
    {

         $request->validate([
            'title' => 'required',
            'mission_type' => 'required',
            'mission_company' => 'required',
            'address' => 'required',
            'estimated_budget' => 'required',
            'deadline' => 'required',
            'mission_privacy' => 'required',
            'mission_description' => 'required',
            'mission_objective' => 'required',
      
        ]);
               
        $mission = new Mission();

        DB::beginTransaction();
       
      
        $mission->title = $request->title;
      if ($request->user=='') {
              $mission->user_id = \Auth::id(); 
        }
        else{
              $mission->user_id = $request->user;  
        }
        $mission->mission_type = $request->mission_type;
        $mission->mission_company_id = $request->mission_company;
        if ($request->is_remote) {
            $mission->is_remote = true;
        }
        $mission->address = $request->address;
        $mission->estimated_budget = $request->estimated_budget;
        if ($request->is_urgent) {
            $mission->is_urgent = true;
        }
        $mission->deadline = $request->deadline;
        $mission->mission_privacy = $request->mission_privacy;
        $mission->mission_description = $request->mission_description;
        $mission->mission_objective = $request->mission_objective;
        if ($request->share_with_public) {
            $mission->files_share_with_public = true;
        }
        if ($request->enable_crowdfunding) {
            $mission->enable_crowdfunding = true;
        }
        if($request->min_raise_amount && $request->max_raise_amount) {
            $mission->min_raise_amount = $request->min_raise_amount;
            $mission->max_raise_amount = $request->max_raise_amount;
        }
      
        if ($request->allow_multiple_source_participate) {
            $mission->allow_multiple_source_participate = true;
        }

       $files = array();
        //$files[] = $f_image_name;

 if ($request->hasFile('avatar')) 
         {
             $this->validate($request, [
                'avatar.*' => 'mimes:jpg,jpeg,png|max:2000',
            ], [
                'avatar.*.mimes' => 'Only jpeg, png, jpg and bmp images are allowed',
                'avatar.*.max' => 'Sorry! Maximum allowed size for an image is 2MB',
            ]);
                    // if($request->file('avatar')->isValid())
                    // {

                            // try
                            // {
                            foreach (request('avatar') as $key => $value) {
                                $file = $request->file('avatar')[$key];
                                $name =  $file->getClientOriginalName();
                                $request->file('avatar')[$key]->move("public/files/avatars/", $name);
                                $files[] = 'public/files/avatars/'.$name;
                            }
                            //}
                            // catch (Illuminate\Filesystem\FileNotFoundException $e)
                            //             {
                            //             }
      //          }
        }
        
/////////////////////////////////////////
        // if (request()->has('avatar')) {
        //     $this->validate($request, [
        //         'avatar.*' => 'mimes:jpg,jpeg,png|max:2000',
        //     ], [
        //         'avatar.*.mimes' => 'Only jpeg, png, jpg and bmp images are allowed',
        //         'avatar.*.max' => 'Sorry! Maximum allowed size for an image is 2MB',
        //     ]);

        //     foreach (request('avatar') as $key => $value) {

        //         $img = Image::make(request('avatar')[$key]);
        //         $thumb = Image::make(request('avatar')[$key])->resize(300, 300);
        //         $imageName = uniqid() . time() . '.' . request()->avatar[$key]->getClientOriginalExtension();
        //         $img->save(base_path() . '/public/public/files/missions/' . $imageName);
        //         $thumb->save(base_path() . '/public/public/files/missions/' . $imageName);
        //         $files[] = $imageName;
        //     }
        // }

       $files = json_encode($files);

       $mission->mission_files = $files;

        $mission->save();
        DB::commit();
        return back()->with('success', 'Mission Added Successfully');

    }


}
