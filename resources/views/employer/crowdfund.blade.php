@extends('employer.layouts.master')

@section('content')
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.2.1/nouislider.min.css">
<style>
    .coolor{
        color:#fff !important;
    }
</style>

<!-- Wrapper -->
<div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')

            <!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar >
		<!-- <div class="dashboard-content-inner" > -->
			
			<!-- Titlebar
================================================== -->
<div class="single-page-header" data-background-image="../assets/images/single-job.jpg" style="box-shadow: 0px 2px 8px 0px gray; ">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image"><a href="single-company-profile.html"><img src="{{asset('backstyling/images/company-logo-03a.png')}}" alt=""></a></div>
						<div class="header-details">
							<h3>Donald Chen</h3>
							<h5>About User</h5>
							<ul>
								<li><a href="single-company-profile.html"><i class="icon-material-outline-business"></i> Novice Member</a></li>
								<li><div class="star-rating" data-rating="4.9"></div></li>
								<li><img class="flag" src="{{asset('backstyling/images/flags/gb.svg')}}" alt=""> United Kingdom</li>
								<li><div class="verified-badge-with-title">Verified</div></li>
							</ul>
						</div>
					</div>

					<!-- <div class="right-side">
						<div class="salary-box">
							<div class="salary-type">Annual Salary</div>
							<div class="salary-amount">$35k - $38k</div>
						</div>
					</div> -->

				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">
		
		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">
            <h3><b>Locate my missing daughter in the UK</b></h3>
           <br>
			<div class="single-page-section">
				<h3 class="margin-bottom-25">Mission Description</h3>
				<p>Sign view am high neat half to what. send late held than set why wife our. if an blessing building sleepest. Agreement distrusts  mrs six affection satisfied.</p>

				<p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring.</p>

				<p>Capitalize on low hanging fruit to identify a ballpark value added activity to beta test. Override the digital divide with additional clickthroughs from DevOps. Nanotechnology immersion along the information highway will close the loop on focusing solely on the bottom line.</p>
			</div>

			<div class="single-page-section">
				<h3 class="margin-bottom-30">Location</h3>
				<div id="single-job-map-container">
					<div id="singleListingMap" data-latitude="51.507717" data-longitude="-0.131095" data-map-icon="im im-icon-Hamburger"></div>
					<a href="#" id="streetView">Street View</a>
				</div>
			</div>
			<div class="col-xl-5">
			<a href="#small-dialog" class="apply-now-button popup-with-zoom-anim">Apply to Mission</a>
            </div>
			<!-- <div class="single-page-section">
				<h3 class="margin-bottom-25">Similar Jobs</h3>
			 -->
				<!-- Listings Container -->
				<!-- <div class="listings-container grid-layout">
				 -->
						<!-- Job Listing -->
						<!-- <a href="#" class="job-listing">
						 -->
							<!-- Job Listing Details -->
							<!-- <div class="job-listing-details">
								 -->	<!-- Logo -->
								<!-- <div class="job-listing-company-logo">
									<img src="images/company-logo-02.png" alt="">
								</div>
								 -->
								<!-- Details -->
								<!-- <div class="job-listing-description">
									<h4 class="job-listing-company">Coffee</h4>
									<h3 class="job-listing-title">Barista and Cashier</h3>
								</div>
															</div> -->

							<!-- Job Listing Footer -->
							<!-- <div class="job-listing-footer">
								<ul>
									<li><i class="icon-material-outline-location-on"></i> San Francisco</li>
									<li><i class="icon-material-outline-business-center"></i> Full Time</li>
									<li><i class="icon-material-outline-account-balance-wallet"></i> $35000-$38000</li>
									<li><i class="icon-material-outline-access-time"></i> 2 days ago</li>
								</ul>
							</div>
													</a> -->

						<!-- Job Listing -->
						<!-- <a href="#" class="job-listing"> -->

							<!-- Job Listing Details -->
						<!-- 	<div class="job-listing-details"> -->
								<!-- Logo -->
								<!-- <div class="job-listing-company-logo">
									<img src="images/company-logo-03.png" alt="">
								</div> -->

								<!-- Details -->
								<!-- <div class="job-listing-description">
									<h4 class="job-listing-company">King <span class="verified-badge" title="Verified Employer" data-tippy-placement="top"></span></h4>
									<h3 class="job-listing-title">Restaurant Manager</h3>
								</div>
															</div> -->

							<!-- Job Listing Footer -->
							<!-- <div class="job-listing-footer">
								<ul>
									<li><i class="icon-material-outline-location-on"></i> San Francisco</li>
									<li><i class="icon-material-outline-business-center"></i> Full Time</li>
									<li><i class="icon-material-outline-account-balance-wallet"></i> $35000-$38000</li>
									<li><i class="icon-material-outline-access-time"></i> 2 days ago</li>
								</ul>
							</div>
													</a>
												</div> -->
					<!-- Listings Container / End -->

				<!-- </div> -->
		</div>
		

		<!-- Sidebar -->
		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

				<a id="launchModalBtn" class="button ripple-effect button-sliding-icon coolor" style="width:90%;">Participate in Crowdfund<i class="icon-feather-arrow-right"></i></a>
					
				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<div class="job-overview">
						<div class="job-overview-headline">Mission Summary</div>
						<div class="job-overview-inner">
							<ul>
								<li>
									<i class="icon-material-outline-location-on"></i>
									<span>Location</span>
									<h5>London, United Kingdom</h5>
								</li>
								<li>
									<i class="icon-material-outline-description"></i>
									<span>Private Investigation</span>
									<!-- <h5>$35k - $38k</h5> -->
								</li>
								<li>
									<i class="icon-material-outline-local-atm"></i>
									<span>Mission Value</span>
									<h5>$35k - $38k</h5>
								</li>
								<li>
									<i class="icon-material-outline-access-time"></i>
									<span>Deadline</span>
									<h5>2 days ago</h5>
								</li>
								<li>
									<i class="icon-material-outline-business-center"></i>
									<span>Crowdfund</span>
									<h5>Active</h5>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<h3>Bookmark or Share</h3>

					<!-- Bookmark Button -->
					<button class="bookmark-button margin-bottom-25">
						<span class="bookmark-icon"></span>
						<span class="bookmark-text">Bookmark</span>
						<span class="bookmarked-text">Bookmarked</span>
					</button>

					<!-- Copy URL -->
					<div class="copy-url">
						<input id="copy-url" type="text" value="" class="with-border">
						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
					</div>

					<!-- Share Buttons -->
					<div class="share-buttons margin-top-25">
						<div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
						<div class="share-buttons-content">
							<span>Interesting? <strong>Share It!</strong></span>
							<ul class="share-buttons-icons">
								<li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
								<li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
								<li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
								<li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

		</div>

      <!-- Dashboard Content / End -->

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->



<!-- Tab -->
	<!-- Tab -->
		<div class="modal fade" id="modal_1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body">
				
				<h5>Mission: Locate my missing daughter in the UK</h5>  

                <!-- Welcome Text -->
                <!-- <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div -->
                    
                <!-- Form -->

          <style>

             .slider1 {
              width: 470px;
              height: 125px;
              display: flex;
              overflow-x: auto;
            }
            .slide {
              width: 100px;
              flex-shrink: 0;
              height: 100%;
            }
            @media (max-width: 768px){
             .slider1 {
             width: 316px;  
             }  
            }

            .content {
  width: 100%;
  margin: 0 auto;
}

.slider-area {
  padding: 5px 40px;
  border: 1px solid #ddd;

}

.slider {
  margin-top: 50px;
  width: 100%;
}


          </style>


          <div class="slider1">
  <div class="slide" id="slide-1">
    <img src="https://pickaface.net/gallery/avatar/unr_john_180223_0027_9qm45u.png">    
  </div>
  <div class="slide" id="slide-2">
    <img src="https://pickaface.net/gallery/avatar/JohnBoley54ab86c9c3327.png"> 
  </div>
  <div class="slide" id="slide-3">
    <img src="https://pickaface.net/gallery/avatar/Garret22785730d3a8d5525.png">    
  </div>
  <div class="slide" id="slide-4">
    <img src="https://pickaface.net/gallery/avatar/unr_johndoe_180313_2047_ycel5.png">  
  </div>
  <div class="slide" id="slide-5">
    <img src="https://pickaface.net/gallery/avatar/CreativeDreams51f6c49a68463.png">    
  </div>
</div>


<center><h5><i>Current Crowdfunders</i></h5></center>

<br>
<center><h3>Crowdfunding Deadline: Oct 12, 2019</h3></center>
<br>
<div class="content">
  
  
  <div class="slider-area">
    <div id="slider-snap" class="slider"></div>
    <br><br>
    <p>
      Minimum: $<span id="slider-snap-value-lower"></span>
    </p>

    <p>
      Maximum: $<span id="slider-snap-value-upper"></span>
    </p>
  </div>
  
</div>

<br>
<h3>Raised Amount: $35,612 USD</h3>
<br>

<div style="overflow-y: scroll; height: 100px;">
    <p>Contractors Assigned to Mission</p>
<div><i class="icon-line-awesome-user-secret"></i>&nbsp;&nbsp;<span>Cyber Investigation: 5</span></div>
<div><i class="icon-line-awesome-user-secret"></i>&nbsp;&nbsp;<span>Private Detective: 3</span></div>
<div><i class="icon-line-awesome-user-secret"></i>&nbsp;&nbsp;<span>Personal Detective: 1</span></div>
</div>

<br>
<style>
    .paddding{
    margin-left: 380px;
    }
    @media (max-width: 768px){
    .paddding{
    margin-left: 250px;
    }   
    }
</style>    
<div style="overflow-y: scroll; height: 150px;">
    <div class="col-xl-12 col-md-12">
    <p>Amount to Participate</p>
    </div>
    <center>
    <div class="col-xl-12 col-md-12">
    <hr>
    </div>
    </center> 
        <div class="col-xl-12 col-md-12">
        <p>Select a Value</p>       
        <input placeholder="$250">
        </div>
        

           <div class="col-xl-12 col-md-12">
            <div class="switch-container">
                    <label class="switch">Participate in Group Voting <input type="checkbox" checked><span class="switch-button paddding" ></span> </label>
                </div>
            </div>

            <div class="col-xl-12 col-md-12">
                <span style="color: silver; font-size: 13px;">
                    Vote to select contractors, approve milestones, payouts and more.
                </span> 
            </div>


</div>
<br>
<center><div class="col-xl-12 col-md-12">
    <h4><strong>Suggest a Retainer Person</strong></h4>
    </div></center>
    
    <hr>
    <br>
    <div class="col-xl-2 col-md-2"></div>
    
        <center>
    <div class="col-xl-8 col-md-8">

            <select class="selectpicker">
                <option>5%</option>
                <option>15%</option>
                <option>25%</option>
            </select>
        </div>
            </center>
    
            <div class="col-xl-2 col-md-2"></div>
<br>

<center><h3>Raised Amount: $35,612 USD</h3></center>
<br>
                     <div class="container">                       
                     <div class="row">
                    <div class="col-xl-3 col-md-3"></div>
                    <div class="col-xl-2 col-md-2">
                        <img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="100%" style="margin-top: -5px;">
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="85%">
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="60%">
                    </div>
                    <div class="col-xl-3 col-md-3"></div>
                    </div>
                </div>

<br>

<center><h3>Term and Conditions</h3></center>
<div class="container">                       
<div class="row">
 <div class="col-xl-2 col-md-2"></div>
    
    <div class="col-xl-6 col-md-6">
        <center>

            <select class="selectpicker">
                <option>Read Disclaimer</option>
                <option>Read Disclaimer</option>
                <option>Read Disclaimer</option>
            </select>
            </center>
        </div>
                <div class="col-xl-2 col-md-2">
               
                <div class="switch-container">
                    <label class="switch"><input type="checkbox" checked><span class="switch-button" ></span> </label>
                </div>  
                
                </div>
            <div class="col-xl-2 col-md-2"></div>

</div>
                </div>

<div class="container">                       
<div class="row">
 <div class="col-xl-2 col-md-2"></div>
    
    <div class="col-xl-8 col-md-8">
                <span style="color: silver; font-size: 13px;">
                    Please read the terms and conditions carefully.
                    By clicking accept you agree to all TOU policies.
                </span> 
            </div>
<div class="col-xl-2 col-md-2"></div>
</div>
</div>

<br>

<div class="container">                       
<div class="row">
 <div class="col-xl-2 col-md-2"></div>
    
<div class="col-xl-8 col-md-8">
 	<center><a class="button ripple-effect coolor" id="launchSecond">Submit</a></center>
</div>
<div class="col-xl-2 col-md-2"></div>
</div>
</div>
	
            </div>
        </div>
    </div>
</div>



 <style>
        .scrolling {
            overflow-y: scroll;
            height: 570px;
        }
        
        @media(max-width:768px) {
            .scrolling {
                overflow-y: scroll;
                height: 750px;
            }
        }
    </style>




          <div class="modal fade" id="modal_2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body scrolling">
          <div class="row">
          	<div class="col-md-8">
          		<h5><b>Test Fixed Price Button</b></h5>
          	</div>
          	<div class="col-md-4">
          		<span style="text-align: right;">
          		<h5>0.005 BTC</h5>
          		</span>
          	</div>
          </div>	
          <div class="row">
          	<div class="col-md-8">
          		<h6>Name: David; Email Address: tawei@gotem.io</h6>
          	</div>
          </div>
          <hr>
          <br>
          <center><h3>Select Payment Currency</h3></center>
          <br>
          <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          	<div style="border: 1px solid #7DBBFF; padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="30%">
          		<span>Bitcoin</span>	
          	</div>	
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 14px;">0.005 BTC</h5>
          	</div>
          	</div>	
          	<hr>
             <div class="row">
          	<div class="col-xl-8">
          	 <div class="switch-container">
					<label class="switch" style="font-size: 12px !important; margin-left: 40px;">
						<input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
					</label>
				</div>
				</div>
				<div class="col-xl-4">
                 <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span>				</div>	
				</div>	

          		</div>
          	</div>
          	<div class="col-xl-1"></div>
          </div>
          
          <br>

          <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="25%">
          		<span>XRP</span>	
          	</div>	
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 8px;">190.91 XRP</h5>
          	</div>
          	</div>
          </div>
           </div>
            <div class="col-xl-1"></div>
        </div>

        <br>

        <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="15%">
          		<span>Ethereum</span>	
          	</div>	
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 8px;">0.2864 ETH</h5>
          	</div>
          	</div>
          </div>
           </div>
            <div class="col-xl-1"></div>
        </div>

        <br>
         
         <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          	<div style=" padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Official_Litecoin_Logo.png" width="30%">
          		<span>Litecoin</span>	
          	</div>	
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 14px;">0.897 LTC</h5>
          	</div>
          	</div>	
          	<hr>
             <div class="row">
          	<div class="col-xl-8">
          	 <div class="switch-container">
					<label class="switch" style="font-size: 12px !important; margin-left: 40px;">
						<input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
					</label>
				</div>
				</div>
				<div class="col-xl-4">
                 <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span>				</div>	
				</div>	

          		</div>
          	</div>
          	<div class="col-xl-1"></div>
          </div>

           <br>

           

           <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          	<div class="accordion js-accordion">

				<!-- Accordion Item -->
				<div class="accordion__item js-accordion-item">
					<div class="accordion-header js-accordion-header" style="background-color: silver;">More Currencies</div> 

					<!-- Accordtion Body -->
					<div class="accordion-body js-accordion-body">

						<!-- Accordion Content -->
						<div class="accordion-body__contents">
							<div class="row">
          	<div class="col-xl-6">
          		<img src="https://cdn.freebiesupply.com/logos/large/2x/ripple-2-logo-png-transparent.png" width="25%">
          		<span>Ripple</span>	
          	</div>	
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 8px;">0.5864 RIP</h5>
          	</div>
          	</div>
						</div>

					</div>
					<!-- Accordion Body / End -->
				</div>
				<!-- Accordion Item / End -->
			</div>
          	</div>
          	<div class="col-xl-1"></div>
          </div>


         <div class="row">
         <div class="col-xl-1"></div>
         <div class="col-xl-10 col-md-10">
		<div class="section-headline margin-top-25 margin-bottom-12">
		</div>
		<div class="input-with-icon-left no-border">
		<i class="icon-material-baseline-mail-outline"></i>
		<input type="text" class="input-text" placeholder="tawei@gotem.io">
		</div>
		</div>
		<div class="col-xl-1"></div>
	    </div>

        <div class="row">
              <div class="col-xl-1"></div>	
              <div class="col-xl-10">
          	  <center><a href="#" class="button ripple-effect">Pay with Bitcoin</a></center>
          	  </div>
          	  <div class="col-xl-1"></div>
              </div>


<div class="row">
              <div class="col-xl-1"></div>	
              <div class="col-xl-10">
          	  <center><p>Currency cannot be changed after proceeding</p></center>
          	  </div>
          	  <div class="col-xl-1"></div>
              </div>









</div>
</div>
</div>



          </div>













<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">
	    

		<div class="popup-tabs-container">

			<!-- Tab -->
			<div class="popup-tab-content" id="tab">

				<h5>Mission:</h5>

				<!-- Welcome Text -->
				<!-- <div class="welcome-text">
				<h3>Do Not Forget</h3>
			</div -->

				<!-- Form -->
				<form method="post" id="add-note">
					
					<!-- 		<select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
				<option>Low Priority</option>
				<option>Medium Priority</option>
				<option>High Priority</option>
			</select> -->
					<textarea cols="10" placeholder="Message" name="message" class="with-border" required></textarea>



				<div class="col-xl-12 col-md-12">
					<div class="row">
						<div class="col-xl-5 col-md-5">
							<div class="checkbox" style="margin-top: 10px;">
								<input type="checkbox" id="chekcbox1" checked name="make_offer" required>
								<label for="chekcbox1"><span class="checkbox-icon"></span> Make an Offer</label>
							</div>
						</div>

						<div class="col-xl-7 col-md-7">

							<input placeholder="Insert $ Amount" name="offer_amount" required>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-6 col-md-6">
							<div class="checkbox">
								<input type="checkbox" id="chekcbox2" name="milestones">
								<label for="chekcbox2"><span class="checkbox-icon"></span> Milestones</label>
							</div>
						</div>
					</div>
				</div>
				<br>
				<style type="text/css">
					hr {
						border: 1px solid silver;
					}
				</style>
				<div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
					<div class="row" style="margin-top: 20px;">
						<!--  <div class="col-xl-2 col-md-2">
			<div class="section-headline margin-top-10 margin-bottom-12">
				<h5>Type Here</h5>
			</div>
		</div -->
		<small>Note: milestone without amount will not be submitted</small>
						<div class="col-xl-12 col-md-12">
							<input placeholder="Milestone One" name="milestone[]">
						</div>
						<div class="col-xl-12 col-md-12">
							<input placeholder="Amount" name="amount[]">
						</div>
					</div>

					<div class="row" style="margin-top: 20px;">
						<!--  <div class="col-xl-2 col-md-2">
			<div class="section-headline margin-top-10 margin-bottom-12">
				<h5>Type Here</h5>
			</div>
		</div -->
						<div class="col-xl-12 col-md-12">
							<input placeholder="Milestone Two" name="milestone[]">
						</div>
						<div class="col-xl-12 col-md-12">
							<input placeholder="Amount" name="amount[]">
						</div>

					</div>

					{{-- <div class="row" style="margin-top: 20px;">

						<div class="col-xl-12 col-md-12">
							<div class="keywords-container">
								<div class="keyword-input-container">
									<input type="text" class="keyword-input" placeholder="Add Milestone" />
									<button class="keyword-input-button ripple-effect"><i
											class="icon-material-outline-add"></i></button>
								</div>
								<div class="keywords-list">
									<!-- keywords go here -->
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div> --}}
				</div>


				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit"
					form="add-note">Submit<i class="icon-material-outline-arrow-right-alt"></i></button>
		</form>
			</div>
			











		</div>
	</div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.2.1/nouislider.min.js"></script>




<script>
        $(document).on('click', '#launchModalBtn', function() {
            $('#modal_1').modal();
        });

        $(document).on('click', '#launchSecond', function() {
            $('#modal_2').modal();
        })

        $('.modal').on('show.bs.modal', function() {
            $('.modal').not($(this)).each(function() {
                $(this).modal('hide');
            });
        });
    </script>


<script>
    var snapSlider = document.getElementById('slider-snap');
var snapValues = [
    document.getElementById('slider-snap-value-lower'),
    document.getElementById('slider-snap-value-upper')
];
var range = {
  'min': 0,
  '10%': 50,
  '10.1%': 51,
  '10.2%': 52,
  '10.25%': 52.5,
  '20%': 100,
  '30%': 150,
  '35%': 160,
  '40%': 500,
  '42.3%': 666,
  '50%': 800,
  'max': 1000
};



noUiSlider.create(snapSlider, {
    start: [ 0, 500 ],
    snap: true,
    connect: true,
  tooltips: true,
    range: range
});

snapSlider.noUiSlider.on('update', function( values, handle ) {
    snapValues[handle].innerHTML = values[handle];
});

$('#range').text(JSON.stringify(range, null, '\t'));

</script>


<!-- Google Autocomplete -->

<!-- 
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!-- 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

@endsection