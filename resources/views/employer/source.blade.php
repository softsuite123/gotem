@extends('employer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')


           <div class="col-xl-9 col-lg-8 content-left-offset">
                <!-- yahan py karna hia -->
                     <form method="post" action="{{url('/filter')}}">
                        @csrf
                <div class="row">
               
                    <div class="col-md-3">
                      <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Country</h5>
                                                </div>

                                                <select class="selectpicker" name="country" id="country" required="">
                                                    <option selected disabled value="">Select One</option>
                                                    @foreach ($countries as $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                    </div>
                    <div class="col-md-3">
                       <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>State</h5>
                                                </div>

                                                <select name="state" id="state" class="state_select form-control"
                                                    autocomplete="address-level1"  data-placeholder="Select an option…">
                                                    <option selected disabled value="">Select an option…</option>
                                                </select>
                    </div>
                    <div class="col-md-3">
                       <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>City</h5>
                                                </div>

                                                <select name="city" id="city" class="city_select form-control"
                                                    autocomplete="address-level1" data-placeholder="Select an option…">
                                                    <option selected disabled value="">Select an option…</option>
                                                </select>
                    </div>
                    <div class="col-md-3"><br><br>
                        <input type="submit" name="" class="btn btn-primary" value="Filter">
                    </div>

                </div>
                  </form>
            <h3 class="page-title">Search Results</h3>

            <div class="notify-box margin-top-15">
                <div class="switch-container">
                    <label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label>
                </div>

                <div class="sort-by">
                    <span>Sort by:</span>
                    <select class="selectpicker hide-tick">
                        <option>Relevance</option>
                        <option>Newest</option>
                        <option>Oldest</option>
                        <option>Random</option>
                    </select>
                </div>
            </div>

            <!-- Freelancers List Container -->
            <div class="freelancers-container freelancers-grid-layout margin-top-35" >

                <!--Freelancer -->
                @foreach ($sources as $source)
            
              <?php  
                  $isBookmarked =  (new \App\SourceBookmark())->isBookmarked($source->id);

              $rating = DB::table('ratings')->where('source_id',$source->id)->sum('percentage');
              $rating_rows = DB::table('ratings')->where('source_id',$source->id)->count();
              if ($rating) {
              $final_rating = $rating/$rating_rows; }

              ?>
                  
              
                <div class="freelancer" style=" width: 300px; height: 400px;">

                    <!-- Overview -->
                    <div class="freelancer-overview">
                        <div class="freelancer-overview-inner">

                            <!-- Bookmark Icon -->
                           <!--  <span class="bookmark-icon"></span> -->
                            <span id="{{$source->id}}" class="bookmark-icon {{$isBookmarked?'bookmarked':''}}"></span>


                            <!-- Avatar -->

                            <div class="freelancer-avatar">
                                <div class="verified-badge"></div>
                                <a href="{{url('/profile/'.$source->id)}}"><img src="{{$source->avatar}}" alt="" style="height: 100px; width: 100px;"></a>
                            </div>

                            <!-- Name -->
                            <div class="freelancer-name">
                                <h4><a href="{{url('/profile/'.$source->id)}}">{{$source->name}}<img class="flag" src="../assets/images/flags/gb.svg" alt="" title="United Kingdom" data-tippy-placement="top"></a></h4>
                                <span>{{$source->skills}}</span>
                            </div>

                            @if($rating)
                            <!-- Rating -->
                            <div class="freelancer-rating">
                                <div class="star-rating" data-rating="{{$final_rating}}"></div>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Details -->
                    <div class="freelancer-details">
                        <div class="freelancer-details-list">
                            <ul>
                                <li>Location <strong><i class="icon-material-outline-location-on"></i>{{$source->city_name}} , {{$source->sname}} , {{$source->cname}}  </strong></li>
                                <li>Hourly Rate <strong>{{$source->hourly_rate}}</strong></li>
                                <li>Job Success <strong>0</strong></li>
                            </ul>
                        </div>
                        <a href="{{url('/profile/'.$source->id)}}" class="button button-sliding-icon ripple-effect">View Profile <i class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>
                </div>
               
                @endforeach

                <!-- Freelancer / End -->

                <!--Freelancer -->
                
                <!-- Freelancer / End -->

            </div>
            <!-- Freelancers Container / End -->


            <!-- Pagination -->
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <!-- Pagination -->
                    <style type="">
                        .page-item{
                            display: inline-block;
                            padding: 10px;
                        }
                    </style>
                    <div class="d-flex justify-content-center">
                        {{$sources->links()}}
                    </div>
                </div>
            </div>
            <!-- Pagination / End -->

        </div>

        <!-- Dashboard content end -->





</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>

<script type="text/javascript">
    $('.bookmark-icon').click(function(e){
        e.preventDefault();

        if ( $( this ).hasClass( "bookmarked" ) ) {
            return false;
        }
        let event = $(this);
        console.log(event);
        let source_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('source.bookmark') }}",
               data:{source_id:source_id},
               success:function(data) {
                   // alert(data);
                  // $("#msg").html(data.msg);
                 alert('Saved successfully');
               }
            });
    })

    //delete bookmark

    $('.bookmarked').click(function(e){
        e.preventDefault();

        let event = $(this);
        console.log(event);
        let source_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('source.unbookmark') }}",
               data:{source_id:source_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Removed successfully');
               }
            });
    })
</script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>
<script>
    $('#country').change(function () {
        var cid = $(this).val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option value="">Select State</option>');
                        $.each(res, function (key, value) {
                            $("#state").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
    $('#state').change(function () {
        var sid = $(this).val();
        if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option value="">Select City</option>');
                        $.each(res, function (key, value) {
                            $("#city").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
</script>

@endsection