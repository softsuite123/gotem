@extends('employer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')


            <!-- Dashboard Content
    ================================================== -->
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Saved</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Saved</li>
                    </ul>
                </nav>
            </div>

            <!-- Row -->
            <div class="row">

                <!-- Dashboard Box -->
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">
 @if(Session::has('success'))
            <li class="alert alert-success">{{Session('success')}}</li>
        @endif
                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-material-outline-business-center"></i> Saved Jobs</h3>
                        </div>

                        <div class="content">
                            <ul class="dashboard-box-list">
                                @foreach ($saved as $bookmark)
                                @php
                                
                                    $mission = \App\Mission::find($bookmark->mission_id);
                                
                                    if($mission){
                                    
                                    $image = json_decode($mission->mission_files);
                                    $company = \App\Company::find($mission->mission_company_id);
                                    }
                                    
                                    
                                @endphp
                                   @if($mission)
                                    <li>
                                    <!-- Job Listing -->
                                    <div class="job-listing">

                                        <!-- Job Listing Details -->
                                        <div class="job-listing-details">

                                            <!-- Logo -->
                                            <a href="" class="job-listing-company-logo">
                                                <img @if($image) src="{{asset('/')}}/{{$image[0]}}" @endif alt="">
                                            </a>

                                            <!-- Details -->
                                         
                                            <div class="job-listing-description">
                                                <h3 class="job-listing-title"><a href="#">{{$mission->title}}</a></h3>

                                                <!-- Job Listing Footer -->
                                                <div class="job-listing-footer">
                                                    <ul>
                                                        <li><i class="icon-material-outline-business"></i> {{$company->name}}</li>
                                                        <li><i class="icon-material-outline-location-on"></i> {{$mission->address}}</li>
                                                        <li><i class="icon-material-outline-business-center"></i> {{$mission->mission_privacy}}</li>
                                                        <li><i class="icon-material-outline-access-time"></i> {{$mission->created_at->diffforhumans()}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                          
                                        </div>
                                    </div>
                                    <!-- Buttons -->
                                    <div class="buttons-to-right">
                                        <a href="{{route('bookmark.delete',['id'=>$bookmark->id])}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>
                                </li>
                                  @endif
                                @endforeach


                                {{-- <li>
                                    <!-- Job Listing -->
                                    <div class="job-listing">

                                        <!-- Job Listing Details -->
                                        <div class="job-listing-details">

                                            <!-- Logo -->
                                            <a href="#" class="job-listing-company-logo">
                                                <img src="{{asset('backstyling/images/company-logo-04.png')}}" alt="">
                                            </a>


                                            <!-- Details -->
                                            <div class="job-listing-description">
                                                <h3 class="job-listing-title"><a href="#">Administrative Assistant</a></h3>

                                                <!-- Job Listing Footer -->
                                                <div class="job-listing-footer">
                                                    <ul>
                                                        <li><i class="icon-material-outline-business"></i> Mates</li>
                                                        <li><i class="icon-material-outline-location-on"></i> San Francisco</li>
                                                        <li><i class="icon-material-outline-business-center"></i> Full Time</li>
                                                        <li><i class="icon-material-outline-access-time"></i> 2 days ago</li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    <div class="buttons-to-right">
                                        <a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>
                                </li>

                                <li>
                                    <!-- Job Listing -->
                                    <div class="job-listing">

                                        <!-- Job Listing Details -->
                                        <div class="job-listing-details">

                                            <!-- Logo -->
                                            <a href="#" class="job-listing-company-logo">
                                                <img src="{{asset('backstyling/images/company-logo-04.png')}}" alt="">
                                            </a>

                                            <!-- Details -->
                                            <div class="job-listing-description">
                                                <h3 class="job-listing-title"><a href="#">Construction Labourers</a></h3>

                                                <!-- Job Listing Footer -->
                                                <div class="job-listing-footer">
                                                    <ul>
                                                        <li><i class="icon-material-outline-business"></i> Podous</li>
                                                        <li><i class="icon-material-outline-location-on"></i> San Francisco</li>
                                                        <li><i class="icon-material-outline-business-center"></i> Full Time</li>
                                                        <li><i class="icon-material-outline-access-time"></i> 2 days ago</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    <div class="buttons-to-right">
                                        <a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>
                                </li> --}}

                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Dashboard Box -->
                <div class="col-xl-12">
                    <div class="dashboard-box">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-material-outline-face"></i> Saved Freelancers</h3>
                        </div>

                        <div class="content">
                            <ul class="dashboard-box-list">
                                @foreach ($sourcesaved as $source)
                                @php
                             $sour = \App\User::find($source->source_id);
                                 
                            $profile = \App\Profile::where('user_id',$sour->id)->first();
                        
                                @endphp
                                <li>
                                    <!-- Overview -->
                                    <div class="freelancer-overview">
                                        <div class="freelancer-overview-inner">

                                            <!-- Avatar -->
                                            <div class="freelancer-avatar">
                                                <div class="verified-badge"></div>
                                                <a href="{{url('/profile/'.$profile->user_id)}}"><img @if($profile) src="{{url('')}}/{{$profile->avatar}}"@endif alt=""></a>
                                            </div>

                                            <!-- Name -->
                                            <div class="freelancer-name">
                                                <h4><a href="{{url('/profile/'.$profile->user_id)}}">{{$sour->name}} <img class="flag" src="" alt="" title="Germany" data-tippy-placement="top"></a></h4>
                                                <span>{{$profile->skills}}</span>
                                                <!-- Rating -->
                                                <div class="freelancer-rating">
                                                    <div class="star-rating" data-rating="4.2"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    <div class="buttons-to-right">
                                        <a href="{{route('source.bookmark.delete',['id'=>$source->id])}}" class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>
                                </li>
                                @endforeach
                               {{--  <li>
                                    <!-- Overview -->
                                    <div class="freelancer-overview">
                                        <div class="freelancer-overview-inner">

                                            <!-- Avatar -->
                                            <div class="freelancer-avatar">
                                                <a href="#"><img src="{{asset('backstyling/images/user-avatar-placeholder.png')}}" alt=""></a>
                                            </div>

                                            <!-- Name -->
                                            <div class="freelancer-name">
                                                <h4><a href="#">Marcin Kowalski <img class="flag" src="{{asset('backstyling/images/flags/pl.svg')}}" alt="" title="Poland" data-tippy-placement="top"></a></h4>
                                                <span>Front-End Developer</span>
                                                <!-- Rating -->
                                                <div class="freelancer-rating">
                                                    <div class="star-rating" data-rating="4.7"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Buttons -->
                                    <div class="buttons-to-right">
                                        <a href="#" class="button red ripple-effect ico" title="Remove" data-tippy-placement="left"><i class="icon-feather-trash-2"></i></a>
                                    </div>
                                </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Row / End -->

            <!-- Footer -->
            <div class="dashboard-footer-spacer"></div>


        </div>
    </div>
    <!-- Dashboard Content / End -->



    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

@endsection