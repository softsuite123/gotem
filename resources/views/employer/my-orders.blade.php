@extends('employer.layouts.master')
@section('content')
<!-- Wrapper -->
<div id="wrapper">
    @include('employer.partials.header')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        @include('employer.partials.sidebar')
        <link rel="stylesheet" href="{{asset('backstyling/css/datatable.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@2.0.0/css/boxicons.min.css">
        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>My Oders</h3>
                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Dashboard</a></li>
                            <li>My Orders</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
                
                
                
                
<style type="text/css">
    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

.datehi { font-size: 1.5em; margin: 10px; }

/****** Style Star Rating Widget *****/

.rating { 
  border: none;
  float: left;
}

.rating > input { display: none; } 
.rating > label:before { 
  margin: 5px;
  font-size: 1.25em;
  font-family: FontAwesome;
  display: inline-block;
  content: "\f005";
}

.rating > .half:before { 
  content: "\f089";
  position: absolute;
}

.rating > label { 
  color: #ddd; 
 float: right; 
}

/***** CSS Magic to Highlight Stars on Hover *****/

.rating > input:checked ~ label, /* show gold star when clicked */
.rating:not(:checked) > label:hover, /* hover current star */
.rating:not(:checked) > label:hover ~ label { color: #FFD700;  } /* hover previous stars in list */

.rating > input:checked + label:hover, /* hover current star when changing rating */
.rating > input:checked ~ label:hover,
.rating > label:hover ~ input:checked ~ label, /* lighten current selection */
.rating > input:checked ~ label:hover ~ label { color: #FFED85;  } 
</style>
                
                
        <style type="text/css">
    .modal-backdrop {
  z-index: -1 !important;
}

</style>        
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
<div class="row">
    <!-- Dashboard Box -->
    <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">
            <!-- Headline -->
            <div class="headline">
                <h3><i class="icon-material-outline-business-center"></i> My Active Missions</h3>
            </div>
            <div class="content">
                <br>
                <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 col-6">
                                                <table id="example" class="table table-hover responsive nowrap" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Mission Name</th>
                                                            <th>Price</th>
                                                            <th>Freelancer Name</th>
                                                            <th>Full Offer/Milestone</th>
                                                            <th>Status</th>
                                                            <th>File</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
             @foreach ($myOrders as $order)
                                                        

@php
 //dd($order->project_id);
    $project = \App\project::find((int)$order->project_id);
    // dd($project);
    $mission = \App\Mission::find($project->mission_id);

@endphp
@if($mission)
@php
    $freelancer = \App\User::find($project->applicant_id);
 $order_delivery = DB::table('order_delivery')->where('order_id',$order->id)->first();
   //dd($order_delivery);
   $is_rating = DB::table('ratings')->where('mission_id',$mission->id)->first();
   $miles = DB::table('milestones')->where('project_id',$order->project_id)->get();
   $t_count = count($miles);
   //dd($t_count);
   //dd($miles);
@endphp

        @if($t_count>0)
        @foreach($miles as $milestone)
              <tr>
            <td><a href="{{url('/mission/'.$mission->id)}}" >{{$mission->title}}</a></td>
            
            <td>{{$milestone->milestone_amount}}</td>
            
            @if($freelancer)
                 <td><a href="{{url('/profile/'.$freelancer->id)}}" >{{$freelancer->name}}</a></td>
            @endif
            <td>Milestone</td>
            
            
                @if($milestone->type=="In progress")
            <td><font style="color:yellow;">{{$milestone->type}}</font></td>
            @endif
            
             @if($milestone->type=="cancelled")
            <td><font style="color:red;">{{$milestone->type}}</font></td>
            @endif
            
             @if($milestone->type=="completed")
            <td><font style="color:green;">{{$milestone->type}}</font></td>
            @endif
            
            @if($order_delivery)
            <td><a href="https://platform.gotem.io/{{$order_delivery->file}}?" class="btn btn-info" download>Download Files</a></td>
            @else
            <td></td>
            @endif
          
            @if($order->status == 'delivered')
            <td><a id="launchModalBtn" name="{{$order->id}}" class="button ripple-effect button-sliding-icon coolor">Accept Delivery <i class="icon-feather-arrow-right"></i></a></td>
            @elseif($order->status == 'accepted')
            @if($is_rating) 
            <!-- // rating start if -->
            <td>Thanks for you Rating!</td>
            @else
            <td><button class="btn btn-info" data-toggle="modal" data-target="#myModal{{$mission->id}}">Rating</button>


  <!-- Modal -->
  <div class="modal fade" id="myModal{{$mission->id}}" style="margin-top: 80px; margin-right: -30px;" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Rate Now</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="container">
            <form method="post" action="{{url('/employer/submit_rating')}}">
                @csrf
                <input type="text" name="mission_id" value="{{$mission->id}}">
                  @if($freelancer) 
                <input type="text" name="source_id" value="{{$freelancer->id}}" >
                @endif
              <div class="row">
                  <div class="col-md-4">
                      <h3 class="datehi">Rate the work!</h3>
<fieldset class="rating">
    <input type="radio" id="star5" name="rating"  value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4.5" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value=".5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
</fieldset>


                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label><h3 class="datehi">Description</h3></label>
                      <textarea class="form-control" name="description"></textarea>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-10"></div>
                  <div class="col-md-2">
                    <button class="btn btn-primary">Rate Now</button>
                  </div>
              </div>
              </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

            </td>
            
            @endif <!-- //rating end if -->
         @else 
         <td>  @if($milestone->type!="cancelled" || $order->status!="completed")<button class=" btn btn-danger" id="{{$milestone->id}}"  onclick="cancel('milestone',this.id)">Cancel </button> @endif </td>
            @endif
        </tr>
        @endforeach
      
        
        <!--for the else part of the tr-->
        @else  
         <tr>
            <td><a href="{{url('/mission/'.$mission->id)}}" >{{$mission->title}}</a></td>
            
            <td>{{$project->offer_amount}}</td>
            
            @if($freelancer)
                 <td><a href="{{url('/profile/'.$freelancer->id)}}" >{{$freelancer->name}}</a></td>
            @endif
            <td>Full Amount Offer</td>
            
            
            @if($order->status=="cancelled")
            <td><font style="color:red;">{{$order->status}}</font></td>
            @endif
            
            @if($order->status=="completed")
            <td><font style="color:green;">{{$order->status}}</font></td>
            @endif
            
            @if($order_delivery)
            <td><a href="https://platform.gotem.io/{{$order_delivery->file}}?" class="btn btn-info" download>Download Files</a></td>
            @else
            <td></td>
            @endif
          
            @if($order->status == 'delivered')
            <td><a id="launchModalBtn" name="{{$order->id}}" class="button ripple-effect button-sliding-icon coolor">Accept Delivery <i class="icon-feather-arrow-right"></i></a></td>
            @elseif($order->status == 'accepted')
            @if($is_rating) 
            <!-- // rating start if -->
            <td>Thanks for you Rating!</td>
            @else
            <td><button class="btn btn-info" data-toggle="modal" data-target="#myModal{{$mission->id}}">Rating</button>


  <!-- Modal -->
  <div class="modal fade" id="myModal{{$mission->id}}" style="margin-top: 80px; margin-right: -30px;" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Rate Now</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="container">
            <form method="post" action="{{url('/employer/submit_rating')}}">
                @csrf
                <input type="text" name="mission_id" value="{{$mission->id}}">
                  @if($freelancer) 
                <input type="text" name="source_id" value="{{$freelancer->id}}" >
                @endif
              <div class="row">
                  <div class="col-md-4">
                      <h3 class="datehi">Rate the work!</h3>
<fieldset class="rating">
    <input type="radio" id="star5" name="rating"  value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4.5" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3.5" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2.5" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1.5" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value=".5" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
</fieldset>

                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label><h3 class="datehi">Description</h3></label>
                      <textarea class="form-control" name="description"></textarea>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-10"></div>
                  <div class="col-md-2">
                    <button class="btn btn-primary">Rate Now</button>
                  </div>
              </div>
              </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

            </td>
            
            @endif <!-- //rating end if -->
         @else  
        
         <td>  @if($order->status=="completed")<button class=" btn btn-danger" id="{{$order->id}}"  onclick="cancel('full',this.id)">Cancel </button> @endif </td>
        
            @endif
        </tr>
        
        @endif
       
         @endif                                              
         @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
    
    
    
    
    
        <div class="modal fade" id="modal_1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body">
                    <form method="post" action="{{route('accept-work')}}">
                        @csrf
                     First Modal  
                    <input type="hidden" name="orderid" id="porder_id">
                    <center>
                        <h3>My Order</h3></center>
                    <br>
                   <textarea cols="30" rows="10" placeholder="Description"></textarea>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-3">
                            <a id="launchthird" class="button gray ripple-effect-dark">Decline</a>
                        </div>
                        <div class="col-xl-1"></div>
                        <div class="col-xl-3"><a class="button gray ripple-effect-dark">Revision</a></div>
                        <div class="col-xl-1"></div>
                        <div class="col-xl-3">
                            <input type="submit" class="button ripple-effect coolor" value="Accept">
                        </div>
                    </div>
                </form>
                     <!--End First Modal -->

                </div>
            </div>
        </div>
    </div>
    
     <div class="modal fade" id="modal_2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body">

                    <!--Start Second Modal  -->

                    <center>
                        <h3><b>Let's Roll!</b></h3></center>
                    <br>
                    <center>
                        <h3>Begin by funding your balance so <br> that your Source can begin your <br> mission.</h3></center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xl-2 col-md-2"></div>
                        <div class="col-xl-8 col-md-8">
                            <input placeholder="Enter Full Name">
                        </div>
                        <div class="col-xl-2 col-md-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xl-2 col-md-2"></div>
                        <div class="col-xl-8 col-md-8">
                            <input placeholder="Enter your E-mail">
                        </div>
                        <div class="col-xl-2 col-md-2"></div>
                    </div>
                    <br>
                    <br>
                    <center>
                        <h4>Balance in Bitcoin: 0.67 BTC</h4></center>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xl-3"></div>
                        <div class="col-xl-6">
                            <center>

                                <a id="launchforth" class="button ripple-effect coolor">Checkout</a>

                            </center>
                        </div>
                        <div class="col-xl-3"></div>
                    </div>
                     <!--End Second Modal -->
                </div>

            </div>

        </div>
    </div>
    
      <div class="modal fade" id="modal_3">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body">
                     <!--Start third Modal -->

                    <center>
                        <h3><b>Offer Declined!</b></h3></center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <center>
                        <h3>Try Messaging your source to come <br> to terms with an arrangements that <br> works better for you.</h3></center>
                    <br>
                    <br>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xl-3"></div>
                        <div class="col-xl-6">
                            <center><a data-dismiss="modal" data-dismiss="modal" aria-hidden="true" class="button ripple-effect coolor">Close</a></center>
                        </div>
                        <div class="col-xl-3"></div>
                    </div>
                </div>

                 <!--End third modal -->
            </div>
        </div>
    </div>

    <style>
        .scrolling {
            overflow-y: scroll;
            height: 570px;
        }

        @media(max-width:768px) {
            .scrolling {
                overflow-y: scroll;
                height: 750px;
            }
        }
    </style>

       <div class="modal fade" id="modal_4">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                 Start forth modal 
                <div class="modal-body scrolling">
                    <div class="row">
                        <div class="col-md-8">
                            <h5><b>Test Fixed Price Button</b></h5>
                        </div>
                        <div class="col-md-4">
                            <span style="text-align: right;">
              <h5>0.005 BTC</h5>
              </span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <h6>Name: David; Email Address: tawei@gotem.io</h6>
                        </div>
                    </div>
                    <hr>
                    <br>
                    <center>
                        <h3>Select Payment Currency</h3></center>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="border: 1px solid #7DBBFF; padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="30%">
                                        <span>Bitcoin</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 14px;">0.005 BTC</h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="switch-container">
                                            <label class="switch" style="font-size: 12px !important; margin-left: 40px;">
                                                <input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span> </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="25%">
                                        <span>XRP</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 8px;">190.91 XRP</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="15%">
                                        <span>Ethereum</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 8px;">0.2864 ETH</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style=" padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Official_Litecoin_Logo.png" width="30%">
                                        <span>Litecoin</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 14px;">0.897 LTC</h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="switch-container">
                                            <label class="switch" style="font-size: 12px !important; margin-left: 40px;">
                                                <input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span> </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div class="accordion js-accordion">

                                 Accordion Item 
                                <div class="accordion__item js-accordion-item">
                                    <div class="accordion-header js-accordion-header" style="background-color: silver;">More Currencies</div>

                                     Accordtion Body 
                                    <div class="accordion-body js-accordion-body">

                                         Accordion Content 
                                        <div class="accordion-body__contents">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <img src="https://cdn.freebiesupply.com/logos/large/2x/ripple-2-logo-png-transparent.png" width="25%">
                                                    <span>Ripple</span>
                                                </div>
                                                <div class="col-xl-6">
                                                    <h5 style="text-align: right; margin-top: 8px;">0.5864 RIP</h5>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                     Accordion Body / End 
                                </div>
                                 Accordion Item / End 
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10 col-md-10">
                            <div class="section-headline margin-top-25 margin-bottom-12">
                            </div>
                            <div class="input-with-icon-left no-border">
                                <i class="icon-material-baseline-mail-outline"></i>
                                <input type="text" class="input-text" placeholder="tawei@gotem.io">
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <center><a href="#" class="button ripple-effect">Pay with Bitcoin</a></center>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <center>
                                <p>Currency cannot be changed after proceeding</p>
                            </center>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                </div>
                 End forth modal 

            </div>
        </div>
    </div>

<input type="hidden" class="form-control" id="offer_id_change">
</div>
                <!-- Row / End -->
                <!-- Footer -->
                <div class="dashboard-footer-spacer"></div>
              
                <!-- Footer / End -->
            </div>
        </div>
        <!-- Dashboard Content / End -->
    </div>
    <!-- Dashboard Container / End -->
</div>
<!-- Wrapper / End -->

    <div id="myModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        {{-- <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <!-- First Modal  -->

                    <center>
                    <h3>You've Received an Offer!</h3></center>
                    <br>
                    <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                        <p>Turned it up should no valley cousin he. Speaking numerous ask did horrible packages set. Ashamed herself has distant can studied mrs. Led therefore its middleton perspetual fulfilled provision frankness. Small he drawn after among every three no. All having but you Edward genius though remark one.
                        <br> Turned it up should no valley cousin he. Speaking numerous ask did horrible packages set. Ashamed herself has distant can studied mrs. Led therefore its middleton perspetual fulfilled provision frankness. Small he drawn after among every three no. All having but you Edward genius though remark one.</p>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-2"></div>
                        <div class="col-xl-8">
                            <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                                <center>
                                <p>Quote: $4800</p>
                                <hr>
                                <br>
                                <p>$960 (20%) Retainer to Begin</p>
                                </center>
                            </div>
                        </div>
                        <div class="col-xl-2"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-12">
                            <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-9">
                                        <p>Milestone 1 - run skip trace, interview most recent contacts, provide report with evidence.</p>
                                    </div>
                                    <div class="col-xl-3">
                                        <div style="border: 1px solid silver; padding: 10px;">$1920</div>
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <div class="row">
                                    <div class="col-xl-9">
                                        <p>Milestone 2 - check missing person files with local law enforcement, fbi. Interview further contacts based on money trail via debit or cc.</p>
                                    </div>
                                    <div class="col-xl-3">
                                        <div style="border: 1px solid silver; padding: 10px;">$1920</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-5">
                            <!--<a href="#myModal2"  class="button gray ripple-effect-dark" data-toggle="modal" >Decline Offer</a>-->
                            
                        </div>
                        <div class="col-xl-3"></div>
                        <div class="col-xl-4">
                            <a href="#myModal1" class="button ripple-effect coolor" data-toggle="modal">Accept Offer</a>
                        </div>
                    </div>
                    <!-- End First Modal -->
                </div>
            </div>
        </div> --}}
    </div>

<div id="myModal1" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!--Start Second Modal  -->

                <center>
                <h3><b>Let's Roll!</b></h3></center>
                <br>
                <center>
                <h3>Begin by funding your balance so <br> that your Source can begin your <br> mission.</h3></center>
                <br>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-2 col-md-2"></div>
                    <div class="col-xl-8 col-md-8">
                        <input type="text" placeholder="Enter Full Name" id="name">
                    </div>
                    <div class="col-xl-2 col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-md-2"></div>
                    <div class="col-xl-8 col-md-8">
                        <input type="email" placeholder="Enter your E-mail" id="email">
                    </div>
                    <div class="col-xl-2 col-md-2"></div>
                </div>
                <br>
                {{-- <br> --}}
                {{-- <center> --}}
                {{-- <h4>Balance in Bitcoin: 0.67 BTC</h4></center> --}}
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <center>

                        {{-- <a href="#myModal3" data-toggle="modal" class="button ripple-effect coolor">Checkout</a> --}}
                        <a id="checkout" class="button ripple-effect coolor">Checkout</a>

                        </center>
                    </div>
                    <div class="col-xl-3"></div>
                </div>
                <!-- End Second Modal -->
            </div>
        </div>
    </div>
</div>
<div id="myModal2" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!-- Start third Modal -->

                <center>
                <h3><b>Offer Declined!</b></h3></center>
                <br>
                <br>
                <br>
                <br>
                <center>
                <h3>Try Messaging your source to come <br> to terms with an arrangements that <br> works better for you.</h3></center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <center><a data-dismiss="modal" data-dismiss="modal" aria-hidden="true" class="button ripple-effect coolor" >Close</a></center>
                    </div>
                    <div class="col-xl-3"></div>
                </div>
            </div>
            <!-- End third modal -->
        </div>
    </div>
</div>
<div id="myModal3" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- Start forth modal -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <h5><b>Test Fixed Price Button</b></h5>
                    </div>
                    <div class="col-md-4">
                        <span style="text-align: right;">
                            <h5>0.005 BTC</h5>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h6>Name: David; Email Address: tawei@gotem.io</h6>
                    </div>
                </div>
                <hr>
                <br>
                <center>
                <h3>Select Payment Currency</h3></center>
                <br>
                <div class="row">
                    <div class="col-xl-1"></div>
                    <div class="col-xl-10">
                        <div style="border: 1px solid #7DBBFF; padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                            <div class="row">
                                <div class="col-xl-6">
                                    <img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="30%">
                                    <span>Bitcoin</span>
                                </div>
                                <div class="col-xl-6">
                                    <h5 style="text-align: right; margin-top: 14px;">0.005 BTC</h5>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xl-8">
                                    <div class="switch-container">
                                        <label class="switch" style="font-size: 12px !important; margin-left: 40px;">
                                            <input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                    <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="25%">
                                        <span>XRP</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 8px;">190.91 XRP</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="15%">
                                        <span>Ethereum</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 8px;">0.2864 ETH</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style=" padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Official_Litecoin_Logo.png" width="30%">
                                        <span>Litecoin</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 14px;">0.897 LTC</h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="switch-container">
                                            <label class="switch" style="font-size: 12px !important; margin-left: 40px;">
                                                <input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10">
                                <div class="accordion js-accordion">
                                    <!-- Accordion Item -->
                                    <div class="accordion__item js-accordion-item">
                                        <div class="accordion-header js-accordion-header" style="background-color: silver;">More Currencies</div>
                                        <!-- Accordtion Body -->
                                        <div class="accordion-body js-accordion-body">
                                            <!-- Accordion Content -->
                                            <div class="accordion-body__contents">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <img src="https://cdn.freebiesupply.com/logos/large/2x/ripple-2-logo-png-transparent.png" width="25%">
                                                        <span>Ripple</span>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <h5 style="text-align: right; margin-top: 8px;">0.5864 RIP</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Accordion Body / End -->
                                    </div>
                                    <!-- Accordion Item / End -->
                                </div>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10 col-md-10">
                                <div class="section-headline margin-top-25 margin-bottom-12">
                                </div>
                                <div class="input-with-icon-left no-border">
                                    <i class="icon-material-baseline-mail-outline"></i>
                                    <input type="text" class="input-text" placeholder="tawei@gotem.io">
                                </div>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10">
                                <center><a href="#" class="button ripple-effect">Pay with Bitcoin</a></center>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10">
                                <center>
                                <p>Currency cannot be changed after proceeding</p>
                                </center>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                    </div>
                    <!-- End forth modal -->
                </div>
            </div>
        </div>
        <!-- Apply for a job popup / End -->
        @endsection
        @section('scripts')
        <!-- Scripts
        ================================================== -->
        <script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
        {{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
        <script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
        <script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
        <script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
        <script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
        <script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('backstyling/js/snackbar.js')}}"></script>
        <script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
        <script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
        <script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
        <script src="{{asset('backstyling/js/slick.min.js')}}"></script>
        <script src="{{asset('backstyling/js/custom.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script>
            $(document).ready(function() {
        $("#example").DataTable({
        aaSorting: [],
        responsive: true,
        columnDefs: [
        {
        responsivePriority: 1,
        targets: 0
        },
        {
        responsivePriority: 2,
        targets: -1
        }
        ]
        });
        $(".dataTables_filter input")
        .attr("placeholder", "Search Forms...")
        .css({
        width: "260px",
        display: "inline-block"
        });
        $('[data-toggle="tooltip"]').tooltip();
        });
        </script>
        <!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
        <script>
        // Snackbar for user status switcher
        $('#snackbar-user-status label').click(function() {

            Snackbar.show({
                text: 'Your status has been changed!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#383838'
            });
        });

        // jQuery(document).ready(function(){
            jQuery('.offer-modal').click(function(e){
                let offer_id = e.target.id;
                // alert(offer_id);
               e.preventDefault();
               $.ajaxSetup({
                  // headers: {
                  //     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  // }
                   
              });
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
               jQuery.ajax({
                  url: "{{ url('getoffer') }}",
                  method: 'post',
                  data: {_token: CSRF_TOKEN,offer_id: offer_id },
                  success: function(result){
                     console.log(result);
                     $('#myModal').html(result);
                     // $('#modal').modal();
                  }});
               });
            // });

            $('#checkout').click(function(){
                let name = $('#name').val();
                let email = $('#email').val();
                if(name=="" || email==""){
                    alert('Please input name and email');
                    return fasle;
                }else{
                    window.location.href = 'https://platform.gotem.io/coingate';
                }
            });
            
            
            
            function get_id(id){
                $('#offer_id_change').val(id);
            }
            
            function cancel(type,id)
            {
                  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
      
        var request = $.ajax({
        url: "{{route('cancel.offer')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, type:type,order_id:id},
        dataType: "html"
        });
              request.done(function( msg ) {
                //   alert(msg);
                //   $('#myModal1').open();
              location.reload();
      });  
            }
         
          
        </script>
        @endsection