@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">
@php
    $authId = auth()->user()->id;
@endphp

    @include('freelancer.partials.header')


    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')


   <!-- Dashboard Content
    ================================================== -->
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" style="background-color: #36383E;">

            <!-- Dashboard Headline -->
           <!-- <div class="dashboard-headline">
                <h3>Group Messages</h3>-->

                <!-- Breadcrumbs -->
               <!-- <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Messages</li>
                    </ul>
                </nav>
            </div>-->

              <!--  <div class="messages-container margin-top-0">-->

                   <!-- <div class="messages-container-inner">-->

                        <!-- Messages -->

                        <!-- Messages / End -->

                        <!-- Message Content -->
                        <div class="message-content" style="background-color: #36383E; margin-top:-60px;">

                            <div class="messages-headline" style="background-color: #36383E;">
                                <h4 style="color:#F0F4E6;">Group Chat</h4>
                               <!-- <h4>{{auth()->user()->name}}</h4>-->
                                <!--<a href="#" class="message-action"><i class="icon-feather-trash-2"></i> Delete Conversation</a>-->
                            </div>

                            <!-- Message Content Inner -->
                            <div class="message-content-inner messenger-body" style="max-height: 62vh;">

                                    <!-- Time Sign -->
                                    <!-- <div class="message-time-sign">
                                        <span style="background-color: #36383E; color:#F0F4E6;">28 June, 2019</span>
                                    </div> -->
                                    @foreach ((object)$messages as $key => $message)
                                    @php
                                        $user = \App\User::find($message->sender_id);
                                       if($user){
                                        if ($user->is_verified) {
                                                $pic = str_replace('public', 'public', $user->profile->avatar);
                                            } else {
                                                $pic = '/images/users_images/user-avatar.jpg';
                                        }
                                        }
                                    @endphp

                                    @if ($message->sender_id != $authId)
                                        <div class="message-bubble">
                                            <div class="message-bubble-inner">
                                                <div class="message-avatar" style="margin-top: 25px;"><img src="{{url('/')}}/{{$pic}}" alt="" /></div>
                                                <span style="margin-left: 75px; font-weight:600; color:#BFC3CD;font-size:14px;">@if($user){{$user->name}}@endif</span>
                                                <br>
                                                <div class="message-text"><p>{{$message->message}}</p></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>


                                    @else
                                    <style>
                                        .manage{
                                            width:93%;
                                        }
                                        @media (max-width:768px){
                                         .manage{
                                            width:81%;
                                        }
                                        }
                                    </style>

                                        <div class="message-bubble me">
                                        <div class="message-bubble-inner">
                                            <div class="message-avatar" style="margin-top:25px;"><img src="{{url('/')}}/{{$pic}}" alt="" /></div>
                                             <div class="manage">
                                                 <span style=" float:right; margin-top:0px; font-weight:600; color:#BFC3CD;font-size:14px;">{{$user->name}}</span>
                                                 </div>
                                                 <br>
                                            <div class="message-text"><p>{{$message->message}}</p></div>

                                        </div>
                                        <div class="clearfix"></div>
                                        </div>
                                    @endif
                                    @endforeach
                            </div>
                            <!-- Message Content Inner / End -->

                            <!-- Reply Area -->
                            <div class="message-reply">
                            <input type="hidden" name="receiverId" value="0">
                                <textarea id="message-body" name="message" style="max-height:35px !important; background-color:#36383E;" cols="1" rows="1" placeholder="Your Message" data-autoresize></textarea>
                                <button class="button ripple-effect" type="submit" id="send-btn">Send</button>
                            </div>

                        </div>
                        <!-- Message Content -->

                    </div>
            </div>
            <!-- Messages Container / End -->




            <!-- Footer -->

            <div class="dashboard-footer-spacer"></div>


        </div>
    </div>
    <!-- Dashboard Content / End -->



    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup ================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit"  form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts ================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>

{{-- messenger --}}
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
        var withId        = 0,
            authId        = {{auth()->id()}},
            messagesCount = {{count($messages)}};
            pusher        = new Pusher('{{config('messenger.pusher.app_key')}}', {

              cluster: '{{config('messenger.pusher.options.cluster')}}'
            });
            pusher.logToConsole = true;
    </script>
    <script src="{{asset('/')}}/vendor/messenger/js/publicmessenger-chat.js" charset="utf-8"></script>
{{-- ends --}}
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
{{-- <script src="{{asset('backstyling/js/infobox.min.js')}}"></script> --}}
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/maps.js')}}"></script> --}}
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
   var height = 0;
$('div p').each(function(i, value){
    height += parseInt($(this).height());
});

height += '';

$('div').animate({scrollTop: height});

</script>
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });

    $(document).on('keypress',function(e) {
    if(e.which == 13) {
       document.getElementById("send-btn").click();
    }
});
</script>


@endsection