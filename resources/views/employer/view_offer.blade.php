@extends('employer.layouts.master')
@section('content')
<!-- Wrapper -->
<div id="wrapper">
    @include('employer.partials.header')
    <!-- Dashboard Container -->
    <div class="dashboard-container">
        @include('employer.partials.sidebar')
        <link rel="stylesheet" href="{{asset('backstyling/css/datatable.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@2.0.0/css/boxicons.min.css">
        <!-- Dashboard Content
        ================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            <div class="dashboard-content-inner" >

                <!-- Dashboard Headline -->
                <div class="dashboard-headline">
                    <h3>View Offers</h3>
                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Dashboard</a></li>
                            <li>View Offers</li>
                        </ul>
                    </nav>
                </div>

                <!-- Row -->
<div class="row">
    <!-- Dashboard Box -->
    <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">
            <!-- Headline -->
            <div class="headline">
                <h3><i class="icon-material-outline-business-center"></i> View Offers</h3>
            </div>
            <div class="content">
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-6">
                            <table id="example" class="table table-hover responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Freelancer Name</th>
                                        <th>My Mission</th>
                                        <th>Offer Type</th>
                                        <th>View Offer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($offers as $offer)

                                        <tr>
                                            <td>
                                                <a href="#">
                                                    <div class="d-flex align-items-center">
                                                        <div class="avatar avatar-blue mr-3"><span class="innner">EB</span></div>
                                                        <a href="{{url('/profile/'.$offer->applicant_id)}}" >
                                                        <div class="">
                                                            <p class="font-weight-bold mb-0">@if($offer->applicant){{$offer->applicant->name}}@endif</p>
                                                            <p class="text-muted mb-0">TD Ameritrade</p>
                                                        </div>
                                                        </a>
                                                    </div>
                                                </a>
                                            </td>
                                            <td>Freelancer</td>
                                            <td>@if($offer->is_milestone==1) Milestone @else FUll Payment  @endif </td>

                                            <td>
                                                <a href="{{route('milestone',[ $offer->id ])}}"  class="button ripple-effect button-sliding-icon coolor" >View Offer <i class="icon-feather-arrow-right"></i></a>
                                            </td>

                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
<input type="hidden" class="form-control" id="offer_id_change">
</div>
                <!-- Row / End -->
                <!-- Footer -->
                <div class="dashboard-footer-spacer"></div>

                <!-- Footer / End -->
            </div>
        </div>
        <!-- Dashboard Content / End -->
    </div>
    <!-- Dashboard Container / End -->
</div>
<!-- Wrapper / End -->

    <div id="myModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        {{-- <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <!-- First Modal  -->

                    <center>
                    <h3>You've Received an Offer!</h3></center>
                    <br>
                    <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                        <p>Turned it up should no valley cousin he. Speaking numerous ask did horrible packages set. Ashamed herself has distant can studied mrs. Led therefore its middleton perspetual fulfilled provision frankness. Small he drawn after among every three no. All having but you Edward genius though remark one.
                        <br> Turned it up should no valley cousin he. Speaking numerous ask did horrible packages set. Ashamed herself has distant can studied mrs. Led therefore its middleton perspetual fulfilled provision frankness. Small he drawn after among every three no. All having but you Edward genius though remark one.</p>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-2"></div>
                        <div class="col-xl-8">
                            <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                                <center>
                                <p>Quote: $4800</p>
                                <hr>
                                <br>
                                <p>$960 (20%) Retainer to Begin</p>
                                </center>
                            </div>
                        </div>
                        <div class="col-xl-2"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-12">
                            <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-9">
                                        <p>Milestone 1 - run skip trace, interview most recent contacts, provide report with evidence.</p>
                                    </div>
                                    <div class="col-xl-3">
                                        <div style="border: 1px solid silver; padding: 10px;">$1920</div>
                                    </div>
                                </div>
                                <hr>
                                <br>
                                <div class="row">
                                    <div class="col-xl-9">
                                        <p>Milestone 2 - check missing person files with local law enforcement, fbi. Interview further contacts based on money trail via debit or cc.</p>
                                    </div>
                                    <div class="col-xl-3">
                                        <div style="border: 1px solid silver; padding: 10px;">$1920</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-5">
                            <!--<a href="#myModal2"  class="button gray ripple-effect-dark" data-toggle="modal" >Decline Offer</a>-->

                        </div>
                        <div class="col-xl-3"></div>
                        <div class="col-xl-4">
                            <a href="#myModal1" class="button ripple-effect coolor" data-toggle="modal">Accept </a>
                        </div>
                    </div>
                    <!-- End First Modal -->
                </div>
            </div>
        </div> --}}
    </div>

<div id="myModal1" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!--Start Second Modal  -->

                <center>
                <h3><b>Let's Roll!</b></h3></center>
                <br>
                <center>
                <h3>Begin by funding your balance so <br> that your Source can begin your <br> mission.</h3></center>
                <br>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-2 col-md-2"></div>
                    <div class="col-xl-8 col-md-8">
                        <input type="text" placeholder="Enter Full Name" id="name">
                    </div>
                    <div class="col-xl-2 col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-xl-2 col-md-2"></div>
                    <div class="col-xl-8 col-md-8">
                        <input type="email" placeholder="Enter your E-mail" id="email">
                    </div>
                    <div class="col-xl-2 col-md-2"></div>
                </div>
                <br>
                {{-- <br> --}}
                {{-- <center> --}}
                {{-- <h4>Balance in Bitcoin: 0.67 BTC</h4></center> --}}
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <center>

                        {{-- <a href="#myModal3" data-toggle="modal" class="button ripple-effect coolor">Checkout</a> --}}
                        <a id="checkout" class="button ripple-effect coolor">Checkout1</a>

                        </center>
                    </div>
                    <div class="col-xl-3"></div>
                </div>
                <!-- End Second Modal -->
            </div>
        </div>
    </div>
</div>
<div id="myModal2" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!-- Start third Modal -->

                <center>
                <h3><b>Offer Declined!</b></h3></center>
                <br>
                <br>
                <br>
                <br>
                <center>
                <h3>Try Messaging your source to come <br> to terms with an arrangements that <br> works better for you.</h3></center>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <center><a data-dismiss="modal" data-dismiss="modal" aria-hidden="true" class="button ripple-effect coolor" >Close</a></center>
                    </div>
                    <div class="col-xl-3"></div>
                </div>
            </div>
            <!-- End third modal -->
        </div>
    </div>
</div>
<div id="myModal3" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <!-- Start forth modal -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8">
                        <h5><b>Test Fixed Price Button</b></h5>
                    </div>
                    <div class="col-md-4">
                        <span style="text-align: right;">
                            <h5>0.005 BTC</h5>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <h6>Name: David; Email Address: tawei@gotem.io</h6>
                    </div>
                </div>
                <hr>
                <br>
                <center>
                <h3>Select Payment Currency</h3></center>
                <br>
                <div class="row">
                    <div class="col-xl-1"></div>
                    <div class="col-xl-10">
                        <div style="border: 1px solid #7DBBFF; padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                            <div class="row">
                                <div class="col-xl-6">
                                    <img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="30%">
                                    <span>Bitcoin</span>
                                </div>
                                <div class="col-xl-6">
                                    <h5 style="text-align: right; margin-top: 14px;">0.005 BTC</h5>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xl-8">
                                    <div class="switch-container">
                                        <label class="switch" style="font-size: 12px !important; margin-left: 40px;">
                                            <input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                    <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span> </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="25%">
                                        <span>XRP</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 8px;">190.91 XRP</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="15%">
                                        <span>Ethereum</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 8px;">0.2864 ETH</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-1"></div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xl-1"></div>
                        <div class="col-xl-10">
                            <div style=" padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
                                <div class="row">
                                    <div class="col-xl-6">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Official_Litecoin_Logo.png" width="30%">
                                        <span>Litecoin</span>
                                    </div>
                                    <div class="col-xl-6">
                                        <h5 style="text-align: right; margin-top: 14px;">0.897 LTC</h5>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="switch-container">
                                            <label class="switch" style="font-size: 12px !important; margin-left: 40px;">
                                                <input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10">
                                <div class="accordion js-accordion">
                                    <!-- Accordion Item -->
                                    <div class="accordion__item js-accordion-item">
                                        <div class="accordion-header js-accordion-header" style="background-color: silver;">More Currencies</div>
                                        <!-- Accordtion Body -->
                                        <div class="accordion-body js-accordion-body">
                                            <!-- Accordion Content -->
                                            <div class="accordion-body__contents">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <img src="https://cdn.freebiesupply.com/logos/large/2x/ripple-2-logo-png-transparent.png" width="25%">
                                                        <span>Ripple</span>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <h5 style="text-align: right; margin-top: 8px;">0.5864 RIP</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Accordion Body / End -->
                                    </div>
                                    <!-- Accordion Item / End -->
                                </div>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10 col-md-10">
                                <div class="section-headline margin-top-25 margin-bottom-12">
                                </div>
                                <div class="input-with-icon-left no-border">
                                    <i class="icon-material-baseline-mail-outline"></i>
                                    <input type="text" class="input-text" placeholder="tawei@gotem.io">
                                </div>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10">
                                <center><a href="#" class="button ripple-effect">Pay with Bitcoin</a></center>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-xl-1"></div>
                            <div class="col-xl-10">
                                <center>
                                <p>Currency cannot be changed after proceeding</p>
                                </center>
                            </div>
                            <div class="col-xl-1"></div>
                        </div>
                    </div>
                    <!-- End forth modal -->
                </div>
            </div>
        </div>
        <!-- Apply for a job popup / End -->
        @endsection
        @section('scripts')
        <!-- Scripts
        ================================================== -->
        <script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
        {{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
        <script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
        <script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
        <script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
        <script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
        <script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
        <script src="{{asset('backstyling/js/snackbar.js')}}"></script>
        <script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
        <script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
        <script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
        <script src="{{asset('backstyling/js/slick.min.js')}}"></script>
        <script src="{{asset('backstyling/js/custom.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script>
            $(document).ready(function() {
        $("#example").DataTable({
        aaSorting: [],
        responsive: true,
        columnDefs: [
        {
        responsivePriority: 1,
        targets: 0
        },
        {
        responsivePriority: 2,
        targets: -1
        }
        ]
        });
        $(".dataTables_filter input")
        .attr("placeholder", "Search Forms...")
        .css({
        width: "260px",
        display: "inline-block"
        });
        $('[data-toggle="tooltip"]').tooltip();
        });
        </script>
        <!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
        <script>
        // Snackbar for user status switcher
        $('#snackbar-user-status label').click(function() {

            Snackbar.show({
                text: 'Your status has been changed!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#383838'
            });
        });

        // jQuery(document).ready(function(){
            jQuery('.offer-modal').click(function(e){
                let offer_id = e.target.id;
                // alert(offer_id);
               e.preventDefault();
               $.ajaxSetup({
                  // headers: {
                  //     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  // }

              });
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
               jQuery.ajax({
                  url: "{{ url('getoffer') }}",
                  method: 'post',
                  data: {_token: CSRF_TOKEN,offer_id: offer_id },
                  success: function(result){
                     console.log(result);
                     $('#myModal').html(result);
                     // $('#modal').modal();
                  }});
               });
            // });

            $('#checkout').click(function(){
                let name = $('#name').val();
                let email = $('#email').val();
                if(name=="" || email==""){
                    alert('Please input name and email');
                    return fasle;
                }else{
                    window.location.href = 'https://platform.gotem.io/coingate';
                }
            });



            function get_id(id){
                $('#offer_id_change').val(id);
            }

            function decline(value)
            {

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var chck = 0 ;
        var oid = $('#offer_id_change').val();
        var request = $.ajax({
        url: "{{route('change.offer_status')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, offer:oid,chck:chck,mile_id:value},
        dataType: "html"
        });
               request.done(function( msg ) {
              location.reload();
      });
            }

            function accept_offer(value){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var oid = $('#offer_id_change').val();
        var chck = 1 ;
        var request = $.ajax({
        url: "{{route('cancel.offer')}}",
        method: "post",
        data: {_token: CSRF_TOKEN, offer:oid,chck:chck,mile_id:value},
        dataType: "html"
        });
              request.done(function( msg ) {
                //   $('#myModal1').open();
            //   location.reload();
      });
            }

        </script>

        @endsection
