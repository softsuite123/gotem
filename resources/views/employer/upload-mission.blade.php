@extends('employer.layouts.master')

@section('content')

<!-- Wrapper -->
<style>
.img_hover:hover{
    background-color:#2a41e8;
}

</style>
<div id="wrapper">
    @if (Session::has('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{!! Session('success') !!}</strong>
    </div>
    @endif

    @if (Session::has('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{!! Session('error') !!}</strong>
    </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @include('employer.partials.header')

<!-- Dashboard Content
    ================================================== -->
<div class="dashboard-container">

    @include('employer.partials.sidebar')
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner">



            <!-- Row -->
            <div class="row">

                <form action="{{url('verify_upload_mission')}}" method="POST" enctype="multipart/form-data" name="myForm" onsubmit="return validateForm()">
                    @csrf
                <!-- Dashboard Box -->
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-feather-upload"></i> Upload Mission Detail</h3>
                        </div>

                        <div class="content with-padding padding-bottom-10">
                            <div class="row">

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Mission Title</h5>
                                        <span style="color:red" id="title_name">Title must be filled</span>
                                        <input type="text" class="with-border" value="{{ Request::old('title') }}" id="title" name="title">
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Mission Type</h5>
                                        <span style="color:red" id="mission_type_v">Mission type is requied</span>
                                        <select class=" with-border" data-size="7" title="Select Mission Type" name="mission_type" id="mission_type">
                                           <option value="" selected disabled>Select One</option>
                                            @if(old('mission_type') == "In-person_Investigation")
                                            <option selected value="In-person_Investigation">In-person Investigation</option>
                                        
                                            
                                            @else
                                               <option value="In-person_Investigation">In-person Investigation</option>
                                           
                                            @endif
                                            
                                            
                                              @if(old('mission_type') == "Cyber_Investigation")
                                         
                                            <option value="Cyber_Investigation">Cyber Investigation</option>
                                            
                                            @else
                                            
                                            <option value="Cyber_Investigation">Cyber Investigation</option>
                                            @endif
                                          
                                        </select>
                                    </div>
                                      <div class="submit-field">
                                        <h5>Mission Category</h5>
                                         <span style="color:red" id="mission_category_v">Mission category is requied</span>
                                        <select class="selectpicker with-border mdb-select md-form" data-size="7"
                                            title="Select Mission Category" multiple searchable="Search here.." name="mission_company[]" id="mission_category">
                                            @foreach ($companies as $company)
                                            @if (old('mission_company') == $company->id)
                                            <option value="{{ $company->id }}" selected>{{ $company->name }}</option>
                                              @else.
                                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                                                  @endif.
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                   <div class="submit-field">
                                
                                        <h5>Location <span style="float: right;"><input type="checkbox" id="is_remote" onclick="disable_input()" name="is_remote"
                                                    style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Remote
                                                Mission</span></h5>
                                        <div class="input-with-icon">
                                            <div id="autocomplete-container">
                                                <!-- <input id="autocomplete-input pac-input" class="with-border" type="text"
                                                    placeholder="Type Address" name="address"> -->

                                                       <input id="pac-input" value="{{ Request::old('address') }}" name="address" class="with-border controls" type="text" placeholder="Search Box">
    <div id="map"></div>
  
                                            </div>
                                            <i class="icon-material-outline-location-on"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Estimated Budget</h5>
                                         <span style="color:red" id="estimated_budget_v">Budget is requied</span>
                                        <input type="text" value="{{ Request::old('estimated_budget') }}" class="with-border" name="estimated_budget" id="estimated_budget">
                                    </div>
                                </div>
                                <div class="col-xl-8">
                                   <div class="submit-field">

                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                <span style="color:red" id="deadline_v">Deadline is requied</span>
                                                    <h5>Deadline <span style="float: right;"><input type="checkbox" name="is_urgent"
                                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Urgent</span>
                                                    </h5>
                                                    <input type="date" value="{{ Request::old('deadline') }}" name="deadline" id="deadline">
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Mission Privacy</h5>
                                                     <span style="color:red" id="mission_privacy_v">Mission privacy is requied</span>
                                                    <select  class=" with-border" data-size="7"
                                                        title="Mission Privacy" name="mission_privacy" id="mission_privacy">
                                                         <option value="" selected disabled>Select One</option>
                                            @if(old('mission_privacy') == "Private")
                                            <option selected value="Private">Private</option>
                                        
                                            
                                            @else
                                               <option value="Private">Private</option>
                                           
                                            @endif
                                            
                                            
                                              @if(old('mission_privacy') == "Public")
                                         
                                            <option value="Public">Public</option>
                                            
                                            @else
                                            
                                            <option value="Public">Public</option>
                                            @endif
                                                        <!--<option>Private</option>-->
                                                        <!--<option>Public</option>-->
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-xl-4">
                                  
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Mission General Description</h5>
                                        <span style="color:red" id="mission_description_v">Mission general description is requied</span>
                                        <textarea cols="30"  rows="5" class="with-border" name="mission_description" id="mission_description">{{ old('mission_description') }}</textarea>

                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Mission Objectives</h5>
                                         <span style="color:red" id="mission_objective_v">Mission general description is requied</span>
                                        <textarea cols="30" rows="5" class="with-border" name="mission_objective" id="mission_objective">{{ old('mission_objective') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class=" margin-top-30">
                                        <h5 class="">Upload Mission Image</h5>
                                        
                                        <div class="col-xl-12" >
                                           
                                            <center>
                                                <input  type="file"
                                                     required  class="img_hover" multiple name="mission_files[]"/>
                                                <!--<label class="uploadButton-button ripple-effect" for="upload"><i-->
                                                <!--        class="icon-feather-upload" style="font-size: 25px;"></i></label>-->
                                            </center>
                                        </div>
                        
                                    </div>
                                </div>
                                
                                
                                <div class="col-xl-12">
                                    <div class="uploadButton margin-top-30">
                                        <h5 class="uploadButton-file-name">Upload Relevant Files (Default is Private)</h5>
                                        <span style=" margin-top: 14px;"><input type="checkbox" name="share_with_public"
                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Share
                                            with Public</span>
                                        <div class="col-xl-12">
                                            <center>
                                                <input class="uploadButton-input" type="file"
                                                    accept="image/*, application/pdf" id="upload1" multiple name="mission_all_files[]"/>
                                                <label class="uploadButton-button ripple-effect" for="upload1"><i
                                                        class="icon-feather-upload" style="font-size: 25px;"></i></label>
                                            </center>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="margin-top-30">
                                        <div style=" margin-top: 14px;"><input type="checkbox" name="enable_crowdfunding"
                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Enable
                                            Crowdfunding</div>

                                        <div class="" style="font-size: 12px;">* This option will
                                            enable delegative voting</div>
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="submit-field">

                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Minimum Raise Amount</h5>
                                                    <input type="text" value="{{ Request::old('min_raise_amount') }}" class="with-border" name="min_raise_amount">
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Maximum Raise Amount</h5>
                                                    <input type="text" value="{{ Request::old('max_raise_amount') }}"  class="with-border" name="max_raise_amount">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6"></div>
                                <div class="col-xl-12">
                                    <div style=" margin-top: 14px;"><input type="checkbox" name="allow_multiple_source_participate"
                                            style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Allow
                                        Multiple Sources to Participate</div>
                                </div>

                  <input type="hidden" name="latitude" id="latitude">           
                  <input type="hidden" name="longitude" id="longitude">
                <div class="col-xl-12">
                    <button type="submit" class="button ripple-effect big margin-top-30" onclick"check_validation()">
                        <!-- <i class="icon-feather-plus"></i> --> Submit and Review Draft</button>
                </div>

                            </div>
                        </div>
                    </div>
                </div>
<br><br>

            </form>
            </div>
            <!-- Row / End -->

            <!-- Footer -->
     
            <!-- Footer / End -->

        </div>
    </div>
</div>
<!-- Dashboard Content / End -->

</div>


<div style="display: none;">

    <button onclick="getLocation()" id="getLocation"></button>

</div>
@endsection

@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>
  <script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();
          console.log('places');
          console.log(places);

          if (places.length == 0) {
            return;
          }

        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          console.log('boubds');
          console.log(bounds);
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();  
            console.log('latitude , longitude');
            console.log(latitude);
            console.log(longitude);
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
      $(document).ready(function() {
$('.mdb-select').materialSelect();

   

});

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV7WeUUT24I6Zog1Kqm_H0ZJcu548R72A&libraries=places&callback=initAutocomplete"
         async defer></script>

<script>
$( document ).ready(function() {
   $('#title_name').hide();
   $('#mission_type_v').hide();
   $('#mission_category_v').hide();
   $('#deadline_v').hide();
   $('#estimated_budget_v').hide();
   $('#mission_privacy_v').hide();
   $('#mission_description_v').hide();
   $('#mission_objective_v').hide();
});

//          window.onload=function(){
//   document.getElementById("getLocation").click();
  
// };
// var x = document.getElementById("getLocation");

// function getLocation() {
//   if (navigator.geolocation) {
//     navigator.geolocation.getCurrentPosition(showPosition);
//   } else { 
//     x.innerHTML = "Geolocation is not supported by this browser.";
//   }
// }

// function showPosition(position) {
 

// $('#latitude').val(position.coords.latitude);
// $('#longitude').val(position.coords.longitude);

// }

function disable_input()
{
    var ch = $('#is_remote').is(':checked');
    if (ch==true) {
       $('#pac-input').prop('disabled', true);
       $('#pac-input').val("");
       $('#pac-input').css('background-color','black');
    }
    else if(ch==false)
    {
     $('#pac-input').prop('disabled', false);
       $('#pac-input').css('background-color','white');   
    }
}


function validateForm() {
   var title = document.forms["myForm"]["title"].value;
   var mission_type = document.forms["myForm"]["mission_type"].value;
   var mission_company = document.forms["myForm"]["mission_company[]"].value; 
   var deadline= document.forms["myForm"]["deadline"].value; 
   var budget= document.forms["myForm"]["estimated_budget"].value;
      var mission_privacy= document.forms["myForm"]["mission_privacy"].value; 
      var mission_description= document.forms["myForm"]["mission_description"].value; 
      var mission_objective= document.forms["myForm"]["mission_objective"].value; 
   
  if (title == "" ) {

   $('#title_name').show();  
   $('#title').focus();
    return false;
  }
  else if(title != "" ){
   $('#title_name').hide(); 
  }
 
 if(mission_type==""){
       $('#mission_type_v').show();  
       $('#mission_type').focus();
      
      return false;
  }
  else if(mission_type!=""){
  $('#mission_type_v').hide();  	
 }
  
 if(mission_company==""){ 
        $('#mission_category_v').show();
        $('#mission_category').focus();
        return false;
  }
  
  else if(mission_company!=""){
  	 $('#mission_category_v').hide();  
  }
 if(budget==""){
        $('#estimated_budget_v').show();
        $('#estimated_budget').focus();
      return false;
  }
  
  else if(budget!=""){
   $('#estimated_budget_v').hide();
  }
  
    if(deadline=="" ){
        
        $('#deadline_v').show();
        $('#deadline').focus();
      return false;
  }
  
  else if(deadline!=""){
   $('#deadline_v').hide();
  }
  
  if(mission_privacy==""){
      $('#mission_privacy_v').show();
      $('#mission_privacy').focus();
       return false;
  }
   else if(mission_privacy!=""){
        
        $('#mission_privacy_v').hide();
  }
  
  if(mission_description==""){
      $('#mission_description_v').show();
      $('#mission_description').focus();
       return false;
  }
   else if(mission_description!=""){
        
        $('#mission_description_v').hide();
  }
  
//     if(mission_objective==""){
//       $('#mission_objective_v').show();
//       $('#mission_objective').focus();
//       return false;
//   }
//   else if(mission_objective!=""){
//         $('#mission_objective_v').hide();
//   }
  
}


</script>

@endsection