<!-- Dashboard Sidebar
	================================================== -->
<div class="dashboard-sidebar">
    <div class="dashboard-sidebar-inner" data-simplebar>
        <div class="dashboard-nav-container">
            <style>
                .skiptranslate{
                    display: contents;
                }
            </style>

        <?php
        if (auth()->user()->user_type == \App\User::USER_FREELANCER) {
            $upart = 'freelancer';
        }

        if (auth()->user()->user_type == \App\User::USER_EMPLOYER) {
            $upart = 'employer';
        }
        ?>



        <!-- Responsive Navigation Trigger -->
            <a href="#" class="dashboard-responsive-nav-trigger">
                    <span class="hamburger hamburger--collapse">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </span>
                <span class="trigger-title">Dashboard Navigation</span>
            </a>

            <!-- Navigation -->
            <div class="dashboard-nav">
                <div class="dashboard-nav-inner">

                    <ul data-submenu-title="Dashboard">
                        <!-- <li class="active"><a href="dashboard.html"><i class="icon-material-outline-dashboard"></i> </a></li> -->
                        <li><a href="{{route('groupmessenger')}}"><i
                                    class="icon-material-outline-dashboard"></i> Group Chat
                                <!-- <span class="nav-tag">2</span> --></a></li>
                        <li><a href="{{route('default.messenger')}}"><i
                                    class="icon-material-outline-question-answer"></i> Messages
                                <!-- <span class="nav-tag">2</span> --></a></li>
                        <li><a href="{{route('browse-mission')}}"><i
                                    class="icon-material-outline-star-border"></i> Browse Missions</a></li>
                        <li><a href="{{route('source')}}"><i
                                    class="icon-material-outline-star-border"></i> Source</a></li>
                        <li><a href="{{url('/'.$upart.'/saved')}}"><i class="icon-material-outline-rate-review"></i>
                                Saved</a></li>


                    </ul>

                    <ul data-submenu-title="Missions">
                    <!-- <li><a href="{{route('personal')}}"><i
                                        class="icon-material-outline-business-center"></i> Personal</a>
                                <ul>
                                <li><a href="dashboard-manage-jobs.html">Manage Jobs <span class="nav-tag">3</span></a></li>
                                <li><a href="dashboard-manage-candidates.html">Manage Candidates</a></li>
                                <li><a href="dashboard-post-a-job.html">Post a Job</a></li>
                            </ul>
                            </li> -->
                        @if(auth()->user()->user_type == \App\User::USER_EMPLOYER)
                            <li><a href="{{url('/employer/view_offer')}}"><i class="icon-material-outline-rate-review"></i>
                                    View-Offers</a></li>
                        @endif

                        @if(auth()->user()->user_type == \App\User::USER_FREELANCER)
                            <li><a href="{{route('freelancer.myjobs')}}"><i class="icon-material-outline-rate-review"></i>
                                    My Jobs</a></li>
                        @endif
                        @if(auth()->user()->user_type == \App\User::USER_EMPLOYER)
                            <li><a href="{{route('employer.myorders')}}"><i class="icon-material-outline-rate-review"></i>
                                    My Orders</a></li>
                            <li><a href="{{route('employer.mymissions')}}"><i class="icon-material-outline-rate-review"></i>
                                    My Missions</a></li>
                        @endif
                        <li><a href="{{url('/'.$upart.'/participated')}}"><i class="icon-material-outline-business-center"></i>
                                Participated</a>
                            <!-- <ul>
                        <li><a href="dashboard-manage-tasks.html">Manage Tasks <span class="nav-tag">2</span></a></li>
                        <li><a href="dashboard-manage-bidders.html">Manage Bidders</a></li>
                        <li><a href="dashboard-my-active-bids.html">My Active Bids <span class="nav-tag">4</span></a></li>
                        <li><a href="dashboard-post-a-task.html">Post a Task</a></li>
                    </ul>    -->
                        </li>
                    </ul>

                    <ul data-submenu-title="Funds">
                        <li><a href="{{url('/'.$upart.'/balance')}}"><i class="icon-material-outline-settings"></i> Balance</a></li>
                    <!--   <li><a href="{{url('/'.$upart.'/crowdfund')}}"><i class="icon-material-outline-local-atm"></i> Crowdfund</a></li> -->

                    </ul>

                    <ul data-submenu-title="Logout">
                        <li><a href="{{route('logout')}}"><i
                                    class="icon-material-outline-power-settings-new"></i> Logout</a></li>
                    </ul>

                    <ul data-submenu-title="Select Language">

                        <li><div id="google_translate_element"></div></li>

                        <style type="text/css">
                            .goog-logo-link {
                                display:none !important;
                            }

                            .goog-te-gadget {
                                color: transparent !important;
                            }

                            .goog-te-gadget .goog-te-combo {
                                border: 1px solid gray;
                                width: 90%;
                                margin: auto;
                                padding: 10px;
                            }
                        </style>
                    </ul>

                </div>
            </div>
            <!-- Navigation / End -->

        </div>
    </div>
</div>
<!-- Dashboard Sidebar / End -->



<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
    }
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
