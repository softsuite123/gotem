@extends('employer.layouts.master')
@section('content')
    <div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
        <div class="dashboard-container">

            @include('employer.partials.sidebar')

            <div class="col-xl-9 col-lg-8 content-left-offset">
                <div class="container">
                    <div class="row ">
                        <h3 class="headline">Mission Milestones</h3>
                    </div>
                    @if (Session::has('success'))
                        <div class="alert alert-primary text-center">
                            <p>{{ Session::get('success') }}</p>
                        </div>
                        @endif

                    <div id="milestones" class="row">
                        <div class="col-md-12">

                            @include('employer.front.partials.progressbar')

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-11 mt-5">
                            <table class="table ">
                                <tbody>
                                <tr class="table-primary">
                                    <td scope="col">#</td>
                                    <td scope="col">Mile Stone</td>
                                    <td scope="col">Fund Amount</td>
                                    <td scope="col">Evidence</td>
                                    <td class="text-center" scope="col">Actions</td>
                                </tr>
                                {{--                    //targetstep is to be made dynamic--}}
{{--{{dd($mission)}}--}}
                                @if(isset($milestones))
                                    @foreach($milestones as $milestone)
                                        @if($milestone->status==1)
                                            <tr class="milestone-table-row" targetstep="1">
                                                <td>{{$counter++}}</td>
                                                <td>{{$milestone->milestone_title}}</td>
                                                <td>$ {{$milestone->milestone_amount}}</td>
                                                <td class="row">
                                                    <a><i class="fas fa-eye"></i></a>
                                                    <a><i class="fas fa-file-download"></i></a>
                                                </td>
                                                <td>
                                                    <div class="ml-auto" style="width: fit-content ;display: flex; flex-direction: column">
                                                        <button type="button" class="btn btn-success table-btn" data-toggle="modal" data-target="#{{$milestone->id}}">Accept</button>
                                                        {{-- <button data-target="#myModal-accept" id="{{$milestone->id}}" data-toggle="modal" class="btn btn-success table-btn accept">Accept</button> --}}
                                                        <button data-target="#myModal-reject" id="{{$milestone->id}}" onclick="decline(this.id)" data-toggle="modal" class="btn btn-danger table-btn reject mt-2">Reject</button>
                                                    </div>

                                                </td>
                                            </tr>
                                            <div class="modal fade bd-example-modal-lg" id="{{$milestone->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                  <div class="modal-content back-acc-modal">
                                                    <div class="panel-body p-5">
                                                        @if (session()->has('status'))
                                                <div class="alert alert-success">
                                                    <p>{{ session('status') }}</p>
                                                </div>
                                            @endif
                                                        <form role="form" action="{{ route('stripe.balance') }}" method="post" class="stripe-payment"
                                                            data-cc-on-file="false" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                            id="stripe-payment">
                                                            @csrf
                                                            <div class="text-center mb-4">
                                                                <h1>Please fund your mission so that it can begin</h1>
                                                            </div>
                                                            <div class='form-row row'>
                                                                
                                                                <div class='col-xs-12 col-md-12 form-group required'>
                                                                    
                                                                    <label class='control-label'>Name on Card</label> <input class='form-control'
                                                                        size='4' type='text'>
                                                                </div>
                                                            </div>

                                                            <div class='form-row row'>
                                                                <div class='col-xs-12 col-md-12 form-group card required' style="border:0px" style="border:0px">
                                                                    <label class='control-label'>Card Number</label> <input autocomplete='off'
                                                                        class='form-control card-num' size='20' type='text'>
                                                                </div>
                                                            </div>

                                                            <div class='form-row row'>
                                                                <div class='col-xs-12 col-md-12 form-group required'>
                                                                    <input class='form-control' name="mile_id" type="hidden" value="{{$milestone->id}}" >
                                                                </div>
                                                            </div>

                                                            <div class='form-row row'>
                                                                <div class='col-xs-12 col-md-3 form-group cvc required'>
                                                                    <label class='control-label'>CVC</label>
                                                                    <input autocomplete='off' class='form-control card-cvc' placeholder='e.g 595'
                                                                        size='4' type='text'>
                                                                </div>
                                                                <div class='col-xs-12 col-md-3 form-group expiration required'>
                                                                    <label class='control-label'>Expiration Month</label> <input
                                                                        class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
                                                                </div>
                                                                <div class='col-xs-12 col-md-3 form-group expiration required'>
                                                                    <label class='control-label'>Expiration Year</label> <input
                                                                        class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
                                                                </div>
                                                                <div class='col-xs-12 col-md-3 form-group expiration required'>
                                                                    <input class='form-control card-expiry-year'name="amount" value="{{$milestone->milestone_amount}}" type="hidden" >
                                                                </div>
                                                            </div>

                                                            <!--<div class='form-row row'>-->
                                                            <!--    <div class='col-md-12 hide error form-group'>-->
                                                            <!--        <div class='alert-danger alert'>Fix the errors before you begin.</div>-->
                                                            <!--    </div>-->
                                                            <!--</div>-->

                                                            <div class="row">
                                                                <button class="btn btn-success btn-lg btn-block" type="submit">Pay</button>
                                                            </div>

                                                        </form>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                        @endif
                                    @endforeach
                                @endif
                                @if($offer->view_offer!=0)
                                    <tr class="milestone-table-row" targetstep="2">
                                        <td>{{$counter++}}</td>
                                        <td>{{$mission->mission_description}}</td>
                                        <td>$ {{$offer->offer_amount}}</td>
                                        <td class="row">
                                            <a><i class="fas fa-eye"></i></a>
                                            <a><i class="fas fa-file-download"></i></a>
                                        </td>
                                        <td>
                                            <div class="ml-auto" style="width: fit-content; display: flex; flex-direction: column">
                                                <button type="button" class="btn btn-success table-btn" data-toggle="modal" data-target="#{{$mission->id}}">Accept</button>
                                                {{-- <button  type="button" class="btn btn-success table-btn " onclick="accept_offer(0)" data-toggle="modal" data-target="#myModal-accept">
                                                    Accept
                                                </button> --}}
                                                {{--<button data-target="#myModal-accept" data-toggle="modal" class="btn btn-success table-btn accept">Accept</button>--}}
                                                <button data-target="#myModal-reject" data-toggle="modal" onclick="decline(0)" class="btn btn-danger table-btn reject mt-2">Reject</button>
                                            </div>
                                        </td>
                                    </tr>
                                    <div class="modal fade bd-example-modal-lg" id="{{$mission->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                          <div class="modal-content back-acc-modal">
                                            <div class="panel-body p-5">
                                                

                                                <form role="form" action="{{ route('stripe.balance') }}" method="post" class="stripe-payment"
                                                    data-cc-on-file="false" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                    id="stripe-payment">
                                                    @csrf
                                                    <div class="text-center mb-4">
                                                                <h1>Please fund your mission so that it can begin</h1>
                                                            </div>
                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 col-md-12 form-group required'>
                                                            <label class='control-label'>Name on Card</label> <input class='form-control'
                                                                size='4' type='text'>
                                                        </div>
                                                    </div>

                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 col-md-12 form-group required'>
                                                        </div>
                                                    </div>
                                                    <div class='col-xs-12 col-md-3 form-group expiration required' >
                                                    <input class='form-control card-expiry-year' value="{{$mission->estimated_amount}}" type="hidden" >
                                                </div>

                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 col-md-12 form-group card required' style="border:0px;">
                                                            <label class='control-label'>Card Number</label> <input autocomplete='off'
                                                                class='form-control card-num' size='20' type='text'>
                                                        </div>
                                                    </div>

                                                    <div class='form-row row'>
                                                        <div class='col-xs-12 col-md-3 form-group cvc required'>
                                                            <label class='control-label'>CVC</label>
                                                            <input autocomplete='off' class='form-control card-cvc' placeholder='e.g 595'
                                                                size='4' type='text'>
                                                        </div>
                                                        <div class='col-xs-12 col-md-3 form-group expiration required'>
                                                            <label class='control-label'>Expiration Month</label> <input
                                                                class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
                                                        </div>
                                                        <div class='col-xs-12 col-md-3 form-group expiration required'>
                                                            <label class='control-label'>Expiration Year</label> <input
                                                                class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
                                                        </div>
                                                    </div>

                                                    <!--<div class='form-row row'>-->
                                                    <!--    <div class='col-md-12 hide error form-group'>-->
                                                    <!--        <div class='alert-danger alert'>Fix the errors before you begin.</div>-->
                                                    <!--    </div>-->
                                                    <!--</div>-->

                                                    <div class="row">
                                                        <button class="btn btn-success btn-lg btn-block" type="submit">Pay</button>
                                                    </div>

                                                </form>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="myModal-accept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <!--Start Second Modal  -->

                    <center>
                        <h3><b>Let's Roll!</b></h3></center>
                    <br>
                    <center>
                        <h3>Begin by funding your balance so <br> that your Source can begin your <br> mission.</h3></center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xl-2 col-md-2"></div>
                        <div class="col-xl-8 col-md-8">
                            <input type="text" placeholder="Enter Full Name" id="name">
                        </div>
                        <div class="col-xl-2 col-md-2"></div>
                    </div>
                    <div class="row">
                        <div class="col-xl-2 col-md-2"></div>
                        <div class="col-xl-8 col-md-8">
                            <input type="email" placeholder="Enter your E-mail" id="email">
                        </div>
                        <div class="col-xl-2 col-md-2"></div>
                    </div>
                    <br>
                    {{-- <br> --}}
                    {{-- <center> --}}
                    {{-- <h4>Balance in Bitcoin: 0.67 BTC</h4></center> --}}
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xl-3"></div>
                        <div class="col-xl-6">
                            <center>

                                {{-- <a href="#myModal3" data-toggle="modal" class="button ripple-effect coolor">Checkout</a> --}}
                                <a onclick="checkout()" class="button ripple-effect checkout-btn coolor">Checkout1</a>

                            </center>
                        </div>
                        <div class="col-xl-3"></div>
                    </div>
                    <!-- End Second Modal -->
                </div>
            </div>
        </div>
    </div>

    <div id="myModal-reject" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <!-- Start third Modal -->

                    <center>
                        <h3><b>Offer Declined!</b></h3></center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <center>
                        <h3>Try Messaging your source to come <br> to terms with an arrangements that <br> works better for you.</h3></center>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="row">
                        <div class="col-xl-3"></div>
                        <div class="col-xl-6">
                            <center><a data-dismiss="modal" data-dismiss="modal" aria-hidden="true" class="button ripple-effect checkout-btn coolor" >Close</a></center>
                        </div>
                        <div class="col-xl-3"></div>
                    </div>
                </div>
                <!-- End third modal -->
            </div>
        </div>
    </div>
    <input id="offer_id_change" type="hidden" value="{{$offer->id}}">
@section('scripts')


<script type="text/javascript" src="https://js.stripe.com/v2/"></script>

<script type="text/javascript">
    $(function () {
        var $form = $(".stripe-payment");
        $('form.stripe-payment').bind('submit', function (e) {
            var $form = $(".stripe-payment"),
                inputVal = ['input[type=email]', 'input[type=password]',
                    'input[type=text]', 'input[type=file]',
                    'textarea'
                ].join(', '),
                $inputs = $form.find('.required').find(inputVal),
                $errorStatus = $form.find('div.error'),
                valid = true;
            $errorStatus.addClass('hide');

            $('.has-error').removeClass('has-error');
            $inputs.each(function (i, el) {
                var $input = $(el);
                if ($input.val() === '') {
                    $input.parent().addClass('has-error');
                    $errorStatus.removeClass('hide');
                    e.preventDefault();
                }
            });

            if (!$form.data('cc-on-file')) {
                e.preventDefault();
                Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.createToken({
                    number: $('.card-num').val(),
                    cvc: $('.card-cvc').val(),
                    exp_month: $('.card-expiry-month').val(),
                    exp_year: $('.card-expiry-year').val()
                }, stripeRes);
            }

        });

        function stripeRes(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                var token = response['id'];
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });

</script>




    <!-- Scripts
        ================================================== -->
    <script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
    {{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
    <script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
    <script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
    <script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
    <script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
    <script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
    <script src="{{asset('backstyling/js/snackbar.js')}}"></script>
    <script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
    <script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
    <script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
    <script src="{{asset('backstyling/js/slick.min.js')}}"></script>
    <script src="{{asset('backstyling/js/custom.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>



    <script>
        $(document).ready(function() {
            $("#example").DataTable({
                aaSorting: [],
                responsive: true,
                columnDefs: [
                    {
                        responsivePriority: 1,
                        targets: 0
                    },
                    {
                        responsivePriority: 2,
                        targets: -1
                    }
                ]
            });
            $(".dataTables_filter input")
                .attr("placeholder", "Search Forms...")
                .css({
                    width: "260px",
                    display: "inline-block"
                });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
    <script>
        // Snackbar for user status switcher
        $('#snackbar-user-status label').click(function() {

            Snackbar.show({
                text: 'Your status has been changed!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#383838'
            });
        });

        // jQuery(document).ready(function(){
        jQuery('.offer-modal').click(function(e){
            let offer_id = e.target.id;
            // alert(offer_id);
            e.preventDefault();
            $.ajaxSetup({
                // headers: {
                //     'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                // }

            });
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            jQuery.ajax({
                url: "{{ url('getoffer') }}",
                method: 'post',
                data: {_token: CSRF_TOKEN,offer_id: offer_id },
                success: function(result){
                    console.log(result);
                    $('#myModal').html(result);
                    // $('#modal').modal();
                }});
        });
        // });

        function checkout(){
            let name = $('#name').val();
            let email = $('#email').val();
            console.log('fvsrfzdcasx')
            if(name=="" || email==""){
                alert('Please input name and email');
                return fasle;
            }else{
                window.location.href = 'https://gotem.io/coingate';
            }
            {{--let name  = $('#name').val();--}}
            {{--let email = $('#email').val();--}}
            {{--let  vlaue = 0;--}}
            {{--if ({{$milestones}}){--}}
            {{--    value= {{$milestones->id}};--}}
            {{--}--}}
            {{--else {--}}
            {{--    value= {{$missions->id}};--}}
            {{--}--}}
            {{--if(name=="" || email==""){--}}
            {{--    alert('Please input name and email');--}}
            {{--    return fasle;--}}
            {{--}else{--}}
            {{--    --}}{{--var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');--}}
            {{--    --}}{{--var oid = $('#offer_id_change').val();--}}
            {{--    --}}{{--var chck = 1 ;--}}
            {{--    --}}{{--var request = $.ajax({--}}
            {{--    --}}{{--    url: "{{route('accept.offer')}}",--}}
            {{--    --}}{{--    method: "post",--}}
            {{--    --}}{{--    data: {_token: CSRF_TOKEN, offer:oid,chck:chck,mile_id:value},--}}
            {{--    --}}{{--    dataType: "html"--}}
            {{--    --}}{{--});--}}
            {{--    request.done(function( msg ) {--}}
            {{--        window.location.href = 'https://platform.gotem.io/coingate';--}}
            {{--    });--}}

            }



        function get_id(id){
            $('#offer_id_change').val(id);
        }

        function decline(value)
        {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var chck = 0 ;
            var oid = $('#offer_id_change').val();
            var request = $.ajax({
                url: "{{route('change.offer_status')}}",
                method: "post",
                data: {_token: CSRF_TOKEN, offer:oid,chck:chck,mile_id:value},
                dataType: "html"
            });
            request.done(function( msg ) {
                location.reload();
            });
        }

        function accept_offer(value){

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var oid = $('#offer_id_change').val();
            var chck = 1 ;
            var request = $.ajax({
                url: "{{route('cancel.offer')}}",
                method: "post",
                data: {_token: CSRF_TOKEN, offer:oid,chck:chck,mile_id:value},
                dataType: "html"
            });
            request.done(function( msg ) {
                //   $('#myModal1').open();
                //   location.reload();
            });
        }

    </script>
    @
@endsection
{{--    <script>--}}
{{--        var counter = 1;--}}
{{--        $('.milestone-table-row').each(function (){--}}

{{--            $('.bubbles').append(`--}}


{{--                <div class="icon-blue text-center" datastep="${counter}" ><p class="pt-3">${counter}</p></div>--}}
{{--                        <div class="horizontal" datastep="${counter}"></div>--}}

{{--            `);--}}

{{--            counter ++;--}}
{{--        });--}}
{{--        $('.bubbles').find('.horizontal').last().remove();--}}


{{--        $(document).ready(function(){--}}
{{--            $('.accept').click(function (){--}}
{{--                var parent = $(this).parent().parent();--}}
{{--                var datastep = parent.attr('targetstep');--}}
{{--                $(`div[datastep=${datastep}]`).addClass('bubble-active');--}}
{{--            });--}}
{{--            $('.reject').click(function (){--}}
{{--                var parent = $(this).parent().parent();--}}
{{--                var datastep = parent.attr('targetstep');--}}
{{--                $(`div[datastep=${datastep}]`).addClass('bubble-active');--}}
{{--            });--}}

{{--        });--}}


{{--    </script>--}}
@endsection
