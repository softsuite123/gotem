<style>
    /*@import "compass/css3";*/

    /*$activeColor: #EF6F00;*/
    /*$inactiveColor: LightGrey;*/

    /*$separationMargin: 7em;*/
    /*$bubbleRadius: 1em;*/
    /*$lineWidth: 2em;*/
    /*$lineLength: $separationMargin * 2.0;*/
    /*$bubbleHeight: $bubbleRadius * 2.0;*/


    /*form styles*/
    #step-list {
        text-align: center;
        position: relative;
    }

    /*progressbar*/
    #progressbar {
        width: 90%;
        height: 100px;
        margin-bottom: 30px;
        overflow: hidden;
        /*CSS counters to number the steps*/
        counter-reset: step;
        color: #fff;
    }
    #progressbar li {
        list-style-type: none;
        color: #EF6F00;;
        text-transform: uppercase;
        font-size: 15px;
        width: 33.33%;
        float: left;
        position: relative;
    }
    /* Circles */
    #progressbar li:before {
        content: counter(step);
        counter-increment: step;
        width: 15%;
        line-height: 50px;
        display: block;
        font-size: 16px;
        color: LightGrey;
        background: white;
        border: 2px solid LightGrey;
        border-radius: 25px;
        top: 46px;
        position: relative;
        margin: 0 auto 5px auto;
    }
    /*progressbar connectors*/
    #progressbar li:after {
        top: 70px;
        position: relative;
        content: '';
        width: 100%;
        height: 5px;
        background: LightGrey;
        position: absolute;
        left: -50%;
        z-index: -1; /*put it behind the numbers*/
    }
    /*connector not needed before the first step*/
    #progressbar li:first-child:after {
        content: none;
    }
    /*marking active/completed steps green*/
    /*The number of the step and the connector before it   green*/
    #progressbar li.active:before,  #progressbar li.active:after{
        background: #3776EA;
        border: none;
        color: white;
    }
    }
</style>
<div id="step-list">
    <!-- progressbar -->
    <ul id="progressbar">
        <li class="active"></li>
        <li class=""></li>
        <li class=""></li>
    </ul>
</div>

<script>

</script>
