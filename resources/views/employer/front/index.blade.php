@extends('employer.layouts.master')
@section('content')
<div id="wrapper">


@include('employer.partials.header')



<!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')

        <div class="col-xl-9 col-lg-8 content-left-offset">

    <div class="container">
        <div class="row tabs">
            <div class=" balance-tab tab-margin">
                <p>Current Balance</p>
                <h6 class="text-center">$ 253</h6>
            </div>
            <div class=" balance-tab tab-margin">
                <p>Pending Balance</p>
                <h6 class="text-center">$ 400</h6>
            </div>
            <div class=" balance-tab">
                <p>Total Earnings</p>
                <h6 class="text-center">$ 4000</h6>
            </div>
        </div>
        <div class="row tabs methods">
            <div class="col-md-12 col-xl-2"><p class="pt-2 withdraw">Withdraw</p></div>
            <div class="col-md-12 col-xl-10">
                <button class="withdraw-method">International Wire</button>
                <button class="withdraw-method">Crypto Wallet</button>
                <button class="withdraw-method">Add Bank Account</button>
            </div>
        </div>
        <div class="row form-panel mb-5">
            <div class="container">
                <div class="row">
                    <div class="credit-card">
                        <h4>Stripe<span>Pay by Credit Card</span></h4>
                    </div>
                    <div class="credit-img">
                        <ul class="row">
                            <li><a><i class="fab fa-cc-visa"></i></a></li>
                            <li><a><i class="fab fa-cc-stripe"></i></a></li>
                            <li><a><i class="fab fa-cc-paypal"></i></a></li>
                            <li><a><i class="fab fa-cc-jcb"></i></a></li>
                            <li><a><i class="fab fa-cc-apple-apy"></i></a></li>
                            <li><a><i class="fab fa-cc-amazon-apy"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 status">
                        <div class="num-icon mt-5 text-center">
                            <p>1</p>
                        </div>
                        <div class="vl"></div>
                        <div class="second-icon mt-5 text-center">
                            <p style="margin-top: -48px">2</p>
                        </div>
                    </div>
                    <div class="col-md-11">
                        <form id="bank-form" class="row">
                            <div class="col-md-12"><h4 class="pt-3">Bank Account</h4></div>
                            <div class="form-group">
                                <label for="">Country of Bank <span>*</span></label>
                                <input type="text" name="country" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">Bank Account Number/IBAN <span>*</span></label>
                                <input type="number" name="account_number" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">Swift / BIC <span>*</span></label>
                                <input type="number" name="swift/bic" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">Name of your Bank Account <span>*</span></label>
                                <input type="text" name="account_name" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">Bank Name <span>*</span></label>
                                <input type="text" name="bank_name" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">Bank Street Address <span>*</span></label>
                                <input type="text" name="bank_address" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">Bank Post Code <span>*</span></label>
                                <input type="number" name="bank_post_code" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                            <div class="form-group">
                                <label for="">City <span>*</span></label>
                                <input type="text" name="bank_city" class="form-control inp" id="" aria-describedby="" placeholder="" required="true">
                            </div>
                        </form>
                        <form class="row">
                            <div class="col-md-12"><h4 class="pt-5">Personal Information</h4></div>
                            <div class="form-group">
                                <label for="">First Name <span>*</span></label>
                                <input type="text" onclick="validate()" name="first_name" class="form-control inp" id="" aria-describedby="" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="">LastName <span>*</span></label>
                                <input type="text" name="last_name" class="form-control inp" id="" aria-describedby="" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="">Residential Address <span>*</span></label>
                                <input type="text" name="address" class="form-control inp" id="" aria-describedby="" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="">Permanent Address <span>*</span></label>
                                <input type="text" name="permanent_address" class="form-control inp" id="" aria-describedby="" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="">Post Code <span>*</span></label>
                                <input type="text" name="post_code" class="form-control inp" id="" aria-describedby="" placeholder="" required>
                            </div>
                            <div class="form-group">
                                <label for="">City <span>*</span></label>
                                <input type="text" name="city" class="form-control inp" id="" aria-describedby="" placeholder="" required>
                            </div>
                            <button type="submit" class="withdraw-method add-btn">Add Bank Account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
</div>
    <script>
        function validate(){
            var validation =validateform('#bank-form');

            console.log(validation);
            if (validation.errors < 1)
            {
                console.log('siruvrbkjrs');
                $('.vl').css('opacity', '1');
                $('.second-icon').css('opacity', '1');

            }
            else{
                $('.vl').css('opacity', '0.5');
                $('.second-icon').css('opacity', '0.5');
            }
        }
    </script>
@endsection
