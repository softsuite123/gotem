@extends('employer.layouts.master')
@section('content')
    <style>
        .icon-blue{
            width: 80%;
            height: 50px;
        }
        .vertical {
            height: 200px;
            background: #737373;
            width: 10%;
            margin-left: 23px;
            margin-top: -25%;
        }
        .bubbles-mission .icon-blue p{
            padding-top: 12px;
        }
        @media only screen and (max-width: 1500px) {
            .vertical {
                margin-left: 16px;
                margin-top: -35%;
            }

        }
        @media only screen and (max-width: 1200px) {
            .vertical {
                margin-left: 12px;
                margin-top: -50%;
            }

        }
        @media only screen and (max-width: 990px) {
            .icon-blue{
                width: 200%;
            }
            .vertical {
                height: 275px;
                background: #737373;
                width: 20%;
                margin-left: 25px;
                margin-top: -81%;
            }

        }
    </style>

    <div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
        <div class="dashboard-container">

            @include('employer.partials.sidebar')

            <div class="col-xl-9 col-lg-8 content-left-offset">
    <div class="container">
        <div class="row">
            <div class="col-md-1">
                <div class="bubbles-mission mt-5"></div>
            </div>
            <div class="col-md-11">
                <table class="table ">
                    <tbody>
                    {{--                    //targetstep is to be made dynamic--}}
                    <tr class="milestone-table-row" targetstep="1">
                        <td style="width: 90%">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada gravida magna, quis fringilla felis consequat ac. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec gravida, mauris condimentum imperdiet ullamcorper, neque felis vulputate dolor, in mollis dui mi viverra quam. Morbi lorem lectus, bibendum vitae ipsum eget, tempor rutrum nisi. Nam venenatis eu neque id eleifend. Sed vel lacus venenatis, tempor risus eget, posuere mi. Integer ac pellentesque dui.</p>
                            <div class="row">
                                <button class="btn btn-secondary table-btn ml-3">View</button>
                                <button class="btn btn-secondary table-btn ml-3">Download</button>
                                <button class="btn btn-success table-btn accept ml-3">Accept</button>
                                <button class="btn btn-danger table-btn reject ml-3">Reject</button>
                            </div>
                        </td>
                        <td style="font-weight: 500; font-size: 22px">$ 400</td>
                    </tr>
                    <tr class="milestone-table-row" targetstep="2">
                        <td style="width: 90%">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada gravida magna, quis fringilla felis consequat ac. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec gravida, mauris condimentum imperdiet ullamcorper, neque felis vulputate dolor, in mollis dui mi viverra quam. Morbi lorem lectus, bibendum vitae ipsum eget, tempor rutrum nisi. Nam venenatis eu neque id eleifend. Sed vel lacus venenatis, tempor risus eget, posuere mi. Integer ac pellentesque dui.</p>
                            <div class="row">
                                <button class="btn btn-secondary table-btn ml-3">View</button>
                                <button class="btn btn-secondary table-btn ml-3">Download</button>
                                <button class="btn btn-success table-btn accept ml-3">Accept</button>
                                <button class="btn btn-danger table-btn reject ml-3">Reject</button>
                            </div>
                        </td>
                        <td style="font-weight: 500; font-size: 22px">$ 400</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>

    <script>
        var counter = 1;
        $('.milestone-table-row').each(function (){

            $('.bubbles-mission').append(`


                <div class="icon-blue text-center" datastep="${counter}" ><p>${counter}</p></div>
                        <div class="vertical" datastep="${counter}"></div>

            `);

            counter ++;
        });
        $('.bubbles-mission').find('.vertical').last().remove();


        $(document).ready(function(){
            $('.accept').click(function (){
                var parent = $(this).parent().parent().parent();
                var datastep = parent.attr('targetstep');
                $(`div[datastep=${datastep}]`).addClass('bubble-active');
            });
            $('.reject').click(function (){
                var parent = $(this).parent().parent().parent();
                var datastep = parent.attr('targetstep');
                $(`div[datastep=${datastep}]`).addClass('bubble-active');
            });

        });


    </script>
@endsection
