@extends('employer.layouts.master')
@section('content')
    <style>
        @media only screen and (max-width: 441px){
            .btn-danger, .btn-success{
                width: auto;
                padding: 5px;
            }
        }
    </style>
    <div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
        <div class="dashboard-container">

            @include('employer.partials.sidebar')

            <div class="col-xl-9 col-lg-8 content-left-offset">
    <div class="container">
        <div class="withdraw text-center p-5">
            <h2>Withdraw Funds</h2>
            <p class="pt-3">Please review your withdrawal details</p>
            <div class="withdraw-details-box text-center p-5">
                <table class="table table-responsive">
                    <div class="row table-border">
                        <p>Transfer To</p>
                        <p class="table-left">Bank Account</p>
                    </div>
                    <div class="row table-border">
                        <p>Account</p>
                        <p class="table-left">email@example.com</p>
                    </div>
                    <div class="row table-border">
                        <p>Amount</p>
                        <p class="table-left">$ 200</p>
                    </div>
{{--                    <tbody>--}}
{{--                    <tr>--}}
{{--                        <td>Transfer To</td>--}}
{{--                        <td class="table-left">Bank Account</td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td>Account</td>--}}
{{--                        <td class="table-left">email@example.com</td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td>Amount</td>--}}
{{--                        <td class="table-left">$ 200</td>--}}
{{--                    </tr>--}}
{{--                    </tbody>--}}
                </table>
                <div class="row">
                    <div class="ml-auto">
                        <button class="btn btn-danger">Cancel</button>
                        <button type="button" class="btn btn-success">Confirm & Withdraw</button>
                    </div>
                </div>
            </div>
            <div class="pt-5 bottom-text">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut efficitur risus. Sed vel lorem commodo, auctor ipsum suscipit, interdum dolor. Fusce feugiat nibh in laoreet facilisis. Sed facilisis vel nunc non porttitor. Vivamus sed pharetra risus. Quisque sit amet magna ac neque sodales bibendum nec sit amet libero. Mauris lacinia lectus vitae nulla semper, sed vestibulum erat aliquam. Praesent sed arcu quis turpis pretium aliquam. Morbi eleifend mauris et aliquet tincidunt. Pellentesque bibendum rhoncus velit rhoncus gravida. Sed consequat metus ut consequat sagittis.</p>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
@endsection
