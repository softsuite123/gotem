@extends('employer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('employer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')


            <!-- Dashboard Content
    ================================================== -->
    <div class="dashboard-content-container" data-simplebar >
        <!-- Titlebar
================================================== -->
<div class="single-page-header freelancer-header" style="box-shadow: 0px 2px 8px 0px gray;" data-background-image="../assets/images/single-freelancer.jpg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="single-page-header-inner">
                    <div class="left-side">
                        <div class="header-image freelancer-avatar"><img src="{{asset('storage/'.str_replace('public','',$user->profile->avatar))}}" alt=""></div>
                        <div class="header-details">
                            <h3>{{$user->profile->name}} <span>iOS Expert + Node Dev</span></h3>
                            <ul>
                                <li><div class="star-rating" data-rating="5.0"></div></li>
                                <li><img class="flag" src="../assets/images/flags/de.svg" alt=""> {{$user->profile->country->name}}</li>
                                @if ($user->is_verified)
                                    <li><div class="verified-badge-with-title">Verified</div></li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
    <div class="row">

        <!-- Content -->
        <div class="col-xl-8 col-lg-8 content-right-offset">

            <!-- Page Content -->
            <div class="single-page-section">
                <h3 class="margin-bottom-25">About Me</h3>
                <p>{{$user->profile->about}}.</p>
            </div>

            <!-- Boxed List -->
            <div class="boxed-list margin-bottom-60">
                <div class="boxed-list-headline">
                    <h3><i class="icon-material-outline-thumb-up"></i> Work History and Feedback</h3>
                </div>
                <ul class="boxed-list-ul">
                    <li>
                        <div class="boxed-list-item">
                            <!-- Content -->
                            <div class="item-content">
                                <h4>Web, Database and API Developer <span>Rated as Freelancer</span></h4>
                                <div class="item-details margin-top-10">
                                    <div class="star-rating" data-rating="5.0"></div>
                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i> August 2019</div>
                                </div>
                                <div class="item-description">
                                    <p>Excellent programmer - fully carried out my project in a very professional manner. </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="boxed-list-item">
                            <!-- Content -->
                            <div class="item-content">
                                <h4>WordPress Theme Installation <span>Rated as Freelancer</span></h4>
                                <div class="item-details margin-top-10">
                                    <div class="star-rating" data-rating="5.0"></div>
                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i> June 2019</div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="boxed-list-item">
                            <!-- Content -->
                            <div class="item-content">
                                <h4>Fix Python Selenium Code <span>Rated as Employer</span></h4>
                                <div class="item-details margin-top-10">
                                    <div class="star-rating" data-rating="5.0"></div>
                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i> May 2019</div>
                                </div>
                                <div class="item-description">
                                    <p>I was extremely impressed with the quality of work AND how quickly he got it done. He then offered to help with another side part of the project that we didn't even think about originally.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="boxed-list-item">
                            <!-- Content -->
                            <div class="item-content">
                                <h4>PHP Core Website Fixes <span>Rated as Freelancer</span></h4>
                                <div class="item-details margin-top-10">
                                    <div class="star-rating" data-rating="5.0"></div>
                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i> May 2019</div>
                                </div>
                                <div class="item-description">
                                    <p>Awesome work, definitely will rehire. Poject was completed not only with the requirements, but on time, within our small budget.</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <!-- Pagination -->
                <div class="clearfix"></div>
                <div class="pagination-container margin-top-40 margin-bottom-10">
                    <nav class="pagination">
                        <ul>
                            <li><a href="#" class="ripple-effect current-page">1</a></li>
                            <li><a href="#" class="ripple-effect">2</a></li>
                            <li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="clearfix"></div>
                <!-- Pagination / End -->

            </div>
            <!-- Boxed List / End -->

            <!-- Boxed List -->
            <div class="boxed-list margin-bottom-60">
                <div class="boxed-list-headline">
                    <h3><i class="icon-material-outline-business"></i> Employment History</h3>
                </div>
                <ul class="boxed-list-ul">
                    @foreach ($user->jobs as $job)
                        <li>
                            <div class="boxed-list-item">
                                <!-- Avatar -->
                                <div class="item-image">
                                    <img src="../assets/images/browse-companies-03.png" alt="">
                                </div>

                                <!-- Content -->
                                <div class="item-content">
                                    <h4>{{$job->title}}</h4>
                                    <div class="item-details margin-top-7">
                                        <div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> {{$job->company}}</a></div>
                                        <div class="detail-item"><i class="icon-material-outline-date-range"></i> May 2019 - Present</div>
                                    </div>
                                    <div class="item-description">
                                        <p>{{$job->description}}.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    {{-- <li>
                        <div class="boxed-list-item">
                            <!-- Avatar -->
                            <div class="item-image">
                                <img src="../assets/images/browse-companies-04.png" alt="">
                            </div>

                            <!-- Content -->
                            <div class="item-content">
                                <h4><a href="#">Lead UX/UI Designer</a></h4>
                                <div class="item-details margin-top-7">
                                    <div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> Acorta</a></div>
                                    <div class="detail-item"><i class="icon-material-outline-date-range"></i> April 2014 - May 2019</div>
                                </div>
                                <div class="item-description">
                                    <p>I designed and implemented 10+ custom web-based CRMs, workflow systems, payment solutions and mobile apps.</p>
                                </div>
                            </div>
                        </div>
                    </li> --}}
                </ul>
            </div>
            <!-- Boxed List / End -->

        </div>


        <!-- Sidebar -->
        <div class="col-xl-4 col-lg-4">
            <div class="sidebar-container">

                <!-- Profile Overview -->
                <div class="profile-overview">
                    <div class="overview-item"><strong>${{$user->profile->hourly_rate}}</strong><span>Hourly Rate</span></div>
                    <div class="overview-item"><strong>53</strong><span>Jobs Done</span></div>
                    <div class="overview-item"><strong>22</strong><span>Rehired</span></div>
                </div>

                <!-- Button -->
                <a href="#small-dialog" class="apply-now-button popup-with-zoom-anim margin-bottom-50">Make an Offer <i class="icon-material-outline-arrow-right-alt"></i></a>

                <!-- Freelancer Indicators -->
                <div class="sidebar-widget">
                    <div class="freelancer-indicators">

                        <!-- Indicator -->
                        <div class="indicator">
                            <strong>88%</strong>
                            <div class="indicator-bar" data-indicator-percentage="88"><span></span></div>
                            <span>Job Success</span>
                        </div>

                        <!-- Indicator -->
                        <div class="indicator">
                            <strong>100%</strong>
                            <div class="indicator-bar" data-indicator-percentage="100"><span></span></div>
                            <span>Recommendation</span>
                        </div>

                        <!-- Indicator -->
                        <div class="indicator">
                            <strong>90%</strong>
                            <div class="indicator-bar" data-indicator-percentage="90"><span></span></div>
                            <span>On Time</span>
                        </div>

                        <!-- Indicator -->
                        <div class="indicator">
                            <strong>80%</strong>
                            <div class="indicator-bar" data-indicator-percentage="80"><span></span></div>
                            <span>On Budget</span>
                        </div>
                    </div>
                </div>

                <!-- Widget -->
                <div class="sidebar-widget">
                    <h3>Social Profiles</h3>
                    <div class="freelancer-socials margin-top-25">
                        <ul>
                            <li><a href="#" title="Dribbble" data-tippy-placement="top"><i class="icon-brand-dribbble"></i></a></li>
                            <li><a href="#" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                            <li><a href="#" title="Behance" data-tippy-placement="top"><i class="icon-brand-behance"></i></a></li>
                            <li><a href="#" title="GitHub" data-tippy-placement="top"><i class="icon-brand-github"></i></a></li>

                        </ul>
                    </div>
                </div>

                <!-- Widget -->
                <div class="sidebar-widget">
                    <h3>Skills</h3>
                    <div class="task-tags">
                        <span>iOS</span>
                        <span>Android</span>
                        <span>mobile apps</span>
                        <span>design</span>
                        <span>Python</span>
                        <span>Flask</span>
                        <span>PHP</span>
                        <span>WordPress</span>
                    </div>
                </div>

                <!-- Widget -->
                <div class="sidebar-widget">
                    <h3>Attachments</h3>
                    <div class="attachments-container">
                        <a href="#" class="attachment-box ripple-effect"><span>Cover Letter</span><i>PDF</i></a>
                        <a href="#" class="attachment-box ripple-effect"><span>Contract</span><i>DOCX</i></a>
                    </div>
                </div>

                <!-- Sidebar Widget -->
                <div class="sidebar-widget">
                    <h3>Bookmark or Share</h3>

                    <!-- Bookmark Button -->
                    <button class="bookmark-button margin-bottom-25">
                        <span class="bookmark-icon"></span>
                        <span class="bookmark-text">Bookmark</span>
                        <span class="bookmarked-text">Bookmarked</span>
                    </button>

                    <!-- Copy URL -->
                    <div class="copy-url">
                        <input id="copy-url" type="text" value="" class="with-border">
                        <button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
                    </div>

                    <!-- Share Buttons -->
                    <div class="share-buttons margin-top-25">
                        <div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
                        <div class="share-buttons-content">
                            <span>Interesting? <strong>Share It!</strong></span>
                            <ul class="share-buttons-icons">
                                <li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
                                <li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
                                <li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
                                <li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>


<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->
    </div>
    <!-- Dashboard Content / End -->


    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

@endsection