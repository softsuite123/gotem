{{-- @php
$authId = auth()->id();
@endphp
@if ($messages)
    @foreach ($messages as $key => $message)
        <div class="row message-row">
            <p title="{{date('d-m-Y h:i A' ,strtotime($message->created_at))}}"
                @if ($message->sender_id === $authId)
                    class="sent"
                @else
                    class="received"
                @endif>
                {{$message->message}}
            </p>
            @if ($message->sender_id === $authId)
                <i class="fa fa-ellipsis-h fa-2x pull-right" aria-hidden="true">
                    <div class="delete" data-id="{{$message->id}}">Delete</div>
                </i>
            @else
                <i class="fa fa-ellipsis-h fa-2x pull-left" aria-hidden="true">
                    <div class="delete" data-id="{{$message->id}}">Delete</div>
                </i>
            @endif
        </div>
    @endforeach
@endif
 --}}


 @php
$authId =  auth()->user()->id;
@endphp
@if ($messages)

    @foreach ($messages as $key => $message)
        <!-- <div class="chat-date"><span>Monday 16 May</span></div> -->
        @if ($message->sender_id == $authId)
        <div  class="ca-send">
            <div class="ca-send__msg-group">
                <div class="ca-send__msgwrapper">
                    <div class="ca-send__msg">{{$message->message}}</div>
                </div>
                <div class="metadata">
                    <span class="time">{{date('d-m-Y h:i A' ,strtotime($message->created_at))}}</span>
                </div>
            </div>

            <div class="user-avatar user-avatar-sm user-avatar-rounded online">
                <img src="{{ url('/public/Call') }}/assets/images/user/250/02.jpg" alt="">
            </div>
        </div>
        @else

        <div class="ca-received">
            <div class="user-avatar user-avatar-sm user-avatar-rounded online">
                <img src="{{ url('/public/Call') }}/assets/images/user/250/01.jpg" alt="">
            </div>
            <div class="ca-received__msg-group">
                <div class="ca-received__msgwrapper">
                    <div class="ca-received__msg">{{$message->message}}</div>
                </div>
                <div class="metadata">
                    <span class="time">{{date('d-m-Y h:i A' ,strtotime($message->created_at))}}</span>
                </div>
            </div>
        </div>

        @endif
        @endforeach
@endif


