@extends('admin.layouts.master')

@section('content')
<!-- Wrapper -->

<div id="wrapper">


    @include('admin.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('admin.partials.sidebar')
         <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >
            
          <div class="dashboard-content-container" data-simplebar>
                <div class="dashboard-content-inner">

                    <!-- Row -->
                    <div class="row">

                        <!-- Dashboard Box -->
                        <div class="col-xl-12">
                                  <div class="dashboard-box">
                        <div class="headline">
                            <h3><i class="icon-material-baseline-notifications-none"></i> Edit User</h3>
                        
                        </div>
                        <div class="content"><br>
                            <div class="container">
                                @foreach($detail as $d)
                            <form method="post" action="{{url('/admin/update_user')}}"  enctype="multipart/form-data">
                                @csrf
                                <div class="row"> 
                                    <div class="col-md-6">
                                        <label>Name</label>
                                        <input type="hidden" name="id" value="{{$d->id}}">
                                         <input type="text" value="{{$d->name}}" required="" name="name" class="form__input" placeholder="Enter Name" />
                                    </div>
                                    <div class="col-md-6">
                                        <label>Email</label>
                                         <input type="email" value="{{$d->email}}" readonly required="" name="email" class="form__input" placeholder="Enter Your Email" />
                                    </div>
                                </div><br>

                                <div class="row"> 
                                    <div class="col-md-6">
                                    <label>Type</label>
                                            <select class="form__input" name="usertype" required="">
                                    <option value="">Choose User Type</option>
                                    @if($d->user_type==1)
                                    <option value="1" selected="">User</option>
                                    <option value="2" >Source</option>
                                    @endif
                                     @if($d->user_type==2)
                                    <option value="1">User</option>
                                    <option value="2" selected="">Source</option>
                                    @endif
                                    
                                    
                                </select>
                                    </div>
                                    <div class="col-md-6">
                                    
                                                    <label>Country</label>
                                               

                                                <select class="selectpicker" name="country" id="country" required="">
                                                    <option selected disabled value="">Select One</option>
                                                    @foreach ($country as $country)
                                                    <option @if($d->cntryId==$country->id) selected @endif  value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                        
                                    </div>
                                </div><br>

                                <div class="row"> 
                                     <div class="col-md-6">
                                              
                                                    <input type="hidden" id="state_id" value="{{$d->state}}">
                                                    <input type="hidden" id="city_id" value="{{$d->city_id}}">
                                                   
                                                
                                                    <label>State</label>
                                                <select name="state" id="state" class="state_select form-control"
                                                    autocomplete="address-level1" required="" data-placeholder="Select an option…">
                                                    <option selected disabled value="">Select an option…</option>
                                                </select>
                                            </div>

                       <div class="col-md-6">
         <label>City</label>
                        
                

                    <select name="city" required="" id="city" class="city_select form-control"
                        autocomplete="address-level1" data-placeholder="Select an option…">
                        <option selected disabled value="">Select an option…</option>
                    </select>
    </div>
                                               

                                            
                                </div>
                      

                                    <div class="row"> 
                                    <div class=" col-md-6"><br>
                                            <label>Hourly Rate</label>

                                        <input type="number" value="{{$d->hourly_rate}}" name="rate" class="form__input" placeholder="Hourly Rate">  
                                            </div>
                                    <div class="col-md-6"><br>
                                       
                                    </div>
                                </div><br>
                                <div class="row"> 
                                    <div class="col-md-6">
                                         <div class="uploadButton margin-top-30">
                                        <h5 class="uploadButton-file-name">Upload <small> (if You Need to Update the Image) </small></h5>
                                        <span style=" margin-top: 14px;"><input type="hidden" name="share_with_public"
                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;</span>
                                        <div class="col-xl-12">
                                            <center>
                                                <input class="uploadButton-input" type="file"
                                                    accept="image/*, application/pdf" id="upload" onclick="remove()" name="avatar"/>
                                                <label class="uploadButton-button ripple-effect" for="upload"><i
                                                        class="icon-feather-upload" style="font-size: 25px;"></i></label>
                                            </center>
                                        </div>

                                    </div>
                                    </div>
                                     <div class="col-md-6">
                                        <label>Image</label><br>
                                        <input type="hidden" name="pre_path" value="{{$d->avatar}}">
                                       <img src="{{url($d->avatar)}}" class="image-thumbnail img-responsive"  width="250">

                                    </div>
                                   
                                </div><br>
                                <div class="row"> 
                                    <div class="col-md-6"><br>
                                      <button class="btn btn-info" >Update User</button>  
                                     
                                    </div>
                                    <div class="col-md-6">
                                           
                                    </div>
                                </div><br><br>
                                <input type="hidden" name="rem_img" value="1" id="rem_img">
                            </form>

                            @endforeach
                         </div>
                          
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
         
        </div>
    </div>
</div>
</div>
</div>


      


      
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!-- 
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!-- 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>
<script type="">
    function load_state(id)
    {
     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var request = $.ajax({
          url: "{{url('admin/load_states')}}",
            method: "get",
          data: {_token: CSRF_TOKEN,id:id },
          dataType: "html"
        });
         request.done(function( msg ) {        
        // alert(msg);
        $('#state').html(msg);
        });
      }



      function load_city(id)
    {
     var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var request = $.ajax({
          url: "{{url('admin/load_city')}}",
            method: "get",
          data: {_token: CSRF_TOKEN,id:id },
          dataType: "html"
        });
         request.done(function( msg ) {        
        // alert(msg);
        $('#city').html(msg);
        });
      }
    
</script>
<script>
    $('#country').change(function () {
        var cid = $(this).val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option value="">Select State</option>');
                        $.each(res, function (key, value) {
                            $("#state").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
    $('#state').change(function () {
        var sid = $(this).val();
        if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option value="">Select City</option>');
                        $.each(res, function (key, value) {
                            $("#city").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
    
    $('document').ready(function () {
         var cid = $('#country').val();
         var sid = $('#state_id').val();
          var cityid = $('#city_id').val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option value="">Select State</option>');
                        var ch = "";
                        $.each(res, function (id, value) {
                            if(sid==id){
                                ch = "selected";
                            }
                            else{
                                var ch = "";
                            }
                            $("#state").append('<option '+ch+' value="' + id + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }

            
             if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {                
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option value="">Select City</option>');
                        var c = "";
                        $.each(res, function (key, value) {
                            if(cityid==key) {
                                c = "selected";
                            }else{
                                var c = "";
                            }
                            $("#city").append('<option '+c+' value="' + key + '">' + value +
                                '</option>');

                        });
                    }
                }

            });
        }



        
    });
    function remove()
    {
        $('#rem_img').val("0");
    }
</script>
@endsection