@extends('admin.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">

    @include('admin.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('admin.partials.sidebar')
         <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >
            
          <div class="dashboard-content-container" data-simplebar>
                <div class="dashboard-content-inner">
    <div class="row">
                @foreach($mission_detail as $d)
                <form action="{{url('/admin/update_mission')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                <!-- Dashboard Box -->
                <div class="col-xl-12">
                    <div class="dashboard-box margin-top-0">

                        <!-- Headline -->
                        <div class="headline">
                            <h3><i class="icon-feather-upload"></i>Edit Missions</h3>
                        </div>
                         <input type="hidden" name="id" value="{{$d->id}}" id="mission_id">
                        <div class="content with-padding padding-bottom-10">
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="submit-field">
                                   <h5>Select User</h5>
                                    <select title="Select User" name="user" class="state_select form-control "  >
                                       <option value=""  disabled>Choose User</option>
                                        @foreach($users as $u)
                                        <option @if($d->user_id==$u->id) selected @endif value="{{$u->id}}">{{$u->name}}</option>
                                        @endforeach
                                    </select><br>
                                </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Mission Title</h5>
                                        <input type="text" value="{{$d->title}}" class="with-border" name="title">
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Mission Type</h5>
                                        <select class="state_select form-control" data-size="7" title="Select Mission Type" name="mission_type">
                                            <option value=""  disabled>Choose Mission Type</option> 
                                            @if($d->mission_type=='In-person Investigation')
                                           <option selected="">In-person Investigation</option>
                                             <option>Cyber Investigation</option>
                                            @endif
                                              @if($d->mission_type=='Cyber Investigation')
                                               <option selected>Cyber Investigation</option>
                                            <option>In-person Investigation</option>
                                            @endif
                                           
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Mission Category</h5>
                                        <select class="state_select form-control" data-size="7"
                                            title="Select Mission Category" name="mission_company">
                                             <option value=""  disabled>Choose Mission Cateogory</option>
                                            @foreach ($mission_com as $company)
                                                <option @if($d->mission_company_id==$company->id) selected @endif value="{{$company->id}}">{{$company->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            <input type="hidden" name="latitude" id="latitude">           
                            <input type="hidden" name="longitude" id="longitude">
                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Location <span style="float: right;"><input type="checkbox" @if($d->is_remote==1) checked @endif name="is_remote"
                                                    style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Remote
                                                Mission</span></h5>
                                        <input id="pac-input" value="{{$d->address}}" name="address" class="with-border controls" type="text" placeholder="Search Box">
    <div id="map" style="display:none;"></div>
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                    <div class="submit-field">
                                        <h5>Estimated Budget</h5>
                                        <input type="text" value="{{$d->estimated_budget}}" class="with-border" name="estimated_budget">
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                    <div class="submit-field">

                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5 >Deadline <span style="float: right;"><input type="checkbox" @if($d->is_urgent==1) checked @endif name="is_urgent"
                                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Urgent</span>
                                                    </h5>
                                                    <input type="date" value="{{$d->deadline}}" name="deadline">
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Mission Privacy</h5>
                                                    <select class="state_select form-control" data-size="7"
                                                        title="Mission Privacy" name="mission_privacy">
                                                        @if($d->mission_privacy=='Private')
                                                        <option selected>Private</option>
                                                        <option>Public</option>
                                                        @endif
                                                        @if($d->mission_privacy=='Public')
                                                        <option selected>Public</option> 
                                                       <option>Private</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Mission General Description</h5>
                                        <textarea cols="30"  rows="5" class="with-border" name="mission_description">{{$d->mission_description}}</textarea>

                                    </div>
                                </div>

                                <div class="col-xl-12">
                                    <div class="submit-field">
                                        <h5>Mission Objectives</h5>
                                        <textarea cols="30" rows="5" class="with-border" name="mission_objective">{{$d->mission_objective}}</textarea>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div class="uploadButton margin-top-30">
                                        <h5 class="uploadButton-file-name">Upload Relevant Files (Default is Private)</h5>
                                        <span style=" margin-top: 14px;"><input type="checkbox" @if($d->files_share_with_public==1) checked @endif name="share_with_public"
                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Share
                                            with Public</span>
                                        <div class="col-xl-12">
                                            <center>
                                                <input class="uploadButton-input" type="file"
                                                    accept="image/*, application/pdf" id="upload" multiple name="avatar[]"/>
                                                <label class="uploadButton-button ripple-effect" for="upload"><i
                                                        class="icon-feather-upload" style="font-size: 25px;"></i></label>
                                            </center>
                                        </div>

                                    </div>
                                </div>

                            
                                <div class="col-xl-12" >
                                    <?php $c=0;$cn=0; ?>
                                    @foreach($images as $i)
                                    <?php $cn++; ?>
                                    @if($c==0)
                                    
                                        <div class="row">
                                        @endif
                                        <div class="col-md-6" id="image_hide{{$cn}}">
                                            <div class="img-thumbnail" id="r">
                                             <img src="{{url('/'.$i)}}" >
                                             <input type="hidden" name="pre_path[]" value="{{$i}}">
                                             <input type="hidden" class="remv" name="remv[]" id="remv{{$cn}}" value="1"> 
                                             
                                             <div class="caption"><br>
                                             <a class="btn btn-link removed_btn" onclick="rmv({{$cn}})">Remove</a> 
                                             </div>
                                             </div>
                                        </div>
                                            <?php  $c++; ?>                                         
                                    @if($c==2)
                                    </div><br>

                                    <p style="display: none;">{{$c=0}}</p>
                                    @endif
                                    @endforeach
                                </div>
                                    <div class="col-xl-12">
                                    <div class="margin-top-30">
                                        <div style=" margin-top: 14px;"><input type="checkbox" @if($d->enable_crowdfunding==1) checked @endif name="enable_crowdfunding"
                                                style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Enable
                                            Crowdfunding</div>

                                        <div class="uploadButton-file-name" style="font-size: 12px;">* This option will
                                            enable delegative voting</div>
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="submit-field">

                                        <div class="row">
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Minimum Raise Amount</h5>
                                                    <input type="text" value="{{$d->min_raise_amount}}" class="with-border" name="min_raise_amount">
                                                </div>
                                            </div>
                                            <div class="col-xl-6">
                                                <div class="submit-field">
                                                    <h5>Maximum Raise Amount</h5>
                                                    <input type="text" value="{{$d->max_raise_amount}}" class="with-border" name="max_raise_amount">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <div style=" margin-top: 14px;"><input type="checkbox" @if($d->allow_multiple_source_participate==1) checked @endif name="allow_multiple_source_participate"
                                            style="width: auto !important; height: auto !important;">&nbsp;&nbsp;Allow
                                        Multiple Sources to Participate</div>
                                </div>

                         

 <div class="col-xl-12">
                    <button type="submit" class="button ripple-effect big margin-top-30">
                        Update Mission</button>
                </div>

                            </div>
                        </div>
                    </div>
                </div>
<br><br>

            </form>
            @endforeach
            </div>
                
                </div>
         
        </div>
    </div>
</div>
</div>
</div>


      


      
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV7WeUUT24I6Zog1Kqm_H0ZJcu548R72A&libraries=places&callback=initAutocomplete"
         async defer></script>
<script>
    // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();
          console.log('places');
          console.log(places);

          if (places.length == 0) {
            return;
          }

        
          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          console.log('boubds');
          console.log(bounds);
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();  
            console.log('latitude , longitude');
            console.log(latitude);
            console.log(longitude);
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
</script>
<!-- 
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!-- 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });


</script>
<script type="">
 
  function rmv(cn)
  {
    var a = $('#remv'+cn).val();
    
    $('#remv'+cn).val("0");
 $('#image_hide'+cn).hide();
  }         
</script>
@endsection