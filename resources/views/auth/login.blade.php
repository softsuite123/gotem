<!doctype html>
    <html lang="en">


    <head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Gotem - Log in</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('backstyling/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('backstyling/css/colors/blue.css') }}">

    </head>
    <style>
        body{
        background-color: #2A323C;
    }
    #titlebar{
        padding: 0px 0 !important;
    }
    .box{
        box-shadow: 0 19px 38px  rgba(0,0,0,0.5), 0 19px 38px rgba(0,0,0,0.5);
         padding: 20px;
    }
    .image-size{
        width: 40%;
    }
    .colorr{
        color: #fff !important;
    }
    .colorr-2{
        color: #F9CE2A;
    }
    .colorr-2:hover{
        color: gray;
    }
    .colorr-3{
        background-color: #3B4654 !important;
         border-color: #2A323C !important;
    }
    .colorr-4{
        background-color: #3B4654 !important;
         border-color: #2A323C !important;
         color: silver !important;
    }
    .colorr-5{
        background-color: #F9CE2A !important;
         border-color: #F9CE2A !important;
         color: #2A323C !important;
    }
    </style>
    <body>
      <div id="titlebar" class="gradient">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- <h2>Log In</h2> -->

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                    <!--    <li><a href="#">Home</a></li>
                    <li>Log In</li> -->
                    </ul>
                </nav>

            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row ">
        <div class="col-xl-5 offset-xl-3 box">
            <div class="login-register-page">

                <div class="card-body">
                    <div class="welcome-text">
                    <center><img src="https://gotem.io/public//homepage/images/go1-copy.png" class="image-size"></center>
                    <br>
                        <h3 class="colorr">We're glad to see you again!</h3>
                        <span class="colorr">Don't have an account? <a href="{{route('register')}}" class="colorr-2">Sign Up!</a></span>
                    </div>
                    <form method="POST" action="{{ route('login') }}" id="login-form">
                        @csrf

                    <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline colorr-3"></i>
                            <input type="text" class="input-text with-border colorr-4" @error('email') is-invalid @enderror" name="email"  value="{{ old('email') }}" id="emailaddress" placeholder="Email Address" required/>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-lock colorr-3"></i>
                            <input type="password" class="input-text with-border colorr-4"  @error('password') is-invalid @enderror" name="password" id="password" placeholder="Password" required/>
                              @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                    <a href="{{ route('password.request') }}" class="forgot-password colorr-2">Forgot Password?</a>



                       {{--  <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div> --}}
                    </form>
                    <button class="button full-width button-sliding-icon ripple-effect margin-top-10 colorr-5" type="submit" form="login-form">Log In <i class="icon-material-outline-arrow-right-alt"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="margin-top-70"></div>

<link rel="stylesheet" href="{{ asset('backstyling/css/colors/blue.css') }}">

<script src="{{ asset('backstyling/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('backstyling/js/jquery-migrate-3.1.0.min.html') }}"></script>
<script src="{{ asset('backstylingjs/mmenu.min.js') }}"></script>
<script src="{{ asset('backstylingjs/tippy.all.min.js') }}"></script>
<script src="{{ asset('backstyling/js/simplebar.min.js') }}"></script>
<script src="{{ asset('backstylingjs/bootstrap-slider.min.js') }}"></script>
<script src="{{ asset('backstyling/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('backstyling/js/snackbar.js') }}"></script>
<script src="{{ asset('backstyling/js/clipboard.min.js') }}"></script>
<script src="{{ asset('backstyling/js/counterup.min.js') }}"></script>
<script src="{{ asset('backstyling/js/magnific-popup.min.js') }}"></script>
<script src="{{ asset('backstyling/js/slick.min.js') }}"></script>
<script src="{{ asset('backstyling/js/custom.js') }}"></script>

<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() {
    Snackbar.show({
        text: 'Your status has been changed!',
        pos: 'bottom-center',
        showAction: false,
        actionText: "Dismiss",
        duration: 3000,
        textColor: '#fff',
        backgroundColor: '#383838'
    });
});
</script>

</body>


</html>
