<!doctype html>
<html lang="en">


<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <!-- Basic Page Needs
================================================== -->
    <title>Gotem - Register</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
================================================== -->
    <link rel="stylesheet" href="{{ asset('backstyling/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('backstyling/css/colors/blue.css') }}">

</head>

<body>

    @php
        $user = 'freelancer';
        if(\Request::segment(1) == 'employer' && \Request::segment(2)=='register'){
            $user = 'employer';
        }
    @endphp

    <!-- Titlebar
================================================== -->
<style>
    body{
        background-color: #2A323C;
    }
    #titlebar{
        padding: 0px 0 !important;
    }
    .box{
        box-shadow: 0 19px 38px  rgba(0,0,0,0.5), 0 19px 38px rgba(0,0,0,0.5);
         padding: 20px;
    }
    .image-size{
        width: 40%;
    }
    .colorr{
        color: #fff !important;
    }
    .colorr-2{
        color: #F9CE2A;
    }
    .colorr-2:hover{
        color: gray;
    }
    .colorr-3{
        background-color: #3B4654 !important;
         border-color: #2A323C !important;
    }
    .colorr-4{
        background-color: #3B4654 !important;
         border-color: #2A323C !important;
         color: silver !important;
    }
    .colorr-5{
        background-color: #F9CE2A !important;
         border-color: #F9CE2A !important;
         color: #2A323C !important;
    }
</style>
    <div id="titlebar" class="gradient">
        <!-- <div class="container">
            <div class="row">
               <div class="col-md-12">
        
                    <div class="logo">
                      <a href="{{url('/')}}" class="link-scroll"><img src="{{url('/')}}/homepage/images/go2.png" style="width: 150px;"></a>
                    </div>
        
                    Breadcrumbs
                    <nav id="breadcrumbs" class="dark">
                        <ul>
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li>User Register</li>
                        </ul>
                    </nav>
        
                </div>
            </div>
        </div> -->
    </div>


    <div class="container">
        <div class="row ">
            <div class="col-xl-5 offset-xl-3 box">
                <div class="login-register-page">

                    <div class="welcome-text">
                        <center><img src="{{asset('homepage/images/go1-copy.png')}}" class="image-size"></center>
                        <br>
                        <h3 class="colorr">Create an account to use the platform!</h3>
                        <span class="colorr">Already have an account? <a href="{{route('login')}}" class="colorr-2">Log In!</a></span>
                    </div>
                    <!-- Account Type -->
                    <form method="POST" action="{{ route('register') }}" id="register-account-form">
                        @csrf
                        <div class="account-type">
                           <!--  <div>
                                <input type="radio" name="account-type-radio" id="freelancer-radio"
                                    class="account-type-radio" value="freelancer" {{$user=='freelancer'?'checked':''}} />
                                <label for="freelancer-radio" class="ripple-effect-dark"><i
                                        class="icon-material-outline-account-circle"></i> Freelancer</label>
                            </div> -->

                            <div>
                                <input type="radio" name="account-type-radio" id="employer-radio"
                                    class="account-type-radio" checked="" value="employer" {{$user=='employer'?'checked':''}} />
                                <label for="employer-radio" class="ripple-effect-dark"><i
                                        class="icon-material-outline-business-center"></i> User</label>
                            </div>
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline colorr-3"></i>
                            <input type="text" class="input-text with-border colorr-4" @error('name') is-invalid @enderror"
                                name="name" value="{{ old('name') }}" id="name"
                                placeholder="Name" required />
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-baseline-mail-outline colorr-3"></i>
                            <input type="email" class="input-text with-border colorr-4" @error('email') is-invalid @enderror"
                                name="email" value="{{ old('email') }}" id="emailaddress-register"
                                placeholder="Email Address" required />
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="input-with-icon-left" title="Should be at least 8 characters long"
                            data-tippy-placement="bottom">
                            <i class="icon-material-outline-lock colorr-3"></i>
                            <input type="password" class="input-text with-border colorr-4" @error('password') is-invalid
                                @enderror" name="password" id="password-register" placeholder="Password" required />
                        </div>

                        <div class="input-with-icon-left">
                            <i class="icon-material-outline-lock colorr-3"></i>
                            <input type="password" class="input-text with-border colorr-4" @error('password_confirmation')
                                is-invalid @enderror" name="password_confirmation" id="password-repeat-register"
                                placeholder="Repeat Password" required />
                        </div>
                         @php
                                    $country = \App\Country::all();
                                   // dd($company);
                                @endphp
                            <div class="row" style="margin-top: -25px;"> 
                                     <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                  
                                                </div>

                                                <select style="height: 52px;" class=" input-text with-border colorr-4" name="country" id="country" required="">
                                                    <option selected disabled value="">Select One</option>
                                                  @foreach($country as $c)
                                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                                  @endforeach
                                                </select>
                                            </div>

                                            <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                  
                                                </div>

                                                <select style="height: 52px;" name="state" id="state" class="state_select input-text with-border colorr-4"
                                                    autocomplete="address-level1" required="" data-placeholder="Select an option…">
                                                    <option selected disabled value="">Select an option…</option>
                                                </select>

                                            </div>
                                </div>

                                    <div class="row" style="margin-top: -30px;"> 
                                    <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                  
                                                </div>

                                                <select style="height: 52px;" name="city" required="" id="city" class="city_select input-text with-border colorr-4"
                                                    autocomplete="address-level1" data-placeholder="Select an option…">
                                                    <option selected disabled value="">Select an option…</option>
                                                </select>
                                            </div>
                                        </div>

                        <input class="button full-width button-sliding-icon ripple-effect margin-top-10 colorr-5" type="submit"
                             value="Register"> <i class="icon-material-outline-arrow-right-alt"></i>

                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="margin-top-70"></div>



    <!-- Scripts
================================================== -->
    <script src="{{ asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
    {{-- <script src="{{ asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
    <script src="{{ asset('backstyling/js/mmenu.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/tippy.all.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/simplebar.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/bootstrap-select.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/snackbar.js')}}"></script>
    <script src="{{ asset('backstyling/js/clipboard.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/counterup.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/magnific-popup.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/slick.min.js')}}"></script>
    <script src="{{ asset('backstyling/js/custom.js')}}"></script>

    <!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
    <script>
        // Snackbar for user status switcher
        $('#snackbar-user-status label').click(function () {
            Snackbar.show({
                text: 'Your status has been changed!',
                pos: 'bottom-center',
                showAction: false,
                actionText: "Dismiss",
                duration: 3000,
                textColor: '#fff',
                backgroundColor: '#383838'
            });
        });
    </script>
    <script>
    $('#country').change(function () {
        var cid = $(this).val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option value="">Select State</option>');
                        $.each(res, function (key, value) {
                           
                            $("#state").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
    $('#state').change(function () {
        var sid = $(this).val();
        if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option value="">Select City</option>');
                        $("#city").append('<option value="141853">Other</option>');
                        $.each(res, function (key, value) {
                            
                            $("#city").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
</script>

</body>


</html>
</div>

{{--
                        ///////// --}}


