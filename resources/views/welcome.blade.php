<!DOCTYPE html>
<html lang="en">


<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

<!-- 
Copyright 2014-2015 ForBetterWeb
To use this theme you must have a license purchased at WrapBootstrap (http://forbetterweb.com)
-->

    <title>gotEM - Crowdsourcing Platform</title>
    <link rel="shortcut icon" type="image/jpg" href="{{url('/public/new_files')}}/img/logo.jpg"/>

    <!-- Bootstrap Core CSS -->
    <script src="../../../cdn-cgi/apps/head/wtjCOTBg-1aOzexT0mQ3ixXGkSM.js"></script><link rel="stylesheet" href="{{url('/public/new_files')}}/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Custom CSS -->
    <link href="{{url('/public/new_files')}}/css/hybrid.css" rel="stylesheet">
    <link href="{{url('/public/new_files')}}/css/animate.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../../../../maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    
    


    <!-- IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        .middle-signup {
    font-weight: 600;
    color: #000;
    text-decoration: none;
}
        .middle-signup:hover{
            color: #000;
    text-decoration: none;
        }
    </style>
</head>

<body>

<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-1x fa-bars"></i></a>
<nav id="sidebar-wrapper">
    <!--  Optional: close button
    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-2x fa-times"></i></a> -->
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <a href="#top"><img src="{{url('/public/new_files')}}/img/logo.jpg" width="20%" alt=""></a>
        </li>
        <li>
            <a href="#top">Home</a>
        </li>
        <li>
            <a href="#about">About</a>
        </li>
        <li>
            <a href="#services">DeFi</a>
        </li>
        <li>
            <a href="#case-study">Detectives</a>
        </li>
        <li>
            <a href="#portfolio">Media</a>
        </li>
        <li>
            <a href="#testimonials">Team</a>
        </li>
        <li>
            <a href="#contact2">Contact</a>
        </li>
        <li>
            <a href="{{url('/public/new_files')}}/Gotem whitepaper.pdf" target="_blank">Whitepaper</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<div class="header-bg">
    <header id="top" class="header">
        <div class="text-vertical">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5 col-md-offset-1">
                        <a href="#"><img src="{{url('/public/new_files')}}/img/go1-copy.png" class="logo-img" alt=""></a>
                        <div class="main-text">
                            <h2>Democratizing Transparency <br>
                                Through the Power of Crowdsourcing
                                </h2>
        
                            <p>A crowdsourcing DeFi platform enabling 
                                private investigations, private security, and humanitarianism 
                                </p>
                        </div>
                    </div>
    
                    <div class="col-md-4 col-md-offset-1">
                        <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. -->
                        <!-- WARNING: Some web hosts do not allow emails to be sent through forms to common mail hosts like Gmail or Yahoo. It's recommended that you use a private domain email address! -->
                        <!-- NOTE: To use the contact form, your site must be on a live web host with PHP! The form will not work locally! -->
    
                        <div class="auth-div">
                            <a href="{{url('/register')}}">User Sign Up</a>
                            <a href="{{url('/login')}}">Login</a>
                        </div>
                        <div class="invetigator">
                            <a href="{{url('/freelancer/register')}}">Investigators Apply Here</a>
                        </div>
                    </div>
                </div>
                <!-- mouse -->
                   <span class="scroll-btn hidden-xs wow fadeInDownBig">
                       <a href="#about"><span class="mouse"><span></span></span></a>
                   </span>
                <!-- mouse -->
            </div>
        </div>
    
    
    </header>
</div>

<div id="sponsor">
    <div class="container-fluid">
        <div class="row sponsor text-center wow fadeInLeftBig ">
            <div class="col-md-12">
                <h3>Featured In</h3>
            </div>
            <div class="col-md-offset-2">
                
                <div class="col-md-2 gray">
                    <a href="https://www.bloomberg.com/press-releases/2020-10-21/crowdsourced-private-investigations-platform-gotem-officially-launches-its-live-beta?fbclid=IwAR102xYfuO7-YjeIlnF3FWyyorxGh40AjluCCQBNdSxH22iW-0Lpb-wHNyc"><img src="{{url('/public/new_files')}}/img/bloomberg.png" alt=""></a>
                </div>
                <div class="col-md-2 gray">
                    <a href="https://finance.yahoo.com/news/societal-wind-change-echoed-crowdsourced-125800860.html"><img src="{{url('/public/new_files')}}/img/yahoo.png" alt=""></a>
                </div>
                <div class="col-md-2 gray">
                    <a href="https://finance.yahoo.com/news/crowdsourced-private-investigations-platform-gotem-153000061.html"><img src="{{url('/public/new_files')}}/img/yahoo-finance.png" alt=""></a>
                </div>
                <div class="col-md-2 gray">
                    <a href="https://www.forbes.com/sites/rebeccafannin/2016/09/16/dog-pcs-sextech-toys-trump-video-games-make-tech-fun-again/amp/"><img src="{{url('/public/new_files')}}/img/forbes.png" alt=""></a>
                </div>
                <div class="col-md-2 gray">
                    <a href="https://www.buzzfeednews.com/amphtml/williamalden/image-recognition-but-for-bread"><img src="{{url('/public/new_files')}}/img/buzzfeed-logo-black-and-white-1.png" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center wow fadeIn">
                
                <h2 class="heading">Launch an investigation within minutes from your device!</h2>
                <p class="lead">Post urgent cases related to private investigations, private security and humanitarian aid  </p><br>
                <div class="yellow-logo text-center">
                    <a href="{{url('/register')}}" class="middle-signup">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- The icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
<section class="about-grid">
    <div class="container-fluid wow fadeIn">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="row">
                    
                    <div class="col-lg-4 hidden-sm img-about-greed text-center wow slideInLeft"  data-wow-duration="2s">
                        <img width="100%" style="border-radius: 10px;" src="{{url('/public/new_files')}}/img/top-side.png" alt="">
                    </div>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-sm-6 wow fadeIn" data-wow-delay=".2s">
                                <h3>Decentralized</h3>
                                <p>Participate and operate relay nodes by holding our native token.</p>
                            </div>
                            <div class="col-sm-6 wow fadeIn" data-wow-delay=".6s">
                                <h3>Crowdfunding</h3>
                                <p> Eliminate borders and raise funds for your mission using Crypto</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 wow fadeIn" data-wow-delay=".6s">
                                <h3>Crowdsourcing</h3>
                                <p> Source aid from all parts of the world to participate in your mission.</p>
                            </div>
                            <div class="col-sm-6 wow fadeIn" data-wow-delay=".8s">
                                <h3>Smart Contracts</h3>
                                <p>Execute agreements and fund escrows over the Ethereum Blockchain.</p>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>

<!-- call-to-action -->
<div class="call-to-action">
    <div class="container-fluid">
            <div class="col-lg-12 text-center wow fadeIn">
                <iframe width="100%" height="500px" src="https://www.youtube.com/embed/uX8hxI-nrI8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
    </div>
</div>

<!-- services -->
<section id="services" class="head">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center wow fadeIn">
                <h2 class="heading">Crowdsourcing DeFi Platform</h2>
                <p class="lead">With the current political climate and the direction our world is taking, it is imperative that there is an outlet for transparency and global welfare. Our platform makes this demand a reality.</p>
            </div>
        </div>
    </div>
</section>

<section class="services">
    <div class="container-fluid">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="row wow fadeIn" data-wow-delay=".2s">
                <div class="col-md-4">
                    <h3><i class="fas fa-sack-dollar"></i> Reserve Fund</h3>
                    <p>Decentrally Financed (DeFi) reserve fund with governance for global missions </p>
                </div>
                <div class="col-md-4">
                    <h3><i class="fas fa-search"></i> Transparent</h3>
                    <p> Outlet for societal transparency and global welfare</p>
                </div>
                <div class="col-md-4">
                    <h3><i class="fas fa-users-class"></i> Crowdsourcing</h3>
                    <p> Utilize the power of crowdsourcing for on-demand investigations</p>
                </div>
            </div>
            <div class="row wow fadeIn" data-wow-delay=".6s">
                <div class="col-md-4">
                    <h3><i class="fas fa-landmark"></i> Governance</h3>
                    <p>Peer-to-peer voting enabling delegative democracy</p>
                </div>
                <div class="col-md-4">
                    <h3><i class="fas fa-layer-group"></i> Staking</h3>
                    <p> Stake and host a node hosting to enable a fully decentralized platform for global</p>
                </div>
                <div class="col-md-4">
                    <h3><i class="fab fa-bitcoin"></i> Yield Mining</h3>
                    <p> Earn interest by being a liquidity provider (LP) by adding funds to our liquidity pool</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- callout -->
<section class="callout">
    <div class="container-fluid text-vertical text-center">
        <div class="col-sm-12 mb-5 wow fadeInDownBig">
            <img src="{{url('/public/new_files')}}/img/logo.jpg" alt=""><br><br>
            <h2 class="heading">The gotEM platform empowers the people</h2>
            <p style="width: 50%; margin: 0px auto;">Using the platform would act as a safe medium for users to contract freelancing investigators in assisting with urgent missions anywhere in the world.</p>
            <!-- <div class="row"><a href="https://wrapbootstrap.com/theme/hybrid-multipurpose-landing-page-WB0N64HDJ" class="btn border-button">DOWNLOAD HYBRID <i class="fa fa-power-off"></i></a></div> -->
            
        </div>
        <div class="mid-auth">
            <a href="{{url('/register')}}" class="user-signup">User Sign Up</a>
            <a href="{{url('/freelancer/register')}}" class="user-signup">Investigator Sign Up</a>
        </div>
    </div>
</section>

<!-- case-study -->
<section id="case-study" class="head">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center wow fadeIn">
                <h2 class="heading"> Detectives in the Gotem Network</h2>
                <div id="detectiveSlider">      <!-- Give wrapper ID to target with jQuery & CSS -->
                    <div class="MS-content">
                        <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/1.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/2.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/3.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/5.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/6.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/7.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/8.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/10.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/11.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/12.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/13.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/14.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/15.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/16.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/17.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/18.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/19.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/20.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/21.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/22.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/23.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/24.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/26.jpg" alt=""> </div>
                            <div class="item"> <img class="img1" src="{{url('/public/new_files')}}/img/27.jpg" alt=""> </div>

                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>

<section class="case-study">
    <div class="container-fluid">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="row">
                <div class="container-fluid">
                    <div class="row text-center">
                        <h2 >Growing Global Demand</h2>
                        <p>Thousands of cases from all parts of the world</p>
                    </div>
                   <div style="display:flex;align-items: center;">
                        <div class="col-lg-6 hidden-sm img-case-study-greed text-center wow fadeInRight">
                            <img width="100%" src="{{url('/public/new_files')}}/img/worldmap.png" alt="">
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-sm-12 wow fadeIn" data-wow-delay=".2s">
                                    <h3>Crowdsourced Investigations</h3>
                                    <p>Relying on private investigators and individuals to collaborate in gathering intel.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 wow fadeIn" data-wow-delay=".6s">
                                    <h3>Affordable Private Security</h3>
                                    <p>Summon and crowdfund for a team of private security anywhere.
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 wow fadeIn" data-wow-delay=".8s">
                                    <h3>Crowdfund for Missions</h3>
                                    <p>Gather massive funding for global investigations to large scale private security issues.
                                    </p>
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Portfolio Grid Section -->
<section id="portfolio" class="head">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 text-center wow fadeIn">
                <img class="display-image" style="border-radius: 10px;" src="{{url('/public/new_files')}}/img/media.png" alt="">
            </div>
        </div>
    </div>
</section>

<div class="portfolio">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <iframe width="100%" height="315px" src="https://www.youtube.com/embed/O3_O14vGmC4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <!-- <a data-target="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-youtube-square fa-4x"></i>
                        </div>
                    </div>
                    <img src="img/3.jpg" class="img-responsive" alt="">
                </a> -->
            </div>
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <iframe width="100%" height="315px" src="https://www.youtube.com/embed/lGcwZKRb_kQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <!-- <a data-target="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-vimeo-square fa-4x"></i>
                        </div>
                    </div>
                    <img src="img/1.jpg" class="img-responsive" alt="">
                </a> -->
            </div>
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/uYaxE2SGzc4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <!-- <a data-target="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-picture-o fa-4x"></i>
                        </div>
                    </div>
                    <img src="img/5.jpg" class="img-responsive" alt="">
                </a> -->
            </div>
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/QpPYM4D4vGA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <!-- <a data-target="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-picture-o fa-3x"></i>
                        </div>
                    </div>
                    <img src="img/8.jpg" class="img-responsive" alt="">
                </a> -->
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <a data-target="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-picture-o fa-3x"></i>
                        </div>
                    </div>
                    <img src="img/7.jpg" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <a data-target="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                         <div class="caption-content">
                            <i class="fa fa-picture-o fa-3x"></i>
                        </div>
                    </div>
                    <img src="img/6.jpg" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <a data-target="#portfolioModal7" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            Caption Text
                        </div>
                    </div>
                    <img src="img/2.jpg" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-6 col-md-3 portfolio-item no-pad">
                <a data-target="#portfolioModal8" class="portfolio-link" data-toggle="modal">
                    <div class="caption">
                        <div class="caption-content">
                            <i class="fa fa-picture-o fa-3x"></i>
                        </div>
                    </div>
                    <img src="img/4.jpg" class="img-responsive" alt="">
                </a>
            </div>
        </div> -->
    </div>
</div>

<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://player.vimeo.com/video/59642733" allowfullscreen></iframe>
            </div>
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
            <div class="embed-responsive embed-responsive-16by9">
                <iframe src="https://player.vimeo.com/video/113287920" allowfullscreen></iframe>
            </div>
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
                <img src="{{url('/public/new_files')}}/img/5.jpg" class="img-responsive img-centered" alt="">
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
                <img src="{{url('/public/new_files')}}/img/8.jpg" class="img-responsive img-centered" alt="">
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
                <img src="{{url('/public/new_files')}}/img/7.jpg" class="img-responsive img-centered" alt="">
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
                <img src="{{url('/public/new_files')}}/img/6.jpg" class="img-responsive img-centered" alt="">
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
                <img src="{{url('/public/new_files')}}/img/2.jpg" class="img-responsive img-centered" alt="">
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <i class="fa fa-times fa-3x fa-fw"></i>
        </div>
        <div class="modal-body">
            <h2>Project Title</h2>
            <hr>
                <img src="{{url('/public/new_files')}}/img/4.jpg" class="img-responsive img-centered" alt="">
            <p>Hybrid is a multipurpose, responsive, one page html (bootstrap) theme, will look beautiful on any device. Hybrid easily and efficiently scales your project with one code base, from phones to tablets to desktops.</p>
            <ul class="list-inline item-details">
                <li>Client: <strong><a href="#">Hybrid</a></strong></li>
                <li>Date: <strong><a href="#">April 2015</a></strong></li>
                <li>Service: <strong><a href="#">Web Development</a></strong></li>
            </ul>
            <button type="button" class="border-button-black" data-dismiss="modal">CLOSE</button>
        </div>
    </div>
</div>

<!-- testimonials -->
<section id="testimonials">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2 class="heading">Team</h2>

                <div id="carousel-testimonials" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                        <li data-target="#carousel-testimonials" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/t1.jpg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>Tawei Chang</p>
                        <p>Founder / CEO</p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/t2.jpg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>Karma Bhutia</p>
                        <p>Co-Founder / CSO</p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/justin.png" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>Justin Bagan</p>
                        <p>Marketing Content Writer</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/ahmed.jpg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>AHMED SHAHZAIB</p>
                        <p>Lead Product Developer</p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/t7.jpg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">
    
                            <p>Rafiq Al Shabaz</p>
                            <p>Attorney / Advisory</p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/t6.jpg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>Myriam Joire</p>
                        <p>Advisory / Media</p>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/Ayodeji Emmanuel.jpeg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>Ayodeji Emmanuel</p>
                        <p>Partnership Liaison</p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/Dr. Leaug Imparable.jpeg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">
    
                            <p>Dr. Leaug Imparable</p>
                            <p>Fund Raising Strategist</p>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{url('/public/new_files')}}/img/OLATUNJI SUNDAY.jpg" alt="" style="width: 150px; height: 150px; border-radius: 150px; ">

                        <p>Olatunji Sunday</p>
                        <p>Community Outreach</p>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- sponsors -->


<!--
Map Section
Google Maps API integration with a custom Google Maps skin. Easy to edit custom PNG map marker.
Custom Google Map Skin, Address and PNG map marker: js/map.js
-->
<div id="" class="text-center map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3613.766932091172!2d121.37422081542789!3d25.07588748395271!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjXCsDA0JzMzLjIiTiAxMjHCsDIyJzM1LjEiRQ!5e0!3m2!1sen!2s!4v1623241167960!5m2!1sen!2s" width="100%" height="550" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

<!-- contacts -->
<div id="contact2">
    <div class="container-fluid overlay text-center">
        <div class="col-md-6 col-md-offset-3 wow fadeIn">
            <h2 class="wow flash animated" data-wow-iteration="999" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-iteration-count: 999; animation-name: flash;"><a href="#top"><i class="fa fa-angle-up"></i></a>&nbsp;</h2>
            <h2 class="heading"><a href="#top"><img width="30%" src="{{url('/public/new_files')}}/img/go1-copy.png" alt=""></a></h2>
                <ul class="list-inline">
                    <li>
                        <a href="https://twitter.com/iogotem"><i class="fab fa-twitter fa-2x fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="https://t.me/joinchat/S9QSMT40FCoVyNnG"><i class="fab fa-telegram-plane fa-2x  fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="https://www.reddit.com/user/GotemGTX"><i class="fab fa-reddit-alien fa-2x  fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="https://medium.com/@GotemGTX"><i class="fab fa-medium-m fa-2x  fa-fw"></i></a>
                    </li>
                    <li>
                        <a href="mailto:contact@gotem.io"><i class="fas fa-envelope fa-2x  fa-fw"></i></a>
                    </li>
                </ul>
            <p>Copyright © 2021 Gotem.io All Rights Reserved.</p>

        </div>
    </div>
</div>

<!-- jQuery -->
<script data-cfasync="false" src="../../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="{{url('/public/new_files')}}/js/jquery.min.js"></script>

    
</script>
<!-- Bootstrap Core JavaScript -->
<script src="{{url('/public/new_files')}}/js/bootstrap.min.js"></script>

<!-- jQuery Plugins -->
<script src="{{url('/public/new_files')}}/js/wow.min.js"></script>
<script src="{{url('/public/new_files')}}/js/jquery.placeholder.min.js"></script>
<script src="{{url('/public/new_files')}}/js/smoothscroll.js"></script>

<!-- Google Map -->
<!-- Google Maps API Key - Use your own API key to enable the map feature. More information on the Google Maps API can be found at https://developers.google.com/maps/ -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbzKw3SKryjUD6eZ4-m9xSlFRyBJ8odsY"></script>
<script src="{{url('/public/new_files')}}/js/map.js"></script>

<!-- Custom Theme JavaScripts -->
<script src="{{url('/public/new_files')}}/js/hybrid.js"></script>

<script defer src="../../../../static.cloudflareinsights.com/beacon.min.js" data-cf-beacon='{"rayId":"6556b349b9c7ca78","version":"2021.5.1","r":1,"token":"99f4e230e2134f17ba76985e2fd7ad4b","si":10}'></script>
<script src="{{url('/public/new_files')}}/js/multislider/js/multislider.min.js"></script>
<script>
    $( document ).ready(function() {

$('#detectiveSlider').multislider({
        continuous: true,
            duration: 5000
            
});
});
</script>
</body>

</html>