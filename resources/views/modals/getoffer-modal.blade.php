@php

    $mission = \App\Mission::where('id',$offer->mission_id)->first();



Session::put('l_mission_amount',$mission->estimated_budget);


Session::put('l_applicant_id',$offer->applicant_id);
Session::put('l_project_id',$offer->project_id);
// Session::put('mission_title',$mission->title);
// Session::put('mission_description',$mission->description);
session_start();
    $_SESSION['mission_amount'] = $offer->offer_amount;

    $_SESSION['mission_title'] = $mission->title;
    $_SESSION['mission_description'] = $mission->description;
    $_SESSION['applicant_id'] = $offer->applicant_id;
    $_SESSION['project_id'] = $offer->id;

    if($offer->is_milestone){
        $milestones = App\Milestone::where('project_id',$offer->id)->get();
    }

    $retainer = (float)$offer->offer_amount *20/100;
@endphp

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!-- First Modal  -->

                <center>
                <h3>You've Received an Offer!</h3></center>
                <br>
                <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                    <p>{{$mission->title}}.</p>
                </div>
                <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                    <p>{{$mission->mission_description}}.</p>
                </div>
                <br>
                @if($offer->is_milestone==0)
                <div class="row">
                    <div class="col-xl-2"></div>
                    <div class="col-xl-8">
                        <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                            <center>
                            <p>Quote: ${{$offer->offer_amount}}</p>
                            <hr>
                            <br>
                            <p>${{$retainer}} (20%) Retainer to Begin</p>
                            </center>
                        </div>
                    </div>
                    <div class="col-xl-2"></div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xl-5">
                        <a href="#myModal2" class="button gray ripple-effect-dark" data-toggle="modal" onclick="decline(0)">Decline Offer</a>
                    </div>
                    <div class="col-xl-3"></div>
                    <div class="col-xl-4">
                        <a href="#myModal1" class="button ripple-effect coolor" onclick="accept_offer(0)" data-toggle="modal">Accept Offer</a>
                    </div>
                </div>
                <br>
                @endif
                @if(isset($milestones))
                <div class="row">
                    <div class="col-xl-12">
                        <div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;">
                            @php
                                $counter=1;
                            @endphp
                            @foreach ($milestones as $milestone)
                            @if($milestone->status==1)
                                <div class="row">
                                    <div class="col-xl-4">
                                        <p>Milestone {{$counter}} - {{$milestone->milestone_title}}.</p>
                                    </div>
                                    <div class="col-xl-3">
                                        <div style="border: 1px solid silver; padding: 10px;">${{$milestone->milestone_amount}}</div>

                                        <?php
                                            $_SESSION['mission_amount'] = $milestone->milestone_amount;
                                        ?>
                                    </div>
                                    <div class="col-xl-5"  >
                                        <!--<a href="#myModal1" style="padding:10px;" class="button " id="{{$milestone->id}}" onclick="accept_offer(this.id)" data-toggle="modal">Accept</a>-->
                                        <a href="#myModal1" class="button ripple-effect coolor" onclick="accept_offer(0)" data-toggle="modal">Accept Offer</a>
                                        <!--<button   class="button">Accept</button>-->
                                         <a href="#myModal2" class="button " style="padding:10px; background-color:red;" id="{{$milestone->id}}" data-toggle="modal" onclick="decline(this.id)">Decline</a>
                                        <!--<button    class="button">Declined</button>-->
                                    </div>
                                </div>
                                <hr>
                                <br>
                                @php
                                    $counter++;
                                @endphp
                                @endif
                            @endforeach

                            {{-- <div class="row">
                                <div class="col-xl-9">
                                    <p>Milestone 2 - check missing person files with local law enforcement, fbi. Interview further contacts based on money trail via debit or cc.</p>
                                </div>
                                <div class="col-xl-3">
                                    <div style="border: 1px solid silver; padding: 10px;">$1920</div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>

                @endif
                <br>
                <!--<div class="row">-->
                <!--    <div class="col-xl-5">-->
                <!--        <a href="#myModal2" class="button gray ripple-effect-dark" data-toggle="modal" onclick="decline()">Decline Offer</a>-->
                <!--    </div>-->
                <!--    <div class="col-xl-3"></div>-->
                <!--    <div class="col-xl-4">-->
                <!--        <a href="#myModal1" class="button ripple-effect coolor" onclick="accept_offer()" data-toggle="modal">Accept Offer</a>-->
                <!--    </div>-->
                <!--</div>-->
                <!-- End First Modal -->
            </div>
        </div>
    </div>
