@extends('freelancer.layouts.master')

@section('content')

<!-- Wrapper -->
<div id="wrapper">


    @include('freelancer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')


    <!-- Dashboard Content
    ================================================== -->
   <div class="dashboard-content-container" data-simplebar>
                <div class="dashboard-content-inner">



                    <!-- Row -->
                    <div class="row">

                        <!-- Dashboard Box -->
                        <div class="col-xl-12">
                            <div class="dashboard-box margin-top-0">

                                <!-- Headline -->
                                <div class="headline">
                                    <h3><i class="icon-material-outline-business-center"></i> My Jobs</h3>
                                </div>
                                <div class="content">
                                    <br>

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12 col-6">
                                                <table id="example" class="table table-hover responsive nowrap" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Mission</th>
                                                            <th>Name</th>
                                                            <th>Price</th>
                                                            <!-- <th>Time</th> -->
                                                            <!-- <th>Stats</th> -->
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($myJobs as $job)
                                                        @php
                                                        // dd($order->project_id);
                                                            $project = \App\project::find((int)$job->project_id);
                                                            // dd($project);
                                                         if($project) {   $mission = \App\Mission::find($project->mission_id); 
                                                            $employer = \App\User::find($project->applicant_id); }
                                                            else{
                                                            $mission ="Null";
                                                            $employer ="Null";
                                                            }
                                                        @endphp
                                                        <tr>
                                                            
                                                        @if($mission == "Null")                                                           
                                                            <td></td>
                                                        @else 
                                                           <td>@if($mission ) {{$mission->title}}@endif</td>>
                                                        @endif
                                                        @if($employer == "Null")                                                           
                                                            <td></td>
                                                        @else
                                                            <td>{{$employer->name}}</td>
                                                         @endif    
                                                         
                                                         @if($project )
                                                            <td>$ {{$project->offer_amount}}</td>
                                                        @else
                                                          <td></td>
                                                         @endif
                                                            <!-- <td>4:53 PM</td> -->
                                                            <!-- <td>9572</td> -->
                                                            <td><a id="launchModalBtn" name="{{$job->id}}" class="button ripple-effect button-sliding-icon coolor">Deliever Now <i class="icon-feather-arrow-right"></i></a></td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <br>

                                </div>

                            </div>
                        </div>

                    </div>
                    <!-- Row / End -->

                    <!-- Footer -->
                    <div class="dashboard-footer-spacer"></div>
                    <!-- <div class="small-footer margin-top-15">
                <div class="small-footer-copyrights">
                    © 2019 <strong>Hireo</strong>. All Rights Reserved.
                </div>
                <ul class="footer-social-links">
                    <li>
                        <a href="#" title="Facebook" data-tippy-placement="top">
                            <i class="icon-brand-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Twitter" data-tippy-placement="top">
                            <i class="icon-brand-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="Google Plus" data-tippy-placement="top">
                            <i class="icon-brand-google-plus-g"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" title="LinkedIn" data-tippy-placement="top">
                            <i class="icon-brand-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div> -->
                    <!-- Footer / End -->

                </div>
            </div>
    <!-- Dashboard Content / End -->


    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


    <div class="modal fade" id="modal_1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body">

                    <!-- First Modal  -->
                    <form method="post" action="{{route('submit.work')}}" enctype="multipart/form-data">
                        @csrf
                    <input type="hidden" name="orderid" id="porder_id">

                    <div class="row">
                        <div class="col-xl-3">
                            <div class="file btn btn-lg btn-primary"
                                style="background-color: transparent; border-color: gray; color: gray; font-weight: normal;">
                                Upload File
                                <input type="file" name="file"
                                    style="position: absolute; font-size: 40px;  opacity: 0; right: 0; top: 0;" required />
                            </div>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-xl-12">
                            <textarea cols="30" rows="10" name="desc" placeholder="Description" required></textarea>
                        </div>
                    </div>



                    <br>
                    <div class="row">
                        <div class="col-xl-5">
                        </div>
                        <div class="col-xl-3"></div>
                        <div class="col-xl-4">
                            <input class="button ripple-effect coolor" type="Submit" id="launchSecond" value="Submit">
                        </div>
                    </div>

                    </form>
                    <!-- End First Modal -->

                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>


<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

 <script>
        $(document).on('click', '#launchModalBtn', function () {
            let event = $(this);
            let order_id = event[0].name;
            $('#porder_id').val(order_id);
            $('#modal_1').modal('show');
        });

        $('.modal').on('show.bs.modal', function () {
            $('.modal').not($(this)).each(function () {
                $(this).modal('hide');
            });
        });

    </script>


@endsection