@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('freelancer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')

            <!-- Dashboard Content
	================================================== -->
	<div class="col-xl-9 col-lg-8 content-left-offset">
@if (session('error'))
  <li class="alert alert-danger">{{session('error')}}</li>
@endif

@if (session('success'))
  <li class="alert alert-success">{{session('success')}}</li>
@endif

			<div class="notify-box margin-top-15">
				<div class="switch-container">
					<!-- <label class="switch"><input type="checkbox"><span class="switch-button"></span><span class="switch-text">Turn on email alerts for this search</span></label> -->
					<h3 class="page-title"><b>Balance</b></h3>
				</div>

				<!-- <div class="sort-by">
					<span>Sort by:</span>
					<select class="selectpicker hide-tick">
						<option>Relevance</option>
						<option>Newest</option>
						<option>Oldest</option>
						<option>Random</option>
					</select>
				</div> -->
			</div>
			<br>



    <span>Minimum withdrawal limit is $10</span>



			<div class="row">
			<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">


              <div class="container">
						<div class="row">
						    <div class="col-lg-4">
						      <div class="section-headline margin-top-25 margin-bottom-12">
				<h5>Account Balance</h5>
			</div>
			<input class="with-border" placeholder="Account Balance" value="{{$balance}}" readonly>
						    </div>
						    <div class="col-lg-8">
						        </div>
						        </div>
<form method="POST" action="{{route('withdraw.post')}}">
  @csrf
						        <div class="row">
						    <div class="col-lg-4">
						      <div class="section-headline margin-top-25 margin-bottom-12">
				<h5>Withdrawal Amount</h5>
			</div>
			<input type="text" name="amount" class="with-border" placeholder="Withdrawal Amount">
      <input type="text" name="hash" class="with-border" placeholder="Enter walet address">
						    </div>
						    <div class="col-lg-8">
						        </div>
						        </div>

		<br>
		<br>
		<br>
		<br>


	<div class="row">
<div class="col-lg-12">
	<center><input type="Submit" class="button big ripple-effect" value="Submit"></center>

</div>





						        </div>

                    </form>


						<!-- Headline -->
						<!--	<br>
						<div class="container">
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-4"><center><h3 class="job-listing-title">Username:</h3></center></div>
							<div class="col-lg-4"><center><h3 class="job-listing-title">Hassan Saeed</h3><center></div>
							<div class="col-lg-2"></div>
						</div>
						<br>
						<div class="row">
							<div class="col-lg-2"></div>
							<div class="col-lg-4"><center><h3 class="job-listing-title">ID:</h3></center></div>
							<div class="col-lg-4"><center><h3 class="job-listing-title">081089</h3><center></div>
							<div class="col-lg-2"></div>
						</div>
                       </div>
 <br><br><br><br><br><br><br>
<style>


.file-upload {
  background-color: #ffffff;
  width: 600px;
  margin: 0 auto;
  padding: 20px;
}
@media (max-width: 768px){
   .file-upload {
  background-color: #ffffff;
  width: 350px;
  margin: 0 auto;
  padding: 20px;
}
}

.file-upload-btn {
  width: 100%;
  margin: 0;
  color: #fff;
  background: #1FB264;
  border: none;
  padding: 10px;
  border-radius: 4px;
  border-bottom: 4px solid #15824B;
  transition: all .2s ease;
  outline: none;
  text-transform: uppercase;
  font-weight: 700;
}

.file-upload-btn:hover {
  background: #1AA059;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.file-upload-btn:active {
  border: 0;
  transition: all .2s ease;
}

.file-upload-content {
  display: none;
  text-align: center;
}

.file-upload-input {
  position: absolute;
  margin: 0;
  padding: 0;
  width: 100%;
  height: 100%;
  outline: none;
  opacity: 0;
  cursor: pointer;
}

.image-upload-wrap {
  margin-top: 20px;
  border: 4px dashed gray;
  position: relative;
}

.image-dropping,
.image-upload-wrap:hover {
  background-color: gray;
  border: 4px dashed #ffffff;
}

.image-title-wrap {
  padding: 0 15px 15px 15px;
  color: #222;
}

.drag-text {
  text-align: center;
}

.drag-text h3 {
  font-weight: 100;
  text-transform: uppercase;
  color: #000;
  padding: 60px 0;
}

.file-upload-image {
  max-height: 200px;
  max-width: 200px;
  margin: auto;
  padding: 20px;
}

.remove-image {
  width: 200px;
  margin: 0;
  color: #fff;
  background: #cd4535;
  border: none;
  padding: 10px;
  border-radius: 4px;
  border-bottom: 4px solid #b02818;
  transition: all .2s ease;
  outline: none;
  text-transform: uppercase;
  font-weight: 700;
}

.remove-image:hover {
  background: #c13b2a;
  color: #ffffff;
  transition: all .2s ease;
  cursor: pointer;
}

.remove-image:active {
  border: 0;
  transition: all .2s ease;
}
</style>
						<div class="content">


<div class="container">
<div class="row">
<div class="col-lg-12">
<div class="file-upload">-->
  <!-- <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button> -->
<!--  <div class="row">
  <div class="col-lg-8">
  <h4>Upload Relevant Files (Default is private)</h4>
</div>
<div class="col-lg-4">
	<div class="checkbox">
				<input type="checkbox" id="chekcbox2">
				<label for="chekcbox2"><span class="checkbox-icon"></span> Share with public</label>
			</div>
</div>
</div>
  <div class="image-upload-wrap">
    <input class="file-upload-input" type='file' onchange="readURL(this);" accept="image/*" />
    <div class="drag-text">
      <h3>Drag and Drop</h3>
    </div>
  </div>
  <div class="file-upload-content">
    <img class="file-upload-image" src="#" alt="your image" />
    <div class="image-title-wrap">
      <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
    </div>
  </div>
</div>
</div>
</div>
</div>

<br><br>

<div class="container">
<div class="row">
<div class="col-lg-4">

			<div class="section-headline margin-top-25 margin-bottom-12">
				<h5>Minimum Raise Amount</h5>
			</div>
			<input class="with-border" placeholder="Min">

</div>
<div class="col-lg-4">

			<div class="section-headline margin-top-25 margin-bottom-12">
				<h5>Maximum Raise Amount</h5>
			</div>
			<input class="with-border" placeholder="Max">

</div>
<div class="col-xl-4"></div>

<div class="col-lg-12">
	<div class="checkbox">
				<input type="checkbox" id="chekcbox2">
				<label for="chekcbox2"><span class="checkbox-icon"></span> Allow Multiple Sources to Participate</label>
			</div>

</div>

</div>
<br>
<div class="row">
<div class="col-lg-12">
	<center><a href="#" class="button big ripple-effect">Submit</a></center>

</div>
</div>
</div>










						</div>-->
					</div>
				</div>
			</div>




		</div>

		<!--Dashboard content end-->


    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

@endsection