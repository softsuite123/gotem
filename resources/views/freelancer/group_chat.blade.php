@extends('employer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('employer.partials.header')

<link rel="stylesheet" type="text/css" href="https://unpkg.com/v-animate-css/dist/v-animate-css.js">

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')


	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container">
		
			
<style>

html {
  box-sizing: border-box;
}

*,
*::before,
*::after {
  box-sizing: inherit;
}

body {
  color: #f0f4ff;
  font: normal 1.25rem/125% "acumin-pro", sans-serif;
  margin: 0;
  text-rendering: optimizeLegibility;
}

input {
  background: none;
  border: none;
  border-radius: 0.25rem;
  color: #f0f4ff;
  outline: none;
}

/* Root grid */
.grid {
  display: grid;
  grid-template-columns: 20rem 1fr;
  height: 75vh;
}

/* Header */
.header {
  background: #2e3036;
}
.header__server:hover {
  background: #282a2e;
}
.header__channel {
  color: rgba(240, 244, 255, 0.5);
  padding: 1rem;
}
.header__profile {
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: #282a2e;
  padding: 0 1rem;
}

.channel__item {
  margin-bottom: 2rem;
}

.channel__item > div {
  margin-bottom: 1rem;
}

.channel__title {
  border-radius: 0.25rem;
  min-height: 3rem;
  min-width: 3rem;
  display: flex;
  align-items: center;
  padding-left: 1.25rem;
  width: 100%;
}
.channel__title:hover {
  background: rgba(240, 244, 255, 0.05);
  color: #f0f4ff;
}

.profile__badge {
  display: flex;
  align-items: center;
}

.profile__avatar,
.friend__avatar {
  background: #36383e;
  border-radius: 50%;
  min-height: 3rem;
  min-width: 3rem;
  margin-right: 1rem;
  position: relative;
}

.avatar__status {
  background: #44b682;
  border-radius: 50%;
  min-height: 1rem;
  min-width: 1rem;
  content: "";
  position: absolute;
  bottom: 0;
  right: 0;
}

.user__id {
  color: rgba(240, 244, 255, 0.25);
}

.profile__settings {
  display: flex;
  align-items: center;
}

.profile__setting {
  background: #36383e;
  border-radius: 50%;
  min-height: 1rem;
  min-width: 1rem;
}
.profile__setting:not(:last-child) {
  margin-right: 1rem;
}

/* Footer */
.footer {
  background: #36383e;
}
.footer__chat {
  height: calc(100vh - 10rem);
  overflow-y: scroll;
  overflow-x: hidden;
  padding: 1rem;
}

.footer__chat::-webkit-scrollbar-track {
  border: 1px solid #000;
  padding: 2px 0;
  background-color: #404040;
}

.footer__chat::-webkit-scrollbar {
  width: 10px;
}

.footer__chat::-webkit-scrollbar-thumb {
  border-radius: 10px;
  box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background-color: #737272;
  border: 1px solid #000;
}
.footer__post {
  display: flex;
  align-items: flex-start;
  position: relative;
}
.footer__post:not(:last-child) {
  margin-bottom: 2rem;
}
.footer__input {
  padding: 1rem;
}

.post__avatar {
  background: #7288da;
  border-radius: 50%;
  min-height: 3rem;
  min-width: 3rem;
  background-position: center;
  background-size: 100% 100%;
  margin-right: 1rem;
}

.post__name {
  display: inline;
}
.post__name:hover {
  text-decoration: underline;
}

.post__timestamp {
  color: rgba(240, 244, 255, 0.25);
  display: inline;
  font-size: 0.75rem;
}

.post__is_you {
  color: rgba(240, 244, 255, 0.25);
  position: absolute;
  /* блок занимает ширину содержимого, max-width её ограничивает */
  top: 0.3em;
  /* прикрепить к верху родителя */
  right: 0px;
  display: inline-block;
  font-size: 0.75rem;
}

.post__message {
  color: rgba(240, 244, 255, 0.75);
  word-break: break-word;
}

.input__container {
  display: contents;
  align-items: center;
  background: #42444c !important;
  height: 100%;
  padding: 0 1rem;
}

.input__message {
  font: normal 1.25rem/100% "acumin-pro", sans-serif;
  width: 100%;
}

/* Main */
.main {
  background: #2e3036;
}
.main__container {
  display: grid;
  grid-template-rows: 5rem 1fr;
}

@media (max-width: 768px){
	.main {
		display: none;
	}
	.footer{
		width: 128%;
	}
}
.main__settings {
  background: #36383e;
}
/* .main__friends {
  height: calc(100vh - 5rem);
  overflow-y:  scroll;
  padding: 1rem;
} */
.large-2 {
  height: calc(100vh - 5rem);
  overflow-y:  scroll;
  padding: 1rem;
}

.force-overflow {
  min-height: 150px;
}

.large-2::-webkit-scrollbar-track {
  border: 1px solid #000;
  padding: 2px 0;
  background-color: #404040;
}

.large-2::-webkit-scrollbar {
  width: 10px;
}

.large-2::-webkit-scrollbar-thumb {
  border-radius: 10px;
  box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background-color: #737272;
  border: 1px solid #000;
}
.main .avatar__status {
  min-height: 0.75rem;
  min-width: 0.75rem;
}
.main .user__name {
  color: rgba(240, 244, 255, 0.75);
}

.main__settings {
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 1rem;
}

.settings__group {
  display: flex;
  flex-direction: row;
}

.group__setting {
  background: #4e5159;
  border-radius: 50%;
  min-height: 1rem;
  min-width: 1rem;
}
.group__setting:not(:last-child) {
  margin-right: 1rem;
}

.settings__search {
  position: relative;
}

.search__input {
  background: #4e5159;
  border-radius: 0.25rem;
  font: normal 1rem/100% "acumin-pro", sans-serif;
  height: 2rem;
  padding: 0 0.5rem;
}

.friend__online {
  display: flex;
  align-items: center;
}
.friend__online:not(:last-child) {
  margin-bottom: 1rem;
}

.friend__online .avatar__status {
  background: #44b682;
  border-radius: 50%;
  min-height: 0.8rem;
  min-width: 0.8rem;
}

.friend__offline .avatar__status {
  background: firebrick;
  border-radius: 50%;
  min-height: 0.8rem;
  min-width: 0.8rem;
}

.friend__dnd .avatar__status {
  background: orangered;
  border-radius: 50%;
  min-height: 0.8rem;
  min-width: 0.8rem;
}

.friend__offline {
  display: flex;
  align-items: center;
}
.friend__offline:not(:last-child) {
  margin-bottom: 1rem;
}

.friend__dnd {
  display: flex;
  align-items: center;
}
.friend__dnd:not(:last-child) {
  margin-bottom: 1rem;
}

.friend__category {
  color: rgba(240, 244, 255, 0.25);
  margin-bottom: 1rem;
}

.friend__avatar {
  background: rgba(255, 255, 255, 0.1);
  border-radius: 50%;
  min-height: 2rem;
  min-width: 2rem;
  margin-right: 1rem;
}

.main__friends .user__name:hover {
  color: #f0f4ff;
}

/* Repeats */
.header__container,
.footer__container {
  display: grid;
  grid-template-rows: 5rem 1fr 5rem;
  height: 100%;
}

.header__server,
.footer__channel {
  padding: 0 1rem;
}

.header__server,
.footer__channel,
.main__title {
  display: flex;
  align-items: center;
  border-bottom: 0.025rem solid #202224;
}

</style>			
	
				<div class="grid">
  <div class="main">
    <div class="main__container">
      <div class="main__settings">
        <input class="search__input" type="text" v-model="searchQuery" placeholder="Type for Search" style="background-color: #42444c; margin-top: 10px;" /></div>
      <div class="main__friends">
      	<div class="large-2">
      <div class="force-overflow">
        <div class="friend__category">Online</div>
        <div class="friend__online" v-for="user in onlineUsers">
          <div class="friend__avatar">
            <div class="avatar__status"></div>
          </div>
          <div class="friend__user">
            <div class="user__name" v-on:click="mentionUser(user.username)">Alice</div>
          </div>
        </div>
        <div class="friend__category">Offline</div>
        <div class="friend__offline" v-for="user in offlineUsers">
          <div class="friend__avatar">
            <div class="avatar__status"></div>
          </div>
          <div class="friend__user">
            <div class="user__name" v-on:click="mentionUser(user.username)">Adam</div>
          </div>
        </div>
        <div class="friend__category">DND</div>
        <div class="friend__dnd" v-for="user in dontDisturbUsers">
          <div class="friend__avatar">
            <status-indicator status="active" />
            <div class="avatar__status"></div>
          </div>
          <div class="friend__user">
            <div class="user__name" v-on:click="mentionUser(user.username)">Michael</div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>
  </div>
  <div class="footer">
    <div class="footer__container">
      <div class="footer__channel">
        <div class="header__title">Group Chat</div>
      </div>
      <div class="footer__chat" ref="chat">
        <div class="footer__post" v-for="post in posts" :key='post' v-animate-css="'slideInRight'">
          <div class="post__avatar" v-bind:style="{'background-image': 'url('+post.avatar+')'}"></div>
          <div class="post__content">
            <div class="post__name">John</div>
            <div class="post__timestamp">15/1/2020</div>
            <div class="post__is_you">Joseph</div>
            <div class="post__message">Hii!!</div>
          </div>
        </div>
      </div>
      <div class="footer__input">
        <div class="input__container"><input class="input__message" type="text" placeholder="Enter message" v-model="post" @keyup.enter="createPost" ref="inputPost" style="background-color: #42444c !important" /></div>
      </div>
    </div>
  </div>
</div>



			
		

			<div class="dashboard-footer-spacer"></div>
		

		</div>






          
         
         
    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://unpkg.com/vuebar"></script>
<script src="https://unpkg.com/vue-status-indicator@latest/dist/vue-status-indicator.js"></script>
<script src="https://unpkg.com/v-animate-css/dist/v-animate-css.js"></script>

<!-- 
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!-- 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
  
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<script>
	Vue.use(VAnimateCss.default);

new Vue({
  el: ".grid",
  data() {
    return {
      active: false,
      post: "",
      searchQuery: "",
      channel: {
        name: "Default" },

      users: [
      {
        username: "Forevka",
        status: 1 },

      {
        username: "volodia",
        status: 1 },

      {
        username: "off",
        status: 2 },

      {
        username: "dont disturb me",
        status: 3 }],


      posts: [],
      current_user: {
        username: "Forevka",
        avatar: "https://ui-avatars.com/api/?name=Forevka&size=128&background=b0a0a1",
        userid: 1 } };


  },
  mounted() {
    to_insert = [
    {
      text: "asd",
      username: "Forevka",
      avatar: "https://ui-avatars.com/api/?name=Forevka&size=128&background=b0a0a1",
      userid: 1,
      timestamp: '1565968319' },

    {
      text: "lol",
      username: "volodia",
      avatar: "https://ui-avatars.com/api/?name=volodia",
      userid: 2,
      timestamp: '1565968319' }];


    for (i = 0; i < to_insert.length; i++) {if (window.CP.shouldStopExecution(0)) break;
      this.insertPost(to_insert[i]);
    }window.CP.exitedLoop(0);
  },
  updated() {
    this.$nextTick(() => {
      let chat = this.$refs.chat;
      chat.scrollTop = chat.scrollHeight;
    });
  },
  computed: {
    timestamp() {
      return moment().format("h:mm");
    } },

  methods: {
    createPost: function () {
      let post = this.post && this.post.trim();
      if (!post) {
        return false;
      }
      post = this.generatePost(post);
      this.insertPost(post);
      this.post = "";
    },
    insertPost: function (post) {
      post.timestamp = moment.unix(post.timestamp).format("h:mm");
      this.posts.push(post);
    },
    mentionUser: function (username) {
      this.post += "@" + username + " ";
      this.$refs.inputPost.focus();
    },
    generatePost: function (text) {
      return {
        text: text,
        username: this.current_user.username,
        avatar: this.current_user.avatar,
        userid: this.current_user.userid,
        timestamp: moment().unix() };

    } },

  computed: {
    onlineUsers: function () {
      let s = this.searchQuery.replace(/ /g, '');
      return this.users.filter(function (u) {
        return u.status == 1 && u.username.replace(/ /g, '').toLowerCase().indexOf(s.toLowerCase()) !== -1;
      });
    },
    offlineUsers: function () {
      let s = this.searchQuery.replace(/ /g, '');
      return this.users.filter(function (u) {
        return u.status == 2 && u.username.replace(/ /g, '').toLowerCase().indexOf(s.toLowerCase()) !== -1;
      });
    },
    dontDisturbUsers: function () {
      let s = this.searchQuery.replace(/ /g, '');
      return this.users.filter(function (u) {
        return u.status == 3 && u.username.replace(/ /g, '').toLowerCase().indexOf(s.toLowerCase()) !== -1;
      });
    } } });
</script>

@endsection