@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">
    @include('freelancer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')
    

                <!-- Dashboard Content
    ================================================== -->
    <div class="dashboard-content-container" >
        <!-- <div class="dashboard-content-inner" >
        <div id="wrapper"> -->
        <div class="full-page-container">

    <div class="full-page-sidebar">
   <form action="{{route('search.browsemission')}}">
 @csrf 

        <div class="full-page-sidebar-inner" data-simplebar>
            <div class="sidebar-container">

                <!-- Location -->
                <div class="sidebar-widget">
                    <h3>Country</h3>
                     <select class="selectpicker default" id="country" name="country" data-selected-text-format="count" data-size="7" title="Countries" >
                        @foreach ($countries as $country)
                            <option value="{{$country->id}}">{{$country->name}}</option>
                        @endforeach
                    </select>
             
                </div>
                <!-- Category -->
                <div class="sidebar-widget">
                    <h3>Category</h3>
                    <select class="selectpicker default" id="company" name="company" data-selected-text-format="count" data-size="7" title="All Categories" >
                        @foreach ($companies as $company)
                            <option value="{{$company->id}}">{{$company->name}}</option>
                        @endforeach
                    </select>
                </div>
                <!-- Job Types -->
                <div class="sidebar-widget">
                    <h3>Funding Type</h3>
                    <div class="switches-list">
                        <div class="switch-container">
                            <label class="switch"><input type="checkbox" id="pfund" name="fundingtype[]" value="private"><span class="switch-button"></span> Private Funds</label>
                        </div>
                        <div class="switch-container">
                            <label class="switch"><input type="checkbox" id="cfund" name="fundingtype[]" value="crowd"><span class="switch-button"></span> Crowdfunded</label>
                        </div>
                        </div>
                        </div>

                        <div class="sidebar-widget"> 
                        <h3>Mission Type</h3>                           
                            <select class="selectpicker default" id="mission_privacy" data-selected-text-format="count" data-size="7" title="Mission Type" name="mission_privacy">
                                <option value="public">Public</option>
                                <option value="private">Private</option>
                            </select>
     
                   </div>
                <!-- Salary -->
      <!--           <div class="sidebar-widget">
                    <h3>Value</h3>
                    <div class="margin-top-55"></div>

                   
                    <input class="range-slider" type="text" name="range" data-slider-currency="$" data-slider-min="1500" data-slider-max="15000" data-slider-step="100" data-slider-value="[1500,15000]"/>
                </div> -->
<!-- 
                <div class="sidebar-widget">
                    <h3>Requirements</h3>

                    <div class="switches-list">
                        <div class="switch-container">
                            <label class="switch"><input id="licensed" type="checkbox" name="licencesed"><span class="switch-button"></span> Licensed</label>
                        </div>
                        </div>
                        </div> -->


            </div>
            <!-- yehan -->

            <!-- Sidebar Container / End -->

            <!-- Search Button -->
            <div class="sidebar-search-button-container">
                <button type="submit"  class="button ripple-effect">Search</button>
               <!--  <a onclick="searchNow()" class="button ripple-effect">Search</a> -->

            </div>
            <!-- Search Button / End-->

        </div>
        </form>
    </div>
    <!-- Full Page Sidebar / End -->

    <!-- Full Page Content -->
    <div class="full-page-content-container" >
        <div class="full-page-content-inner">

            <h3 class="page-title">Search Results</h3>

            <div class="notify-box margin-top-15">
                <div class="switch-container">
                    <!-- <label class="switch"><input type="checkbox"><span class="switch-button"></span>< --><!-- span class="switch-text">Turn on email alerts for this search</span></label> -->
                </div>

                <div class="sort-by">
                    <span>Sort by:</span>
                    <select class="selectpicker hide-tick">
                        <option>Relevance</option>
                        <option>Newest</option>
                        <option>Oldest</option>
                        <option>Random</option>
                    </select>
                </div>
            </div>
<div id="detail">
            <div class="listings-container grid-layout margin-top-35">

                    <!-- Job Listing -->
                    @foreach ($missions as $mission)
                    @php
                        $image = json_decode($mission->mission_files);
                        $isBookmarked =  (new \App\Bookmark())->isBookmarked($mission->id);
                    @endphp
                    <a href="{{url('/mission/'.str_slug($mission->title).'-'.$mission->id)}}" class="job-listing">

                        <!-- Job Listing Details -->
                        <div class="job-listing-details">
                            <!-- Logo -->
                            <div class="job-listing-company-logo">
                                 <?php if ($image == []) {
                                    ?>
                                    <?php
                                } 
                                else {
                                    ?>
                                <img src="{{$image[0]}}" alt="">
                            <?php } ?>
                            </div>

                            <!-- Details -->
                            <div class="job-listing-description">
                                <h4 class="job-listing-company">{{$mission->company->name}} <span class="verified-badge"
                                        title="Verified Employer" data-tippy-placement="top"></span></h4>
                                <h3 class="job-listing-title">{{$mission->title}}</h3>
                            </div>
                        </div>

                        <!-- Job Listing Footer -->
                        <div class="job-listing-footer">
                            <span id="{{$mission->id}}" class="bookmark-icon {{$isBookmarked?'bookmarked':''}}"></span>
                            <ul>
                                @if($mission->is_urgent==1)
                                <li><i class="icon-material-outline-location-on"></i> 
                                    Urgent Delivery
                                </li>
                                @endif
                                <li><i class="icon-material-outline-location-on"></i> {{$mission->address}}</li>
                                <li><i class="icon-material-outline-business-center"></i> {{$mission->mission_privacy}}</li>
                                <li><i class="icon-material-outline-account-balance-wallet"></i> ${{$mission->estimated_budget}}</li>
                                <li><i class="icon-material-outline-account-balance-wallet"></i> ${{$mission->estimated_budget}}</li>
                                @if($mission->enable_crowdfunding==1)
                                <li><i class="icon-material-outline-account-balance-wallet"></i>Participate in crowdfund</li>
                                @endif
                                <li><i class="icon-material-outline-access-time"></i> {{$mission->updated_at->diffForHumans()}}</li>
                            </ul>
                        </div>
                    </a>
                    @endforeach



                </div>

            <!-- Pagination -->
            {{-- <div class="clearfix"></div>
            <div class="pagination-container margin-top-20 margin-bottom-20">
                <nav class="pagination">
                    <ul>
                        <li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-left"></i></a></li>
                        <li><a href="#" class="ripple-effect">1</a></li>
                        <li><a href="#" class="ripple-effect current-page">2</a></li>
                        <li><a href="#" class="ripple-effect">3</a></li>
                        <li><a href="#" class="ripple-effect">4</a></li>
                        <li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
                    </ul>
                </nav>
            </div>
            <div class="clearfix"></div> --}}

            {{$missions->links('general.partials.pagination')}}
            <!-- Pagination / End -->
</div>


        </div>
    </div>
    <!-- Full Page Content / End -->

<!-- </div>


</div> -->

         </div>
        </div>
    </div>
    <!-- Dashboard Content / End -->



    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script type="text/javascript">
    $('.bookmark-icon').click(function(e){
        e.preventDefault();

        if ( $( this ).hasClass( "bookmarked" ) ) {
            return false;
        }
        let event = $(this);
        console.log(event);
        let mission_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('bookmark') }}",
               data:{mission_id:mission_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Saved successfully');
               }
            });
    })

    //delete bookmark

    $('.bookmarked').click(function(e){
        e.preventDefault();

        let event = $(this);
        console.log(event);
        let mission_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('unbookmark') }}",
               data:{mission_id:mission_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Removed successfully');
               }
            });
    })
</script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }
      $(document).ready(function() {
$('.mdb-select').materialSelect();

   

});

function searchNow(){
    country = category = 0;
    country   = Number(country)+Number($("#country").val());
    category  = Number(country)+Number($("#company").val());
    cfund     = $('#cfund').val();
    publicT   = $("#publicT").val();
    privateT  = $('#privateT').val();
    licensed  = $('#licensed').val();
    
if ($('#pfund').is(':checked')) {
pfund = 1;
}
else{
    pfund=0;
}
if ($('#cfund').is(':checked')) {
cfund = 1;
}
else{
    cfund=0;
}
if ($('#publicT').is(':checked')) {
publicT = 1;
}
else{
    publicT=0;
}
if ($('#privateT').is(':checked')) {
privateT = 1;
}
else{
    privateT=0;
}
if ($('#licensed').is(':checked')) {
licensed = 1;
}
else{
    licensed=0;
}

//alert(country+"-"+category+"-"+pfund+"-"+cfund+"-"+publicT+"-"+privateT+"-"+licensed);
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
var request    = $.ajax({
  url: "browseByCall",  
  method: "post",
  data: {_token: CSRF_TOKEN,mission_company_id:category,address:country,mission_privacy:},
  cache: false,
  dataType: "html"
});
request.done(function( msg ) {
    alert(msg);
});


}
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV7WeUUT24I6Zog1Kqm_H0ZJcu548R72A&libraries=places&callback=initAutocomplete"
         async defer></script>
@endsection