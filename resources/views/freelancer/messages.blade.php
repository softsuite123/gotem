@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">
@php
    $authId = auth()->user()->id;
@endphp

    @include('employer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('employer.partials.sidebar')


   <!-- Dashboard Content
    ================================================== -->
    <div class="dashboard-content-container" data-simplebar>
        <div class="dashboard-content-inner" >

            <!-- Dashboard Headline -->
            <div class="dashboard-headline">
                <h3>Messages</h3>

                <!-- Breadcrumbs -->
                <nav id="breadcrumbs" class="dark">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Dashboard</a></li>
                        <li>Messages</li>
                    </ul>
                </nav>
            </div>

                <div class="messages-container margin-top-0">

                    <div class="messages-container-inner">

                        <!-- Messages -->
                        <div class="messages-inbox threads">
                           {{--  <div class="messages-headline">
                                <div class="input-with-icon">
                                        <input id="autocomplete-input" type="text" placeholder="Search">
                                    <i class="icon-material-outline-search"></i>
                                </div>
                            </div> --}}

                            <ul>
                                @foreach ($threads as $key => $thread)
                                @if ($thread->lastMessage)
                                    <li class="@if ($thread->withUser->id === $withUser->id)
                                            active-message
                                        @endif">
                                        <a href="/messenger/t/{{$thread->withUser->id}}" >
                                            <div class="message-avatar"><i class="status-icon status-online"></i><img src="{{url('')}}/{{$thread->withUser->Profile->avatar}}" alt="" /></div>

                                            <div class="message-by">
                                                <div class="message-by-headline">
                                                    <h5>{{$thread->withUser->name}}</h5>
                                                    <span>4 hours ago</span>
                                                </div>
                                                <p>{{substr($thread->lastMessage->message, 0, 20)}}</p>
                                            </div>
                                        </a>
                                    </li>
                                @endif
                                @endforeach

                                {{-- <li class="active-message">
                                    <a href="#">
                                        <div class="message-avatar"><i class="status-icon status-offline"></i><img src="{{asset('backstyling/images/user-avatar-small-02.jpg')}}" alt="" /></div>

                                        <div class="message-by">
                                            <div class="message-by-headline">
                                                <h5>Sindy Forest</h5>
                                                <span>Yesterday</span>
                                            </div>
                                            <p>Hi Tom! Hate to break it to you but I'm actually on vacation</p>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <div class="message-avatar"><i class="status-icon status-offline"></i><img src="{{asset('backstyling/images/user-avatar-placeholder.png')}}" alt="" /></div>

                                        <div class="message-by">
                                            <div class="message-by-headline">
                                                <h5>Sebastiano Piccio</h5>
                                                <span>2 days ago</span>
                                            </div>
                                            <p>Hello, I want to talk about my project if you don't mind!</p>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <div class="message-avatar"><i class="status-icon status-online"></i><img src="{{asset('backstyling/images/user-avatar-placeholder.png')}}" alt="" /></div>

                                        <div class="message-by">
                                            <div class="message-by-headline">
                                                <h5>Marcin Kowalski</h5>
                                                <span>2 days ago</span>
                                            </div>
                                            <p>Yes, I received payment. Thanks for cooperation!</p>
                                        </div>
                                    </a>
                                </li> --}}

                            </ul>
                        </div>
                        <!-- Messages / End -->

                        <!-- Message Content -->
                        <div class="message-content">

                            <div class="messages-headline">
                                @if(isset($withUser))
                                <h4>{{$withUser->name}}</h4>
                                @endif
                                {{-- <a href="#" class="message-action"><i class="icon-feather-trash-2"></i> Delete Conversation</a> --}}
                            </div>

                            <!-- Message Content Inner -->
                            <div class="message-content-inner messenger-body">

                                    <!-- Time Sign -->
                                    <div class="message-time-sign">
                                        {{-- <span>28 June, 2019</span> --}}
                                    </div>
                                    @foreach ((object)$messages as $key => $message)
                                    @php
                                        $user = \App\User::find($message->sender_id);
                                        if ($user->is_verified) {
                                                $pic = str_replace('public', 'storage', $user->profile->avatar);
                                            } else {
                                                $pic = '/images/user_images/user-avatar.jpg';
                                        }
                                    @endphp
                                    @if ($message->sender_id == $authId)
                                        <div class="message-bubble me">
                                            <div class="message-bubble-inner">
                                                <div class="message-avatar"><img src="{{url('/')}}/{{$user->profile->avatar}}" alt="" /></div>
                                                <div class="message-text"><p>{{$message->message}}</p></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>


                                    @else
                                        <div class="message-bubble">
                                        <div class="message-bubble-inner">
                                            <div class="message-avatar"><img src="{{url('/')}}/{{$user->profile->avatar}}" alt="" /></div>
                                            <div class="message-text"><p>{{$message->message}}</p></div>
                                        </div>
                                        <div class="clearfix"></div>
                                        </div>
                                    @endif
                                    @endforeach
                            </div>
                            <!-- Message Content Inner / End -->

                            <!-- Reply Area -->
                            @if(isset($withUser))
                            <div class="message-reply">
                            <input type="hidden" name="receiverId" value="{{$withUser->id}}">
                                <textarea id="message-body" name="message" cols="1" rows="1" placeholder="Your Message" data-autoresize></textarea>
                                <button class="button ripple-effect" type="submit" id="send-btn">Send</button>
                            </div>
                            @endif

                        </div>
                        <!-- Message Content -->

                    </div>
            </div>
            <!-- Messages Container / End -->




            <!-- Footer -->

            <div class="dashboard-footer-spacer"></div>


        </div>
    </div>
    <!-- Dashboard Content / End -->



    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit"  form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>

{{-- messenger --}}
@if(!isset($convo))
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
        var withId        = {{$withUser->id}},
            authId        = {{auth()->id()}},
            messagesCount = {{count($messages)}};
            pusher        = new Pusher('{{config('messenger.pusher.app_key')}}', {
              cluster: '{{config('messenger.pusher.options.cluster')}}'
            });
    </script>

    <script src="{{url('/public')}}/vendor/messenger/js/messenger-chat.js" charset="utf-8"></script>
    @endif
{{-- ends --}}
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });

    
$(document).on('keypress',function(e) {
    if(e.which == 13) {
      document.getElementById("send-btn").click();
    }
});
     
</script>


@endsection