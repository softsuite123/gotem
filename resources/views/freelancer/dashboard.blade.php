@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('freelancer.partials.header')



    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')



        <div class="full-page-content-container" data-simplebar>
            @if(Session::has('success'))
            <li class="alert alert-success">{{Session('success')}}</li>
        @endif
            <?php Session::forget('success');?>

             @if(Session::has('error'))
            <li class="alert alert-danger">{{Session('error')}}</li>
            @endif
            <?php Session::forget('error');?>
            <div class="full-page-content-inner">

                <h3 class="page-title">Trending Missions</h3>

                <div class="notify-box margin-top-15">
                    <!-- <div class="switch-container"> -->
                    <!-- <label class="switch"><input type="checkbox"><span class="switch-button"></span>< -->
                    <!-- span class="switch-text">Turn on email alerts for this search</span></label> -->
                    <!-- </div> -->

                    <div class="sort-by">
                        <span>Search by:</span>
                        <select class="selectpicker hide-tick">
                            <option>Relevance</option>
                            <option>Newest</option>
                            <option>Oldest</option>
                            <option>Random</option>
                        </select>
                    </div>
                </div>

                <div class="listings-container grid-layout margin-top-35">

                    <!-- Job Listing -->
                    @foreach ($missions as $mission)
                       @if($mission->mission_privacy=='Public')
                    @php
                        $image = json_decode($mission->mission_files);
                        
                        $isBookmarked =  (new \App\Bookmark())->isBookmarked($mission->id);
                    @endphp
                    <a href="{{url('/mission/'.str_slug($mission->title).'-'.$mission->id)}}" class="job-listing">

                        <!-- Job Listing Details -->
                        <div class="job-listing-details">
                            <!-- Logo -->
                            <div class="job-listing-company-logo">
                                <?php if ($image == []) {
                                    
                                } 
                                else {
                                    ?>
                                <img src="{{url('/'.$image[0])}}" alt="">
                                <?php } ?>
                            </div>

                            <!-- Details -->
                            <div class="job-listing-description">
                                <h4 class="job-listing-company">{{$mission->company->name}} <span class="verified-badge"
                                        title="Verified Employer" data-tippy-placement="top"></span></h4>
                                <h3 class="job-listing-title">{{$mission->title}}</h3>
                            </div>
                        </div>

                        <!-- Job Listing Footer -->
                        <div class="job-listing-footer">
                            <span id="{{$mission->id}}" class="bookmark-icon {{$isBookmarked?'bookmarked':''}}"></span>
                            <ul>
                                <li><i class="icon-material-outline-location-on"></i> {{$mission->address}}</li>
                                <li><i class="icon-material-outline-business-center"></i> {{$mission->mission_privacy}}</li>
                                <li><i class="icon-material-outline-account-balance-wallet"></i> ${{$mission->estimated_budget}}</li>
                                <li><i class="icon-material-outline-account-balance-wallet"></i> ${{$mission->estimated_budget}}</li>
                                @if($mission->enable_crowdfunding==1)
                                <li><i class="icon-material-outline-account-balance-wallet"></i>Participate in crowdfund</li>
                                @endif
                                <li><i class="icon-material-outline-access-time"></i> {{$mission->updated_at->diffForHumans()}}</li>
                            </ul>
                        </div>
                    </a>
                    @endif
                    @endforeach



                </div>

                <!-- Pagination -->
                <!--<div class="clearfix"></div>-->
                <!--<div class="pagination-container margin-top-20 margin-bottom-20">-->
                <!--    <nav class="pagination">-->
                <!--        <ul>-->
                <!--            <li class="pagination-arrow"><a href="#" class="ripple-effect"><i-->
                <!--                        class="icon-material-outline-keyboard-arrow-left"></i></a></li>-->
                <!--            <li><a href="#" class="ripple-effect">1</a></li>-->
                <!--            <li><a href="#" class="ripple-effect current-page">2</a></li>-->
                <!--            <li><a href="#" class="ripple-effect">3</a></li>-->
                <!--            <li><a href="#" class="ripple-effect">4</a></li>-->
                <!--            <li class="pagination-arrow"><a href="#" class="ripple-effect"><i-->
                <!--                        class="icon-material-outline-keyboard-arrow-right"></i></a></li>-->
                <!--        </ul>-->
                <!--    </nav>-->
                <!--</div>-->
                <!--<div class="clearfix"></div>-->

                {{$missions->links('general.partials.pagination')}}
                <!-- Pagination / End -->

                <!-- Footer -->
                <!-- <div class="small-footer margin-top-15">
				<div class="small-footer-copyrights">
					Â© 2019 <strong>Gotem</strong>. All Rights Reserved.
				</div>
				<ul class="footer-social-links">
					<li>
						<a href="#" title="Facebook" data-tippy-placement="top">
							<i class="icon-brand-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Twitter" data-tippy-placement="top">
							<i class="icon-brand-twitter"></i>
						</a>
					</li>
					<li>
						<a href="#" title="Google Plus" data-tippy-placement="top">
							<i class="icon-brand-google-plus-g"></i>
						</a>
					</li>
					<li>
						<a href="#" title="LinkedIn" data-tippy-placement="top">
							<i class="icon-brand-linkedin-in"></i>
						</a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div> -->
                <!-- Footer / End -->

            </div>
        </div>
        <!-- Full Page Content / End -->

        <style>
            @media (max-width: 768px) {
                .maap {
                    max-width: 100% !important;
                }
            }
        </style>
        <!-- Full Page Map -->
        <div class="full-page-map-container maap" style="max-width: 32%;">

            <!-- Enable Filters Button -->
            <div class="filter-button-container">
                <button class="enable-filters-button">
                    <i class="enable-filters-button-icon"></i>
                    <span class="show-text">Show Filters</span>
                    <span class="hide-text">Hide Filters</span>
                </button>
                <!-- <div class="filter-button-tooltip">Click to expand sidebar with filters!</div> -->
            </div>

            <!-- Map -->
            <iframe class=""
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d100939.9855503722!2d-122.50764053791957!3d37.75781499705857!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80859a6d00690021%3A0x4a501367f076adff!2sSan%20Francisco%2C%20CA%2C%20USA!5e0!3m2!1sen!2s!4v1576762084787!5m2!1sen!2s"
                width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
        <!-- Full Page Map / End -->




    </div>


</div>

</div>
<!-- Dashboard Container / End -->

</div>
<!-- Wrapper / End -->


<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

    <!--Tabs -->
    <div class="sign-in-form">

        <ul class="popup-tabs-nav">
            <li><a href="#tab">Add Note</a></li>
        </ul>

        <div class="popup-tabs-container">

            <!-- Tab -->
            <div class="popup-tab-content" id="tab">

                <!-- Welcome Text -->
                <div class="welcome-text">
                    <h3>Do Not Forget ðŸ˜Ž</h3>
                </div>

                <!-- Form -->
                <form method="post" id="add-note">

                    <select class="selectpicker with-border default margin-bottom-20" data-size="7" title="Priority">
                        <option>Low Priority</option>
                        <option>Medium Priority</option>
                        <option>High Priority</option>
                    </select>

                    <textarea name="textarea" cols="10" placeholder="Note" class="with-border"></textarea>

                </form>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Add
                    Note <i class="icon-material-outline-arrow-right-alt"></i></button>

            </div>

        </div>
    </div>
</div>
<!-- Apply for a job popup / End -->
@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script type="text/javascript">
    $('.bookmark-icon').click(function(e){
        e.preventDefault();

        if ( $( this ).hasClass( "bookmarked" ) ) {
            return false;
        }
        let event = $(this);
        console.log(event);
        let mission_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('bookmark') }}",
               data:{mission_id:mission_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Saved successfully');
               }
            });
    })

    //delete bookmark

    $('.bookmarked').click(function(e){
        e.preventDefault();

        let event = $(this);
        console.log(event);
        let mission_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('unbookmark') }}",
               data:{mission_id:mission_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Removed successfully');
               }
            });
    })
</script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaoOT9ioUE4SA8h-anaFyU4K63a7H-7bc&amp;libraries=places"></script -->
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->

<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>

<!-- Chart.js // documentation: http://www.chartjs.org/docs/latest/ -->
<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });



</script>


@endsection