@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('freelancer.partials.header')

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')

        <!-- Dashboard Content
	================================================== -->
        <div class="dashboard-content-container" data-simplebar>
            @if(Session::has('success'))
            <li class="alert alert-success">{{Session('success')}}</li>
        @endif
            <?php Session::forget('success');?>

             @if(Session::has('error'))
            <li class="alert alert-danger">{{Session('error')}}</li>
            @endif
            <?php Session::forget('error');?>
            <div class="dashboard-content-inner">

@if ($errors->any())
     @foreach ($errors->all() as $error)
         <div>{{$error}}</div>
     @endforeach
 @endif
                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-account-circle"></i> Personal Info</h3>
                            </div>

                            <div class="content">
                                <form action="{{url('/edit_profile')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-4 col-md-4">
                                                
                                                       <div class="col-auto">
                                    <div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
                                        <img class="profile-pic" @if($profile->avatar!="")  src="{{url($profile->avatar)}}" @endif alt="" />
                                        <div class="upload-button"></div>
                                        <input class="file-upload" onclick="remove_pic()" value="{{$profile->avatar}}" name="avatar" type="file" accept="image/*"/>
                                    </div>
                                </div>
                                                
                                            </div>

                                            <div class="col-xl-4 col-md-4">
                                               <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Name</h5>
                                                </div>
                                                <input class="with-border" name="name" placeholder="David Joe"
                                                    value="{{@$profile->name}}">
                                            </div>

                                            <div class="col-xl-4 col-md-4">

                                               <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Country</h5>
                                                </div>

                                               
                                                <select class="selectpicker" name="country" id="country" required="">
                                                    <option selected disabled value="">Select One</option>
                                                    @foreach ($countries as $country)
                                                    <option @if($profile->country_id==$country->id) selected @endif  value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-xl-4 col-md-4">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                        <input type="hidden" id="state_id" value="{{@$profile->state_id}}">
                                                    <input type="hidden" id="city_id" value="{{$profile->city_id}}">
                                                    <h5>State</h5>
                                                </div>
                                                    <select name="state" id="state" class="state_select form-control"
                                                    autocomplete="address-level1" required="" data-placeholder="Select an option…">
                                                    <option selected disabled value="">Select an option…</option>
                                                </select>  
                                            </div>

                                            <div class="col-xl-4 col-md-4">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>City</h5>
                                                </div>
            <select name="city" required="" id="city" class="city_select form-control"
            autocomplete="address-level1" data-placeholder="Select an option…">
            <option selected disabled value="">Select an option…</option>
            <option></option>
            </select>

                                            </div>
                                            <div class="col-xl-4 col-md-4">
                                               <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Hourly Rate</h5>
                                                </div>
                                                <input type="number" class="with-border" name="hourly_rate"
                                                    placeholder="0" value="{{@$profile->hourly_rate}}">
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-xl-4 col-md-4">
                                               <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Upload Resume</h5>
                                                </div>
                                                <input type="hidden" name="old_resume" value="{{$profile->resume}}">
                                                <input type="file" onclick="remove_resume()" name="resume" class="with-border" value="{{$profile->resume}}">
                                            </div>
                                            <div class="col-xl-4 col-md-4">
                                                  <input type="hidden" name="old_pic" value="{{$profile->avatar}}">
                                             <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Upload Cover Letter</h5>
                                                           <input type="hidden" name="old_cover_letter" value="{{$profile->cover_letter}}">
                                                </div>
                                                <input type="file" value="{{$profile->cover_letter}}" onclick="remove_letter()" name="cover_letter" class="with-border">
                                            </div>
                                            <div class="col-xl-4 col-md-4">
                                                 <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Skills</h5>
                                                </div>

                                                <div class="keywords-container">
                                                    <div class="keyword-input-container">
                                                        <input type="text" class="keyword-input"
                                                            placeholder="Add Keywords" name="skills" data-role="tagsinput" />
                                                        <button type="button"
                                                            class="keyword-input-button ripple-effect"><i
                                                                class="icon-material-outline-add"></i></button>
                                                    </div>
                                                    <input type="text" value="{{@$profile->skills}}" data-role="tagsinput" name="skills">
                                                    <div class="keywords-list">
                                                        <!-- keywords go here -->
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>


                                        <div class="row">
                                            <div class="col-xl-12 col-md-12">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>About Me</h5>
                                                </div>
                                                <textarea name="about" cols="10" placeholder="Message"
                                                    class="with-border">{{@$profile->about}}</textarea>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-xl-4 col-md-4">
                                                <input type="submit" class="button ripple-effect" value="Update">
                                            </div>
                                        </div>

                                        <br>

                                    </div>
                                     <input type="hidden" name="rem_resume" value="1" id="rem_resume">
                                        <input type="hidden" name="rem_pic" value="1" id="rem_pic">
                                         <input type="hidden" name="rem_letter" value="1" id="rem_letter">
                                </form>

                            </div>
                        </div>
                    </div>

                </div>
                <!-- Row / End -->
                <br>
                <!-- Row -->
                @if(!auth()->user()->is_verified)
                <div class="row">
                    <div class="col-xl-12">
                    <form action="{{route('verify.identity')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <!-- Dashboard Box -->
                            <div class="dashboard-box margin-top-0">
                                <!-- Headline -->
                                <div class="headline">
                                    <h3><i class="icon-material-outline-account-circle"></i> Verify Identity</h3>
                                </div>
                                <div class="content">

                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Upload Driving Licence</h5>
                                                </div>
                                                <input type="file" name="driving_license" class="with-border">
                                            </div>

                                            <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Upload Passport</h5>
                                                </div>
                                                <input type="file" name="passport" class="with-border">
                                            </div>

                                        </div>

                                        {{-- <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Upload Profile Photo</h5>
                                                </div>
                                                <input type="file" class="with-border">
                                            </div>

                                            <div class="col-xl-6 col-md-6"></div>

                                        </div> --}}


                                        <br>
                                        <div class="row">
                                            <div class="col-xl-4 col-md-4">
                                                <input type="submit" class="button ripple-effect" value="Update">
                                            </div>
                                        </div>

                                        <br>

                                    </div>

                                </div>
                            </div>
                            </form>
                        </div>
                    
                </div>
                @endif
                <!-- Row / End -->

                <br>

                <!-- Row -->
                <div class="row">

                    <!-- Dashboard Box -->
                    <div class="col-xl-12">
                        <div class="dashboard-box margin-top-0">

                            <!-- Headline -->
                            <div class="headline">
                                <h3><i class="icon-material-outline-account-circle"></i> Employment History</h3>
                            </div>

                            <div class="content">
                                <div class="container">
                                    <form action="{{route('employment.history')}}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Job Title</h5>
                                                </div>
                                                <input class="with-border" placeholder="Job Title" name="title">
                                            </div>

                                            <div class="col-xl-6 col-md-6">
                                                <div class="section-headline margin-top-25 margin-bottom-12">
                                                    <h5>Company</h5>
                                                </div>
                                                <input class="with-border" placeholder="Company" name="company">
                                            </div>

                                        </div>
                                    
                                    <div class="row">
                                        <div class="col-xl-6 col-md-6">
                                            <div class="section-headline margin-top-25 margin-bottom-12">
                                                <h5>Duration From</h5>
                                            </div>
                                            <input type="date" class="with-border" name="from">
                                        </div>

                                        <div class="col-xl-6 col-md-6">
                                            <div class="section-headline margin-top-25 margin-bottom-12">
                                                <h5>Duration To</h5>
                                            </div>
                                            <input type="date" class="with-border" name="to">
                                        </div>

                                    </div>

                                    <br>
                                    <div class="row">
                                        <div class="col-xl-12 col-md-12">
                                            <div class="checkbox">
                                                <input type="checkbox" id="chekcbox2" name="currently_working_here">
                                                <label for="chekcbox2"><span class="checkbox-icon"></span> Currently
                                                    Working here</label>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-xl-12 col-md-12">
                                            <div class="section-headline margin-top-25 margin-bottom-12">
                                                <h5>Description</h5>
                                            </div>
                                            <textarea cols="10" placeholder="Message"
                                                class="with-border" name="description"></textarea>
                                        </div>
                                    </div>


                                    <br>
                                    <div class="row">
                                        <div class="col-xl-4 col-md-4">
                                            <input type="submit" class="button ripple-effect" value="Save">
                                        </div>
                                    </div>
                                </form>
                                    <br>

                                </div>

                            </div>
                        </div>
                    </div>



                </div>
                <br>
                <!-- Row / End -->
    <div class="row">
    <!-- Dashboard Box -->
    <div class="col-xl-12">
        <div class="dashboard-box margin-top-0">
            <!-- Headline -->
            <div class="headline">
                <h3><i class="icon-material-outline-business-center"></i>Employment Histories</h3>
            </div>
            <div class="content">
                <br>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-6">
                            <table id="example" class="table table-hover responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Job Title</th>
                                        <th>Company</th>
                                        <th>Duration From </th>
                                        <th>Duration To</th>
                                        <th>Description</th>
                                        <th>Operations</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @foreach($employmenthistory as $emp)
                                        <tr>
                                            <td>{{$emp->job_title}}</td>
                                            <td>{{$emp->company}}</td>
                                            <td>{{$emp->from}}</td>
                                            <td>{{$emp->to}}</td>
                                            <td>{{$emp->description}}</td>
                                            <td>
                                               
                                                <a href="{{url('/del_employment_history')}}/{{$emp->id}}" class="btn btn-danger btn-xs" onclick="return confirm('Are you sure?')">Delete</a>
                                            </td>

                                        </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>

</div>
                <!-- Footer -->
                <div class="dashboard-footer-spacer"></div>

            </div>
        </div>
        <!-- Dashboard Content / End -->
    </div>

</div>
<!-- Wrapper / End -->



@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

{{-- <script src="{{asset('backstyling/js/infobox.min.js')}}"></script> --}}
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/maps.js')}}"></script> --}}


  


<script>
    $('#country').change(function () {
        var cid = $(this).val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option value="">Select State</option>');
                        $.each(res, function (key, value) {
                            $("#state").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
    $('#state').change(function () {
        var sid = $(this).val();
        if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option value="">Select City</option>');
                        $.each(res, function (key, value) {
                            $("#city").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
       $('document').ready(function () {
         var cid = $('#country').val();
         var sid = $('#state_id').val();
          var cityid = $('#city_id').val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option value="">Select State</option>');
                        var ch = "";
                        $.each(res, function (id, value) {
                            
                            if(sid==id){
                                ch = "selected";
                            }
                            else{
                                var ch = "";
                            }
                            $("#state").append('<option '+ch+' value="' + id + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }

            
             if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {                
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option value="">Select City</option>');
                         $("#city").append('<option value="141853">Other</option>');
                        var c = "";
                        $.each(res, function (key, value) {
                            if(cityid==key) {
                                c = "selected";
                            }else{
                                var c = "";
                                 
                            }
                           
                            $("#city").append('<option '+c+' value="' + key + '">' + value +
                                '</option>');

                        });
                    }
                }

            });
        }



        
    });
          function remove_resume()
    {
        $('#rem_resume').val("0");
    }
           function remove_pic()
    {
        $('#rem_pic').val("0");
    }
           function remove_letter()
    {
        $('#rem_letter').val("0");
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script>
            $(document).ready(function() {
        $("#example").DataTable({
        aaSorting: [],
        responsive: true,
        columnDefs: [
        {
        responsivePriority: 1,
        targets: 0
        },
        {
        responsivePriority: 2,
        targets: -1
        }
        ]
        });
        $(".dataTables_filter input")
        .attr("placeholder", "Search Forms...")
        .css({
        width: "260px",
        display: "inline-block"
        });
        $('[data-toggle="tooltip"]').tooltip();
        });
        </script>

@endsection