@extends('freelancer.layouts.master')

@section('content')
<!-- Wrapper -->
<div id="wrapper">


    @include('freelancer.partials.header')

    <!-- Dashboard Container -->
    <div class="dashboard-container">

        @include('freelancer.partials.sidebar')

        <!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar >
		<!-- Titlebar
================================================== -->
<div class="single-page-header freelancer-header" style="box-shadow: 0px 2px 8px 0px gray;" data-background-image="../assets/images/single-freelancer.jpg">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
						<div class="header-image freelancer-avatar"><img src="{{url('/')}}/{{$user->profile->avatar}}" alt=""></div>
						
						<div class="header-details">
							<h3>{{$user->profile->name}} <span>{{$user->profile->skills}}</span></h3>
							<ul>
								<li><div class="star-rating" data-rating="5.0"></div></li>
								<li><img class="flag" src="../assets/images/flags/de.svg" alt=""> @if($user->profile->country){{$user->profile->country->name}}@endif</li>
								@if ($user->is_verified)
									<li><div class="verified-badge-with-title">Verified</div></li>
								@endif

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">

		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">

			<!-- Page Content -->
			<div class="single-page-section">
				<h3 class="margin-bottom-25">About Me</h3>
                  <p>{{$user->profile->about}}</p>
			</div>
				<div class="single-page-section">
				<h4 class="margin-bottom-25">Address</h4>
                  <p>@if($user->profile->country){{$user->profile->country->name}}@endif - @if($user->profile->state_id){{$user->profile->State->name}}@endif, @if($user->profile->city_id){{$user->profile->City->name}}@endif</p>
			</div>
			

			<!-- Boxed List -->
			<div class="boxed-list margin-bottom-60">
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-thumb-up"></i> Work History and Feedback</h3>
				</div>
		<?php $ratings = DB::table('ratings')->where('source_id',$user->id)->get();  ?>
		@if($ratings == null)
				<ul class="boxed-list-ul">
					@foreach($ratings as $rating)
					<li>
						<div class="boxed-list-item">
							<!-- Content -->
							<div class="item-content">
		<?php $mission_name = DB::table('missions')->where('id',$rating->mission_id)->first();  ?>
								@if($mission_name)
								<h4>{{$mission_name->title}}</h4>
								@else
								<h4>No title</h4>
								@endif
								<div class="item-details margin-top-10">
									<div class="star-rating" data-rating="{{$rating->percentage}}"></div>
									<div class="detail-item"><i class="icon-material-outline-date-range"></i> August 2019</div>
								</div>
								<div class="item-description">
									<p>{{$rating->description}} </p>
								</div>
							</div>
						</div>
					</li>
					@endforeach
					@else
					<br>
					<center><h6 style="color:grey;">No Work History and Feedback!</h6></center>
					@endif
				</ul>

				<!-- Pagination -->
				<div class="clearfix"></div>
			<!-- 	<div class="pagination-container margin-top-40 margin-bottom-10">
					<nav class="pagination">
						<ul>
							<li><a href="#" class="ripple-effect current-page">1</a></li>
							<li><a href="#" class="ripple-effect">2</a></li>
							<li class="pagination-arrow"><a href="#" class="ripple-effect"><i class="icon-material-outline-keyboard-arrow-right"></i></a></li>
						</ul>
					</nav>
				</div> -->
				<div class="clearfix"></div>
				<!-- Pagination / End -->

			</div>
			<!-- Boxed List / End -->

			<!-- Boxed List -->
			<div class="boxed-list margin-bottom-60">
				<div class="boxed-list-headline">
					<h3><i class="icon-material-outline-business"></i> Employment History</h3>
				</div>
				<ul class="boxed-list-ul">
				    
				   @if($user->jobs)
                    @foreach ($user->jobs as $job)

                   
                        <li>
                            <div class="boxed-list-item">
                                <!-- Avatar -->
                                <div class="item-image">
                                    <img  src="https://cdn.imgbin.com/14/20/13/imgbin-globe-logo-social-work-sphere-globe-g4xJT6cXCgZFa9skDKqkDcQmv.jpg" alt="">
                                </div>

                                <!-- Content -->
                                <div class="item-content">
                                    <h4>{{$job->title}}</h4>
                                    <div class="item-details margin-top-7">
                                        <div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> {{$job->company}}</a></div>
                                        <div class="detail-item"><i class="icon-material-outline-date-range"></i> {{$job->from}} - @if($job->currently_working_here == 0)  {{$job->from}} @else Present @endif</div>
                                    </div>
                                    <div class="item-description">
                                        <p>{{$job->description}}.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                      
                    @endforeach
                    
                    @else
                   <br>
					<center><h6 style="color:grey;">No Employment Record Found!</h6></center>
                    @endif
					{{-- <li>
						<div class="boxed-list-item">
							<!-- Avatar -->
							<div class="item-image">
								<img src="../assets/images/browse-companies-04.png" alt="">
							</div>

							<!-- Content -->
							<div class="item-content">
								<h4><a href="#">Lead UX/UI Designer</a></h4>
								<div class="item-details margin-top-7">
									<div class="detail-item"><a href="#"><i class="icon-material-outline-business"></i> Acorta</a></div>
									<div class="detail-item"><i class="icon-material-outline-date-range"></i> April 2014 - May 2019</div>
								</div>
								<div class="item-description">
									<p>I designed and implemented 10+ custom web-based CRMs, workflow systems, payment solutions and mobile apps.</p>
								</div>
							</div>
						</div>
					</li> --}}
				</ul>
			</div>
			<!-- Boxed List / End -->

		</div>


		<!-- Sidebar -->
		<div class="col-xl-4 col-lg-4">
			<div class="sidebar-container">

				<!-- Profile Overview -->
				<div class="profile-overview">
					<div class="overview-item"><strong>${{$user->profile->hourly_rate}}</strong><span>Hourly Rate</span></div>
					<div class="overview-item"><strong>53</strong><span>Jobs Done</span></div>
					<div class="overview-item"><strong>22</strong><span>Rehired</span></div>
				</div>

				<!-- Button -->
				<a href="#small-dialog" class="apply-now-button popup-with-zoom-anim margin-bottom-50 message_btn">Direct Message <i class="icon-material-outline-arrow-right-alt"></i></a>

				<!-- Freelancer Indicators -->
				<div class="sidebar-widget">
					<div class="freelancer-indicators">

						<!-- Indicator -->
						<div class="indicator" style="width:100%;">
							<strong>0%</strong>
							<div class="indicator-bar" data-indicator-percentage="0"><span></span></div>
							<span>Job Success</span>
						</div>


                    	<div class="indicator" style="width:100%;">
    						<strong>0%</strong>
        						<div class="indicator-bar" data-indicator-percentage="0"><span></span></div>
    						<span>On Budget</span>
						</div>
						<!-- Indicator -->
						<!--<div class="indicator">-->
						<!--	<strong>100%</strong>-->
						<!--	<div class="indicator-bar" data-indicator-percentage="100"><span></span></div>-->
						<!--	<span>Recommendation</span>-->
						<!--</div>-->

						<!-- Indicator -->
						<!--<div class="indicator">-->
						<!--	<strong>90%</strong>-->
						<!--	<div class="indicator-bar" data-indicator-percentage="90"><span></span></div>-->
						<!--	<span>On Time</span>-->
						<!--</div>-->

						<!-- Indicator -->
					
					</div>
				</div>

				<!-- Widget -->
				<!--<div class="sidebar-widget">-->
				<!--	<h3>Social Profiles</h3>-->
				<!--	<div class="freelancer-socials margin-top-25">-->
				<!--		<ul>-->
				<!--			<li><a href="#" title="Dribbble" data-tippy-placement="top"><i class="icon-brand-dribbble"></i></a></li>-->
				<!--			<li><a href="#" title="Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>-->
				<!--			<li><a href="#" title="Behance" data-tippy-placement="top"><i class="icon-brand-behance"></i></a></li>-->
				<!--			<li><a href="#" title="GitHub" data-tippy-placement="top"><i class="icon-brand-github"></i></a></li>-->

				<!--		</ul>-->
				<!--	</div>-->
				<!--</div>-->

				<!-- Widget -->
				<div class="sidebar-widget">
					<h3>Skills</h3>
					<div class="task-tags">
					
					</div>
				</div>

				<!-- Widget -->
				@if($user->Profile->cover_letter)
				<div class="sidebar-widget">
					<h3>Attachments</h3>
					<div class="attachments-container">
					<a href="https://platform.gotem.io/{{$user->Profile->cover_letter}}?" class="attachment-box ripple-effect" download><span>Cover Letter</span><i>PDF</i></a>
					@if($user->Profile->resume)
					<a href="https://platform.gotem.io/{{$user->Profile->resume}}?" class="attachment-box ripple-effect" download><span>Resume</span><i>PDF</i></a>
					@endif
						<!--<a href="#" class="attachment-box ripple-effect" download><span>Contract</span><i>DOCX</i></a>-->
					</div>
				</div>
                @endif
				<!-- Sidebar Widget -->
					<?php 
						 $isBookmarked =  (new \App\SourceBookmark())->isBookmarked($user->id);
					 ?>
				<div class="sidebar-widget">
					<h3>Bookmark or Share</h3>

					<!-- Bookmark Button -->
					<!-- <button class="bookmark-button margin-bottom-25">
						 
						<span class="bookmark-text">Bookmark</span>
						<span class="bookmarked-text">Bookmarked</span>
					</button> -->
					<button style="border-style: double;" id="{{$user->id}}" class="bookmark-icon {{$isBookmarked?'bookmarked':''}}">
						Bookmark
					</button>
					<span ></span>


					<!-- Copy URL -->
					<div class="copy-url">
						<input id="copy-url" type="text" value="" class="with-border">
						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
					</div>

					<!-- Share Buttons -->
					<div class="share-buttons margin-top-25">
						<div class="share-buttons-trigger"><i class="icon-feather-share-2"></i></div>
						<div class="share-buttons-content">
							<span>Interesting? <strong>Share It!</strong></span>
							<ul class="share-buttons-icons">
								<li><a href="#" data-button-color="#3b5998" title="Share on Facebook" data-tippy-placement="top"><i class="icon-brand-facebook-f"></i></a></li>
								<li><a href="#" data-button-color="#1da1f2" title="Share on Twitter" data-tippy-placement="top"><i class="icon-brand-twitter"></i></a></li>
								<li><a href="#" data-button-color="#dd4b39" title="Share on Google Plus" data-tippy-placement="top"><i class="icon-brand-google-plus-g"></i></a></li>
								<li><a href="#" data-button-color="#0077b5" title="Share on LinkedIn" data-tippy-placement="top"><i class="icon-brand-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			
			</div>
		</div>

	</div>
</div>



<!-- Apply for a job popup
================================================== -->
<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

	<!--Tabs -->
	<div class="sign-in-form">

		<ul class="popup-tabs-nav">
			<li><a href="#tab">Message</a></li>
		</ul>

		<div class="popup-tabs-container">
			<!-- Tab -->
			<div class="popup-tab-content" id="tab">
			{{-- <form> --}}

				<!-- Welcome Text -->
				<div class="welcome-text">
					<h3>Message Me 😎</h3>
				</div>
				<!-- Form -->
				<form method="post" id="add-note" action="{{route('message.store')}}">
					@csrf
					<input type="hidden" name="withId" value="{{\Request::segment(2)}}">
					<textarea name="message" cols="10" placeholder="Note" class="with-border"></textarea>

				{{-- </form> --}}

				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-note">Submit <i class="icon-material-outline-arrow-right-alt"></i></button>
</form>
			</div>

		</div>
	</div>
</div>
<!-- Apply for a job popup / End -->




    <!-- modal -->
  <!-- <div class="modal" id="message_modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Message Me</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('message.store')}}">
            @csrf
            <input type="hidden" id="current_advisor_id" name="withId">
            <div class="form-group">
            <label for="comment">Message Now:</label>
              <textarea class="form-control" rows="5" name="message"></textarea>
            </div>
            <input type="submit" class="btn btn-primary" value="Send Message">
        </form>
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
  </div>
  {{-- end --}} -->

<!-- Spacer -->
<div class="margin-top-15"></div>
<!-- Spacer / End-->
	</div>
	<!-- Dashboard Content / End -->
    </div>

</div>
<!-- Wrapper / End -->

@include('general.partials.locationscript')


@endsection
@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<!-- Google Autocomplete -->

{{-- <script src="{{asset('backstyling/js/infobox.min.js')}}"></script> --}}
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/maps.js')}}"></script> --}}

<script>
    $('#country').change(function () {
        var cid = $(this).val();
        if (cid) {
            $.ajax({
                type: "get",
                url: "{{url('get-state-list')}}?country_id=" + cid,
                success: function (res) {
                    if (res) {
                        $("#state").empty();
                        $("#city").empty();
                        $("#state").append('<option>Select State</option>');
                        $.each(res, function (key, value) {
                            $("#state").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
    $('#state').change(function () {
        var sid = $(this).val();
        if (sid) {
            $.ajax({
                type: "get",
                url: "{{url('get-city-list')}}?state_id=" + sid,
                success: function (res) {
                    if (res) {
                        $("#city").empty();
                        $("#city").append('<option>Select City</option>');
                        $.each(res, function (key, value) {
                            $("#city").append('<option value="' + key + '">' + value +
                                '</option>');
                        });
                    }
                }

            });
        }
    });
</script>
<script type="text/javascript">
  $('.bookmark-icon').click(function(e){
        e.preventDefault();

        if ( $( this ).hasClass( "bookmarked" ) ) {
            return false;
        }
        let event = $(this);
        console.log(event);
        let source_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('source.bookmark') }}",
               data:{source_id:source_id},
               success:function(data) {
                   // alert(data);
                  // $("#msg").html(data.msg);
                 alert('Saved successfully');
               }
            });
    })

    //delete bookmark

    $('.bookmarked').click(function(e){
        e.preventDefault();

        let event = $(this);
        console.log(event);
        let source_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('source.unbookmark') }}",
               data:{source_id:source_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Removed successfully');
               }
            });
    })
</script>
@endsection