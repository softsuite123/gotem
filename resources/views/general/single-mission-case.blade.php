@extends('employer.layouts.master')

@section('content')
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.2.1/nouislider.min.css">

<style>
    .coolor{
        color:#fff !important;
    }
</style>

<!-- Wrapper -->
<div id="wrapper">
    @if (Session::has('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">Ã—</button>
            <strong>{!! Session('success') !!}</strong>
    </div>
	@endif

	@if (Session::has('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">Ã—</button>
            <strong>{!! Session('error') !!}</strong>
    </div>
    @endif

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @include('employer.partials.header')
    <?php //dd('here');?>

<!-- Dashboard Content
    ================================================== -->
<div class="dashboard-container">

    @include('employer.partials.sidebar')
    <!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar >
		<!-- <div class="dashboard-content-inner" > -->

			<!-- Titlebar
================================================== -->
<?php
$image = $mission->mission_files;
$img = (array) json_decode($image);
?>
<div class="single-page-header" data-background-image="{{asset('/images/missions_images/thumbs/'.json_decode($mission->mission_files[0]))}}" style="box-shadow: 0px 2px 8px 0px gray; ">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="single-page-header-inner">
					<div class="left-side">
					    @if($img)
					    @foreach($img as $imgs)
						<div class="header-image"><a href="#">
							    <?php if ($img == []) {
                                    ?>
                                    <?php
                                } 
                                else {
                                    ?>
							<img src="{{url('/'.$imgs)}}" alt=""></a>
							<?php } ?>
							</div>
							@endforeach
							@endif
						<div class="header-details">
							<h3>{{$mission->user->name}}</h3>
							<h5>About User</h5>
							<ul>
								<li><a href="#"><i class="icon-material-outline-business"></i> Novice Member</a></li>
								<li><div class="star-rating" data-rating="4.9"></div></li>
								<li><img class="flag" src="../assets/images/flags/gb.svg" alt=""> {{@$mission->user->profile->country->name}}</li>
								@if (auth()->user()->is_verified)
								<li><div class="verified-badge-with-title">Verified</div></li>
								@endif
							</ul>
						</div>
					</div>

			

				</div>
			</div>
		</div>
	</div>
</div>


<!-- Page Content
================================================== -->
<div class="container">
	<div class="row">

		<!-- Content -->
		<div class="col-xl-8 col-lg-8 content-right-offset">
            <h3><b>{{$mission->title}}</b></h3>
           <br>
			<div class="single-page-section">
				<h3 class="margin-bottom-25"><b>Mission Description</b></h3>
				<p>{{$mission->mission_description}}.</p>
			</div>
			<div class="single-page-section">
				<h3 class="margin-bottom-25"><b>Mission Objective</b></h3>
				<p>{{$mission->mission_objective}}.</p>
			</div>
			<div class="single-page-section">
				<h3 class="margin-bottom-30"><b>Location</b></h3>
				<div id="single-job-map-container">
					<div id="singleListingMap" data-latitude="{{$mission->latitude}}" data-longitude="{{$mission->longitude}}" data-map-icon="im im-icon-Hamburger"></div>
					
				</div>
			</div>
			<div class="col-xl-5">
			   <?php $uid= Auth::id(); ?> 
		    @if($mission->user_id!=$uid)
		    @if($current_date<$mission->deadline)
			<a href="#small-dialog" class="apply-now-button popup-with-zoom-anim">Apply to Mission</a>
			@endif
			@endif
            </div>
		
		</div>


		<!-- Sidebar -->
		<div class="col-xl-4 col-lg-4 ">
			<div class="sidebar-container">
@php
	$isBookmarked =  (new \App\Bookmark())->isBookmarked($mission->id);
@endphp

@if($mission->user_id!=$uid)
@if ($mission->enable_crowdfunding)
@if($current_date<$mission->deadline)
<a id="launchModalBtn" class="button ripple-effect button-sliding-icon coolor" style="width:90%;" {{$mission->enable_crowdfunding?'':'disabled'}}>Participate in Crowdfund<i class="icon-feather-arrow-right"></i></a>
@endif
@endif
@endif
				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<div class="job-overview">
						<div class="job-overview-headline " style="background-color:#2A41E8;color:white" >Mission Summary</div>
						<div class="job-overview-inner">
							<ul>
							   
                                
                              
                                 @if($mission->is_urgent==1)
                                <li>
									<i class="fa fa-clock-o"></i>
						
									<h5> Urgent Delivery</h5>
								</li>
                                  @endif
                                
								<li>
									<i class="icon-material-outline-location-on"></i>
									<span>Location</span>
									<h5>{{$mission->address}}</h5>
								</li>
								<li>
									<i class="icon-material-outline-description"></i>
									<span>Private Investigation</span>
									<!-- <h5>$35k - $38k</h5> -->
								</li>
								<li>
									<i class="icon-material-outline-local-atm"></i>
									<span>Mission Value</span>
									<h5>${{$mission->estimated_budget}}</h5>
								</li>
								<li>
									<i class="icon-material-outline-access-time"></i>
									<span>Deadline</span>
									<h5>{{$mission->deadline}}</h5>
								</li>
								<li>
									<i class="icon-material-outline-business-center"></i>
									<span>Crowdfund</span>
									@if ($mission->enable_crowdfunding)
										<h5>Active</h5>
									@else
										<h5>Not Active</h5>
									@endif

								</li>
							</ul>
						</div>
					</div>
				</div>

				<!-- Sidebar Widget -->
				<div class="sidebar-widget">
					<h3>Bookmark or Share</h3>

					<!-- Bookmark Button -->
					<button class="bookmark-button margin-bottom-25 {{$isBookmarked?'bookmarked':''}}" id="{{$mission->id}}">
						<span class="bookmark-icon"></span>
						<span class="bookmark-text">Bookmark</span>
						<span class="bookmarked-text">Bookmarked</span>
					</button>

					<!-- Copy URL -->
					<div class="copy-url">
						<input id="copy-url" type="text" value="" class="with-border">
						<button class="copy-url-button ripple-effect" data-clipboard-target="#copy-url" title="Copy to Clipboard" data-tippy-placement="top"><i class="icon-material-outline-file-copy"></i></button>
					</div>

		
				</div>

			</div>
			</div> 
			<!--dsfsd-->
		</div>

	</div>
</div>

		</div>
	</div>
	<!-- Dashboard Content / End -->
</div>
<!-- Dashboard Content / End -->

</div>
<!-- Apply for a job popup
================================================== -->
<!-- Tab -->
@if ($mission->enable_crowdfunding)
		<div class="modal fade" id="modal_1">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body">

				<h5>Mission: {{$mission->title}}</h5>

                <!-- Form -->

          <style>

             .slider1 {
              width: 470px;
              height: 125px;
              display: flex;
              overflow-x: auto;
            }
            .slide {
              width: 100px;
              flex-shrink: 0;
              height: 100%;
            }
            @media (max-width: 768px){
             .slider1 {
             width: 316px;
             }
            }

            .content {
  width: 100%;
  margin: 0 auto;
}

.slider-area {
  padding: 5px 40px;
  border: 1px solid #ddd;

}

.slider {
  margin-top: 50px;
  width: 100%;
}


          </style>


          <div class="slider1">
<?php $croud_funders = DB::table('crowd_funding')->where('mission_id',$mission->id)->get();  ?>
@if($croud_funders)
@foreach($croud_funders as $croud_funder)
<?php $img_funders = DB::table('profiles')->where('user_id',$croud_funder->user_id)->first();  ?>
@if($img_funders)
  <div class="slide" id="slide-1"  style="padding: 5px;">
    <img src="{{url('/')}}/{{$img_funders->avatar}}" width="80" height="50" title="{{$img_funders->name}}">
    <center><h6>${{$croud_funder->amount}}</h6></center>
  </div>
  @else
  <div class="slide" id="slide-1" style="padding: 5px;">
    <img src="{{url('/')}}/backstyling/images/user-avatar-small-01.jpg">
  </div>

  @endif
@endforeach

@endif

</div>


<center><h5><i>Current Crowdfunders</i></h5></center>

<br>
<center><h3>Crowdfunding Deadline: {{$mission->deadline}}</h3></center>
<br>
<div class="content">


</div>
<?php $total_fund = DB::table('crowd_funding')->where('mission_id',$mission->id)->sum('amount'); ?>
<br>
@if($total_fund)
<h3>Raised Amount: ${{$total_fund}} USD</h3>

@else
<h3>Raised Amount: $0 USD</h3>
@endif
<br>

{{-- <div style="overflow-y: scroll; height: 100px;">
    <p>Contractors Assigned to Mission</p>
<div><i class="icon-line-awesome-user-secret"></i>&nbsp;&nbsp;<span>Cyber Investigation: 5</span></div>
<div><i class="icon-line-awesome-user-secret"></i>&nbsp;&nbsp;<span>Private Detective: 3</span></div>
<div><i class="icon-line-awesome-user-secret"></i>&nbsp;&nbsp;<span>Personal Detective: 1</span></div>
</div> --}}

<br>
<style>
    .paddding{
    margin-left: 380px;
    }
    @media (max-width: 768px){
    .paddding{
    margin-left: 250px;
    }
    }
</style>
<div style="overflow-y: scroll; height: 150px;">
    <div class="col-xl-12 col-md-12">
    <p>Amount to Participate</p>
    </div>
    <center>
    <div class="col-xl-12 col-md-12">
    <hr>
    </div>
    </center>
        <div class="col-xl-12 col-md-12">
        <p>Select a Value</p>
        <input type="number" onkeyup="amount()" placeholder="Minimum to contribute is $50" min="50" class="form-control" style="border: 1px solid grey;" id="amount">
        </div>


           {{-- <div class="col-xl-12 col-md-12">
            <div class="switch-container">
                    <label class="switch">Participate in Group Voting <input type="checkbox" checked><span class="switch-button paddding" ></span> </label>
                </div>
            </div> --}}

            {{-- <div class="col-xl-12 col-md-12">
                <span style="color: silver; font-size: 13px;">
                    Vote to select contractors, approve milestones, payouts and more.
                </span>
            </div> --}}


</div>
<br>
 {{-- <center><div class="col-xl-12 col-md-12">
    <h4><strong>Suggest a Retainer Person</strong></h4>
    </div></center>

    <hr>
    <br>
    <div class="col-xl-2 col-md-2"></div>

        <center>
    <div class="col-xl-8 col-md-8">

            <select class="selectpicker">
                <option>5%</option>
                <option>15%</option>
                <option>25%</option>
            </select>
        </div>
            </center> --}}

            <div class="col-xl-2 col-md-2"></div>
<br>

@if($total_fund)
<center><h3>Raised Amount: ${{$total_fund}} USD</h3></center>
<center><h4>Total Remaining Amount: ${{$mission->estimated_budget-$total_fund}} USD</h4></center>
@else
<center><h3>Raised Amount: $0USD</h3></center>
@endif
<br>
                     <div class="container">
                     <div class="row">
                    <div class="col-xl-3 col-md-3"></div>
                    <div class="col-xl-2 col-md-2 col-4">
                    	<div style="text-align: center;">
                        <img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="100%" style="margin-top: -5px;">
                    </div>
                    </div>
                    <div class="col-xl-2 col-md-2 col-4">
                    	<div style="text-align: center;">
                        <img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="85%">
                         </div>
                    </div>
                    <div class="col-xl-2 col-md-2 col-4">
                    	<div style="text-align: center;">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="60%">
                         </div>
                    </div>
                    <div class="col-xl-3 col-md-3"></div>
                    </div>
                </div>

<br>

<center><h3>Term and Conditions</h3></center>
<div class="container">
<div class="row">
 <div class="col-xl-2 col-md-2"></div>

    <div class="col-xl-6 col-md-6">
        <center>

            <select class="" onchange="disclaimer_page()">
                <option>Read Disclaimer</option>
                <option>Read Disclaimer</option>
                <option>Read Disclaimer</option>
            </select>
            </center>
        </div>
                <div class="col-xl-2 col-md-2">

                <div class="switch-container">
                    <label class="switch"><input type="checkbox" checked><span class="switch-button" ></span> </label>
                </div>

                </div>
            <div class="col-xl-2 col-md-2"></div>

</div>
                </div>

<div class="container">
<div class="row">
 <div class="col-xl-2 col-md-2"></div>

    <div class="col-xl-8 col-md-8">
                <span style="color: silver; font-size: 13px;">
                    Please read the terms and conditions carefully.
                    By clicking accept you agree to all TOU policies.
                </span>
            </div>
<div class="col-xl-2 col-md-2"></div>
</div>
</div>

<br>

<div class="container">
<div class="row">
 <div class="col-xl-2 col-md-2"></div>

<div class="col-xl-12 col-md-12">
 	<center><a class="button ripple-effect coolor participatecrowd" id="launchSecond">Pay with Coingate</a> <h6><b>OR</b></h6> 

 		<form action="{{url('/stripe_payment_postdata')}}" method="post">
 			@csrf
 			<input type="hidden" name="mission_id" value="{{$mission->id}}">
 			<input type="hidden" name="user_id" value="{{\Auth::id()}}">
 			<input type="hidden" name="amount" value="" id="amt">
 		<button  class="button ripple-effect coolor participatecrowd" >Pay with Card</button>
 		</form>


 	</center>
</div>
<div class="col-xl-2 col-md-2"></div>
</div>
</div>

            </div>
        </div>
    </div>
</div>

@endif

 <style>
        .scrolling {
            overflow-y: scroll;
            height: 570px;
        }

        @media(max-width:768px) {
            .scrolling {
                overflow-y: scroll;
                height: 750px;
            }
        }
    </style>




          <div class="modal fade" id="modal_2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>

                <div class="modal-body scrolling">
          <div class="row">
          	<div class="col-md-8">
          		<h5><b>Test Fixed Price Button</b></h5>
          	</div>
          	<div class="col-md-4">
          		<span style="text-align: right;">
          		<h5>0.005 BTC</h5>
          		</span>
          	</div>
          </div>
          <div class="row">
          	<div class="col-md-8">
          		<h6>Name: David; Email Address: tawei@gotem.io</h6>
          	</div>
          </div>
          <hr>
          <br>
          <center><h3>Select Payment Currency</h3></center>
          <br>
          <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          	<div style="border: 1px solid #7DBBFF; padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://www.freepnglogos.com/uploads/bitcoin-png/bitcoin-all-about-bitcoins-9.png" width="30%">
          		<span>Bitcoin</span>
          	</div>
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 14px;">0.005 BTC</h5>
          	</div>
          	</div>
          	<hr>
             <div class="row">
          	<div class="col-xl-8">
          	 <div class="switch-container">
					<label class="switch" style="font-size: 12px !important; margin-left: 40px;">
						<input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
					</label>
				</div>
				</div>
				<div class="col-xl-4">
                 <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span>				</div>
				</div>

          		</div>
          	</div>
          	<div class="col-xl-1"></div>
          </div>

          <br>

          <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://assets.coingate.com/images/currencies/256x256/xrp-edited.png" width="25%">
          		<span>XRP</span>
          	</div>
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 8px;">190.91 XRP</h5>
          	</div>
          	</div>
          </div>
           </div>
            <div class="col-xl-1"></div>
        </div>

        <br>

        <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          <div style="padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Ethereum_logo_2014.svg/256px-Ethereum_logo_2014.svg.png" width="15%">
          		<span>Ethereum</span>
          	</div>
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 8px;">0.2864 ETH</h5>
          	</div>
          	</div>
          </div>
           </div>
            <div class="col-xl-1"></div>
        </div>

        <br>

         <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          	<div style=" padding: 10px; box-shadow: 0px 2px 8px 0px gray; border-radius: 5px;">
          	<div class="row">
          	<div class="col-xl-6">
          		<img src="https://upload.wikimedia.org/wikipedia/commons/a/a8/Official_Litecoin_Logo.png" width="30%">
          		<span>Litecoin</span>
          	</div>
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 14px;">0.897 LTC</h5>
          	</div>
          	</div>
          	<hr>
             <div class="row">
          	<div class="col-xl-8">
          	 <div class="switch-container">
					<label class="switch" style="font-size: 12px !important; margin-left: 40px;">
						<input type="checkbox"><span class="switch-button" style="margin-left: -35px !important;"></span>Lightning Network
					</label>
				</div>
				</div>
				<div class="col-xl-4">
                 <span style="float: right; color: #7DBBFF; font-size: 13px;"><a href="#">What's this?</a></span>				</div>
				</div>

          		</div>
          	</div>
          	<div class="col-xl-1"></div>
          </div>

           <br>



           <div class="row">
          	<div class="col-xl-1"></div>
          	<div class="col-xl-10">
          	<div class="accordion js-accordion">

				<!-- Accordion Item -->
				<div class="accordion__item js-accordion-item">
					<div class="accordion-header js-accordion-header" style="background-color: silver;">More Currencies</div>

					<!-- Accordtion Body -->
					<div class="accordion-body js-accordion-body">

						<!-- Accordion Content -->
						<div class="accordion-body__contents">
							<div class="row">
          	<div class="col-xl-6">
          		<img src="https://cdn.freebiesupply.com/logos/large/2x/ripple-2-logo-png-transparent.png" width="25%">
          		<span>Ripple</span>
          	</div>
          	<div class="col-xl-6">
          		<h5 style="text-align: right; margin-top: 8px;">0.5864 RIP</h5>
          	</div>
          	</div>
						</div>

					</div>
					<!-- Accordion Body / End -->
				</div>
				<!-- Accordion Item / End -->
			</div>
          	</div>
          	<div class="col-xl-1"></div>
          </div>


         <div class="row">
         <div class="col-xl-1"></div>
         <div class="col-xl-10 col-md-10">
		<div class="section-headline margin-top-25 margin-bottom-12">
		</div>
		<div class="input-with-icon-left no-border">
		<i class="icon-material-baseline-mail-outline"></i>
		<input type="text" class="input-text" placeholder="tawei@gotem.io">
		</div>
		</div>
		<div class="col-xl-1"></div>
	    </div>

        <div class="row">
              <div class="col-xl-1"></div>
              <div class="col-xl-10">
          	  <center><a href="#" class="button ripple-effect">Pay with Bitcoin</a></center>
          	  </div>
          	  <div class="col-xl-1"></div>
              </div>


<div class="row">
              <div class="col-xl-1"></div>
              <div class="col-xl-10">
          	  <center><p>Currency cannot be changed after proceeding</p></center>
          	  </div>
          	  <div class="col-xl-1"></div>
              </div>

</div>
</div>
</div>



          </div>


<div id="small-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs ">

	<!--Tabs -->
	<div class="sign-in-form ">


		<div class="popup-tabs-container ">

			<!-- Tab -->
			<div class="popup-tab-content modal-lg" id="tab">

				<h5>Mission: {{$mission->title}}</h5>
			</div -->

				<!-- Form -->
				<form method="post" id="add-note" action="{{route('apply.mission',['mission'=>$mission->id])}}">
					@csrf
				
					<textarea cols="10" placeholder="Message" name="message" class="with-border" required></textarea>
					<input type="hidden" name="mission_id_notify" value="{{$mission->id}}">
					<input type="hidden" name="title" value="{{$mission->title}}">
					<input type="hidden" name="user_id" value="{{$mission->user_id}}">


				<div class="col-xl-12 col-md-12">
					<div class="row">
					    
						<div class="col-xl-5 col-md-5">
							<div class="checkbox" style="margin-top: 10px;">
								<input type="checkbox" id="chekcbox1"    name="make_offer" >
								<label for="chekcbox1"><span class="checkbox-icon"></span> Make an Offer</label>
							</div>
						</div>

						<div class="col-xl-7 col-md-7">
							<input placeholder="Insert $ Amount" type="number" name="offer_amount" >
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-xl-6 col-md-6">
							<div class="checkbox">
								<input type="checkbox" id="chekcbox2"   name="milestones">
								<label for="chekcbox2"><span class="checkbox-icon"></span> Milestones</label>
							</div>
						</div>
					</div>
					
				</div>
				
				<br>
				<style type="text/css">
					hr {
						border: 1px solid silver;
					}
				</style>
				<div style="box-shadow: 0px 2px 8px 0px silver; padding: 15px; border-radius: 5px;" id="mile">
					<div class="row" style="margin-top: 20px;">
				
		<p><small>Note: milestone without amount will not be submitted</small></p>
		                	<div class="col-xl-12 col-md-12">
		                	     <center><label><h5><b>Milestone 1</b></h5></label></center>
							<select class="" name="timer[]" required>
							    <option selected disabled>Select Days</option>
							    <option >1 Day</option>
							    <option >2 </option>
							    <option >3 </option>
							    <option >4 Day</option>
							    <option >5 Day</option>
							    <option >6 Day</option>
							    <option >7 Day</option>
							    <option >8 Day</option>
							    <option >9 Day</option>
							    <option >10 Day</option>
							    <option >11 Day</option>
							    <option >12 Day</option>
							    <option >13 Day</option>
							    <option >14 Day</option>
							    <option >15 Day</option>
							    <option >16 Day</option>
							    <option >17 Day</option>
							    <option >18 Day</option>
							    <option >19 Day</option>
							    <option >20 Day</option>
							    <option >21 Day</option>
							    <option >22 Day</option>
							    <option >23 Day</option>
							    <option >24 Day</option>
							    <option >25 Day</option>
							    <option >26 Day</option>
							    <option >27 Day</option>
							    <option >28 Day</option>
							    <option >29 Day</option>
							    <option >30 Day</option>
							   
							</select>
						</div>
		                    
						<div class="col-xl-12 col-md-12">
						    <br>
							<input placeholder="Milestone One" name="milestone[]">
						</div>
						
						<div class="col-xl-12 col-md-12">
							<input placeholder="Description" name="description[]">
						</div>
						
						<div class="col-xl-12 col-md-12">
							<input placeholder="Amount" type="number" name="amount[]">
						</div>
						
					
						
					</div>

                <div id="detail">
					<div class="row" style="margin-top: 20px;">
					    	<div class="col-xl-12 col-md-12">
					    	    
					    	 
					    	     <center><label><h5><b>Milestone 2</b></h5></label></center>
							<select class="" name="timer[]" required>
							    <option selected disabled>Select Days</option>
							    <option >1 Day</option>
							    <option >2 Day</option>
							    <option >3 Day</option>
							    <option >4 Day</option>
							    <option >5 Day</option>
							    <option >6 Day</option>
							    <option >7 Day</option>
							    <option >8 Day</option>
							    <option >9 Day</option>
							    <option >10 Day</option>
							    <option >11 Day</option>
							    <option >12 Day</option>
							    <option >13 Day</option>
							    <option >14 Day</option>
							    <option >15 Day</option>
							    <option >16 Day</option>
							    <option >17 Day</option>
							    <option >18 Day</option>
							    <option >19 Day</option>
							    <option >20 Day</option>
							    <option >21 Day</option>
							    <option >22 Day</option>
							    <option >23 Day</option>
							    <option >24 Day</option>
							    <option >25 Day</option>
							    <option >26 Day</option>
							    <option >27 Day</option>
							    <option >28 Day</option>
							    <option >29 Day</option>
							    <option >30 Day</option>
							   
							</select>
						</div>
					    <br>
						<div class="col-xl-12 col-md-12">
						   <br>
							<input placeholder="Milestone " name="milestone[]">
						</div>
						
						<div class="col-xl-12 col-md-12">
							<input placeholder="Description" name="description[]">
						</div>
						
						<div class="col-xl-12 col-md-12">
							<input placeholder="Amount" type="number" name="amount[]">
						</div>

					</div>
				 </div>
				 <br>
				 <div class="row">
                    <div class="col-md-12">
                       <span id="addmore" class="pull-right btn btn-success" onclick="addmore()">Add More <i class="fa fa-plus"></i></span>
                    </div>
                </div>

					{{-- <div class="row" style="margin-top: 20px;">

						<div class="col-xl-12 col-md-12">
							<div class="keywords-container">
								<div class="keyword-input-container">
									<input type="text" class="keyword-input" placeholder="Add Milestone" />
									<button class="keyword-input-button ripple-effect"><i
											class="icon-material-outline-add"></i></button>
								</div>
								<div class="keywords-list">
									<!-- keywords go here -->
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div> --}}
				</div>


				<!-- Button -->
				<button class="button full-width button-sliding-icon ripple-effect" type="submit"
					form="add-note">Submit<i class="icon-material-outline-arrow-right-alt"></i></button>
			</div>
		</form>


		</div>
	</div>
</div>
<button  style="display: none;" id="btn_modal_session" class="btn btn-info btn-lg" data-toggle="modal" data-target="#disclaimer">open</button>



<div id="disclaimer" class="modal modal-child" data-backdrop-limit="1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-modal-parent="#myModal">
    <div class="modal-dialog">
         Modal content
        <div class="modal-content">
            <div class="modal-body">
                  

                <center>
                <h3><b>Disclaimer Page!</b></h3></center>
            
                <center>
                <h4>Lorem is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry�s<br> standard dummy text ever since the 1500s.
</h4></center>
              
                <div class="row">
                    <div class="col-xl-3"></div>
                    <div class="col-xl-6">
                        <br>
                        <center><a data-dismiss="modal" data-dismiss="modal" aria-hidden="true" class="button ripple-effect coolor">Close</a></center>
                    </div>
                    <div class="col-xl-3"></div>
                </div>
            </div>
           
        </div>
    </div>
</div>

<input type="hidden" id="mission" value="{{$mission->id}}">
<input type="hidden" id="user" value="{{\Auth::id()}}">

<!-- Apply for a job popup / End -->
@include('general.partials.locationscript')
@endsection

@section('scripts')
<!-- Scripts
================================================== -->
<script src="{{asset('backstyling/js/jquery-3.4.1.min.js')}}"></script>
{{-- <script src="{{asset('backstyling/js/jquery-migrate-3.1.0.min.html')}}"></script> --}}
<script src="{{asset('backstyling/js/mmenu.min.js')}}"></script>
<script src="{{asset('backstyling/js/tippy.all.min.js')}}"></script>
<script src="{{asset('backstyling/js/simplebar.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('backstyling/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('backstyling/js/snackbar.js')}}"></script>
<script src="{{asset('backstyling/js/clipboard.min.js')}}"></script>
<script src="{{asset('backstyling/js/counterup.min.js')}}"></script>
<script src="{{asset('backstyling/js/magnific-popup.min.js')}}"></script>
<script src="{{asset('backstyling/js/slick.min.js')}}"></script>
<script type="text/javascript">
    $('.bookmark-button').click(function(e){
        e.preventDefault();

        if ( $( this ).hasClass( "bookmarked" ) ) {
            return false;
        }
        let event = $(this);
        let mission_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('bookmark') }}",
               data:{mission_id:mission_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Saved successfully');
               }
            });
    })

    //delete bookmark

    $('.bookmarked').click(function(e){
        e.preventDefault();

        let event = $(this);
        console.log(event);
        let mission_id = event[0].id;
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });

         $.ajax({
               type:'POST',
               url: "{{ route('unbookmark') }}",
               data:{mission_id:mission_id},
               success:function(data) {
                  // $("#msg").html(data.msg);
                  alert('Removed successfully');
               }
            });
    })
</script>
<script src="{{asset('backstyling/js/custom.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/8.2.1/nouislider.min.js"></script>
<!-- Google Autocomplete -->

<!--
<script>
function initAutocomplete() {
     var options = {
      types: ['(cities)'],
      // componentRestrictions: {country: "us"}
     };

     var input = document.getElementById('autocomplete-input');
     var autocomplete = new google.maps.places.Autocomplete(input, options);
}
</script -->

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script>
        $(document).on('click', '#launchModalBtn', function() {
            $('#modal_1').modal();
        });

        // $(document).on('click', '#launchSecond', function() {
        //     $('#modal_2').modal();
        // })

        $('.modal').on('show.bs.modal', function() {
            $('.modal').not($(this)).each(function() {
                $(this).modal('hide');
            });
        });
    </script>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh8kTpyFYKblD830zAmtY2zBmVQD5jL5"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBysXloxcGVj9ZGbpmf36ZRnOLQMOB7oRY&libraries=places&callback=initAutocomplete"
        async defer></script>
<script>
    var snapSlider = document.getElementById('slider-snap');
var snapValues = [
    document.getElementById('slider-snap-value-lower'),
    document.getElementById('slider-snap-value-upper')
];
var range = {
  'min': {{$mission->min_raise_amount}},
  '10%': 50,
  '10.1%': 51,
  '10.2%': 52,
  '10.25%': 52.5,
  '20%': 100,
  '30%': 150,
  '35%': 160,
  '40%': 500,
  '42.3%': 666,
  '50%': 800,
  'max': {{$mission->max_raise_amount}}
};



noUiSlider.create(snapSlider, {
    start: [ {{$mission->min_raise_amount}}, {{$mission->max_raise_amount}} ],
    snap: true,
    connect: true,
  tooltips: true,
    range: range
});

snapSlider.noUiSlider.on('update', function( values, handle ) {
    snapValues[handle].innerHTML = values[handle];
});

$('#range').text(JSON.stringify(range, null, '\t'));

</script>
         <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBysXloxcGVj9ZGbpmf36ZRnOLQMOB7oRY&amp;libraries=places"></script"></script>

{{-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDh8kTpyFYKblD830zAmtY2zBmVQD5jL5g&callback=initMap"
  type="text/javascript"></script>--}}
<script src="{{asset('backstyling/js/infobox.min.js')}}"></script>
<script src="{{asset('backstyling/js/markerclusterer.js')}}"></script>
<script src="{{asset('backstyling/js/maps.js')}}"></script>
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
    // Snackbar for user status switcher
    $('#snackbar-user-status label').click(function () {
        Snackbar.show({
            text: 'Your status has been changed!',
            pos: 'bottom-center',
            showAction: false,
            actionText: "Dismiss",
            duration: 3000,
            textColor: '#fff',
            backgroundColor: '#383838'
        });
    });
</script>


<script src="{{asset('backstyling/js/chart.min.js')}}"></script>
<script>
    Chart.defaults.global.defaultFontFamily = "Nunito";
    Chart.defaults.global.defaultFontColor = '#888';
    Chart.defaults.global.defaultFontSize = '14';

    var ctx = document.getElementById('chart').getContext('2d');

    var chart = new Chart(ctx, {
        type: 'line',

        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196, 132, 215, 362, 210, 252],
                pointRadius: 5,
                pointHoverRadius: 5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

        // Configuration options
        options: {

            layout: {
                padding: 10,
            },

            legend: {
                display: false
            },
            title: {
                display: false
            },

            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        borderDash: [6, 10],
                        color: "#d8d8d8",
                        lineWidth: 1,
                    },
                }],
                xAxes: [{
                    scaleLabel: {
                        display: false
                    },
                    gridLines: {
                        display: false
                    },
                }],
            },

            tooltips: {
                backgroundColor: '#333',
                titleFontSize: 13,
                titleFontColor: '#fff',
                bodyFontColor: '#fff',
                bodyFontSize: 13,
                displayColors: false,
                xPadding: 10,
                yPadding: 10,
                intersect: false
            }
        },


    });
</script>

<script type="text/javascript">

function amount()
{
	var a = $('#amount').val();
	//alert(a);
	$('#amt').val(a);
}

	 $('.participatecrowd').click(function(){
              
                	let amount = $('#amount').val();
                	let mission = $('#mission').val();
                	if(amount == ""){
                		alert('Please enter amount');
                		return false;
                	}

                    window.location.href = 'https://platform.gotem.io/coingate?crowdfund_amount='+amount+'&mission='+mission+'&crowdfund=1';
              
            });
         function radioSwitch(opposite) {
    document.getElementById(opposite).checked = false;
}
document.getElementById("chekcbox1").addEventListener("click",
    function() { radioSwitch("chekcbox2");
    $('#mile').hide();
     });
document.getElementById("chekcbox2").addEventListener("click",
    function() { radioSwitch("chekcbox1"); 
    	$('#mile').show();
	});
	


// disclaimer modals of the select option
    function disclaimer_page()
    {
        //  $(this).modal('hide');
         $('#modal_1').hide();
        $('#btn_modal_session').click();
    }


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV7WeUUT24I6Zog1Kqm_H0ZJcu548R72A&libraries=places&callback=initAutocomplete"
         async defer></script>
         
         
         
         
         
       
<script>
var counter = 2;  
      function addmore()
    {
       counter++;
       var row="";
       row +='<br><div class="row" style="margin-top: 20px;">'+
                '<div class="col-xl-12 col-md-12">'+
                '<center><label><h5><b>Milestone '+counter+'<b></h5></label></center>'+
                '<br>'+
							'<select class="" name="timer[]" required>'+
							    '<option selected disabled>Select Days</option>'+
							    '<option >1 Day</option>'+
							    '<option >2 Day</option>'+
							    '<option >3 Day</option>'+
							    '<option >4 Day</option>'+
							    '<option >5 Day</option>'+
							    '<option >6 Day</option>'+
							    '<option >7 Day</option>'+
							    '<option >8 Day</option>'+
							    '<option >9 Day</option>'+
							    '<option >10 Day</option>'+
							    '<option >11 Day</option>'+
							    '<option >12 Day</option>'+
							    '<option >13 Day</option>'+
							    '<option >14 Day</option>'+
							    '<option >15 Day</option>'+
							    '<option >16 Day</option>'+
							    '<option >17 Day</option>'+
							    '<option >18 Day</option>'+
							    '<option >19 Day</option>'+
							    '<option >20 Day</option>'+
							    '<option >21 Day</option>'+
							    '<option >22 Day</option>'+
							    '<option >23 Day</option>'+
							    '<option >24 Day</option>'+
							    '<option >25 Day</option>'+
							    '<option >26 Day</option>'+
							    '<option >27 Day</option>'+
							    '<option >28 Day</option>'+
							    '<option >29 Day</option>'+
							    '<option >30 Day</option>'+
							'</select>'+
						'</div>'+
                '<div class="col-xl-12 col-md-12">'+
                '<br>'+
                    '<input placeholder="Milestone " name="milestone[]"></div>'+
                '<div class="col-xl-12 col-md-12">'+
            '<input placeholder="Description" name="description[]">'+
               ' </div>'+
              '<div class="col-xl-12 col-md-12">'+
            '<input placeholder="Amount" type="number" name="amount[]">'+
               ' </div>'+
               '<br>'+
               '<span id="remove" style="margin-left:15px;" class="pull-right btn btn-danger remove-fac-row" >Remove \</span></div>';
       $('#detail').append(row);
    $(document).on('click', '.remove-fac-row', function(){
        // 
    //  $(this).remove();
    $(this).closest('div').remove();
    // counter--;
        });
    }

     
    
</script>         
         
@endsection