  <!--<div class="clearfix"></div>-->
  <!--  <div class="pagination-container margin-top-20 margin-bottom-20">-->
  <!--      <nav class="pagination">-->
  <!--          <ul>-->
                <!--<li class="pagination-arrow"><a href="#" class="ripple-effect"><i-->
                <!--            class="icon-material-outline-keyboard-arrow-left"></i></a></li>-->
  <!--              <li><a href="#" class="ripple-effect">1</a></li>-->
  <!--              <li><a href="#" class="ripple-effect current-page">2</a></li>-->
  <!--              <li><a href="#" class="ripple-effect">3</a></li>-->
  <!--              <li><a href="#" class="ripple-effect">4</a></li>-->
                <!--<li class="pagination-arrow"><a href="#" class="ripple-effect"><i-->
                <!--            class="icon-material-outline-keyboard-arrow-right"></i></a></li>-->
  <!--          </ul>-->
  <!--      </nav>-->
  <!--  </div>-->
  
  
  
  
 @if ($paginator->hasPages())
    
        {{-- Previous Page Link --}}
        <nav class="pagination">
            <ul>
        @if ($paginator->onFirstPage())
        
        
            <li class="page-item disabled" aria-disabled="true">
                <span class="page-link">Previous</span>
            </li>
            
            
        @else
            <li class="page-item">
                <a class="pagination-arrow" href="{{ $paginator->previousPageUrl() }}" style="width: 60px !important;" rel="prev">Previous</a>
            </li>
           
        @endif
         </ul>
          </nav>
        {{-- Next Page Link --}}
        <nav class="pagination">
            <ul>
        @if ($paginator->hasMorePages())
        
            <li class="page-item">
                <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a>
            </li>
        
        @else
        
            <li class="page-item disabled" aria-disabled="true">
                <span class="page-link">Next</span>
            </li>
            
        
        @endif
        </ul>
    </nav>
@endif